import pymel.core as pm

from jb_destruna.maya.utility.misc import pn
# DONE: block in attribute.add()


def add(*args, **kwargs):
    """
    This function adds an attribute in a modified way to mc.addAttr.
    Args:
        arg[0]:                     (name, node)
        name, n:                    (str)
        type, ty:                   (str)
        enumName, en:               (str)
        alias, a:                   (str)
        defaultValue, dv     (float, int)
        minValue, min:       (float, int)
        maxValue, max:       (float, int)
        softMin, smn:        (float, int)
        softMax, smx         (float, int)

        channelBox, cb:            (bool)
        keyable, k:                (bool)
        lock, l:                   (bool)

    Returns:
        Attribute Pynode
    """

    obj =   args[0]
    name =  kwargs.get('name',          kwargs.get('n'))
    type =  kwargs.get('type',          kwargs.get('t'))
    en =    kwargs.get('enumName',      kwargs.get('en'))
    alias = kwargs.get('alias',         kwargs.get('a'))
    dv =    kwargs.get('defaultValue',  kwargs.get('dv'))
    min =   kwargs.get('minValue',      kwargs.get('min'))
    max =   kwargs.get('maxValue',      kwargs.get('max'))
    smn =   kwargs.get('softMin',       kwargs.get('smn'))
    smx =   kwargs.get('softMax',       kwargs.get('smx'))

    cb =    kwargs.get('channelBox',    kwargs.get('cb', True))
    k =     kwargs.get('keyable',       kwargs.get('k', False))
    l =     kwargs.get('lock',          kwargs.get('l', False))

    # handle object.
    if isinstance(obj, pm.PyNode):
        pass
    if pn(obj):
        obj = pn(obj)
    else:
        pm.warning('This function requires an node to operate on.')
        return

    # automated behaviour regarding type
    if type:
        pass
    elif dv:
        if isinstance(dv, str):
            type = 'string'
        if isinstance(dv, int):
            type = 'int'
        if isinstance(dv, (float, long)):
            type = 'float'
        if isinstance(dv, bool):
            type = 'bool'

    # reformat en variable for pm.addAttr
    if en:
        output = ''
        for i in en:
            if i != en[-1]:
                output = output+i+':'
            else:
                output = output+i
        en = output

    # account for separator preset
    if type == 'separator':
        attr_kwargs = {'ln': name, 'nn': ' ', 'type': 'enum', 'en': [' :']}
        attr_state = {'cb': True, 'k': False, 'l': False}

    # build cmd.
    else:
        attr_kwargs = {}

        if name:
            attr_kwargs['ln'] = name
        if alias:
            attr_kwargs['nn'] = alias
        if en:
            attr_kwargs['en'] = en


        if type != 'bool':
            if min != None:
                attr_kwargs['min'] = min
            if max != None:
                attr_kwargs['max'] = max
            if smn != None:
                attr_kwargs['smn'] = smn
            if smx != None:
                attr_kwargs['smx'] = smx

        if type in ['bool', 'float', 'enum', 'message']:
            attr_kwargs['at'] = type

        elif type in ['string']:
            attr_kwargs['dt'] = type

        elif type in ['int']:
            attr_kwargs['at'] = 'long'

        else:
            pm.error("attribute.add(): supplied type not currently supported.")

        if cb and k:
            cb = False

        attr_state = {'cb': cb, 'k': k, 'l': l}


    # run command with kwargs and set attribute state.
    pm.addAttr(obj, **attr_kwargs)
    return_attr = pn(obj.attr(name))

    # set defaultValue (doing it this way means it works for strings.)
    if dv:
        return_attr.set(dv)

    return_attr.set(**attr_state)

    return return_attr


def connect_reverse(sourceAttr, targetAttr):

    rev_name = pn(sourceAttr).name().replace('.', '_')+'_reverse'
    rev = pm.createNode('reverse', n=rev_name)

    sourceAttr >> rev.inputX
    rev.outputX >> targetAttr


def connect_reverse_pair(sourceAttr, targetAttr1, targetAttr2):

    rev_name = pn(sourceAttr).name().replace('.', '_') + '_reverse'
    rev = pm.createNode('reverse', n=rev_name)

    sourceAttr >> rev.inputX
    sourceAttr >> targetAttr1
    rev.outputX >> targetAttr2


def add_separator(node, name=' '):

    count = 0

    while pm.attributeQuery('sep_{}'.format(count), node=node, exists=True):
        count += 1

    add(node, type='enum', en='___', n='sep_{}'.format(count), lock=True, a=name)



