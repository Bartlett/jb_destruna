

# Create driver attributes on master control
attribute.add_separator(self.MASTER_CONTROL, name='Blouse')
attribute.add(self.MASTER_CONTROL, n='fitting', type='enum', en=['normal', 'folded'], dv=0, cb=True, k=False)
attribute.add(self.MASTER_CONTROL, n='condition', type='enum', en=['torn', 'normal'], dv=1, cb=True, k=False)

# Input attrs
fit_attr = self.MASTER_CONTROL + '.fitting'
cond_attr = self.MASTER_CONTROL + '.condition'

# Create logic Nodes
fit_condition = mc.createNode('condition', n='fit_condition')
normal_cond_condition = mc.createNode('condition', n='normal_cond_condition')
rolled_cond_condition = mc.createNode('condition', n='torn_cond_condition')

# Fit node config
mc.connectAttr(fit_attr, fit_condition + '.firstTerm')
mc.setAttr(fit_condition + ".secondTerm", 1)
mc.setAttr(fit_condition + ".colorIfTrueR", 1)
mc.setAttr(fit_condition + ".colorIfFalseR", 0)

# Connect Fit node to Cond nodes
mc.connectAttr(cond_attr, normal_cond_condition + '.firstTerm')
mc.connectAttr(fit_condition + '.outColorG', normal_cond_condition + '.colorIfTrueR')
mc.connectAttr(fit_condition + '.outColorG', normal_cond_condition + '.colorIfFalseG')

mc.connectAttr(cond_attr, rolled_cond_condition + '.firstTerm')
mc.connectAttr(fit_condition + '.outColorR', rolled_cond_condition + '.colorIfTrueR')
mc.connectAttr(fit_condition + '.outColorR', rolled_cond_condition + '.colorIfFalseG')

# normal_cond_condition config
mc.setAttr(normal_cond_condition + ".colorIfFalseR", 0)
mc.setAttr(normal_cond_condition + ".secondTerm", 1)

# rolled_cond_condition
mc.setAttr(rolled_cond_condition + ".colorIfFalseR", 0)
mc.setAttr(rolled_cond_condition + ".secondTerm", 1)

# Connect Outputs to geo
mc.connectAttr(normal_cond_condition + '.outColorR', self.BLOUSE_NORMAL_GEO + '.visibility')
mc.connectAttr(normal_cond_condition + '.outColorG', self.BLOUSE_TORN_GEO + '.visibility')

mc.connectAttr(rolled_cond_condition + '.outColorR', self.BLOUSE_ROLLED_NORMAL_GEO + '.visibility')
mc.connectAttr(rolled_cond_condition + '.outColorG', self.BLOUSE_ROLLED_TORN_GEO + '.visibility')

























