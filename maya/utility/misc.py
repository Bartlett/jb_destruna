import pymel.core as pm
import maya.cmds as mc
# ======================================================================================================================
def pn(node):
    """
    test
    Args:
        node:

    Returns:

    """
    #return [node]
    if node:
        if pm.objExists(node):
            return pm.PyNode(node)
        else:
            return None
    else:
        return None

# ======================================================================================================================
def zero_grp(*args):
    # whether to run off args or off selection
    if args:
        node_list = args
    else:
        node_list = pm.selected()

    returnList = []
    for s in [pn(s) for s in node_list]:
        if s.getParent():
            grp = pm.group(em=True, n=s.name()+'_ZERO', p=s.getParent())
        else:
            grp = pm.group(em=True, n=s.name()+'_ZERO')

        pm.delete(pm.parentConstraint(s, grp, mo=False))
        pm.delete(pm.scaleConstraint(s, grp, mo=False))
        pm.parent(s, grp)
        # zero joint orient
        if s.nodeType() == 'joint':
            s.jo.set([0, 0, 0])
        returnList.append(grp)

    # restore original selection (if run off a selection)
    if not args:
        pm.select(node_list, r=True)

    # return node or [list of nodes] created


    elif len(returnList) == 1:
        return returnList[0]
    else:
        return returnList


def flatten_geo(target, mult=1.0):
    """
    Pproject all vtxs to flattened copy.

    :param target:
    :param mult:
    :return: copyGeo
    """
    copy = mc.duplicate(target, n=target + '_copy')[0]
    mc.select(copy + '.vtx[*]', r=True)

    vtx_list = mc.ls(sl=True, fl=True)

    for x, vtx in enumerate(vtx_list):
        mc.select(vtx, r=True)
        map = mc.polyListComponentConversion(fv=True, tuv=True)
        # print map
        uval, vval = mc.polyEditUV(map, q=True)
        mc.xform(vtx, t=[uval*mult, vval*mult, 0], ws=True, r=False)

        return copy


def locator(name='loc1', size=0.1):

    loc = pm.spaceLocator(name=name)
    for atr in ['localScaleX', 'localScaleY', 'localScaleZ']:
        loc.getShape().attr(atr).set(size)

    return loc