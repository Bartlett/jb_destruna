import json
print 're'
def data_to_file(data, path):
    """
    This is a wrapper for saving data to json files.

    Args:
        data: (dict)    data to store in a json file.
        path: (str)     filepath to store data to (eg. 'C:/Users/jordan.bartlett/Downloads/test8.json'

    Returns:
        None
    """

    with open(path, 'w') as outFile:
        json.dump(data, outFile, indent=4, sort_keys=True)


def data_from_file(path):
    """
    This is a wrapper for loading data from json files.
    Args:
        path: (str)     filepath to store data to (eg. 'C:/Users/jordan.bartlett/Downloads/test8.json'

    Returns:
        data
    """
    with open(path) as json_file:
        data = json.load(json_file)

    return data