import pymel.core as pm


def create(name):
    new_curve = pm.createNode('transform', n=name)

    shp = pm.curve(d=1, p=[[0.059, 0.0, 0.0], [-0.059, 0.0, 0.0], [-0.059, 0.0, 0.667], [-0.118, 0.0, 0.667], [0.0, 0.0, 0.934], [0.118, 0.0, 0.667], [0.059, 0.0, 0.667], [0.059, 0.0, 0.0], [0.0, 0.057, 0.0], [-0.059, 0.0, 0.0]], k=[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0])
    pm.parent(shp.getShape(), new_curve, r=True, s=True)
    pm.delete(shp)

    if len(new_curve.getShapes()) > 1:
        for x, s in enumerate(new_curve.getShapes()):
            pm.rename(s, new_curve+'Shape'+str(x+1))
    else:
        new_curve.getShape().rename(new_curve+'Shape')

    return new_curve