import pymel.core as pm


def create(name):

    new_curve = pm.createNode('transform', n=name)

    circle_shp = pm.curve(d=1, p=[[-1.0, 0.005, 0.0], [-0.924, 0.005, 0.383], [-0.707, 0.005, 0.707], [-0.383, 0.005, 0.924], [0.0, 0.005, 1.0], [0.383, 0.005, 0.924], [0.707, 0.005, 0.707], [0.924, -0.005, 0.383], [1.0, -0.005, 0.0], [0.924, 0.005, -0.383], [0.707, 0.005, -0.707], [0.383, 0.005, -0.924], [0.0, 0.005, -1.0], [-0.383, 0.005, -0.924], [-0.707, 0.005, -0.707], [-0.924, 0.005, -0.383], [-1.0, 0.005, 0.0]], k=[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0])
    pm.parent(circle_shp.getShape(), new_curve, r=True, s=True)
    pm.delete(circle_shp)

    hair_shp = pm.curve(d=2, p=[[-0.094, 0.0, 1.0000000000000078], [-0.3885005448213814, 0.0, 0.9345554344841451], [-1.243390793568806, 0.0, 0.7041276321431922], [-2.0442036655007407, 0.0, 0.17496360818133108], [-3.1403777732618177, 0.0, -1.4805561178223947], [-2.044203665500745, 0.0, -0.17496360818132795], [-1.2433907935688018, 0.0, -0.7041276321431851], [-0.3885005448213861, 0.0, -0.9345554344841353], [-0.094, 0.0, -0.9999999999999982]])
    pm.parent(hair_shp.getShape(), new_curve, r=True, s=True)
    pm.delete(hair_shp)

    if len(new_curve.getShapes()) > 1:
        for x, s in enumerate(new_curve.getShapes()):
            pm.rename(s, new_curve+'Shape'+str(x+1))
    else:
        new_curve.getShape().rename(new_curve+'Shape')

    # Post edit
    for s in (new_curve.getShapes()):
        pm.rotate(s + '.cv[:]', [0, 0, -90], r=True, os=True)
        pm.rotate(s + '.cv[:]', [0, -90, 0], r=True, os=True)
    pm.select(cl=1)

    return new_curve



