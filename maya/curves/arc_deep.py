import pymel.core as pm


def create(name):
    new_curve = pm.createNode('transform', n=name)

    shp = pm.curve(d=1, p=[[0.725, 0.0, -0.725], [0.725, 0.0, 0.0], [0.7, 0.0, 0.188], [0.628, 0.0, 0.362], [0.513, 0.0, 0.513], [0.362, 0.0, 0.628], [0.188, 0.0, 0.7], [-0.0, 0.0, 0.725], [-0.188, 0.0, 0.7], [-0.362, 0.0, 0.628], [-0.513, 0.0, 0.513], [-0.628, 0.0, 0.362], [-0.7, 0.0, 0.188], [-0.725, 0.0, -0.0], [-0.725, 0.0, -0.725]], k=[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0])
    pm.parent(shp.getShape(), new_curve, r=True, s=True)
    pm.delete(shp)

    if len(new_curve.getShapes()) > 1:
        for x, s in enumerate(new_curve.getShapes()):
            pm.rename(s, new_curve+'Shape'+str(x+1))
    else:
        new_curve.getShape().rename(new_curve+'Shape')

    return new_curve