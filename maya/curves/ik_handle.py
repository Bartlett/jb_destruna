import pymel.core as pm


def create(name):
    new_curve = pm.createNode('transform', n=name)

    shp = pm.curve(d=1, p=[[-0.471, -0.0, 0.553], [-0.446, -0.0, 0.578], [-0.322, -0.0, 0.578], [-0.305, -0.0, 0.595], [-0.305, -0.0, 0.912], [-0.279, -0.0, 0.937], [0.083, -0.0, 0.937], [0.232, -0.0, 0.904], [0.357, -0.0, 0.809], [0.442, -0.0, 0.667], [0.471, -0.0, 0.499], [0.471, 0.0, -0.424], [0.424, 0.0, -0.477], [-0.424, 0.0, -0.477], [-0.471, 0.0, -0.424], [-0.471, -0.0, 0.553]], k=[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0])
    pm.parent(shp.getShape(), new_curve, r=True, s=True)
    pm.delete(shp)

    if len(new_curve.getShapes()) > 1:
        for x, s in enumerate(new_curve.getShapes()):
            pm.rename(s, new_curve+'Shape'+str(x+1))
    else:
        new_curve.getShape().rename(new_curve+'Shape')

    return new_curve