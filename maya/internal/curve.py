from jb_destruna.maya.utility.misc import pn
from pymel.core import nodetypes as nt
import pymel.core as pm
from maya import cmds as mc
from jb_destruna.maya.internal import cpom


def copy(curve, n=''):
    """

    Args:
        curve: (node or str)    curve node to copy
        n:     (str)

    Returns:

    """

    curve = pn(curve)

    # make sure this is a curve
    if isinstance(pn(curve).getShape(), nt.NurbsCurve):
        pass
    else:
        raise Exception('copy(): curve=%s, this is not a curve.' % curve)

    # variables.
    curve_shp = curve.getShape()

    # get name if none
    if n:
        pass
    else:
        n = curve+'_copy'

    # create new nurbs
    new_curve_shp = pm.createNode('nurbsCurve', n=n+'Shape')
    new_curve = new_curve_shp.getParent()

    # take shape from curve.
    pm.connectAttr(curve_shp.local, new_curve_shp.create, f=True)
    new_curve_shp.create.disconnect()

    # match position & scale
    pm.delete(pm.parentConstraint(curve, new_curve, mo=False))
    pm.delete(pm.scaleConstraint(curve, new_curve, mo=False))

    return new_curve




def created_fitted_curve(crv):
    crv_fitted = mc.fitBspline(crv, n=crv + '_fitted', ch=1, tol=0.01)[0]

    deg = mc.getAttr(crv + '.degree')
    spans = mc.getAttr(crv + '.spans')

    adjusted_position_list = []

    for i in range(deg + spans):
        pos = mc.pointPosition(crv + '.cv[{}]'.format(i))
        p = cpom.nearest_position_on_curve_from_position(position=pos, curve=crv)
        adjusted_position_list.append(p)

    for i, p in enumerate(adjusted_position_list):
        mc.move(p[0], p[1], p[2], crv + '.cv[{}]'.format(i), a=1, ws=1)

    return crv_fitted



