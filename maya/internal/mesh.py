from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.utility.misc import pn
from maya import OpenMaya
from maya import cmds as mc
import pymel.core as pm


def copy(geo, n='geo_copy'):
    """
    This function creates a clean duplicate of geo arg.
    Args:
        geo: (str or pm.PyNode)     target geo to duplicate.
        n: (str)                    intended name of geo copy.

    Returns:
        new_geo (nt.Transform)
    """

    # cast geo arg as pm.PyNode
    geo = pn(geo)

    # create new transform and shape nodes
    tmp_shp = pm.createNode('mesh', n=n+'TempShape')
    tmp_geo = tmp_shp.getParent()

    # connect geo shape to new shape then disconnect
    pm.connectAttr(geo.getShape().outMesh, tmp_shp.inMesh, f=True)
    pm.connectAttr(geo.getShape().uvSet[0], tmp_shp.uvSet[0], f=True)

    # default shader assign.
    pm.sets('initialShadingGroup', fe=tmp_shp)

    # dgdirty to give the shape a forced update
    pm.flushIdleQueue()
    pm.dgdirty(tmp_shp)

    # using maya duplicate, because uvs are dropping off with the above method when inmesh is disconnected.
    # might be my version of maya.
    new_geo = pm.duplicate(tmp_geo, n=n)[0]

    # disconnect now
    pm.delete(tmp_geo)

    # return new geo
    return new_geo


def branch_geo(geo, n='geo_branch'):

    geo = pn(geo)

    # create result geo
    branch = copy(geo, n=n)
    driver = copy(geo, n=n+'_driver')

    # create blendshape
    blendshape.apply(geo, branch, dv=1)
    blendshape.apply(geo, driver, dv=1)

    return branch, driver

def smooth(geo, n='', divisions=1, continuity=1.0, keepBorder=False, keepHardEdge=False, smoothUVs=False):

    smooth = pm.polySmooth(
        geo,
        constructionHistory=1,
        continuity=continuity,
        divisions=divisions,
        divisionsPerEdge=1,
        keepBorder=keepBorder,
        keepHardEdge=keepHardEdge,
        keepMapBorders=1,
        keepSelectionBorder=1,
        keepTesselation=1,
        method=0,
        osdCreaseMethod=0,
        osdFvarBoundary=3,
        osdFvarPropagateCorners=0,
        osdSmoothTriangles=0,
        osdVertBoundary=1,
        propagateEdgeHardness=0,
        pushStrength=0.1,
        roundness=1,
        smoothUVs=smoothUVs,
        bnr=1,
        subdivisionLevels=1,
        subdivisionType=0,
    )[0]

    if n:
        smooth_name = n
    else:
        smooth_name = geo+'_smooth'

    pm.rename(smooth, smooth_name)

    return smooth

def quad_from_points(node, name=''):
    
    from jb_destruna.maya.internal import follicle as fol

    for target in mc.ls(sl=1):
        # target = 'Hand_IndexFinger2_joint_L'

        p_scale = 0.5
        p = mc.polyPlane()[0]
        p_shape = mc.listRelatives(p, c=1)[0]
        node = mc.listConnections(p_shape, type='polyPlane')[0]
        mc.setAttr(node + '.subdivisionsWidth', 1)
        mc.setAttr(node + '.subdivisionsHeight', 1)
        mc.setAttr(node + '.height', p_scale)
        mc.setAttr(node + '.width', p_scale)

        mc.delete(mc.parentConstraint(target, p))

        fol.create(p, n=target + '__fol', setUV=True, uvVal=(0.5, 0.5))
