import pymel.core as pm

from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility import misc
import json

reload(misc)
pn = misc.pn

def getTagged(tag, attr='tags', ns=''):

    if ns:
        ns = ns+':'
        print 'ns:', ns

    nodes_with_attr = list(set([n.node() for n in pm.ls(ns+'*.'+attr)]))
    tagged_nodes = [n for n in nodes_with_attr if hasTag(n, tag, attr=attr)]

    return tagged_nodes


def addTag(*args, **kwargs):

    if isinstance(args[0], list):
        node_list = args[0]
    else:
        node_list = [args[0]]

    for node in node_list:

        if pn(node):
            node = pn(node)
        else:
            pm.warning('This function requires an existing object to operate on.')
            return

        if len(args) >= 2:
            tag = args[1]
        else:
            pm.warning('This function requires a tag string as a second argument')
            return

        if len(args) > 2:
            tag_val = args[2]
        else:
            tag_val = True

        attr = kwargs.get('attr', 'tags')

        if node.hasAttr(attr):
            pass
        else:
            attribute.add(node, n=attr, type='string')
            node.attr(attr).set('{}')

        attr_dict = json.loads(node.attr(attr).get())

        attr_dict[str(tag)] = tag_val

        node.attr(attr).set(json.dumps(attr_dict))


def setTag(*args, **kwargs):
    addTag(args, kwargs)


def getTag(*args, **kwargs):
    """
    Args:
        *args:
            node        (pm.PyNode or string)
            tag         (string)

        **kwargs:
            attr        (string)
    Returns:
        Tag value or None
    """
    if pn(args[0]):
        node = pn(args[0])
    else:
        pm.warning('This function requires an existing object to operate on.')
        return

    if len(args) >= 2:
        tag = args[1]
    else:
        pm.warning('This function requires a tag string as a second argument')
        return

    attr = kwargs.get('attr', 'tags')

    if node.hasAttr(attr):
        pass
    else:
        return None

    attr_dict = json.loads(node.attr(attr).get())

    if tag in attr_dict.keys():
        return attr_dict.get(tag)

def hasTag(*args, **kwargs):
    """
    Args:
        *args:
            node        (pm.PyNode or string)
            tag         (string)

        **kwargs:
            attr        (string)
    Returns:
        True or False
    """
    if pn(args[0]):
        node = pn(args[0])
    else:
        pm.warning('This function requires an existing object to operate on.')
        return

    if len(args) >= 2:
        tag = args[1]
    else:
        pm.warning('This function requires a tag string as a second argument')
        return

    attr = kwargs.get('attr', 'tags')

    if node.hasAttr(attr):
        pass
    else:
        return None

    attr_dict = json.loads(node.attr(attr).get())

    if tag in attr_dict.keys():
        return True
    else:
        return False