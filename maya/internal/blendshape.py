from jb_destruna.maya.utility import datafiles
from jb_destruna.maya.utility.misc import pn
import maya.cmds as mc
import os
import pymel.core as pm
import time



def apply(*args, **kwargs):
    """

    Args:
        target (string or pm.PyNode) optional target to apply as blendShapeTarget
        geo    (string or pm.PyNode) geo to which blendshape deformer is added

    Kwargs:
        name (n)
        defaultValue (dv)
        alias (a)
        frontOfChain (foc)
        deleteTargetGeo (dtg)
        tangentSpace (ts): (bool)     if True this kwarg applies target as tangent space. (default=False)

    Returns:
        nt.BlendShape
    """

    # check for args.
    for node in args:
        if not pn(node):
            pm.error('blendshape.apply: '+node+'does not exist.')

    # process args.
    if len(args) == 0:
        target = None
        geo = None
        pm.error('blendshape.apply: this function requires at least one argument.')
    elif len(args) == 1:
        target = None
        geo = args[0]
    else:
        target = args[0]
        geo = args[1]



    # process kwargs
    bs_name = kwargs.get('name', kwargs.get('n', geo+'_blendShape'))
    default_value = kwargs.get('defaultValue', kwargs.get('dv', 0))
    alias = kwargs.get('alias', kwargs.get('a', None))
    foc = kwargs.get('frontOfChain', kwargs.get('foc', True))
    delete_target = kwargs.get('deleteTargetGeo', kwargs.get('dtg', False))
    tangent_space = kwargs.get('tangentSpace', kwargs.get('ts', False))

    # create blendshape
    if pn(bs_name):
        bs = pn(bs_name)
    else:
        bs = pm.blendShape(geo, n=bs_name)[0]

    # complete function if no target exists.
    if target:
        pass
    else:
        return bs

    # calculate index at which to apply target
    if len(bs.weightIndexList()) == 0:
        index = 0
    else:
        index = bs.weightIndexList()[-1]+1

    # apply target
    t_data = [geo, index, target, default_value]
    pm.blendShape(bs, t=t_data, e=True, tc=False)

    # set default wt
    if default_value:
        bs.w[index].set(default_value)

    # set alias
    if alias:
        bs.w[index].setAlias(alias)

    # delete target geo
    if delete_target:
        pm.delete(target)

    return bs


def get_target_list(blendshape):

    blendshape = pn(blendshape)

    target_list = pm.listAttr(blendshape + '.w', m=True)
    if not target_list:
        target_list = []

    return target_list


def get_target_index(blendshape, target):

    attr = blendshape + '.w[{}]'
    weightCount = pm.blendShape(blendshape, q=True, wc=True)
    for index in xrange(weightCount):
        if pm.aliasAttr(attr.format(index), q=True) == target:
            return index
    return -1


def extract_targets(blendshape, group='extracted_shapes'):

    blendshape = pn(blendshape)

    target_list = get_target_list(blendshape)

    new_target_list = list()
    for target in target_list:
        target_idx = get_target_index(blendshape=blendshape, target=target)
        new_target = pm.sculptTarget(blendshape, e=True, target=target_idx, regenerate=True)[0]
        new_target_list.append(new_target)

    if group:
        grp = group
        if not pm.objExists(group):
            grp = pm.group(em=True, n=group)

        pm.parent(new_target_list, grp)

    return new_target_list


def export_weights_to_file(filepath=None, geo=None, blendshape=None, force=False):

    blendshape = pn(blendshape)

    # test if required kwargs are supplied.
    if geo is None:
        raise Exception('export_to_file: please pass a geo to this function.')

    if blendshape is None:
        raise Exception('export_to_file: please pass a blendshape to this function.')

    # apply default path if None.
    if filepath:
        pass
    else:
        filepath = 'C:/Users/' + os.environ['USER'] + '/Desktop/weights/blendShape/' + blendshape + '_001.wts'

    # get folder of intended filepath
    filefolder = '/'.join(filepath.split('/')[:-1])
    filename = filepath.split('/')[-1]

    # if folder doesn't exist, create it.
    if not os.path.isdir(filefolder):
        os.makedirs(filefolder)

    # test if intended filename exists
    if os.path.isfile(filepath):

        if force:
            print 'export_to_file: this file already exists. replacing file because force kwarg = True.'
        else:
            raise Exception('export_to_file: this file already exists. consider versioning up.')

    # build blendshape data
    print '\t - building data for '+str(blendshape)
    blendshape_data = build_blendshape_data(geo=geo, blendshape=blendshape)

    # write to json file
    print '\t - saving weights to ' + str(filepath)
    datafiles.data_to_file(data=blendshape_data, path=filepath)

    print '\t - DONE!'
    return filepath


def import_weights_from_file(filepath=None, geo=None, blendshape='', set_attrs=True):

    t1 = time.time()

    # test if required kwargs are supplied.
    if geo is None:
        raise Exception('export_to_file: please pass a geo to this function.')

    if filepath is None:
        raise Exception('export_to_file: please pass a blendshape to this function.')

    # test if file exists.
    if os.path.isfile(filepath):
        pass
    else:
        raise Exception('export_to_file: supplied file does not exist.')

    # establish blendshape name.
    if blendshape:
        bs_name = str(blendshape)
    else:
        bs_name = geo+'_blendShape'

    # get data
    bs_data = datafiles.data_from_file(filepath)

    # create blendshape node if it doesnt exist.
    if mc.objExists(bs_name):
        blendshape = bs_name
    else:
        blendshape = apply(geo, n=bs_name)

    # build target lists from
    existing_target_list = get_target_list(blendshape)
    target_list = bs_data['target_list']

    # iterate through target_list
    print('setting weights')
    for target in target_list:

        print(target)

        if target in existing_target_list:

            wt_list = bs_data['weights'][target]
            tgt_index = get_target_index(blendshape, target=target)

            print 'setting '+target+' weights on '+blendshape
            for vtx_idx, wt in enumerate(wt_list):

                #pm.setAttr(blendshape+'.inputTarget['+str(tgt_index)+'].paintTargetWeights['+str(vtx_idx)+']', wt)
                mc.setAttr(blendshape + '.inputTarget[0].inputTargetGroup[{0}].targetWeights[{1}]'.format(tgt_index, vtx_idx), wt)
        else:
            print 'skipping ' + target + ' because it not on blendshape.'

    # set attrs
    if set_attrs:
        for attr, val in bs_data['attr_list'].items():
            pm.setAttr(blendshape + '.' + attr, val)

    elapsed = time.time() - t1
    print 'Loaded blendshape data for', geo, 'in', elapsed, 'seconds.'


def build_blendshape_data(geo, blendshape):
    """
    This function creates a dictionary with all data needed to rebuild a blendshape.

    Args:
        geo: (str)              geo with blendshape applied to it.
        blendshape: (str)     target blendshape

    Returns:

    """

    geo = str(geo)
    blendshape = str(blendshape)

    # gathering information
    vtx_count = mc.polyEvaluate(geo, v=True)

    # get target list
    target_list = get_target_list(blendshape)
    print("DAMO EDIT")
    # build weights dict
    weights_dict = dict()
    for target in target_list:

        tgt_idx = get_target_index(blendshape, target)

        wt_list = list()
        for vtx_idx in range(vtx_count):
            #wt = pm.getAttr(blendshape+'.inputTarget['+str(tgt_idx)+'].paintTargetWeights['+str(vtx_idx)+']')

            wt = mc.getAttr(blendshape + '.inputTarget[0].inputTargetGroup[{0}].targetWeights[{1}]'.format(tgt_idx, vtx_idx))

            wt_list.append(wt)

        weights_dict[str(target)] = wt_list

    # save blendshape attr values I care about.
    attr_list = [
        'origin',
    ]
    attribute_values = dict()
    for atr in attr_list:
        val = pm.getAttr(blendshape+'.'+atr)
        attribute_values[atr] = val

    # build return dictionary
    return_dict = dict()
    return_dict['geo'] = geo
    return_dict['target_list'] = [str(t) for t in target_list]
    return_dict['weights'] = weights_dict
    return_dict['attr_list'] = attribute_values

    return return_dict

"""
bs_target_metadata = {
idx = 0,
alias = 'motherflippa',
ibw_wt = 1.0,
}
"""