import pymel.core as pm

import importlib

from jb_destruna.maya.internal import meta
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn


# DONE: finish control.create()
# DONE: write a control.cmd_from_curve()
# TODO: write a create all in lib


# ======================================================================================================================
def     create(*args, **kwargs):
        """
        Args:
            arg[0]:         (string, PyNode or None)        node to derive control from.

        Kwargs:
            shape:          (string)                    shape of control curve (default='box')
            colorIdx:       (int)                       color index of control curve
            translation:    (tuple)
            rotation:       (tuple)
            zeroGroup:      (bool)
            offsetGroup     (bool)
            hideJoint:      (bool)
            shapeModule:    (string)

        Returns:
            ctl (PyNode)
        """

        if args:
            if pn(args[0]):
                ctl = pn(args[0])
            else:
                ctl = pm.group(em=True, name=args[0])
        else:
            pm.warning('control.create() requires name of control or existing node as first arg.')
            return

        shape = kwargs.get('shape')
        color_idx = kwargs.get('colorIdx')
        hide_jnt = kwargs.get('hideJoint')
        translation = kwargs.get('translation')
        rotation = kwargs.get('rotation')
        size = kwargs.get('size')
        zero_grp = kwargs.get('zeroGroup', False)
        off_grp = kwargs.get('offsetGroup', False)
        module = kwargs.get('shapeModule', 'jb_destruna.maya.curves')

        # use shapeModule to create nurbsCurve
        if shape:
            func = importlib.import_module(module+'.'+shape)
            reload(func)
            crv_node = func.create('crv')
            pm.parent(crv_node.getShapes(), ctl, s=True, r=True)
            pm.delete(crv_node)

        # hide joint
        if hide_jnt:
            if ctl.type() == 'joint':
                ctl.drawStyle.set(2)

        # set shape color
        if color_idx:
            for s in ctl.getShapes():
                s.overrideEnabled.set(1)
                s.overrideColor.set(color_idx)

        # set shape translation
        if translation:
            for s in ctl.getShapes():
                c, c_h = pm.cluster(s.cv[:])
                pm.move(c_h, translation, r=True, os=True)
                pm.delete(s, ch=True)

        # set shape rotation
        if rotation:
            for s in ctl.getShapes():
                pm.rotate(s.cv[:], rotation, r=True, os=True)


        # set shape size
        if size:
            for s in ctl.getShapes():
                pm.scale(s.cv[:], size, size, size, r=True, os=True)


        # zero group
        if zero_grp:
            zgrp = pm.group(em=True, n=ctl+'_ZERO')
            pm.parent(zgrp, ctl, r=True)
            pm.parent(zgrp, ctl.getParent())
            pm.parent(ctl, zgrp)

        # offset group
        if off_grp:
            ogrp = pm.group(em=True, n=ctl+'_OFFSET')
            ctl_children = [c for c in ctl.getChildren() if c not in ctl.getShapes()]
            pm.parent(ogrp, ctl, r=True)
            if len(ctl_children):
                pm.parent(ctl_children, ogrp)

        for shp in ctl.getShapes():
            pm.rename(shp, ctl+'Shape')

        return ctl


# ======================================================================================================================
def create_from_existing(name='ctl', sourceControl=None, colorIdx=None, zeroGroup=None, offsetGroup=None):

        sourceControl = pn(sourceControl)
        ctl = pm.circle(n=name, ch=False)[0]
        sourceControl.getShape().local >> ctl.getShape().create
        ctl.getShape().create.disconnect()

        # set shape color
        if colorIdx:
            for s in ctl.getShapes():
                s.overrideEnabled.set(1)
                s.overrideColor.set(colorIdx)

        # zero group
        if zeroGroup:
            zgrp = pm.group(em=True, n=ctl+'_ZERO')
            pm.parent(zgrp, ctl, r=True)
            pm.parent(zgrp, ctl.getParent())
            pm.parent(ctl, zgrp)

        # offset group
        if offsetGroup:
            ogrp = pm.group(em=True, n=ctl+'_OFFSET')
            ctl_children = [c for c in ctl.getChildren() if c not in ctl.getShapes()]
            pm.parent(ogrp, ctl, r=True)
            if len(ctl_children):
                pm.parent(ctl_children, ogrp)

        return ctl


# ======================================================================================================================
def cmd_from_curve():

    crv = pm.selected()[0]
    return_str = "\n\nimport pymel.core as pm\n\n\ndef create(name):\n    new_curve = pm.createNode('transform', n=name)\n"

    for i, crv_shp in enumerate(crv.getShapes()):

        # get degree
        degree = pm.getAttr(crv_shp + '.degree')
        spans = pm.getAttr(crv_shp + '.spans')
        cvs_count = degree + spans

        # get point list
        points = []
        for cvi in range(cvs_count):
            cv_pos = pm.xform(crv.cv[cvi], q=True, t=True, ws=True)
            rounded_cv_pos = []
            for p in cv_pos:
                p = "{0:.3f}".format(p)
                p = float(p)
                rounded_cv_pos.append(p)
            points.append(rounded_cv_pos)

        # get knot list
        knots = crv_shp.getKnots()

        return_str = return_str + "\n    shp = pm.curve(d="+str(degree)+", p="+str(points)+", k="+str(knots)+")\n    pm.parent(shp.getShape(), new_curve, r=True, s=True)\n    pm.delete(shp)\n"

    return_str = return_str + "\n    if len(new_curve.getShapes()) > 1:\n        for x, s in enumerate(new_curve.getShapes()):\n            pm.rename(s, new_curve+'Shape'+str(x+1))\n    else:\n        new_curve.getShape().rename(new_curve+'Shape')\n\n    return new_curve\n\n"

    print '='*100, '\n'
    print return_str


# ======================================================================================================================
def create_extra_control(ctl, name='extra_ctl', colorIdx=17):

    ctl = pn(ctl)
    ctl_child_nodes = [c for c in ctl.getChildren() if c.type() not in ['nurbsCurve']]
    attribute.add(ctl, n='extraControlVis', type='enum', en=['hide', 'show'], dv=0, min=0, max=1)

    extra_ctl = create_from_existing(name=name, sourceControl=ctl, colorIdx=colorIdx)
    pm.scale(extra_ctl.cv[:], [.85, .85, .85], r=True, os=True)

    pm.delete(pm.parentConstraint(ctl, extra_ctl, mo=False))

    pm.parent(extra_ctl, ctl)
    ctl.extraControlVis >> extra_ctl.getShape().v

    pm.parent(ctl_child_nodes, extra_ctl)

    return extra_ctl


# ======================================================================================================================
def create_pivot_control(ctl, colorIdx=17, size=1):
    ctl = pn(ctl)

    attribute.add(ctl, n='showPivot', type='enum', en=['hide', 'show'], dv=0, min=0, max=1)
    pivot = pm.group(em=True, n=ctl+'_pivot')
    pm.delete(pm.parentConstraint(ctl, pivot, mo=False))
    pivot = create(
        pivot,
        shape='pivot',
        colorIdx=colorIdx,
        size=size
    )

    if ctl.getParent():
        pm.parent(pivot, ctl.getParent())

    ctl.showPivot >> pivot.getShape().v

    for attr, rp_attr, sp_attr in zip(['tx', 'ty', 'tz'], ['rpx', 'rpy', 'rpz'], ['spx', 'spy', 'spz']):
        pivot.attr(attr) >> ctl.attr(rp_attr)
        pivot.attr(attr) >> ctl.attr(sp_attr)

    for attr in ['rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v']:
        pivot.attr(attr).set(cb=False, k=False, l=True)

    return pivot


def build_groups(prefix='Test', side='', parent=None, return_as='list'):
    """
    This function creates the standard hierachy of groups for use in rigging.

    Args:
        prefix: (str)   prefix part of the group naming
        side: (str)     side suffix of the group naming
        parent: (str or pm.PyNode)  if used, top group will be parented under this node.
        return_as (str) determines formatting for function return.

    Returns:
        if return_as == 'list':
            [top_grp, ctl_grp, jnt_grp, dnt_grp]
        if return_as == 'dict':
            {
                'top': top_grp,
                'ctl': ctl_grp,
                'jnt': jnt_grp,
                'dnt': dnt_grp,
            }
    """
    top_name_list = [prefix]
    ctl_name_list = [prefix, 'controls']
    jnt_name_list = [prefix, 'joints']
    dnt_name_list = [prefix, 'doNotTouch']

    if side:
        top_name_list.append(side)
        ctl_name_list.append(side)
        jnt_name_list.append(side)
        dnt_name_list.append(side)

    top_grp = pm.group(em=True, n=''.join(top_name_list))
    ctl_grp = pm.group(em=True, n=''.join(ctl_name_list), p=top_grp)
    jnt_grp = pm.group(em=True, n=''.join(jnt_name_list), p=top_grp)
    dnt_grp = pm.group(em=True, n=''.join(dnt_name_list), p=top_grp)

    if parent:
        if pm.objExists(parent):
            pm.parent(top_grp, parent)
        else:
            raise Exception('build_groups(): parent node could not be found.'+str(parent))

    if return_as == 'dict':
        return {
            'top': top_grp,
            'ctl': ctl_grp,
            'jnt': jnt_grp,
            'dnt': dnt_grp,
        }
    elif return_as == 'list':
        return [top_grp, ctl_grp, jnt_grp, dnt_grp]
    else:
        raise Exception('build_groups(): return_as type not supported.'+str(return_as))

