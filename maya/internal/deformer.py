from jb_destruna.maya.utility.misc import pn
import pymel.core as pm


def create_shrinkwrap(
        wrapped,
        wrapper,
        name='',
        projection='closest'
):
    """
    This function creates a shrinkwrap deformer.

    Args:
        wrapped (str, pm.PyNode)    geo that is being wrapped.
        wrapper (str, pm.PyNode)    geo that is acting as wrapper.
        name (str)                  name of shrink wrap node.
        projection (str)               type of shrinkwrap projection to use (towards_inner, towards_center, parallel_to_axes, vertex_normals, closest)
    Returns:
        shrinkWrap node (pm.PyNode)
    """

    projection_map = {
        'towards_inner': 0,
        'towards_center': 1,
        'parallel_to_axes': 2,
        'vertex_normals': 3,
        'closest': 4,
    }

    # convert to pymel
    wrapped = pn(wrapped)
    wrapper = pn(wrapper)

    # validate geos
    if wrapped:
        pass
    else:
        raise Exception('create_shrinkwrap(): requires a valid wrapped geo to perform shrinkwrap.')

    if wrapper:
        pass
    else:
        raise Exception('create_shrinkwrap(): requires a valid wrapper geo to perform shrinkwrap.')

    # format name.
    if name:
        pass
    else:
        name = wrapped+'_shrinkWrap'

    # create shrinkwrap deformer on wrapped
    shrinkwrap = pm.deformer(wrapped, type='shrinkWrap', n=name)[0]

    # connect wrapper
    pm.connectAttr(wrapper.w, shrinkwrap.tgt)
    for atr in ['co', 'suv', 'kb', 'bnr', 'khe', 'peh', 'kmb']:
        pm.connectAttr(wrapper.attr(atr), shrinkwrap.attr(atr))

    # set projection value
    shrinkwrap.projection.set(projection_map[projection])

    return shrinkwrap


def create_wire(
        geo='',
        curve='',
        name='',
        rotation=1.0,
        dropoff_distance=1.0,
        scale=1.0
):
    """
    This function creates a wire deformer with a curve and a geo.

    Args:
        geo: (str, pm.PyNode)       geo to create wire deformer on
        curve: (str, pm.PyNode)     curve to create wire deformer on
        name: (str)                 name of wire deformer (default= geo+'_wire')
        rotation: (float)           amount of rotation to set on wire deformer (default=1.0)
        dropoff_distance: (float)    amount of dropoff distance to set on wire deformer (default=1.0)
        scale:                      amount of scale to set on wire deformer (default=1.0

    Returns:
        wire (pm.PyNode)
    """

    # convert to pymel
    geo = pn(geo)
    curve = pn(curve)

    # validate geos
    if geo:
        pass
    else:
        raise Exception('create_wire(): requires a valid wrapped geo to perform shrinkwrap.')

    if curve:
        pass
    else:
        raise Exception('create_wire(): requires a valid curve to perform shrinkwrap.')

    # format name
    if name:
        pass
    else:
        name = geo+'_wire'

    # create wire deformer on geo
    wire = pm.wire(geo, w=curve, n=name)[0]

    # set attributes
    wire.rotation.set(rotation)
    wire.dropoffDistance[0].set(dropoff_distance)
    wire.scale[0].set(scale)

    return wire


def create_deltamush(
        geo='',
        name='',
        smoothing_iterations=10,
        smoothing_step=0.5,
        inward_constraint=0.0,
        outward_constraint=0.0,
        distance_weight=0.0,
        pin_border_vertices=0.0
):
    """
    This function creates a deltamush on a geo.

    Args:
        geo: (str, pm.PyNode)           geo to apply a deltaMush deformer to
        name: (str)                     name of deltaMush deformer (default= geo+'_deltaMush')
        smoothing_iterations: (int)     number of smoothing iterations performed by the delta mush node. (default=10)
        smoothing_step: (float)         step amount used per smoothing iteration. (default=0.5)
        inward_constraint: (float)      constrains the movement of the vertex to not move inward from the input deforming shape to preserve the contour. (default=0.0)
        outward_constraint: (float)     constrains the movement of the vertex to not move outward from the input deforming shape to preserve the contour. (default=0.0)
        distance_weight: (float)        controls how the distance between verts will play a role in how the verts are averaged. (default=0.0)
        pin_border_vertices: (float)    how much to pin to border.

    Returns:
        deltamush (pm.PyNode)
    """

    # convert to pymel.
    geo = pn(geo)

    # validate
    if geo:
        pass
    else:
        raise Exception('create_wire(): requires a valid wrapped geo to perform shrinkwrap.')

    # format name
    if name:
        pass
    else:
        name = geo+'_deltaMush'

    # create deltaMush deformer on geo
    pm.select(geo, r=True)
    deltamush = pm.deltaMush(name=name)

    # set attrs
    deltamush.smoothingIterations.set(smoothing_iterations)
    deltamush.smoothingStep.set(smoothing_step)
    deltamush.inwardConstraint.set(inward_constraint)
    deltamush.outwardConstraint.set(outward_constraint)
    deltamush.distanceWeight.set(distance_weight)
    deltamush.pinBorderVertices.set(pin_border_vertices)

    return deltamush

