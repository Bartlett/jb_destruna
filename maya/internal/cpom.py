from jb_destruna.maya.utility.misc import pn
import pymel.core as pm


def closest_point_on_geo_from_transform(node='transform1', geo='test_geo'):

    node = pn(node)
    geo = pn(geo)

    cpom = pm.createNode('closestPointOnMesh', n='tmp_cpom')

    pm.connectAttr(geo.getShape().outMesh, cpom.inMesh, f=True)
    pm.connectAttr(geo.worldMatrix, cpom.inputMatrix, f=True)
    node_pos = pm.xform(node, t=True, q=True, ws=True)
    cpom.inPosition.set(node_pos)

    point_on_mesh = cpom.position.get()

    pm.delete(cpom)

    return point_on_mesh


def closest_point_on_geo_from_position(position=[0, 0, 0], geo='test_geo'):

    geo = pn(geo)

    cpom = pm.createNode('closestPointOnMesh', n='tmp_cpom')

    pm.connectAttr(geo.getShape().outMesh, cpom.inMesh, f=True)
    cpom.inPosition.set(position)

    point_on_mesh = cpom.position.get()

    pm.delete(cpom)

    return point_on_mesh


def closest_uv_on_geo_from_transform(node='transform1', geo='test_geo'):

    node = pn(node)
    geo = pn(geo)

    cpom = pm.createNode('closestPointOnMesh', n='tmp_cpom')

    pm.connectAttr(geo.getShape().outMesh, cpom.inMesh, f=True)
    pos = pm.xform(node, t=True, q=True, ws=True)
    cpom.inPosition.set(pos)

    u_value = cpom.parameterU.get()
    v_value = cpom.parameterV.get()

    pm.delete(cpom)

    return [u_value, v_value]

def closest_uv_on_geo_from_position(position=[0, 0, 0], geo='test_geo'):

    geo = pn(geo)

    cpom = pm.createNode('closestPointOnMesh', n='tmp_cpom')

    pm.connectAttr(geo.getShape().outMesh, cpom.inMesh, f=True)
    cpom.inPosition.set(position)

    u_value = cpom.parameterU.get()
    v_value = cpom.parameterV.get()

    #pm.delete(cpom)

    return [u_value, v_value]

def nearest_pr_on_curve_from_position(position=[0, 0, 0], curve='test_curve'):

    curve = pn(curve)

    npoc = pm.createNode('nearestPointOnCurve', n='tmp_npoc')

    pm.connectAttr(curve.getShape().local, npoc.inputCurve, f=True)
    npoc.inPosition.set(position)

    pr_value = npoc.parameter.get()

    pm.delete(npoc)

    return pr_value

def nearest_position_on_curve_from_position(position=[0, 0, 0], curve='test_curve'):

    curve = pn(curve)

    npoc = pm.createNode('nearestPointOnCurve', n='tmp_npoc')

    pm.connectAttr(curve.getShape().local, npoc.inputCurve, f=True)
    npoc.inPosition.set(position)

    pr_value = npoc.position.get()

    pm.delete(npoc)

    return pr_value

def nearest_uvalue_on_curve(position=[0, 0, 0], curve='test_curve'):

    curve = pn(curve)

    rebuild = pm.createNode('rebuildCurve', n='tmp_rebuild')
    pm.connectAttr(curve.getShape().local, rebuild.inputCurve, f=True)
    rebuild.keepRange.set(0)
    rebuild.keepControlPoints.set(1)

    npoc = pm.createNode('nearestPointOnCurve', n='tmp_npoc')

    pm.connectAttr(rebuild.outputCurve, npoc.inputCurve, f=True)
    npoc.inPosition.set(position)

    pr_value = npoc.parameter.get()

    pm.delete(npoc, rebuild)

    return pr_value