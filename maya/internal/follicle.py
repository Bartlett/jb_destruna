from maya import cmds as mc
from jb_destruna.maya.utility.misc import pn
import pymel.core as pm


def create(geo, n='follicle1', setUV=False, uvVal=(0, 0)):
    """
    This function creates a follicle on geo, and optionally sets its uv position.
    Args:
        geo: (str or pm.PyNode)     geo to put follicle on.
        n: (str)                    name of follicle (default='follicle1')
        setUV: (bool)               whether to set uv.
        uvVal: (tuple)              u and v value to set on follicle

    Returns:
        follicle
    """
    geo = pn(geo)

    geo_shp = geo.getShape()

    # create actual follicle
    fol_shp = pm.createNode('follicle', n=n+'Shape')
    fol = fol_shp.getParent()

    # connect to geo
    pm.connectAttr(geo_shp.outMesh, fol_shp.inputMesh, f=True)
    pm.connectAttr(geo_shp.worldMatrix[0], fol_shp.inputWorldMatrix, f=True)

    # connect follicle and transform
    pm.connectAttr(fol_shp.outTranslate, fol.translate, f=True)
    pm.connectAttr(fol_shp.outRotate, fol.rotate, f=True)

    if setUV:
        fol_shp.parameterU.set(uvVal[0])
        fol_shp.parameterV.set(uvVal[1])

    return fol


def onSelected(selection=None, n=''):
    """
    Creates a follicle/follicles on component selection. Should work for (vtx, edge and face)

    Args:
        selection: (str or pm.PyNode)   if None, this function will run on scene selection.
        n: (str)                        name of follicle. (default geo+'_fol')

    Returns:
        list of follicles
    """

    if selection:
        sel = selection
    else:
        sel = pm.selected()

    follicle_list = list()

    for s in pm.ls(sel):

        geo_shp = pn(s).node()
        geo = geo_shp.getParent()

        # convert selection to uvs
        pm.select(s, r=True)
        pm.mel.PolySelectConvert(4)

        # selections will convert to multiple uv pts, so average is needed.
        u_list = list()
        v_list = list()

        for i in pm.ls(sl=True, fl=True):
            u, v = pm.polyEditUV(i, q=True)
            u_list.append(u)
            v_list.append(v)

        u_val = sum(u_list) / len(u_list)
        v_val = sum(v_list) / len(v_list)

        if n:
            fol_name = n
        else:
            fol_name = geo+'_fol'

        fol = create(geo, n=fol_name, setUV=True, uvVal=(u_val, v_val))

        follicle_list.append(fol)

    if len(follicle_list) == 1:
        follicle_list = follicle_list[0]

    return follicle_list

def uv_from_position(position_list, geo):

    """

    Returns a list of the closest UV positions on mesh to given point positions

    :param position_list:        List of point positions [(x,y,z), (x,y,z) ...]
    :param geo:                  Mesh on which to find UV position

    :return:                     List of UV positions [(u,v), (u,v) ...]

    """

    eval_node = mc.createNode('closestPointOnMesh', n='uv_from_position_TEMP')
    mc.connectAttr(geo + '.outMesh', eval_node + '.inMesh', f=1)
    mc.connectAttr(geo + '.worldMatrix[0]', eval_node + '.inputMatrix', f=1)

    uv_list = []

    for position in position_list:

        #mc.spaceLocator(p=position)

        for value, attr in zip(position, ['X', 'Y', 'Z']):

            mc.setAttr(eval_node + '.inPosition' + attr, value)

        u = mc.getAttr(eval_node + '.result.parameterU')
        v = mc.getAttr(eval_node + '.result.parameterV')

        uv_list.append([u, v])

    mc.delete(eval_node)

    return uv_list
