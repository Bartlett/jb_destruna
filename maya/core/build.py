from jb_destruna.maya.core import storeFunctions
from jb_destruna.maya.core import tweak
from jb_destruna.maya.internal import follicle
from jb_destruna.maya.internal import meta
from jb_destruna.maya.internal import skin
from jb_destruna.maya.internal import skin_ng
from jb_destruna.maya.internal import wrap
from jb_destruna.maya.utility.misc import pn
import pymel.core as pm
import maya.cmds as mc

reload(skin)
reload(skin_ng)


def imports(data):

    import_data_list = check_data_type('imports', data)

    if import_data_list:

        # imports files from
        for data in import_data_list:
            file_path = data.get('filepath')
            group_name = data.get('group', '')
            namespace = data.get('namespace')

            # prep keyword arguments
            import_kwargs = {'i': True, 'dns': True}

            # add group if necessary
            if group_name:
                if pn(group_name):
                    grp = pn(group_name)
                else:
                    grp = pm.group(em=True, n=group_name)

                import_kwargs['gr'] = True
                import_kwargs['gn'] = 'temp_grp'

            # namespace shit
            if namespace:
                import_kwargs['dns'] = False
                import_kwargs['ns'] = namespace
                import_kwargs['i'] = True

            print '\timporting '+file_path, import_kwargs
            mc.file(file_path, **import_kwargs)

            if group_name:
                for s in pn('temp_grp').getChildren():
                    if s.getShape():
                        if s.getShape().type() == 'mesh':
                            pm.sets('initialShadingGroup', fe=s.getShape())

                # delete extras
                pm.mel.hyperShadePanelMenuCommand("hyperShadePanel1", "deleteUnusedNodes")

                pm.parent(pn('temp_grp').getChildren(), grp)
                pm.delete('temp_grp')


def groups(data):
    """
    Builds a bunch of groups from a data list.

    data: [{
        'g': (str)      group name
        'p': (str)      parent name
        'v': (str)      group visibility
    }]
    """

    group_data_list = check_data_type('groups', data=data)

    if group_data_list:

        # operate on data
        for data in group_data_list:
            group_name = data.get('g')
            parent_name = data.get('p')
            vis = data.get('v')

            if pn(group_name):
                grp = pn(group_name)
            else:
                grp = pm.group(em=True, n=group_name)

            if parent_name is not None:
                pm.parent(grp, parent_name)

            if vis is not None:
                grp.v.set(vis)


def skin_bind(data):
    """
    Builds skinClusters from saved weights file.

    data: [{
        'geo': (str)            geo name
        'filepath': (path)      path to weights file
        'import_module': (str)         skin imports mode options are ['skin', 'skin_ng'], (default='skin'
    }]
    """

    skin_data_list = check_data_type('skin_bind', data=data)

    # iterate through list.
    if skin_data_list:

        # iterate through list.
        for data in skin_data_list:

            # extract variables
            geo = data.get('geo')
            filepath = data.get('filepath')
            import_module = data.get('import_module', 'skin')

            print 'import_module', import_module
            # do skin bind.
            if import_module == 'skin':
                skin.import_from_file(geo=geo, filepath=filepath)
            elif import_module == 'skin_ng':
                skin_ng.import_from_file(geo=geo, filepath=filepath)
            else:
                raise Exception('skin_bind(): mode kwarg supplied is not supported.')


def skin_copy(data):
    """
    Copies skinweights between source and target geo.

    data: [{
        'source': (str)     skin copy source geo.
        'target': (str)     skin copy target geo.
        'surfaceAssociation': (str)   sample methods: ['closestPoint', 'rayCast', 'closestComponent']
    }]

    Returns:

    """
    copy_data_list = check_data_type('skin_copy', data=data)

    # if there is data
    if copy_data_list:

        # iterate through list.
        for data in copy_data_list:

            # extract variables
            source = data.get('source', None)
            target = data.get('target', None)
            surface_association = data.get('surfaceAssociation', None)

            # do skin copy
            skin.copy(source=source, target=target, surfaceAssociation=surface_association)


def proxy_setup(data):
    """
    This function takes all geo with tag attr (use meta.addTag to apply tag) and parent these geo to stored parent data
    (use storeFunctions.set_parentStore to store parent data).

    data: [{
        'tag': (str)    tag to pick proxies up from 'proxyGeo'
        'lockGeo': (bool)   if True, this function will lock proxies so they arent selectable.
    }]
    """

    proxy_data = check_data_type('proxy_setup', data)

    # if there is data
    if proxy_data:

        # iterate through list.
        for data in proxy_data:

            # extract variables
            tag = data.get('tag')
            lock_geo = data.get('lockGeo')

            proxy_geo_list = meta.getTagged(tag)
            storeFunctions.get_parentStore(proxy_geo_list)

            # lock geo
            if lock_geo:
                for geo in proxy_geo_list:
                    geo.getShape().overrideEnabled.set(True)
                    geo.getShape().overrideDisplayType.set(2)


def wrapDeformer_create(data):
    """
    This function creates wraps in a build context. see internal.wrap.create for better documentation.

    Args:
        data: [{
            'wrapped': ''
            'wrapper': ''
            'name': (str/None)
            'base': (str/None)
            'baseParent': (str/None)
            'wrapPose': (str/None)
            'wrapPoseParent': (str/None)
            'asBlendshape': (str/None)
            'blendshape_weights': (str/None)
        }]

    Returns:
        None
    """
    wrap_data = check_data_type('wrapDeformer_create', data)

    # if there is data
    if wrap_data:

        # iterate through list.
        for data in wrap_data:

            print data

            wrapped = data.get('wrapped')
            wrapper = data.get('wrapper')
            name = data.get('name', wrapped+'_wrap')
            base = data.get('base')
            baseParent = data.get('baseParent')
            wrapPose = data.get('wrapPose')
            wrapPoseParent = data.get('wrapPoseParent', None)
            asBlendshape = data.get('asBlendshape', False)
            blendshape_weights = data.get('blendshape_weights', None)

            print 'asBlendshape', asBlendshape

            # iterate through list.
            wrap.create(
                source=wrapped,
                target=wrapper,
                n=name,
                base=base,
                baseParent=baseParent,
                wrapPose=wrapPose,
                wrapPoseParent=wrapPoseParent,
                asBlendshape=asBlendshape,
                blendshape_weights=blendshape_weights
            )


def check_data_type(function_name='', data=list()):

    if isinstance(data, dict):
        data = [data]
        return data

    elif isinstance(data, list):
        return data
    else:
        pm.warning('groups: data provided is not a valid format. Must be either dict() or list().')
        return None


