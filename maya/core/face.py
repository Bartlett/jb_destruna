import maya.cmds as mc
import pymel.core as pm


class FaceRig(object):
    """
    This class is a central place to keep code that is copied from one face rig to another.
    Every function that can be put here, standardizes the process more.
    """

    ASSET_NAME = 'test_asset'
    RIG_TYPE = 'faceRig'

    TOP_GRP = str(RIG_TYPE)
    RIG_GRP = 'Rig'
    GEO_GRP = 'Geo'
    DO_NOT_TOUCH_GRP = 'DoNotTouch'

    BROW = 'Brow'
    EYE = 'Eye'
    LASH = 'Lash'

    BROW_STUFF = BROW + '_stuff'
    EYE_STUFF = EYE + '_stuff'
    LASH_STUFF = LASH + '_stuff'

    DELETE_GRP = 'delete_grp'
    DO_DELETE_GRP_DELETE = True

    REGION = 'Region'
    REGION_CTL_GRP = REGION+'_controls'
    REGION_JNT_GRP = REGION+'_joints'

    REGION_CTL_BROW_L = 'Region_CTL_brow_L'
    REGION_CTL_BROW_R = 'Region_CTL_brow_R'
    REGION_CTL_CHEEK_L = 'Region_CTL_cheek_L'
    REGION_CTL_CHEEK_R = 'Region_CTL_cheek_R'
    REGION_CTL_CHIN = 'Region_CTL_chin'
    REGION_CTL_EYE_L = 'Region_CTL_eye_L'
    REGION_CTL_EYE_R = 'Region_CTL_eye_R'
    REGION_CTL_MOUTH = 'Region_CTL_mouth'
    REGION_CTL_NOSE = 'Region_CTL_nose'

    def __init__(self):
        super(FaceRig, self).__init__()

        self.groups = [
            {'g': self.TOP_GRP, 'p': None, 'v': True},
            {'g': self.RIG_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.GEO_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.DO_NOT_TOUCH_GRP, 'p': self.TOP_GRP, 'v': False},
        ]

    def do_cleanup(self):
        """
        This function locks control vis attrs. and deletes the delete group.
        """
        # lock vis attr
        ctl_list = list(set([mc.listRelatives(c, p=True)[0] for c in mc.ls(type='nurbsCurve') if '_CTL_' in c]))
        for c in ctl_list:
            pm.PyNode(c).v.set(k=False, cb=False, l=True)

        # delete DELETE_GRP
        if self.DO_DELETE_GRP_DELETE:
            pm.delete(self.DELETE_GRP)

    def go(self):
        """
        This wrapper function runs the self.build_contents function and resets the scene to a new file.
        """
        print '='*80
        print 'Starting '+str(self.ASSET_NAME)+' Rig Build.'
        mc.file(new=True, f=True)
        self.build_contents()
        print 'Finished!'

    def build_contents(self):
        """
        This is where the main contents of the face build goes on.
        """
        pass

