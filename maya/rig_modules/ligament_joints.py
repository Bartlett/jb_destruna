from jb_destruna.maya.rig_modules import module_utility
from jb_destruna.maya.utility.misc import zero_grp as build_zg
from jb_destruna.maya.utility.misc import pn
from maya import cmds as mc
import pymel.core as pm


class Ligament(object):

    # Define standard rig module groups
    top_grp = None
    ctl_grp = None
    jnt_grp = None
    dnt_grp = True

    def __init__(self, ligament_name, bend_joint, center_offset, **kwargs):

        self.module_name = ligament_name

        self.bend_joint = bend_joint
        self.bend_joint_parent = mc.listRelatives(self.bend_joint, p=1)[0]

        self.center_offset = center_offset

        self.ligament_scale = kwargs.get('ligament_scale', [3, 0, 0])
        self.build_mirror_lig = kwargs.get('build_mirror', False)

    def build_ligament(self):

        self.create_rig_module_grps()

        if self.build_mirror_lig:
            side_list = ['_inner_', '_outer_']
            side_mult = [1, -1]
        else:
            side_list = ['']
            side_mult = [1]

        for side, side_mult in zip(side_list, side_mult):

            zero_grp, pos_reader, rot_reader = self.build_readers(side, side_mult)
            jnt_list, jnt_zero = self.build_joints(zero_grp, side)
            self.connect_vector_nodes(pos_reader, rot_reader, jnt_zero, side)

            mc.orientConstraint(self.bend_joint, jnt_list[1])
            mc.parentConstraint(self.bend_joint_parent, self.top_grp, mo=1)

    def build_readers(self, side, side_mult):

        # Create and position a zero group on bend joint
        reader_zero_grp = mc.group(n=self.module_name + side + 'reader_zero_grp', em=1, w=1)
        mc.setAttr(reader_zero_grp + '.visibility', 0)
        mc.delete(mc.parentConstraint(self.bend_joint, reader_zero_grp))
        mc.parent(reader_zero_grp, self.dnt_grp)

        # Apply center offset
        center_offset_side = [i * side_mult for i in self.center_offset]
        pm.move(reader_zero_grp, center_offset_side,  r=True, os=True)

        # Create guide locators
        position_reader_loc = self.module_name + side + '_pos_reader_loc'
        rotation_reader_loc = self.module_name + side +'_rot_reader_loc'

        for name in [position_reader_loc, rotation_reader_loc]:
            mc.spaceLocator(n=name)

        mc.parent(rotation_reader_loc, position_reader_loc)
        mc.delete(mc.parentConstraint(reader_zero_grp, position_reader_loc))
        mc.parent(position_reader_loc, reader_zero_grp)

        # offset rotation reader with ligament_scale
        pm.move(rotation_reader_loc, self.ligament_scale, r=True, os=True)

        # Constrain readers
        driver_a = self.bend_joint
        driver_b = self.bend_joint_parent

        for reader in [position_reader_loc, rotation_reader_loc]:
            mc.parentConstraint(driver_a, driver_b, reader, sr=['x', 'y', 'z'], mo=1)

        return reader_zero_grp, position_reader_loc, rotation_reader_loc

    def build_joints(self, zero_grp, side):

        vector_mult = lambda m, v: [i*m for i in v]
        j_name = self.module_name + side + '_lig{}_jnt'

        jnt_list = []
        mc.select(cl=1)
        for jnt_letter, mult in zip(['A', 'B', 'C'], [-1, 1, 1]):

            jnt = mc.joint(n=j_name.format(jnt_letter))
            jnt_list.append(jnt)
            pm.move(jnt, vector_mult(mult, self.ligament_scale), r=True, os=True)

        jnt_offset_grp = mc.group(em=1, w=1, n=self.module_name + side + '_jnt_offset_grp')
        mc.parent(jnt_list[0], jnt_offset_grp, a=1)
        mc.delete(mc.parentConstraint(zero_grp, jnt_offset_grp))
        build_zg(jnt_offset_grp)
        pm.parent(pn(jnt_offset_grp).getParent(), self.jnt_grp)

        return jnt_list, jnt_offset_grp

    def connect_vector_nodes(self, pos_reader, rot_reader, jnt_zero, side):

        pos_normal_vpr = mc.createNode('vectorProduct', n=self.module_name + side + '_positionNormal_VPR')
        normal_vpr = mc.createNode('vectorProduct', n=self.module_name + side + '_Normal_VPR')

        intersection_div = mc.createNode('multiplyDivide', n=self.module_name + side + '_intersection_div')
        mc.setAttr(intersection_div + '.operation', 2)
        distance_mult = mc.createNode('multiplyDivide', n=self.module_name + side + '_distance_mult')

        mc.connectAttr(pos_reader + '.translate', pos_normal_vpr + '.input1')
        mc.connectAttr(rot_reader + '.translate', pos_normal_vpr + '.input2')

        mc.setAttr(normal_vpr + '.input1X', 1)
        mc.connectAttr(rot_reader + '.translate', normal_vpr + '.input2')

        mc.connectAttr(pos_normal_vpr + '.output', intersection_div + '.input1')
        mc.connectAttr(normal_vpr + '.output', intersection_div + '.input2')

        mc.setAttr(distance_mult + '.input1X', 1)
        mc.connectAttr(intersection_div + '.output', distance_mult + '.input2')

        mc.connectAttr(distance_mult + '.output', jnt_zero + '.translate')


    def create_rig_module_grps(self):

        parent_group_dict = module_utility.create_module_groups(self.module_name, dnt=True)

        self.top_grp = parent_group_dict.get('top_grp', None)
        self.ctl_grp = parent_group_dict.get('ctl_grp', None)
        self.jnt_grp = parent_group_dict.get('jnt_grp', None)
        self.dnt_grp = parent_group_dict.get('dnt_grp', None)



"""
from jb_destruna.maya.rig_modules import ligament_joints as lig
reload(lig)

l_rig = lig.Ligament('Name', 'joint', 'offset')
"""