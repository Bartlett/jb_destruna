from maya import cmds as mc

def create_module_groups(namespace, **kwargs):

    """
    This method creates and parents the default groups required to house a rig module

    namespace :
        m_name = (string) The name space of the new module

    Kwargs:
       parent, p

        The node under which the groups will be parented
      doNotTouch, dnt  Create a local do not touch group

    :return:
    """

    if not isinstance(namespace, str):
        print('namespace arg passed as {}, must be str'.format(type(namespace)))
        raise TypeError

    m_parent = kwargs.get('p', None)
    do_dnt = kwargs.get('dnt', False)

    m_top_grp_name = namespace
    m_ctl_grp_name = namespace + '_control_grp'
    m_jnt_grp_name = namespace + '_joint_grp'
    m_dnt_grp_name = namespace + '_dnt_grp'

    m_top_grp = mc.group(em=True, n=m_top_grp_name, w=1)
    m_ctl_grp = mc.group(em=True, n=m_ctl_grp_name, p=m_top_grp)
    m_jnt_grp = mc.group(em=True, n=m_jnt_grp_name, p=m_top_grp)

    if do_dnt:
        m_dnt_grp = mc.group(em=True, n=m_dnt_grp_name, p=m_top_grp)
    else:
        m_dnt_grp = None

    if m_parent:
        mc.parent(m_top_grp, m_parent)

    group_dictionary = dict()

    group_dictionary['top_grp'] = m_top_grp
    group_dictionary['ctl_grp'] = m_ctl_grp
    group_dictionary['jnt_grp'] = m_jnt_grp
    group_dictionary['dnt_grp'] = m_dnt_grp

    return group_dictionary
