from jb_destruna.maya.utility import name
from jb_destruna.maya.rig_modules import module_utility
from jb_destruna.maya.internal import control
from maya import cmds as mc
import pymel.core as pm

chain_dict = {
    'chain_name': 'name',
    'sub_chain_name': 'name',
    'do_ctls': True,
    'parent': None,
    'guides': [1, 2, 3, 4],
    'ctl_shape': 'Box',
    'ctl_col': 17,
    'ctl_scale': [0, 0, 0],
    'ctl_offset': [0, 0, 0],
    #TODO ctl rotation
    'ctl_scale_range': [1, 5],
    'orient_ctl': True,
}

class FkChain(object):

    top_grp = None
    ctl_grp = None
    jnt_grp = None
    dnt_grp = True

    def __init__(self, **kwargs):



        self.module_name = kwargs.get('module_name', 'FkChain')
        self.chains_list = kwargs.get('chains', False)
        self.do_groups = kwargs.get('do_groups', True)
        self.dnt_grp = kwargs.get('do_dnt', False)

        self.orient_controls = kwargs.get

        self.parent_group_dict = None

        self.validate_module_name(self.module_name)
        #self.validate_chains(self.chains_list)


    def build(self):

        if self.do_groups:

            parent_group_dict = module_utility.create_module_groups(self.module_name, dnt=self.dnt_grp)

            self.top_grp = parent_group_dict.get('top_grp', None)
            self.ctl_grp = parent_group_dict.get('ctl_grp', None)
            self.jnt_grp = parent_group_dict.get('jnt_grp', None)
            self.dnt_grp = parent_group_dict.get('dnt_grp', None)


        self.build_chains_from_list(self.chains_list)

    def build_chains_from_list(self, chain_list):

        for i_chain_dict in chain_list:

            chain_name = i_chain_dict['chain_name']
            sub_chain_name = chain_dict.get('sub_chain_name', '')
            sub_chain_name = '_' + sub_chain_name if sub_chain_name else ''

            chain_ctl_grp_name = self.module_name + '_' + chain_name + '_CTLS_constrained'
            chain_ctl_grp = mc.group(n=chain_ctl_grp_name, em=1, w=1)

            if self.do_groups:
                mc.parent(chain_ctl_grp, self.ctl_grp)

            parent = i_chain_dict.get('parent', False)

            self.create_chain(i_chain_dict, chain_ctl_grp)

    def create_chain(self, chain_dict, chain_ctl_grp):

        chain_name = chain_dict.get('chain_name', 'chain')
        sub_chain_name = chain_dict.get('sub_chain_name', '')
        sub_chain_name = '_' + sub_chain_name if sub_chain_name else ''

        parent = chain_dict.get('parent', None)
        guides = chain_dict.get('guides', None)

        do_ctls = chain_dict.get('do_ctls', True)
        orient_ctl = chain_dict.get('orient_ctl', True)
        ctl_shape = chain_dict.get('ctl_shape', 'Box')
        ctl_col = chain_dict.get('ctl_col', 17)
        ctl_scale = chain_dict.get('ctl_scale', [0, 0, 0])

        ctl_offset = chain_dict.get('ctl_offset', [0, 0, 0])
        ctl_scale_range = chain_dict.get('ctl_scale_range', None)

        control_list = []
        joint_list = []


        for i, guide in enumerate(chain_dict['guides']):

            # DEFINE NAMES
            node_name = self.module_name + '_{}_' + chain_name + sub_chain_name + '_' + str(i)
            ctl_transform_name = node_name.format('CTL')
            jnt_transform_name = node_name.format('JNT')

            # CONTROLS
            # Position control transform
            ctl_transform = mc.group(em=1, w=1, n=ctl_transform_name)
            mc.delete(mc.parentConstraint(guide, ctl_transform))

            # Create control shapes
            ctl_shape = chain_dict.get('ctl_shape', 'box')
            ctl_col = chain_dict.get('ctl_col', 17)
            ctl_scale = chain_dict.get('ctl_scale', [1, 1, 1])
            ctl_offset = chain_dict.get('ctl_offset', None)
            ctl_scale_range = chain_dict.get('ctl_scale_range', None)

            chain_ctl = control.create(
                ctl_transform,
                shape=ctl_shape,
                size=ctl_scale,
                colorIdx=ctl_col,
                zeroGroup=True,
            )

            if ctl_offset:
                FkChain.offset_control_shape(chain_ctl, ctl_offset)

            if ctl_scale_range:
                chain_length = len(chain_dict['guides'])
                FkChain.scale_range_control_shape(chain_ctl, ctl_scale_range, chain_length, i)

            # Parent Controls
            if i == 0:
                pm.parent(chain_ctl.getParent(), chain_ctl_grp)
            else:
                pm.parent(chain_ctl.getParent(), control_list[i - 1])

            control_list.append(chain_ctl)


            # JOINTS
            # Position joint transform
            mc.select(cl=1)
            jnt = mc.joint(n=jnt_transform_name)
            mc.delete(mc.parentConstraint(guide, jnt))

            # Parent Joints
            if self.do_groups and i == 0:
                mc.parent(jnt, self.jnt_grp)
            elif i > 0:
                mc.parent(jnt, joint_list[i-1])

            joint_list.append(jnt)

            # Connect control to joint
            pm.parentConstraint(chain_ctl, jnt)
            pm.scaleConstraint(chain_ctl, jnt)

    def validate_module_name(self, m_name):

        correct_type = isinstance(m_name, str)

        if correct_type:
            return
        else:
            print('Module name is type {}, must be string'.format(type(m_name)))
            raise TypeError

    def validate_chains(self, chains):

        correct_type = False
        correct_leng = False
        correct_contents = False

        correct_type = isinstance(chains, list)

        if correct_type:
            correct_leng = True if len(chains) > 0 else False

        if correct_leng:

            correct_contents = 1

            for chain in chains:

                if isinstance(chain, dict):

                    name = 1 if 'chain_name' in chain.keys() else 0
                    guides = 1 if 'guides' in chain.keys() else 0

                    correct_contents *= name * guides

                else:

                    correct_contents *= 0

        if correct_type and correct_leng and correct_contents:
            return

    @staticmethod
    def offset_control_shape(shape, offset):
        mc.move(str(shape) + '.cv[:]', offset, r=True, os=True)

    @staticmethod
    def scale_range_control_shape(shape, scale_range, chain_length, chain_index):

        scale_list = list()

        min = float(scale_range[0])
        max = float(scale_range[1])

        delta = (max - min) / float(chain_length - 1)

        for index in range(chain_length):
            scale_list.append(min)
            min += delta

        scale = [scale_list[chain_index]]*3

        pm.scale(str(shape) + '.cv[:]', scale, r=True, os=True)













