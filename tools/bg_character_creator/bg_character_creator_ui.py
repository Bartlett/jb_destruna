import os
from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2 import QtGui
from maya import cmds as mc
from maya import mel
import pymel.core as pm

from .bg_character_local_func import imports
from .bg_character_local_func import pn

from jb_destruna.tools import bg_character_creator as bg
reload(bg)

from maya.app.general.mayaMixin import MayaQWidgetDockableMixin

BUILD = 16

class BG_Creator_UI(MayaQWidgetDockableMixin, QtWidgets.QDialog):

    transfer_set_directory_path = ''
    character_directory_path = ''

    rigging_directory = ''

    scene_geo = ''

    # Hand transfer
    hand_transfer_local_path = ''

    def __init__(self, parent=None):
        super(BG_Creator_UI, self).__init__(parent=parent)

        # WINDOW PROPERTIES
        self.setWindowTitle("BG Character Creator: V{}".format(BUILD))
        self.resize(500, 750)
        self.setMinimumWidth(327)

        # MASTER UI
        self.master_layout = QtWidgets.QHBoxLayout()
        #self.master_spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.setLayout(self.master_layout)


        self.action_segment = QtWidgets.QVBoxLayout()
        self.checkbox_segment = QtWidgets.QVBoxLayout()

        self.master_layout.addLayout(self.action_segment)
        self.master_layout.addLayout(self.checkbox_segment)

        self.build_action_segments(self.action_segment)

        # self.header_object = static_UI_object.HeaderWidget()
        # self.master_layout.addWidget(self.header_object)
        #
        # self.pageWidget = QtWidgets.QWidget()
        # self.pageWidget.setMinimumSize(300, 500)
        # # BUILD DYNAMIC UI
        # # Populate UI elements based on input (eg. QC Checks, Scene Itinerary items)
        # # self.populate()
        #
        # self.frame_layout = QtWidgets.QVBoxLayout()
        # self.frame_btn = QtWidgets.QPushButton("inside frame")
        #
        # self.frame = QtWidgets.QFrame()
        #
        # self.frame.setLayout(self.frame_layout)
        # #self.frame_layout.addWidget(self.pageWidget)
        #
        # self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        # self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        # self.frame.setMidLineWidth(3)
        #
        # self.mygroupbox = QtWidgets.QGroupBox('QC_TESTS')
        # self.scroll = QtWidgets.QScrollArea()
        # self.scroll.setWidget(self.mygroupbox)
        # self.scroll.setWidgetResizable(True)
        # self.scroll.setFixedHeight(400)
        # self.layout = QtWidgets.QVBoxLayout(self)
        #
        # self.frame_layout.addWidget(self.scroll)
        #
        # self.btn1 = QtWidgets.QPushButton('start')
        # self.master_layout.addWidget(self.btn1)
        #
        # self.master_layout.addWidget(self.frame)
        #
        # self.btn = QtWidgets.QPushButton('end')
        # self.master_layout.addWidget(self.btn)
        #
        #
        #
        # self.master_layout.addItem(self.master_spacer)

    # UI

    def build_action_segments(self, action_main_layout):

        # Create action group boxes
        directory_pathing_group_box = self.create_directory_pathing_group_box()
        rigging_directory_group_box = self.create_rigging_directory_group_box()
        geo_import_group_box = self.create_geo_import_group_box()
        hand_transfer_group_box = self.create_hand_transfer_group_box()
        skeleton_transfer_group_box = self.create_skeleton_transfer_group_box()

        tool_box_page_dict = {}
        tool_box_page_dict['Transfer Hand'] = hand_transfer_group_box
        tool_box_page_dict['Transfer Skeleton'] = skeleton_transfer_group_box

        toolbox = self.create_tool_box(tool_box_page_dict)

        #  Append action group boxes to layout
        for widget in [
                directory_pathing_group_box,
                rigging_directory_group_box,
                geo_import_group_box,
                #skeleton_transfer_group_box,
                toolbox,

        ]:

            action_main_layout.addWidget(widget)

        action_spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum,
                                                   QtWidgets.QSizePolicy.Expanding)
        action_main_layout.addItem(action_spacer)

    def create_directory_pathing_group_box(self):

        group_box_name = 'Source required folders'

        # Create layouts
        main_layouyt = QtWidgets.QVBoxLayout()

        transfer_path_row_layout = QtWidgets.QHBoxLayout()
        character_path_row_layout = QtWidgets.QHBoxLayout()

        # Create Widgets
        group_box_container = QtWidgets.QGroupBox(group_box_name)

        transfer_path_label = QtWidgets.QLabel('Transfer Kit Directory')
        self.transfer_path_line_edit = QtWidgets.QLineEdit()
        self.transfer_path_check = QtWidgets.QCheckBox()
        self.transfer_path_check.setDisabled(True)

        character_path_label = QtWidgets.QLabel('Character Directory')
        self.character_path_line_edit = QtWidgets.QLineEdit()
        self.character_path_check = QtWidgets.QCheckBox()
        self.character_path_check = QtWidgets.QCheckBox()
        self.character_path_check.setDisabled(True)

        # Attach widgets to group box
        group_box_container.setLayout(main_layouyt)

        main_layouyt.addWidget(transfer_path_label)
        main_layouyt.addLayout(transfer_path_row_layout)
        main_layouyt.addWidget(character_path_label)
        main_layouyt.addLayout(character_path_row_layout)


        for widget in [self.transfer_path_line_edit, self.transfer_path_check]:
            transfer_path_row_layout.addWidget(widget)


        for widget in [self.character_path_line_edit, self.character_path_check]:
            character_path_row_layout.addWidget(widget)

        # Connect widgets
        self.character_path_line_edit.textChanged.connect(self.character_path_changed)
        self.transfer_path_line_edit.textChanged.connect(self.transfer_path_changed)

        return group_box_container

    def create_rigging_directory_group_box(self):

        group_box_name = 'Create rigging directory'

        # Create layouts
        main_layouyt = QtWidgets.QVBoxLayout()
        status_layout = QtWidgets.QHBoxLayout()

        # Create Widgets
        group_box_container = QtWidgets.QGroupBox(group_box_name)

        status_title_label = QtWidgets.QLabel('Rig folder:')
        myFont = QtGui.QFont()
        myFont.setBold(True)
        self.rig_dir_status_label = QtWidgets.QLabel('Does Not exist')
        self.rig_dir_status_label.setFont(myFont)

        self.rig_dir_go_to_btn = QtWidgets.QPushButton('Go to Folder')
        self.rig_dir_go_to_btn.setEnabled(False)

        self.rig_dir_create_btn = QtWidgets.QPushButton('Create Rigging Directory')
        self.rig_dir_create_btn.setEnabled(False)

        # Attach widgets to group box
        group_box_container.setLayout(main_layouyt)

        main_layouyt.addLayout(status_layout)
        main_layouyt.addWidget(self.rig_dir_create_btn)

        for widget in [status_title_label, self.rig_dir_status_label, self.rig_dir_go_to_btn]:
            status_layout.addWidget(widget)

        # Connect widgets:
        self.rig_dir_go_to_btn.clicked.connect(self.go_to_directory_action)
        self.rig_dir_create_btn.clicked.connect(self.create_rigging_directory_action)
        return group_box_container

    def create_geo_import_group_box(self):

        group_box_name = 'Import chracter geo'

        # Create layouts
        main_layouyt = QtWidgets.QHBoxLayout()

        status_layout = QtWidgets.QHBoxLayout()

        main_btn_layout = QtWidgets.QVBoxLayout()
        row_btn_layout = QtWidgets.QHBoxLayout()

        # Create Widgets
        group_box_container = QtWidgets.QGroupBox(group_box_name)

        status_title_label = QtWidgets.QLabel('Body_geo:')
        myFont = QtGui.QFont()
        myFont.setBold(True)
        self.scene_geo_status_label = QtWidgets.QLabel('Not Loaded')
        self.scene_geo_status_label.setFont(myFont)
        self.load_geo_btn = QtWidgets.QPushButton('Load')
        self.load_all_btn = QtWidgets.QPushButton('Load all')
        self.delete_btn = QtWidgets.QPushButton('Delete')

        # Attach widgets to group box
        group_box_container.setLayout(main_layouyt)

        main_layouyt.addLayout(status_layout)
        main_layouyt.addLayout(main_btn_layout)

        for widget in [status_title_label, self.scene_geo_status_label]:
            status_layout.addWidget(widget)

        main_btn_layout.addWidget(self.load_geo_btn)
        main_btn_layout.addLayout(row_btn_layout)

        for widget in [self.load_all_btn, self.delete_btn]:
            row_btn_layout.addWidget(widget)

        # Connect widgets
        self.load_geo_btn.clicked.connect(self.load_body_geo)
        self.load_all_btn.clicked.connect(self.load_all_geo)
        self.delete_btn.clicked.connect(self.delete_scene_geo)
        return group_box_container

    def create_tool_box(self, page_dict):

        styleSheet = """
                                QToolBox::tab {
                                    border: 1px solid #C4C4C3;
                                    border-bottom-color: RGB(0, 0, 255);                            
                                }
                                QToolBox::tab:selected {
                                    background-color: #f14040;
                                    border-bottom-style: none;
                                }
                             """

        toolbox = QtWidgets.QToolBox()
        #toolbox.setStyle(styleSheet)

        for title, wid in page_dict.items():
            toolbox.addItem(wid, title)

        return toolbox

    def create_hand_transfer_group_box(self):

        group_box_name = 'Transfer Hand guides'

        # Create layouts
        widget_layout = QtWidgets.QVBoxLayout()

        main_layouyt = QtWidgets.QVBoxLayout()
        import_row_layout = QtWidgets.QHBoxLayout()

        toggle_box_main_laytout = QtWidgets.QVBoxLayout()
        toggle_row_layout = QtWidgets.QHBoxLayout()

        # Create Widgets
        widget_container = QtWidgets.QWidget()
        group_box_container = QtWidgets.QGroupBox()

        info_label = QtWidgets.QLabel('1) Make a local copy of the transfer set within the character directory\n'
                                      '2) Import and attach transfer set to body geo\n'
                                      '3) Align hands\n'
                                      '4) Export')
        info_label.setWordWrap(True)

        self.copy_transfer_set_local_btn = QtWidgets.QPushButton('Make local copy')
        self.copy_transfer_set_local_btn.setEnabled(False)

        self.import_transfer_set_btn = QtWidgets.QPushButton('Import hand transfer set')
        self.import_transfer_set_btn.setEnabled(False)

        self.import_line_edit = QtWidgets.QLineEdit()
        self.import_line_edit.setEnabled(False)

        toggle_group_box = QtWidgets.QGroupBox('')

        attach_detach_btn = QtWidgets.QPushButton('Attach transfer rig')

        toggle_radio_btn = QtWidgets.QRadioButton('Toggle Blendshape')
        toggle_slider = QtWidgets.QSlider()
        toggle_slider.setOrientation(QtCore.Qt.Horizontal)

        export_transfer_set_btn = QtWidgets.QPushButton('Export hand transfer set')
        export_guides_btn = QtWidgets.QPushButton('Export hand guides')

        # Attach widgets to group box
        group_box_container.setLayout(main_layouyt)
        toggle_group_box.setLayout(toggle_box_main_laytout)

        main_layouyt.addWidget(self.copy_transfer_set_local_btn)
        main_layouyt.addLayout(import_row_layout)
        main_layouyt.addWidget(toggle_group_box)
        main_layouyt.addWidget(export_transfer_set_btn)
        main_layouyt.addWidget(export_guides_btn)

        toggle_box_main_laytout.addWidget(attach_detach_btn)
        toggle_box_main_laytout.addLayout(toggle_row_layout)

        for widget in [self.import_transfer_set_btn, self.import_line_edit]:
            import_row_layout.addWidget(widget)

        for widget in [toggle_radio_btn, toggle_slider]:
            toggle_row_layout.addWidget(widget)

        widget_container.setLayout(widget_layout)
        widget_layout.addWidget(info_label)
        widget_layout.addWidget(group_box_container)

        return widget_container

    def create_skeleton_transfer_group_box(self):

        group_box_name = ''

        # Create layouts
        widget_layout = QtWidgets.QVBoxLayout()

        main_layouyt = QtWidgets.QVBoxLayout()

        import_divider_row_layout = QtWidgets.QHBoxLayout()
        import_btn_col_layout = QtWidgets.QVBoxLayout()
        import_line_edit_col_layout = QtWidgets.QVBoxLayout()

        # Create Widgets

        info_label = QtWidgets.QLabel('Info')

        widget_container = QtWidgets.QWidget()
        group_box_container = QtWidgets.QGroupBox(group_box_name)

        import_skeleton_guide_btn = QtWidgets.QPushButton('Import skeleton guide set')
        import_skeleton_guide_line_edit = QtWidgets.QLineEdit()

        import_hand_guide_btn = QtWidgets.QPushButton('Import hand guide set')
        import_hand_guide_line_edit = QtWidgets.QLineEdit()

        transfer_btn = QtWidgets.QPushButton('TRANSFER')

        export_transfer_result = QtWidgets.QPushButton('Export transfer result')

        # Attach widgets to group box
        group_box_container.setLayout(main_layouyt)

        main_layouyt.addLayout(import_divider_row_layout)
        import_divider_row_layout.addLayout(import_btn_col_layout)
        import_divider_row_layout.addLayout(import_line_edit_col_layout)

        for widget in [import_skeleton_guide_btn, import_hand_guide_btn]:
            import_btn_col_layout.addWidget(widget)

        for widget in [import_skeleton_guide_line_edit, import_hand_guide_line_edit]:
            import_line_edit_col_layout.addWidget(widget)

        for widget in [transfer_btn, export_transfer_result]:
            main_layouyt.addWidget(widget)

        widget_container.setLayout(widget_layout)
        widget_layout.addWidget(info_label)
        widget_layout.addWidget(group_box_container)

        return widget_container

    #################################################################

    # LOGIC

    def character_path_changed(self):

        character_path = format_windows_path(self.character_path_line_edit.text())

        if os.path.exists(character_path):

            self.character_path_check.setChecked(True)
            print(character_path)
            self.character_directory_path = character_path
            self.rig_dir_create_btn.setEnabled(True)

            self.find_rigging_directory()
        else:
            self.character_directory_path = ''
            self.character_path_check.setChecked(False)
            self.rig_dir_create_btn.setEnabled(False)

        self.refresh_ui()

    def transfer_path_changed(self):

        transfer_path = format_windows_path(self.transfer_path_line_edit.text())

        if os.path.exists(transfer_path):

            self.transfer_path_check.setChecked(True)
            print(transfer_path)
            self.transfer_set_directory_path = transfer_path
        else:
            self.transfer_set_directory_path = ''
            self.transfer_path_check.setChecked(False)

        self.refresh_ui()

    def refresh_ui(self):
        print('refresh')
        self.populate_rigging_directory_action()
        self.populate_geo_import_action()
        self.populate_hand_transfer_action()

    def populate_rigging_directory_action(self):

        rigging_directory = self.does_rigging_dir_exist()

        if rigging_directory:
            self.rig_dir_go_to_btn.setEnabled(True)
            self.rig_dir_create_btn.setEnabled(False)
            self.rig_dir_status_label.setText('Exists')

            self.rigging_directory = rigging_directory
        else:
            self.rig_dir_go_to_btn.setEnabled(False)
            #self.rig_dir_create_btn.setEnabled(True)
            self.rig_dir_status_label.setText('Does Not exist')

            self.rigging_directory = ''

    def populate_geo_import_action(self):

        if self.scene_geo:
            self.scene_geo_status_label.setText('LOADED')
        else:
            self.scene_geo_status_label.setText('Not Loaded')

    def populate_hand_transfer_action(self):

        if self.rigging_directory:
            self.copy_transfer_set_local_btn.setEnabled(True)
        else:
            self.copy_transfer_set_local_btn.setEnabled(False)

    def refresh_maya_scene(self):

        # If scene geo exists in ui, confirm it still exists in scene
        if self.scene_geo:

            scene_geo_found = self.find_scene_geo(self.scene_geo)

            if not scene_geo_found:
                self.scene_geo = ''

    @staticmethod
    def find_scene_geo(scene_geo):

        return_val = True

        for geo in scene_geo:
            return_val += pm.objExists(geo)

        return return_val


    def go_to_directory_action(self):

        if self.rigging_directory:
            os.startfile(self.rigging_directory)

    def create_rigging_directory_action(self):

        rig_path = os.path.join(self.character_directory_path, '02_rig')

        if not os.path.exists(rig_path):
            os.mkdir(os.path.join(self.character_directory_path, '02_rig'))

        folders = ['imports', 'elements', 'rigs']

        for folder in folders:
            os.mkdir(os.path.join(rig_path, folder))

        import_folders = ['asRig', 'skin_clusters', 'guides', 'low_geo']

        for folder in import_folders:
            top_path = os.path.join(rig_path, 'imports')
            os.mkdir(os.path.join(top_path, folder))

        element_folders = ['as_fitted_rig', 'skinning_WIPS', 'hand_guides', 'face_guides']

        for folder in element_folders:
            top_path = os.path.join(rig_path, 'elements')
            os.mkdir(os.path.join(top_path, folder))


        self.refresh_ui()

    def find_rigging_directory(self):

        rigging_directory = ''

        possible_rig_folder_names = ['rig', '02_rig']

        for folder_name in possible_rig_folder_names:

            search_directory = os.path.join(self.character_directory_path, folder_name)

            if os.path.exists(search_directory):
                rigging_directory = search_directory

        return rigging_directory

    def does_rigging_dir_exist(self):

        rigging_directory = self.find_rigging_directory()

        if rigging_directory:

            dir_contents = os.listdir(rigging_directory)

            if len(dir_contents) > 0:
                return rigging_directory

        return False

    def get_geo_path(self):

        geo_path = os.path.join(self.character_directory_path, '01_model')

        geo_file = ''

        if os.path.exists(geo_path):

            if len(os.listdir(geo_path)) > 0:

                get_ext = lambda f: os.path.splitext(f)[1]

                maya_files = [f for f in os.listdir(geo_path) if get_ext(f) == '.mb']

                geo_file = os.path.join(geo_path, (maya_files[-1]))

        return geo_file

    def load_body_geo(self):

        geo_filepath = self.get_geo_path()

        if geo_filepath:
            self.scene_geo = imports([{'filepath': geo_filepath, 'keep': ['body_geo']}])

            self.import_clean_up()

            self.refresh_maya_scene()
            self.refresh_ui()

    def load_all_geo(self):

        geo_filepath = self.get_geo_path()

        if geo_filepath:
            self.scene_geo = imports([{'filepath': geo_filepath}])

            self.import_clean_up()

            self.refresh_maya_scene()
            self.refresh_ui()

    def delete_scene_geo(self):
        print(self.scene_geo)

        for geo in self.scene_geo:

            try:
                pm.delete(geo)
                self.scene_geo = ''

            except pm.general.MayaNodeError:
                print('Cant find', geo.name(), 'deleting others')


        self.refresh_maya_scene()
        self.refresh_ui()

    def import_clean_up(self):
        mel.eval('hyperShadePanelMenuCommand("hyperShadePanel1", "deleteUnusedNodes");')

# TODO  : build class for all hand transfer controls
#class
def showUI():
    ui = BG_Creator_UI()
    ui.show(dockable=True, floating=True)
    return ui



def format_windows_path(path):

    path = r'{}'.format(path)
    path = path.replace('\\', '/').replace(' ', '').replace('\n', '')

    return path




"""
from jb_destruna.tools.bg_character_creator import bg_character_creator_ui as bg_ui
reload(bg_ui)



bg_ui.showUI()
"""
