import pymel.core as pm
import maya.cmds as mc



def pn(node):
    """
    test
    Args:
        node:

    Returns:

    """
    #return [node]
    if node:
        if pm.objExists(node):
            return pm.PyNode(node)
        else:
            return None
    else:
        return None

def imports(data):

    import_data_list = check_data_type('imports', data)

    if import_data_list:

        # imports files from
        for data in import_data_list:
            file_path = data.get('filepath')
            group_name = data.get('group')
            namespace = data.get('namespace')
            keep_geo = data.get('keep', '')

            # prep keyword arguments
            import_kwargs = {'i': True, 'dns': True}

            # add group if necessary
            if group_name:
                if pn(group_name):
                    grp = pn(group_name)
                else:
                    grp = pm.group(em=True, n=group_name)

                import_kwargs['gr'] = True
                import_kwargs['gn'] = 'temp_grp'

            # namespace
            if namespace:
                import_kwargs['dns'] = False
                import_kwargs['ns'] = namespace
                import_kwargs['i'] = True

            print('\timporting '+file_path, import_kwargs)
            mc.file(file_path, **import_kwargs)

            if keep_geo:
                top_grp = pn(keep_geo[0]).getParent()
                mc.parent(keep_geo, w=1)
                pm.delete(top_grp)

                return keep_geo

            top_grp = pn('body_geo').getParent()
            top_grp_children = top_grp.getChildren()
            return top_grp_children + [top_grp]
            # if keep_geo:
            #     print('TRIGGER')
            #     for geo in pn('temp_grp').getChildren():
            #         if geo not in keep_geo:
            #             pm.delete(geo)


def check_data_type(function_name='', data=list()):

    if isinstance(data, dict):
        data = [data]
        return data

    elif isinstance(data, list):
        return data
    else:
        pm.warning('groups: data provided is not a valid format. Must be either dict() or list().')
        return None



