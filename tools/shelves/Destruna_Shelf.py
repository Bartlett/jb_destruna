from maya import cmds as mc
import os

def _null(*args):
    pass

class Shelf(object):
    '''A simple class to build shelves in maya. Since the build method is empty,
    it should be extended by the derived class to build the necessary shelf elements.
    By default it creates an empty shelf called "customShelf".'''

    def __init__(self, name="test", icon_path=""):
        self.name = name

        self.icon_path = icon_path

        self.labelBackground = (0, 0, 0, 0)
        self.labelColour = (.9, .9, .9)

        self._cleanOldShelf()
        mc.setParent(self.name)
        self.build()

    def build(self):
        '''This method should be overwritten in derived classes to actually build the shelf
        elements. Otherwise, nothing is added to the shelf.'''
        pass

    def addButon(self, label, icon="commandButton.png", command=_null, doubleCommand=_null):
        '''Adds a shelf button with the specified label, command, double click command and image.'''
        mc.setParent(self.name)
        if icon:
            icon = self.icon_path + icon
        mc.shelfButton(width=37, height=37, image=icon, l=label, command=command, dcc=doubleCommand, imageOverlayLabel=label, olb=self.labelBackground, olc=self.labelColour)

    def addMenuItem(self, parent, label, command=_null, icon=""):
        '''Adds a shelf button with the specified label, command, double click command and image.'''
        if icon:
            icon = self.icon_path + icon
        return mc.menuItem(p=parent, l=label, c=command, i="")

    def addDividerMenuItem(self, parent, label):
        '''Adds a shelf button with the specified label, command, double click command and image.'''
        return mc.menuItem(p=parent, d=1, dl=label)

    def addSubMenu(self, parent, label, icon=None):
        '''Adds a sub menu item with the specified label and icon to the specified parent popup menu.'''
        if icon:
            icon = self.icon_path + icon
        return mc.menuItem(p=parent, l=label, i=icon, subMenu=1)

    def add_separator(self):
        mc.separator(p=self.name, width=12, height = 35, style='shelf', hr=False)

    def _cleanOldShelf(self):
        '''Checks if the shelf exists and empties it if it does or creates it if it does not.'''
        if mc.shelfLayout(self.name, ex=1):
            if mc.shelfLayout(self.name, q=1, ca=1):
                for each in mc.shelfLayout(self.name, q=1, ca=1):
                    mc.deleteUI(each)
        else:
            mc.shelfLayout(self.name, p="ShelfLayout")


# DESTRUNA CHARACTER SHELF
###################################################################################
SHELF_NAME = "DESTRUNA_CHARACTERS"
ICON_PATH = 'D:/_Active_Projects/Destruna/util/character_thumbnails/'


class CHARACTER_SHELF(Shelf):


    default_path = "D:/_Active_Projects/Destruna/masterAssets_forRef/characters/"

    # DON MOCAP
    prefix = 'don/3D/don_teen_mocap/'
    don_teen_mocap_model_folder = os.path.join(default_path, prefix + '01_model')
    don_teen_mocap_texture_folder = os.path.join(default_path, prefix + 'sourceimages')
    don_teen_mocap_rigs_folder = os.path.join(default_path, prefix + '02_rig/rigs')

    # DON CASUAL
    don_teen_casual_model_folder = os.path.join(default_path, 'don/3D/don_teen_casualClean/01_model')
    don_teen_casual_rigs_folder = os.path.join(default_path, 'don/3D/don_teen_casualClean/02_rig/elements')
    don_teen_casual_import_folder = os.path.join(default_path, 'don/3D/don_teen_casualClean/02_rig/imports')
    don_teen_casual_element_folder = os.path.join(default_path, 'don/3D/don_teen_casualClean/02_rig/elements')

    # DON CHILD
    don_child_model_folder = os.path.join(default_path, 'don/3D/don_child_casualClean/01_model')
    don_child_rigs_folder = os.path.join(default_path, 'don/3D/don_child_casualClean/02_rig/elements')
    don_child_import_folder = os.path.join(default_path, 'don/3D/don_child_casualClean/02_rig/imports')
    don_child_element_folder = os.path.join(default_path, 'don/3D/don_child_casualClean/02_rig/elements')

    # Kat teen Mocap
    kat_teen_mocap_model_folder = os.path.join(default_path, 'kat/3D/kat_teen_mocapTorn/01_model')
    #kat_teen_mocap_rigs_folder = os.path.join(default_path, 'kat/3D/kat_teen_mocapTorn/01_modelelements')
    #kat_teen_mocap_import_folder = os.path.join(default_path, 'don/3D/don_teen_mocap/02_rig/imports')
    #kat_teen_mocap_element_folder = os.path.join(default_path, 'don/3D/don_teen_mocap/02_rig/elements')

    # YONZARA CASUAL
    yonzara_casual_model_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_casual/01_model')
    yonzara_casual_rigs_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_casual/02_rig/rigs')
    yonzara_casual_import_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_casual/02_rig/imports')
    yonzara_casual_element_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_casual/02_rig/elements')


    # YONZARA CASUAL
    yonzara_AR_model_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_arSuit/01_model')
    yonzara_AR_rigs_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_arSuit/02_rig/rigs')
    yonzara_AR_import_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_arSuit/02_rig/imports')
    yonzara_AR_element_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_arSuit/02_rig/elements')

    # GUSTAVO
    gustavo_model_folder = os.path.join(default_path, '/gustavo/01_model')
    gustavo_rigs_folder = os.path.join(default_path, '/gustavo/02_rig/rigs')
    gustavo_import_folder = os.path.join(default_path, 'gustavo/02_rig/imports')
    gustavo_element_folder = os.path.join(default_path, 'gustavo/02_rig/elements')


    def __init__(self):
        super(CHARACTER_SHELF, self).__init__(name=SHELF_NAME, icon_path=ICON_PATH)

    def build(self):

        # DON
        self.addButon(label=" ", icon='Don/don_mocap.PNG')
        p = mc.popupMenu(b=1)
        self.addDividerMenuItem(p, 'Open Rig folder')
        self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.don_teen_mocap_model_folder))
        self.addMenuItem(p, "Open Imports Folder", command=self.open_folder_command(self.don_teen_mocap_import_folder))
        self.addMenuItem(p, "Open Elements Folder", command=self.open_folder_command(self.don_teen_mocap_element_folder))
        self.addMenuItem(p, "Open Rigs Folder", command=self.open_folder_command(self.don_teen_mocap_rigs_folder))

        # self.addButon(label=" ", icon='Don/don_casual.PNG')
        # p = mc.popupMenu(b=1)
        # self.addDividerMenuItem(p, 'Open Rig folder')
        # self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.don_teen_casual_model_folder))
        # self.addMenuItem(p, "Open Imports Folder", command=self.open_folder_command(self.don_teen_casual_import_folder))
        # self.addMenuItem(p, "Open Elements Folder", command=self.open_folder_command(self.don_teen_casual_element_folder))
        # self.addMenuItem(p, "Open Rigs Folder", command=self.open_folder_command(self.don_teen_casual_rigs_folder))
        #
        # self.addButon(label=" ", icon='Don/don_child.PNG')
        # p = mc.popupMenu(b=1)
        # self.addDividerMenuItem(p, 'Open Rig folder')
        # self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.don_child_model_folder))
        # self.addMenuItem(p, "Open Imports Folder", command=self.open_folder_command(self.don_child_import_folder))
        # self.addMenuItem(p, "Open Elements Folder", command=self.open_folder_command(self.don_child_element_folder))
        # self.addMenuItem(p, "Open Rigs Folder", command=self.open_folder_command(self.don_child_rigs_folder))
        #
        # self.add_separator()
        #
        # # Kat
        # self.addButon(label=" ", icon='Kat/Kat_teen_mocap.PNG')
        # p = mc.popupMenu(b=1)
        # self.addDividerMenuItem(p, 'Open Rig folder')
        # self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.kat_teen_mocap_model_folder))
        #
        # self.add_separator()
        #
        # self.addButon(label=" ", icon='Yon/yonzara_casual.PNG')
        # p = mc.popupMenu(b=1)
        # self.addDividerMenuItem(p, 'Open Rig folder')
        # self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.yonzara_casual_model_folder))
        # self.addMenuItem(p, "Open Imports Folder", command=self.open_folder_command(self.yonzara_casual_import_folder))
        # self.addMenuItem(p, "Open Elements Folder", command=self.open_folder_command(self.yonzara_casual_element_folder))
        # self.addMenuItem(p, "Open Rigs Folder", command=self.open_folder_command(self.yonzara_casual_rigs_folder))
        #
        # self.addButon(label=" ", icon='Yon/yonzara_AR.PNG')
        # p = mc.popupMenu(b=1)
        # self.addDividerMenuItem(p, 'Open Rig folder')
        # self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.yonzara_AR_model_folder))
        # self.addMenuItem(p, "Open Imports Folder", command=self.open_folder_command(self.yonzara_AR_import_folder))
        # self.addMenuItem(p, "Open Elements Folder", command=self.open_folder_command(self.yonzara_AR_element_folder))
        # self.addMenuItem(p, "Open Rigs Folder", command=self.open_folder_command(self.yonzara_AR_rigs_folder))
        #
        # self.add_separator()

        # self.addButon(label=" ", icon='Gus/gustavo.PNG')
        # p = mc.popupMenu(b=1)
        # self.addDividerMenuItem(p, 'Open Rig folder')
        # self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.gustavo_model_folder))
        # self.addMenuItem(p, "Open Imports Folder", command=self.open_folder_command(self.gustavo_import_folder))
        # self.addMenuItem(p, "Open Elements Folder", command=self.open_folder_command(self.gustavo_element_folder))
        # self.addMenuItem(p, "Open Rigs Folder", command=self.open_folder_command(self.gustavo_rigs_folder))





    def open_folder_command(self, folder_path):
        return "os.startfile('{}')".format(folder_path)

def build_DESTRUNA_CHARACTER_SHELF():
    CHARACTER_SHELF()
###################################################################################

# DESTRUNA RIGGING SHELF
###################################################################################
SHELF_NAME = "DESTRUNA_CHARACTERS"
ICON_PATH = 'D:/_Active_Projects/Destruna/util/character_thumbnails/'

class RIGGING_SHELF(Shelf):

    default_path = "D:/_Active_Projects/Destruna/masterAssets_forRef/characters/"

    # DON MOCAP
    don_teen_mocap_model_folder = os.path.join(default_path, 'don/3D/don_teen_mocap/01_model')
    don_teen_mocap_rigs_folder = os.path.join(default_path, 'don/3D/don_teen_mocap/02_rig/elements')
    don_teen_mocap_import_folder = os.path.join(default_path, 'don/3D/don_teen_mocap/02_rig/imports')
    don_teen_mocap_element_folder = os.path.join(default_path, 'don/3D/don_teen_mocap/02_rig/elements')
    don_teen_mocap_build = 'from jb_destruna.rigs.e_don import don_teen_mocap_animRig\nreload(don_teen_mocap_animRig)\nrig = don_teen_mocap_animRig.Rig()\nrig.go()'
    don_teen_mocap_import = 'from jb_destruna.rigs.e_don import don_teen_mocap_animRig\nreload(don_teen_mocap_animRig)\nrig = don_teen_mocap_animRig.Rig()\n'\
                            'from jb_destruna.maya.core import build\nreload(build)\nbuild.imports(rig.imports)'

    # DON CASUAL
    don_teen_casual_model_folder = os.path.join(default_path, 'don/3D/don_teen_casualClean/01_model')
    don_teen_casual_rigs_folder = os.path.join(default_path, 'don/3D/don_teen_casualClean/02_rig/elements')
    don_teen_casual_import_folder = os.path.join(default_path, 'don/3D/don_teen_casualClean/02_rig/imports')
    don_teen_casual_element_folder = os.path.join(default_path, 'don/3D/don_teen_casualClean/02_rig/elements')
    don_teen_casual_build = 'from jb_destruna.rigs.e_don import don_casual_animRig\nreload(don_casual_animRig)\nrig = don_casual_animRig.Rig()\nrig.go()'
    don_teen_casual_import = 'from jb_destruna.rigs.e_don import don_casual_animRig\nreload(don_casual_animRig)\nrig = don_casual_animRig.Rig()\n' \
                             'from jb_destruna.maya.core import build\nreload(build)\nbuild.imports(rig.imports)'

    # DON CHILD
    don_child_model_folder = os.path.join(default_path, 'don/3D/don_child_casualClean/01_model')
    don_child_rigs_folder = os.path.join(default_path, 'don/3D/don_child_casualClean/02_rig/elements')
    don_child_import_folder = os.path.join(default_path, 'don/3D/don_child_casualClean/02_rig/imports')
    don_child_element_folder = os.path.join(default_path, 'don/3D/don_child_casualClean/02_rig/elements')
    don_child_build = 'from jb_destruna.rigs.e_don import don_child_casualClean_animRig\nreload(don_child_casualClean_animRig)\nrig = don_child_casualClean_animRig.Rig()\nrig.go()'
    don_child_import = 'from jb_destruna.rigs.e_don import don_child_casualClean_animRig\nreload(don_child_casualClean_animRig)\nrig = don_child_casualClean_animRig.Rig()\n' \
                       'from jb_destruna.maya.core import build\nreload(build)\nbuild.imports(rig.imports)'

    # Kat teen Mocap
    kat_teen_mocap_model_folder = os.path.join(default_path, 'kat/3D/kat_teen_mocapTorn/01_model')
    #kat_teen_mocap_rigs_folder = os.path.join(default_path, 'kat/3D/kat_teen_mocapTorn/01_modelelements')
    #kat_teen_mocap_import_folder = os.path.join(default_path, 'don/3D/don_teen_mocap/02_rig/imports')
    #kat_teen_mocap_element_folder = os.path.join(default_path, 'don/3D/don_teen_mocap/02_rig/elements')
    kat_teen_build = 'from jb_destruna.rigs.b_kat import kat_teen_mocapTorn_animRig\nreload(kat_teen_mocapTorn_animRig)\nrig = kat_teen_mocapTorn_animRig.Rig()\nrig.go()'
    kat_teen_import = 'from jb_destruna.rigs.b_kat import kat_teen_mocapTorn_animRig\nreload(kat_teen_mocapTorn_animRig)\nrig = kat_teen_mocapTorn_animRig.Rig()\n' \
                       'from jb_destruna.maya.core import build\nreload(build)\nbuild.imports(rig.imports)'

    # YONZARA CASUAL
    yonzara_casual_model_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_casual/01_model')
    yonzara_casual_rigs_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_casual/02_rig/rigs')
    yonzara_casual_import_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_casual/02_rig/imports')
    yonzara_casual_element_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_casual/02_rig/elements')
    yonzara_casual_build = 'from jb_destruna.rigs.yonzara import yonzara_casual_animRig\nreload(yonzara_casual_animRig)\nrig = yonzara_casual_animRig.Rig()\nrig.go()'
    yonzara_casual_import = 'from jb_destruna.rigs.yonzara import yonzara_casual_animRig\nreload(yonzara_casual_animRig)\nrig = yonzara_casual_animRig.Rig()\n' \
                       'from jb_destruna.maya.core import build\nreload(build)\nbuild.imports(rig.imports)'

    # YONZARA CASUAL
    yonzara_AR_model_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_arSuit/01_model')
    yonzara_AR_rigs_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_arSuit/02_rig/rigs')
    yonzara_AR_import_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_arSuit/02_rig/imports')
    yonzara_AR_element_folder = os.path.join(default_path, 'Yonzara/3D/yonzara_arSuit/02_rig/elements')
    yonzara_AR_build = 'from jb_destruna.rigs.yonzara import yonzara_arSuit_animRig\nreload(yonzara_arSuit_animRig)\nrig = yonzara_arSuit_animRig.Rig()\nrig.go()'
    yonzara_AR_import = 'from jb_destruna.rigs.yonzara import yonzara_arSuit_animRig\nreload(yonzara_arSuit_animRig)\nrig = yonzara_arSuit_animRig.Rig()\n' \
                            'from jb_destruna.maya.core import build\nreload(build)\nbuild.imports(rig.imports)'

    # GUSTAVO
    gustavo_model_folder = os.path.join(default_path, '/gustavo/01_model')
    gustavo_rigs_folder = os.path.join(default_path, '/gustavo/02_rig/rigs')
    gustavo_import_folder = os.path.join(default_path, 'gustavo/02_rig/imports')
    gustavo_element_folder = os.path.join(default_path, 'gustavo/02_rig/elements')
    gustavo_build = 'from jb_destruna.rigs.h_gustavo import gustavo_animRig\nreload(gustavo_animRig)\nrig = gustavo_animRig.Rig()\nrig.go()'
    gustavo_import = 'from jb_destruna.rigs.rigs import gustavo_animRig\nreload(gustavo_animRig)\nrig = gustavo_animRig.Rig()\n' \
                        'from jb_destruna.maya.core import build\nreload(build)\nbuild.imports(rig.imports)'

    """
    #      GUSTAVO      # 

    from jb_destruna.rigs.h_gustavo import gustavo_animRig
    reload(gustavo_animRig)
    rig = gustavo_animRig.Rig()
    rig.go()
    """

    def __init__(self):
        super(CHARACTER_SHELF, self).__init__(name=SHELF_NAME, icon_path=ICON_PATH)

    def build(self):

        # DON
        self.addButon(label=" ", icon='Don/don_mocap.PNG')
        p = mc.popupMenu(b=1)
        self.addDividerMenuItem(p, 'Run Build Script')
        self.addMenuItem(p, "Build Don Teen Mocap", command=self.don_teen_mocap_build)
        self.addMenuItem(p, "Import only", command=self.don_teen_mocap_import)
        self.addDividerMenuItem(p, 'Open Rig folder')
        self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.don_teen_mocap_model_folder))
        self.addMenuItem(p, "Open Imports Folder", command=self.open_folder_command(self.don_teen_mocap_import_folder))
        self.addMenuItem(p, "Open Elements Folder", command=self.open_folder_command(self.don_teen_mocap_element_folder))
        self.addMenuItem(p, "Open Rigs Folder", command=self.open_folder_command(self.don_teen_mocap_rigs_folder))

        self.addButon(label=" ", icon='Don/don_casual.PNG')
        p = mc.popupMenu(b=1)
        self.addDividerMenuItem(p, 'Run Build Script')
        self.addMenuItem(p, "Build Don Teen Casual", command=self.don_teen_casual_build)
        self.addMenuItem(p, "Import only", command=self.don_teen_casual_import)
        self.addDividerMenuItem(p, 'Open Rig folder')
        self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.don_teen_casual_model_folder))
        self.addMenuItem(p, "Open Imports Folder", command=self.open_folder_command(self.don_teen_casual_import_folder))
        self.addMenuItem(p, "Open Elements Folder", command=self.open_folder_command(self.don_teen_casual_element_folder))
        self.addMenuItem(p, "Open Rigs Folder", command=self.open_folder_command(self.don_teen_casual_rigs_folder))

        self.addButon(label=" ", icon='Don/don_child.PNG')
        p = mc.popupMenu(b=1)
        self.addDividerMenuItem(p, 'Run Build Script')
        self.addMenuItem(p, "Build Don child", command=self.don_child_build)
        self.addMenuItem(p, "Import only", command=self.don_child_import)
        self.addDividerMenuItem(p, 'Open Rig folder')
        self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.don_child_model_folder))
        self.addMenuItem(p, "Open Imports Folder", command=self.open_folder_command(self.don_child_import_folder))
        self.addMenuItem(p, "Open Elements Folder", command=self.open_folder_command(self.don_child_element_folder))
        self.addMenuItem(p, "Open Rigs Folder", command=self.open_folder_command(self.don_child_rigs_folder))

        self.add_separator()

        # Kat
        self.addButon(label=" ", icon='Kat/Kat_teen_mocap.PNG')
        p = mc.popupMenu(b=1)
        self.addDividerMenuItem(p, 'Run Build Script')
        self.addMenuItem(p, "Build Kat teen", command=self.kat_teen_build)
        self.addMenuItem(p, "Import only", command=self.kat_teen_import)
        self.addDividerMenuItem(p, 'Open Rig folder')
        self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.kat_teen_mocap_model_folder))

        self.add_separator()

        self.addButon(label=" ", icon='Yon/yonzara_casual.PNG')
        p = mc.popupMenu(b=1)
        self.addDividerMenuItem(p, 'Run Build Script')
        self.addMenuItem(p, "Build Yonzara Casual", command=self.don_child_build)
        self.addMenuItem(p, "Import only", command=self.don_child_import)
        self.addDividerMenuItem(p, 'Open Rig folder')
        self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.yonzara_casual_model_folder))
        self.addMenuItem(p, "Open Imports Folder", command=self.open_folder_command(self.yonzara_casual_import_folder))
        self.addMenuItem(p, "Open Elements Folder", command=self.open_folder_command(self.yonzara_casual_element_folder))
        self.addMenuItem(p, "Open Rigs Folder", command=self.open_folder_command(self.yonzara_casual_rigs_folder))

        self.addButon(label=" ", icon='Yon/yonzara_AR.PNG')
        p = mc.popupMenu(b=1)
        self.addDividerMenuItem(p, 'Run Build Script')
        self.addMenuItem(p, "Build Yonzara AR", command=self.don_child_build)
        self.addMenuItem(p, "Import only", command=self.don_child_import)
        self.addDividerMenuItem(p, 'Open Rig folder')
        self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.yonzara_AR_model_folder))
        self.addMenuItem(p, "Open Imports Folder", command=self.open_folder_command(self.yonzara_AR_import_folder))
        self.addMenuItem(p, "Open Elements Folder", command=self.open_folder_command(self.yonzara_AR_element_folder))
        self.addMenuItem(p, "Open Rigs Folder", command=self.open_folder_command(self.yonzara_AR_rigs_folder))

        self.add_separator()

        self.addButon(label=" ", icon='Gus/gustavo.PNG')
        p = mc.popupMenu(b=1)
        self.addDividerMenuItem(p, 'Run Build Script')
        self.addMenuItem(p, "Build Gustavo", command=self.gustavo_build)
        self.addMenuItem(p, "Import only", command=self.gustavo_import)
        self.addDividerMenuItem(p, 'Open Rig folder')
        self.addMenuItem(p, "Open Geo Folder", command=self.open_folder_command(self.gustavo_model_folder))
        self.addMenuItem(p, "Open Imports Folder", command=self.open_folder_command(self.gustavo_import_folder))
        self.addMenuItem(p, "Open Elements Folder", command=self.open_folder_command(self.gustavo_element_folder))
        self.addMenuItem(p, "Open Rigs Folder", command=self.open_folder_command(self.gustavo_rigs_folder))





    def open_folder_command(self, folder_path):
        return "os.startfile('{}')".format(folder_path)

def build_DESTRUNA_CHARACTERS():
    CHARACTER_SHELF()
###################################################################################

