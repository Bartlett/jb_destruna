from maya import cmds as mc
from jb_destruna.maya.internal import follicle
from jb_destruna.maya.internal import wrap
from jb_destruna.maya.internal import mesh
from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.internal import cpom
import math

def transfer_skeleton(geo_source, geo_target, root_joint='Root'):

    # Set up
    transfer_locators, transfer_attr = create_transfer_set_up(geo_source, geo_target, root_joint)

    # Transfer
    mc.setAttr(transfer_attr, 1)

    # Clean up result
    clean_locators = clean_transfer_result(transfer_locators)

    # Set
    snap_skeleton_to_locators(root_joint, clean_locators)


def create_transfer_set_up(geo_source, geo_target, root_joint):

    # Create transfer mesh and transfer blendeshape, return mesh and transfer attribute
    t_mesh, t_attr = create_transfer_mesh(geo_source, geo_target)

    # Encode joint translation onto locators
    position_locs = create_position_locators(root_joint)

    # Separate symmetrical and asymmetrical locators (eg. chest is symmetrical , hand is asymmetrical)
    sym_locs, asym_locs = sort_symmetrical_locators(position_locs)

    wrap_driven_list = []  # A list that will contain locators driven by the follicle wraps
    top_grp_list = []  # A list that will contain each top group of the wrap set ups

    # Wrap asymmetrical locators with exclusive bind, symmetrical without, I've found these settings yield better result
    for wrap_locs, n, eb in zip([asym_locs, sym_locs], ['asymmetricalWrap', 'symmetricalWrap', ], [True, False]):
        top_grp, wrap_driven = wrap_transform(wrap_locs, t_mesh, name=n, exclusive_bind=eb)

        top_grp_list.append(top_grp)
        wrap_driven_list += wrap_driven

    mc.group(top_grp_list, n='Wrap_sets')

    return wrap_driven_list, t_attr

def clean_transfer_result(transfer_locators):

    # Create copy of wrap locators for cleaning
    c_locs = create_clean_locators(transfer_locators)

    # Sort into groups
    sorted_c_locs = sort_clean_locators(c_locs)
    spine_locators = sorted_c_locs['spine']
    clav_locators = sorted_c_locs['clav']
    arm_locators = sorted_c_locs['arm']
    leg_locators = sorted_c_locs['leg']

    # Force symmertry
    align_spine_locators(spine_locators)

    # Snap limbs to 2D plane
    align_leg_locators(leg_locators)
    align_arm_locators(arm_locators)

    return c_locs

def sort_clean_locators(c_locs):

    spine_locators_tag = ['Neck', 'Chest', 'Spine1', 'Root', 'HeadEnd', 'JawEnd', 'Jaw', 'Head']
    clav_locators_tag = ['Scapula', 'Shoulder']
    arm_locators_tag = ['Shoulder', 'Elbow', 'Wrist']
    leg_locators_tag = ['Hip', 'Knee', 'Ankle']

    spine_locators = [str(i) for i in c_locs if i.replace('__Clean_loc', '') in spine_locators_tag]
    clav_locators = [str(i) for i in c_locs if i.replace('__Clean_loc', '') in clav_locators_tag]
    arm_locators = [str(i) for i in c_locs if i.replace('__Clean_loc', '') in arm_locators_tag]
    leg_locators = [str(i) for i in c_locs if i.replace('__Clean_loc', '') in leg_locators_tag]

    return_dict = dict()

    return_dict['spine'] = spine_locators
    return_dict['clav'] = clav_locators
    return_dict['arm'] = arm_locators
    return_dict['leg'] = leg_locators

    return return_dict

def create_transfer_mesh(source_geo, target_geo):

    transfer_mesh = mesh.copy(source_geo, n='TEMP_TRANSFER_GEO')

    transfer_blendshape = 'transfer_blendshape'
    blendshape.apply(target_geo, transfer_mesh, n=transfer_blendshape, dv=1)

    transfer_attr = transfer_blendshape + '.envelope'

    mc.setAttr(transfer_attr, 0)

    return transfer_mesh, transfer_attr

def wrap_transform(sel, driver_geo, name='wrap_transform', exclusive_bind=True):

    wrap_geo = name + '_wrap_geo'

    mesh_grp = []

    # Create Wrap Geo
    for target in sel:
        # target = 'Hand_IndexFinger2_joint_L'

        p_scale = 0.5
        p = mc.polyPlane()[0]
        mesh_grp.append(str(p))
        p_shape = mc.listRelatives(p, c=1)[0]
        node = mc.listConnections(p_shape, type='polyPlane')[0]
        mc.setAttr(node + '.subdivisionsWidth', 1)
        mc.setAttr(node + '.subdivisionsHeight', 1)
        mc.setAttr(node + '.height', p_scale)
        mc.setAttr(node + '.width', p_scale)
        mc.delete(mc.parentConstraint(target, p))

    mc.polyUnite(*mesh_grp, ch=0, mergeUVSets=1, centerPivot=1, n=wrap_geo)
    mc.polyAutoProjection(wrap_geo, l=2, sc=1, o=1, p=6, lm=0, ps=0.5, ch=0)

    # Attach follicles
    position_list = [mc.xform(t, ws=1, q=1, t=1) for t in sel]
    uv_pos_list = follicle.uv_from_position(position_list, wrap_geo)

    follicles_list = list()
    for loc, uv_val in zip(sel, uv_pos_list):
        follicle_name = name + '_' + loc + '_fol'
        f = follicle.create(wrap_geo, n=follicle_name, setUV=True, uvVal=uv_val)
        follicles_list.append(f.name())

    fol_grp = mc.group(follicles_list, n=name + 'follices_grp')
    mc.setAttr(fol_grp + '.visibility', 0)

    # Create driven locator
    wrapDriven_list = list()
    for node, fol in zip(sel, follicles_list):
        node = node.replace('__positionLocator', '')
        loc_n = name + '_' + node + '_wrapDriven'
        loc = mc.spaceLocator(n=loc_n)[0]
        wrapDriven_list.append(loc)
        mc.delete(mc.parentConstraint(node, loc))
        mc.parentConstraint(fol, loc, mo=1)
        mc.setAttr(loc + ".overrideEnabled", 1)
        mc.setAttr(loc + ".overrideColor", 17)

    wrapDriven_grp = mc.group(wrapDriven_list, n=name + '_wrapDriven_grp')

    top_grp = mc.group(wrapDriven_grp, fol_grp, wrap_geo, n=name + '_grp')

    # Create wrap
    wrap_name = name + '_wrap'
    wrap.create(wrap_geo, driver_geo, n=wrap_name, baseParent=top_grp)
    eb = 1 if exclusive_bind else 0
    mc.setAttr(wrap_name + '.exclusiveBind', eb)

    return top_grp, wrapDriven_list

def sort_symmetrical_locators(locators):
    # Split symmetrical and asymmetrical locators for separate wraps

    sym_prefixes = ['Root', 'Spine1', 'Chest', 'Neck', 'Head', 'Jaw', 'JawEnd', 'HeadEnd']
    sym_locs_names = [i + '__positionLocator' for i in sym_prefixes]

    sym_p_locs = [i for i in locators if i in sym_locs_names]
    asym_p_locs = [i for i in locators if i not in sym_p_locs]

    return sym_p_locs, asym_p_locs

def create_position_locators(root_joint):

    mc.select(cl=1)
    mc.select(root_joint, hi=1)

    loc_list = create_locators(mc.ls(sl=1), suffix='__positionLocator')
    mc.group(loc_list, n="Position_Locators")

    return loc_list

def create_clean_locators(transfer_locators):

    old_name_loc_list = create_locators(transfer_locators, '__Clean_loc')
    new_name_loc_list = []

    for loc in old_name_loc_list:
        new_name = loc.replace('asymmetricalWrap_', '')
        new_name = new_name.replace('symmetricalWrap_', '')
        new_name = new_name.replace('_wrapDriven', '')

        mc.rename(loc, new_name)
        new_name_loc_list.append(new_name)

    mc.group(new_name_loc_list, n='Clean_Locators')

    return new_name_loc_list

def align_spine_locators(spine_locators):

    # Fix ZY plane symmetry
    for loc in spine_locators:
        mc.setAttr(loc + '.translateX', 0)


    # Aim Locators at each other

    # Create up object
    up_target = mc.spaceLocator(n='UP TARGET')[0]
    mc.move(50, 10, 0, up_target, a=1)

    get_loc = lambda tag: [i for i in spine_locators if tag in i][0]

    root = get_loc('Root')
    spine = get_loc('Spine1')
    chest = get_loc('Chest')
    neck = get_loc('Neck')
    head = get_loc('Head')
    head_end = get_loc('HeadEnd')

    forward_vector_list = [spine, chest, neck, head, head_end ]
    aim_driven_list = [root, spine, chest, neck, head]

    for forward_vector, aim_driven in zip(forward_vector_list, aim_driven_list):

        mc.delete(mc.aimConstraint(
            forward_vector,
            aim_driven,
            aimVector=[1, 0, 0],
            upVector=[0, 1, 0],
            worldUpType='object',
            worldUpObject=up_target))

def align_leg_locators(leg_locators):

    get_loc = lambda tag: [i for i in leg_locators if tag in i][0]

    hip = get_loc('Hip')
    knee = get_loc('Knee')
    ankle = get_loc('Ankle')

    axis_reference = 'Knee'

    align_limb(hip, knee, ankle, axis_reference)

def align_arm_locators(arm_locators):

    get_loc = lambda tag: [i for i in arm_locators if tag in i][0]

    SHOULDER = get_loc('Shoulder')
    KNEE = get_loc('Elbow')
    ANKLE = get_loc('Wrist')

    axis_reference = 'Elbow'

    align_limb(SHOULDER, KNEE, ANKLE, axis_reference)

def create_locators(node_list, suffix):

    loc_list = list()

    for i in node_list:
        loc_n = i + suffix
        loc = mc.spaceLocator(n=loc_n)[0]
        mc.delete(mc.parentConstraint(i, loc))
        loc_list.append(loc)

    return loc_list

def align_limb(root_node, mid_node, end_node, axis_reference):

    # Build up target
    up_target = mc.spaceLocator(n='plane_axis')[0]
    mc.delete(mc.parentConstraint(axis_reference, up_target))
    mc.delete(mc.pointConstraint(mid_node, up_target))
    mc.move(0, 50, 0, up_target, r=1, os=1)

    # Align mid node, project the mid nodes position onto a 2D plane that runs between root and end, aimed at up target
    alignment_plane_geo = build_alignment_plane(root_node, end_node, up_target)
    new_mid_pos = cpom.closest_point_on_geo_from_transform(mid_node, alignment_plane_geo)

    mc.move(new_mid_pos[0], new_mid_pos[1], new_mid_pos[2], mid_node, ws=1, a=1)

    mc.delete(mc.aimConstraint(
        mid_node,
        root_node,
        aimVector=[1, 0, 0],
        upVector=[0, 1, 0],
        worldUpType='object',
        worldUpObject=up_target))

    mc.delete(mc.aimConstraint(
        end_node,
        mid_node,
        aimVector=[1, 0, 0],
        upVector=[0, 1, 0],
        worldUpType='object',
        worldUpObject=up_target))

    mc.delete(alignment_plane_geo)
    mc.delete(up_target)

def build_alignment_plane(root_node, end_node, axis_loc):

    p1 = mc.xform(root_node, t=1, q=1, ws=1)
    p2 = mc.xform(end_node, t=1, q=1, ws=1)

    plane_length = get_euclidean_dist(p1, p2)

    alignment_plane_geo = mc.polyPlane(w=plane_length, h=plane_length, ch=0)[0]
    mc.move(0, 0, plane_length / 2, alignment_plane_geo + '.vtx[:]', r=1, os=1)

    mc.delete(mc.pointConstraint(root_node, alignment_plane_geo))

    mc.aimConstraint(
        end_node,
        alignment_plane_geo,
        aimVector=[0, 0, 1],
        upVector=[1, 0, 0],
        worldUpType='object',
        worldUpObject=axis_loc)

    return alignment_plane_geo

def snap_skeleton_to_locators(root_joint, clean_locators):

    mc.select(cl=1)
    mc.select(root_joint, hi=1)
    skel_joints = mc.ls(sl=1)

    for joint in skel_joints:
        mc.getAttr('JawEnd.translateX', settable=1)



def get_euclidean_dist(position_a, position_b):

    delta_x, delta_y, delta_z = [x - y for (x, y) in zip(position_a, position_b)]

    euclidean_dist = math.sqrt(delta_x ** 2 + delta_y ** 2 + delta_z ** 2)

    return euclidean_dist




