from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import control
from jb_destruna.maya.internal import paths
from jb_destruna.maya.internal import motionpath
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility import misc
from jb_destruna.maya.utility.misc import pn
from pymel.core import datatypes as dt
# from jb_destruna.tools import path as path_tools
import maya.cmds as mc
import pymel.core as pm

reload(paths)
reload(motionpath)
reload(misc)

class HairRig(rig.Rig):
    """
    This is class is for building hair rigs in a generic enough form for wide reuse.
    """

    PREFIX = 'Hair'

    TOP_GRP = str(PREFIX)
    CTL_GRP = PREFIX + '_controls'
    CNX_GRP = PREFIX + '_connectors'
    JNT_GRP = PREFIX + '_joints'
    DNT_GRP = PREFIX + '_doNotTouch'

    HEAD_JOINT = 'Head_Head_joint_M'

    FK_CTL_SHAPE = 'square'
    FK_CTL_COLOR = 18
    FK_CTL_TRANSLATE_OFFSET = [0, 0, 0]
    FK_CTL_ROTATE_OFFSET = [0, 0, 0]
    FK_CTL_SIZE_OFFSET = [1, 1, 1]

    IK_CTL_SHAPE = 'circle'
    IK_CTL_COLOR = 20
    IK_CTL_TRANSLATE_OFFSET = [0, 0, 0]
    IK_CTL_ROTATE_OFFSET = [0, 0, 0]
    IK_CTL_SIZE_OFFSET = [.8, .8, .8]

    SETTINGS_CTL_SHAPE = 'ball'
    SETTINGS_CTL_COLOR = 17
    SETTINGS_CTL_TRANSLATE_OFFSET = [0, 4, 0]
    SETTINGS_CTL_ROTATE_OFFSET = [0, 0, 0]
    SETTINGS_CTL_SIZE_OFFSET = [.2, .2, .2]

    def __init__(self):
        super(HairRig, self).__init__()

        self.build_data = [
            {
                'name': name,
                'base_crv': name + '_crv',
                'up_crv': name + '_up',
                'ctl_size': 2,
                'ctl_count': 5,
                'joint_count': 7,
            } for name in [
                'Hair_cheek_L',
                'Hair_cheek_R',
                'Hair_fringeA_R',
                'Hair_fringeB_R',
                'Hair_fringeC_R',
                'Hair_fringeD_R',
                'Hair_fringe_L',
                'Hair_sideA_L',
                'Hair_sideA_R',
                'Hair_sideB_L',
                'Hair_sideB_R',
                'Hair_sideC_R',
                'Hair_tailA',
                'Hair_tailB',
                'Hair_tailC',
                'Hair_tailD',
                'Hair_tailE',
                'Hair_tailF',
                'Hair_tailG',
                'Hair_tailH',
            ]
        ]

    def setup_main_hierarchy(self):
        """
        This module creates main group hierarchy for other bits to parent into.
        """

        top_grp = pm.group(em=True, n=self.TOP_GRP)
        ctl_grp = pm.group(em=True, n=self.CTL_GRP, p=top_grp)
        jnt_grp = pm.group(em=True, n=self.JNT_GRP, p=top_grp)
        connectors = pm.group(em=True, n=self.CNX_GRP, p=top_grp)
        dnt_grp = pm.group(em=True, n=self.DNT_GRP, p=top_grp)

        dnt_grp.inheritsTransform.set(False)
        dnt_grp.v.set(False)

        attribute.add(connectors, n='rigScale', type='float', min=0.001, dv=1)

    def build_hair_rig(self):

        # create head constrained grp
        head_constrained_grp = pm.group(em=True, n=self.PREFIX+'_headConstrainedGroup')
        pm.parent(head_constrained_grp, self.CNX_GRP)
        if pm.objExists(self.HEAD_JOINT):
            pm.delete(pm.parentConstraint(self.HEAD_JOINT, head_constrained_grp, mo=False))

        # main loop for hair chain creation. iterating on self.build_data
        for hair_rig_data in self.build_data:

            name = hair_rig_data['name']
            base_crv = hair_rig_data['base_crv']
            up_crv = hair_rig_data['up_crv']
            ctl_count = hair_rig_data['ctl_count']
            jnt_count = hair_rig_data['joint_count']
            ctl_size = hair_rig_data['ctl_size']

            # create controls
            fk_ctl_list = list()
            ik_ctl_list = list()
            crv_loc_list = list()
            crv_loc_up_list = list()
            transform_data_list = list()

            for i in range(ctl_count):
                uval = (float(1)/float(ctl_count-1))*float(i)

                # get tmp grp from guide curves + uval
                mp_data = motionpath.group_to_motion_path_with_up(
                    name='tmp_'+str(i),
                    crv=base_crv,
                    upCrv=up_crv,
                    uVal=uval
                )
                mp = mp_data['motionPath']
                up_mp = mp_data['motionPathUp']
                grp = mp_data['drivenTransform']
                up_grp = mp_data['drivenUpTransform']

                description = name.replace(self.PREFIX+'_', '')

                fk_ctl_name = self.PREFIX + '_CTL_' + description + '_FK' + str(i)
                ik_ctl_name = self.PREFIX + '_CTL_' + description + '_IK' + str(i)
                loc_name = self.PREFIX + '_CTL_' + description + '_loc'
                loc_up_name = self.PREFIX + '_CTL_' + description + '_loc'

                # ctl creation
                fk_ctl = control.create(
                    fk_ctl_name,
                    shape=self.FK_CTL_SHAPE,
                    size=ctl_size,
                    colorIdx=self.FK_CTL_COLOR,
                    zeroGroup=True,
                )
                fk_ctl_list.append(fk_ctl)
                fk_ctl.v.set(k=False, cb=False, l=True)

                # fk adjustments
                pm.xform(fk_ctl.cv[:], s=self.FK_CTL_SIZE_OFFSET, r=True, os=True)
                pm.xform(fk_ctl.cv[:], ro=self.FK_CTL_ROTATE_OFFSET, r=True, os=True)
                pm.xform(fk_ctl.cv[:], t=self.FK_CTL_TRANSLATE_OFFSET, r=True, os=True)

                ik_ctl = control.create(
                    ik_ctl_name,
                    shape=self.IK_CTL_SHAPE,
                    size=ctl_size,
                    colorIdx=self.IK_CTL_COLOR,
                    zeroGroup=False,
                )
                ik_ctl_list.append(ik_ctl)
                ik_ctl.v.set(k=False, cb=False, l=True)

                # ik adjustments
                pm.xform(ik_ctl.cv[:], s=self.IK_CTL_SIZE_OFFSET, r=True, os=True)
                pm.xform(ik_ctl.cv[:], ro=self.IK_CTL_ROTATE_OFFSET, r=True, os=True)
                pm.xform(ik_ctl.cv[:], t=self.IK_CTL_TRANSLATE_OFFSET, r=True, os=True)

                # curve driving locator creation
                loc = misc.locator(name=loc_name)
                crv_loc_list.append(loc)

                loc_up = misc.locator(name=loc_up_name)
                crv_loc_up_list.append(loc_up)

                pm.parent(loc, loc_up, ik_ctl, r=True)
                pm.hide(loc.getShape(), loc_up.getShape())

                # assemble ctls!
                pm.parent(ik_ctl, fk_ctl)
                pm.delete(pm.parentConstraint(grp, fk_ctl.getParent(), mo=False))
                pm.delete(pm.parentConstraint(up_grp, loc_up, mo=False))

                # cleanup
                pm.delete(mp, up_mp, grp, up_grp)

            # parent ctls
            for i, ctl in enumerate(fk_ctl_list):
                if i != 0:
                    print [fk_ctl_list[i-1]]
                    pm.parent(ctl.getParent(), fk_ctl_list[i-1])

            # hook up control constraining.
            ctl_constrain_tgt = pm.group(em=True, n=fk_ctl_list[0] + '_target')
            ctl_constrain_grp = pm.group(em=True, n=fk_ctl_list[0] + '_constrained')
            for g in [ctl_constrain_tgt, ctl_constrain_grp]:
                pm.delete(pm.parentConstraint(fk_ctl_list[0], g, mo=False))

            pm.parentConstraint(ctl_constrain_tgt, ctl_constrain_grp)
            pm.scaleConstraint(ctl_constrain_tgt, ctl_constrain_grp)
            pm.parent(fk_ctl_list[0].getParent(), ctl_constrain_grp)
            pm.parent(ctl_constrain_grp, self.CTL_GRP)
            pm.parent(ctl_constrain_tgt, head_constrained_grp)

            # connect curves to locators
            crv = paths.curve_from_loc_list(crv_loc_list, n=self.PREFIX + '_' + description+'_drivenCrv', connect=True)
            crv_up = paths.curve_from_loc_list(crv_loc_up_list, n=self.PREFIX + '_' + description+'_drivenUpCrv', connect=True)

            # create fitted curves.
            crv_fitted = pm.fitBspline(crv, n=crv+'_fitted', ch=1, tol=0.01)[0]
            crv_up_fitted = pm.fitBspline(crv_up, n=crv_up + '_fitted', ch=1, tol=0.01)[0]

            crv_grp = pm.group(em=True, n=name + '_drivenCurves', p=self.DNT_GRP)
            for c in [crv, crv_up, crv_fitted, crv_up_fitted]:
                pm.parent(c, crv_grp)

            # build joints
            jnt_list = paths.build_joints_from_curve(
                curve=crv_fitted,
                up_curve=crv_up_fitted,
                prefix=name,
                suffix='',
                number_of_joints=jnt_count,
                up_vector=[1, 0, 0],
                joint_aim_vector=[0, 1, 0],
                joint_up_vector=[1, 0, 0],
                mode='linear'
            )
            pm.parent(jnt_list[0], self.JNT_GRP)

            # attach joints to path
            attach_data = paths.attach_joints_to_curve(
                curve=crv_fitted,
                up_curve=crv_up_fitted,
                up_vector=[1, 0, 0],
                joint_list=jnt_list,
                joint_aim_vector=[0, 1, 0],
                joint_up_vector=[1, 0, 0]
            )

            jnt_driver_grp = pm.group(em=True, n=name+'_jointDrivers', p=self.DNT_GRP)
            pm.parent(attach_data['drivers'], jnt_driver_grp)
            pm.parent(attach_data['driversUp'], jnt_driver_grp)

            for driver in attach_data['drivers']:
                pm.connectAttr(self.CNX_GRP+'.rigScale', driver+'.rigScale', f=True)

            # cleanup parenting
            guide_grp = pm.group(em=True, n=name+'_guides', p=self.DNT_GRP)
            pm.parent(base_crv, up_crv, guide_grp)

            # create settings ctl
            settings_ctl_name = self.PREFIX + '_CTL_' + description + '_settings'
            settings_ctl = control.create(
                settings_ctl_name,
                shape=self.SETTINGS_CTL_SHAPE,
                size=ctl_size,
                colorIdx=self.SETTINGS_CTL_COLOR,
                zeroGroup=True,
            )

            # settings adjustments
            pm.xform(settings_ctl.cv[:], s=self.SETTINGS_CTL_SIZE_OFFSET, r=True, os=True)
            pm.xform(settings_ctl.cv[:], ro=self.SETTINGS_CTL_ROTATE_OFFSET, r=True, os=True)
            pm.xform(settings_ctl.cv[:], t=self.SETTINGS_CTL_TRANSLATE_OFFSET, r=True, os=True)

            pm.parent(settings_ctl.getParent(), ik_ctl_list[-1], r=True)

            for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v']:
                settings_ctl.attr(atr).set(k=False, cb=False, l=True)

            # add custom attrs
            attribute.add(settings_ctl, n='FK_vis', type='enum', en=['hide', 'show'], cb=True, dv=1)
            attribute.add(settings_ctl, n='IK_vis', type='enum', en=['hide', 'show'], cb=True, dv=0)
            attribute.add(settings_ctl, n='stretchy', type='float', min=0, max=1, dv=0, cb=True, k=True)
            attribute.add(settings_ctl, n='nonStretchLength', type='float', min=0.001, dv=1, cb=True, k=True)

            # connections
            for driver in attach_data['drivers']:
                pm.connectAttr(settings_ctl.stretchy, driver.stretchy, f=True)
                pm.connectAttr(settings_ctl.nonStretchLength, driver.fixedScale, f=True)

            for ctl in fk_ctl_list:
                pm.connectAttr(settings_ctl.FK_vis, ctl.getShape().v, f=True)

            for ctl in ik_ctl_list:
                pm.connectAttr(settings_ctl.IK_vis, ctl.getShape().v, f=True)

    # ==================================================================================================================
    def build(self):

        self.setup_main_hierarchy()
        self.build_hair_rig()


"""
from jb_destruna.tools import hair_rig
reload(hair_rig)
rig = hair_rig.HairRig()
rig.build()



CHAIN 


ctl_count = 50
for i in range(ctl_count):
    uval = (float(1)/float(ctl_count-1))*float(i)

    # get tmp grp from guide curves + uval
    mp_data = motionpath.group_to_motion_path_with_up(
        name='tmp_'+str(i),
        crv='base',
        upCrv='up',
        uVal=uval
    )
    mc.select(cl=1)
    ball = mc.joint(n='tmp_'+str(i) + '_jnt')
    pm.delete(pm.parentConstraint(mp_data['drivenUpTransform'], ball))    
    pm.parent(ball, mp_data['drivenUpTransform'])c
"""