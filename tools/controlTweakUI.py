import pymel.core as pm
import functools as func


class GUI(object):
    def __init__(self):
        self.uiName = 'jbControlTweakUI'

        if pm.window(self.uiName, ex=True):
            pm.deleteUI(self.uiName)

        self.showUI(self.uiName)

    def showUI(self, *args):

        self.WIN = pm.window(self.uiName)
        self.LYTA = pm.rowColumnLayout(nc=1)
        self.LYTA_1 = pm.rowColumnLayout(nc=10)
        self.but0 = pm.button(label='0',   w=20, bgc=[0.471, 0.471, 0.471], command=func.partial(self.setColor, 0))
        self.but1 = pm.button(label='1',   w=20, bgc=[0.000, 0.000, 0.000], command=func.partial(self.setColor, 1))
        self.but2 = pm.button(label='2',   w=20, bgc=[0.251, 0.251, 0.251], command=func.partial(self.setColor, 2))
        self.but3 = pm.button(label='3',   w=20, bgc=[0.502, 0.502, 0.502], command=func.partial(self.setColor, 3))
        self.but4 = pm.button(label='4',   w=20, bgc=[0.608, 0.000, 0.157], command=func.partial(self.setColor, 4))
        self.but5 = pm.button(label='5',   w=20, bgc=[0.000, 0.016, 0.376], command=func.partial(self.setColor, 5))
        self.but6 = pm.button(label='6',   w=20, bgc=[0.000, 0.000, 1.000], command=func.partial(self.setColor, 6))
        self.but7 = pm.button(label='7',   w=20, bgc=[0.000, 0.275, 0.098], command=func.partial(self.setColor, 7))
        self.but8 = pm.button(label='8',   w=20, bgc=[0.149, 0.000, 0.263], command=func.partial(self.setColor, 8))
        self.but9 = pm.button(label='9',   w=20, bgc=[0.784, 0.000, 0.784], command=func.partial(self.setColor, 9))
        self.but10 = pm.button(label='10', w=20, bgc=[0.541, 0.282, 0.200], command=func.partial(self.setColor, 10))
        self.but11 = pm.button(label='11', w=20, bgc=[0.247, 0.137, 0.122], command=func.partial(self.setColor, 11))
        self.but12 = pm.button(label='12', w=20, bgc=[0.600, 0.149, 0.000], command=func.partial(self.setColor, 12))
        self.but13 = pm.button(label='13', w=20, bgc=[1.000, 0.000, 0.000], command=func.partial(self.setColor, 13))
        self.but14 = pm.button(label='14', w=20, bgc=[0.000, 1.000, 0.000], command=func.partial(self.setColor, 14))
        self.but15 = pm.button(label='15', w=20, bgc=[0.000, 0.255, 0.600], command=func.partial(self.setColor, 15))
        self.but16 = pm.button(label='16', w=20, bgc=[1.000, 1.000, 1.000], command=func.partial(self.setColor, 16))
        self.but17 = pm.button(label='17', w=20, bgc=[1.000, 1.000, 0.000], command=func.partial(self.setColor, 17))
        self.but18 = pm.button(label='18', w=20, bgc=[0.392, 0.863, 1.000], command=func.partial(self.setColor, 18))
        self.but19 = pm.button(label='19', w=20, bgc=[0.263, 1.000, 0.639], command=func.partial(self.setColor, 19))
        self.but20 = pm.button(label='20', w=20, bgc=[1.000, 0.690, 0.690], command=func.partial(self.setColor, 20))
        self.but21 = pm.button(label='21', w=20, bgc=[0.894, 0.675, 0.475], command=func.partial(self.setColor, 21))
        self.but22 = pm.button(label='22', w=20, bgc=[1.000, 1.000, 0.388], command=func.partial(self.setColor, 22))
        self.but23 = pm.button(label='23', w=20, bgc=[0.000, 0.600, 0.329], command=func.partial(self.setColor, 23))
        self.but24 = pm.button(label='24', w=20, bgc=[0.631, 0.412, 0.188], command=func.partial(self.setColor, 24))
        self.but25 = pm.button(label='25', w=20, bgc=[0.624, 0.631, 0.188], command=func.partial(self.setColor, 25))
        self.but26 = pm.button(label='26', w=20, bgc=[0.408, 0.631, 0.188], command=func.partial(self.setColor, 26))
        self.but27 = pm.button(label='27', w=20, bgc=[0.188, 0.631, 0.365], command=func.partial(self.setColor, 27))
        self.but28 = pm.button(label='28', w=20, bgc=[0.188, 0.631, 0.631], command=func.partial(self.setColor, 28))
        self.but29 = pm.button(label='29', w=20, bgc=[0.188, 0.404, 0.631], command=func.partial(self.setColor, 29))
        self.but30 = pm.button(label='30', w=20, bgc=[0.435, 0.188, 0.631], command=func.partial(self.setColor, 30))
        self.but31 = pm.button(label='31', w=20, bgc=[0.631, 0.188, 0.412], command=func.partial(self.setColor, 31))

        pm.LYTB = pm.rowColumnLayout(nc=1, parent=self.WIN)
        pm.LYTB_1 = pm.rowColumnLayout(nc=5)
        self.butReset = pm.button(label='RESET', command=self.colorReset)
        self.butReset = pm.button(label='PRINT', command=self.colorPrint)

        self.WIN.show()

    def setColor(self, *args):
        colorIdx = args[0]
        print colorIdx

        for sel in pm.selected():
            for shape in sel.getShapes():
                if shape.type() == 'nurbsCurve':
                    shape.overrideEnabled.set(1)
                    shape.overrideColor.set(colorIdx)

    def colorReset(self, *args):

        for sel in pm.selected():
            for shape in sel.getShapes():
                if shape.type() == 'nurbsCurve':
                    shape.overrideEnabled.set(0)
                    shape.overrideColor.set(0)

    def colorPrint(self, *args):

        print '\n\nPrinting control colors:\n'
        for sel in pm.selected():
            for shape in sel.getShapes():
                if shape.type() == 'nurbsCurve':
                    print '\t-', shape.overrideColor.get(), shape
