from PySide2.QtGui import QGuiApplication






def convert_path():
    from PySide2.QtGui import QGuiApplication
    '''
    Convert windows file path to standard file path compatible with python. This method will copy from clipboard,
    operate on the path then return formatted path to clipboard.

    :return: a file path with ' / ' instead of ' \ '
    '''

    my_clipboard = QGuiApplication.clipboard()
    originalText = my_clipboard.text()

    if type(originalText) == unicode:
        path = r'{}'.format(originalText)
        formated_path = path.replace('\\', '/').replace(' ', '').replace('\n', '')
        clipboard.setText(formated_path)





def batch_copy():
    from jb_destruna.maya.internal import mesh
    import pymel.core as pm

    for geo in pm.ls(sl=1):
        mesh.copy(geo, n=geo.name() + '_MESH_COPY')



