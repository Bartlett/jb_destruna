'''
DESTRUNA CODE DEPLOY
For Maya 2019 (Release 1)

VERSION 1.0

UPDATED: 10_11_2020 by Damien Rogers

Notes: First public release, Woo!
'''

from maya import cmds
import sys
import os


def deploy():

    # jb_destruna PATH
    path = 'D:/Google Drive/Destruna/masterAssets_forRef/_resources/dev'

    abspath = (os.path.abspath(path))
    if abspath not in sys.path:
        print('*'*30)
        print('DEPLOY')
        print('Appending {}'.format(path))
        print('*'*30)
        sys.path.append(abspath)

    # SHELVES
    from jb_destruna.tools.shelves import shelf_sets

        # CHARACTER SHELF
    print('INSTALLING CHARACTER SHELF')
    shelf_sets.build_DESTRUNA_CHARACTERS()


cmds.evalDeferred(deploy)