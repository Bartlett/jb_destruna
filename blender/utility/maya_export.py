import os
from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2 import QtGui
from maya import cmds
import pprint


from maya.app.general.mayaMixin import MayaQWidgetDockableMixin


class AnimExport(MayaQWidgetDockableMixin, QtWidgets.QDialog):

    def __init__(self, parent=None):
        super(AnimExport, self).__init__(parent=parent)

        # WINDOW PROPERTIES
        self.setWindowTitle("Export Anim Cache")
        self.resize(300, 200)
        self.setMinimumWidth(327)

        self.create_default_layouts()
        self.create_default_widgets()
        self.populate_layouts()

    def create_default_layouts(self):

        self.master_layout = QtWidgets.QVBoxLayout()
        self.box_layout = QtWidgets.QVBoxLayout()
        self.geo_group_row = QtWidgets.QHBoxLayout()
        self.filepath_row = QtWidgets.QHBoxLayout()


    def create_default_widgets(self):

        self.help_label = QtWidgets.QLabel('blagh')

        self.box = QtWidgets.QGroupBox()

        self.group_geo_label = QtWidgets.QLabel('Geo group')
        self.group_geo_lineEdit = QtWidgets.QLineEdit()

        self.filepath_btn = QtWidgets.QPushButton('Open Filepath')
        self.filepath_label = QtWidgets.QLabel('filepath')
        self.filepath_lineEdit = QtWidgets.QLineEdit()

        self.export_btn = QtWidgets.QPushButton('Export')


        self.filepath_btn.clicked.connect(self.open_file)

    def populate_layouts(self):

        self.box.setLayout(self.box_layout)
        self.box_layout.addLayout(self.geo_group_row)
        self.geo_group_row.addWidget(self.group_geo_label)
        self.geo_group_row.addWidget(self.group_geo_lineEdit)

        self.box_layout.addWidget(self.filepath_btn)
        self.box_layout.addLayout(self.filepath_row)
        self.filepath_row.addWidget(self.filepath_label)
        self.filepath_row.addWidget(self.filepath_lineEdit)

        self.master_layout.addWidget(self.help_label)
        self.master_layout.addWidget(self.box)
        self.master_layout.addWidget(self.export_btn)

        self.setLayout(self.master_layout)


    def open_file(self):
        scnFilter = "*.abc"

        chosenFile = cmds.fileDialog2(fm=0, ds=0, cap="Open", ff=scnFilter, okc="Select scene file", hfe=0)
        #cmds.textField('myMayaScenePathField', edit=1, text=chosenFile)
        print(chosenFile)
        self.filepath_lineEdit.setText(chosenFile[0])


    def exc_export(self):

        pass

def showUI():
    ui = AnimExport()
    ui.show(dockable=True, floating=True)
    return ui

"""
from Demi_tools.QC import QC_UI_001 as qc
reload(qc)



qc.showUI()




from jb_destruna.maya.internal import mesh
from jb_destruna.maya.internal import blendshape
from maya import cmds as mc
import pymel.core as pm

geo_group = 'Geometry'
conversion_group = mc.group(n='blender_conversion_transform', em=1, w=1)

export_items = []


for i in mc.listRelatives(geo_group, c=1):
    
    output_mesh_name = i + '__cacheMesh'
    export_items.append(output_mesh_name)
    output_mesh = mesh.copy(i, n=output_mesh_name)
    pm.parent(output_mesh, conversion_group)

    blendshape.apply(i, output_mesh, dv=1, n=output_mesh_name + '_BS')
    mc.setAttr(output_mesh_name + '_BS.origin', 0)
    
mc.setAttr(conversion_group + '.rotateX', -90)


mc.makeIdentity(conversion_group, apply=True, r=True)


# export files

items = ''

for i in export_items:
    items += "-root {} ".format(i)
    
start = str(1)
end = str(50)
root = items
save_name = "C:/Users/Damien/Desktop/cache/my_don_cache_09.abc"

command = "-frameRange " + start + " " + end + " -uvWrite -worldSpace " + root + " -file " + save_name

print command
mc.AbcExport(j=command)

mc.delete(conversion_group)









"""
