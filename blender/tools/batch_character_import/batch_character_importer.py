#print('test')
import bpy
import os




class CharacterAnimCache(object):

    def __init__(self, char_collection):

        self.collection_name = char_collection

        self.target_geo = self.get_target_geo(self.collection_name)

        self.cache_directory = None

        print('INSTANCE CREATED')
    def set_cache_directory(self, cache_dir_filepath):

        if self.validate_cache_folder(cache_dir_filepath):

            self.cache_directory = cache_dir_filepath
            print('cache_directory set:', self.cache_directory)

    def apply_anim_cache(self, cache_name):

        if self.validate_cache_file(cache_name):

            cache_obj = self.get_cache_object(cache_name)
            self.cache_load_bug_fix(cache_obj)

            match_dict = self.match_cache_to_target_geo(cache_obj, self.target_geo)

            for i_target_geo in self.target_geo:

                mesh_cache_mod = self.get_mesh_cache_modifier(i_target_geo, create_if_none_found=True)
                print('>>', i_target_geo, i_target_geo.name)
                cache_object_path = match_dict[i_target_geo.name]

                self.set_mesh_cache_modifier_param(mesh_cache_mod, cache_obj, cache_object_path)
            print('pass')
    def remove_anim_cache(self):

        for i_target_geo in self.target_geo:

            modifier = self.get_mesh_cache_modifier(i_target_geo, create_if_none_found=False)
            i_target_geo.modifiers.remove(modifier)

    def get_mesh_cache_modifier(self, target_geo, create_if_none_found=True):

        modifier = None

        modifier_name = target_geo.name + '__sequence_cache'
        modifier_list = [i[0] for i in target_geo.modifiers.items()]

        if modifier_name in modifier_list:
            print('Modifier for {} found'.format(target_geo.name))
            modifier_index = modifier_list.index(modifier_name)
            modifier = target_geo.modifiers.items()[modifier_index][1]

        elif modifier_name not in modifier_list and create_if_none_found:
            print('Modifier for {} not found, creating new modifier'.format(target_geo.name))
            modifier = self.create_mesh_cache_modifier(target_geo, modifier_name)

        else:
            pass

        return modifier

    def create_mesh_cache_modifier(self, target_geo, modifier_name):

        return target_geo.modifiers.new(name=modifier_name, type='MESH_SEQUENCE_CACHE')

    def set_mesh_cache_modifier_param(self, mesh_cache_mod, cache_obj, cache_object_path):

        # Set the file used by this modifier
        mesh_cache_mod.cache_file = cache_obj

        # Specify which geo within the alembic file is being used for this modifier
        mesh_cache_mod.object_path = cache_object_path

    def get_cache_object(self, cache_name):
        """
        Returns a cache_object, if it already exists in the scene returns a reference else creates a new one

        Args:  cache_name: The name of the cache_file

        :return: cache_object
        """

        if self.does_cache_exist_in_scene(cache_name):

            cache_obj = bpy.data.cache_files.get(cache_name)

        else:

            cache_obj = self.create_cache_object(cache_name)

        return cache_obj

    def create_cache_object(self, cache_name):

        filepath = os.path.join(self.cache_directory, cache_name)
        bpy.ops.cachefile.open(filepath=filepath)
        bpy.context.view_layer.update()
        cachefile_obj = bpy.data.cache_files.get(cache_name)

        return cachefile_obj

    def match_cache_to_target_geo(self, cache_obj, target_geo_list):

        match_dict = dict()

        cache_geo_list = cache_obj.object_paths.items()
        print('CACHE OBJECT', cache_obj)
        print('CACHE OBJECT PATHS', cache_geo_list)

        cache_geo_list = [cache[0].split('/')[1] for cache in cache_geo_list]
        target_geo_list = [g.name for g in target_geo_list]

        print('%'*100)
        print(cache_geo_list)
        print(target_geo_list)
        print('%' * 100)

        for i_target_geo in target_geo_list:

            cache_geo_name = i_target_geo + '__cacheMesh'

            if cache_geo_name in cache_geo_list:

                match_dict[i_target_geo] = "/{0}/{0}Shape".format(cache_geo_name)

        return match_dict

    def validate_cache_file(self, cache_name):

        # is the file an alembic?
        if os.path.splitext(cache_name)[1] != '.abc':

            message = 'Warning, cannot find .abc extension on given file: {}'.format(cache_name)
            print(star_wrap(message))
            raise TypeError

        # is the file within current cache directory?
        if cache_name not in os.listdir(self.cache_directory):

            message = 'Warning, file not found in current cache directory: {}'.format(self.cache_directory)
            print(star_wrap(message))
            raise IOError

        # TODO: this has moved to get get_cache_object
        # # has the file already been added to the scene?
        # if not self.is_cache_unique(cache_name):
        #
        #     message = 'Warning, {} has already been added to scene'.format(cache_name)
        #     print(star_wrap(message))
        #     raise IOError

        return True

    def cache_load_bug_fix(self, cache_obj):
        """
        This is a bug fix for a strange issue where querying a cachefile object immediately after creation results in an empty
        list of object paths. However running the same code a second time will show a populated list. I believe this to be
        an dg eval related issue.

        The following fix is simply adding the cachefile to the dg network by applying it to a dummy node then
        calling bpy.ops.cachefile.reload. This is a poor mans dg.flushIdleQue
        """

        geo = self.target_geo[0]
        temp_modifier_name = 'TEMP_MODIFIER_FOR_FIX'
        temp_modifier = self.create_mesh_cache_modifier(geo, temp_modifier_name)
        temp_modifier.cache_file = cache_obj
        bpy.ops.cachefile.reload()

        geo.modifiers.remove(temp_modifier)

    @staticmethod
    def validate_cache_folder(cache_dir_filepath):
        # TODO: test is path is file or folder

        """
        Check if the given directory exists and and contains .abc file

        Arg:
            c_filepath  (str) The file path of the cache directory

        Returns:
                        (bool, str) A pass/fail bool and a fail message
        """

        if not os.path.exists(cache_dir_filepath):
            message = 'Warning, path does not exist'
            print(star_wrap(message))
            raise IOError

        dir_contents = os.listdir(cache_dir_filepath)
        dir_contents_extensions = [os.path.splitext(ext)[1] for ext in dir_contents]

        if '.abc' not in dir_contents_extensions:
            message = 'Warning, no .abc files in given directory'
            print(star_wrap(message))
            return True

        return True

    @staticmethod
    def does_cache_exist_in_scene(cache_name):
        """
        Test to see if a cache is already present in the scene
        :param cache_name:
        :return:
        """

        scene_caches = bpy.data.cache_files.items()

        if scene_caches:

            scene_caches_names = [c[0] for c in scene_caches]

            if cache_name in scene_caches_names:

                return True

        return False

    @staticmethod
    def get_target_geo(char_collection):

        """
        Gets and returns the geometry under the given collection.

        Args
            char_collection:
                                (str)   The name of a  blender collection with a look dev characters geo parented underneath

        Returns:
                                (list)  The children geo of char_geo_top_grp.
        """
        collection_items = bpy.data.collections[char_collection].objects
        target_geo = [g for g in collection_items if g.type == 'MESH']

        if target_geo:
            return target_geo
        else:
            print('WARNING, no geo found under given character collection (char_collection)')
            raise TypeError


def star_wrap(message, start_num=150):
    return '*'*start_num + '\n' + message + '\n' + '*'*start_num



"""
import bpy

filepath = 'C:/Users/Damien/Desktop/cache/my_don_cache_07.abc'
import_s = bpy.ops.wm.alembic_import(filepath=filepath)

cache = bpy.data.cache_files.items()[0][1]

print(cache)
#for geo in bpy.data.collections["geo_collection"].objects[3:6]:
#    object_path = "/{0}/{0}Shape".format(geo.name)#

#    print(object_path)

   bpy.context.view_layer.objects.active = geo
   modifier = geo.modifiers.new(name="cache", type='MESH_SEQUENCE_CACHE')
   modifier.cache_file = cache
   modifier.object_path = object_path




import sys
from importlib import reload

file_path = 'C:/Users/Damien/Dev/Destruna_dev'

if file_path not in sys.path:
    sys.path.append(file_path)


from jb_destruna.blender.tools.batch_character_import import batch_character_importer as bci
reload(bci)






import sys
from importlib import reload
import bpy

file_path = 'C:/Users/Damien/Dev/Destruna_dev'

if file_path not in sys.path:
    sys.path.append(file_path)


from jb_destruna.blender.tools.batch_character_import import batch_character_importer as bci
reload(bci)



don_cache = bci.CharacterAnimCache('don_teen_mocap:geoCollection')
c_filepath = 'C:/Users/Damien/Desktop/cache'

don_cache.set_cache_directory(c_filepath)



don_cache.apply_anim_cache('my_don_cache_09.abc')


import sys
from importlib import reload
import bpy

file_path = 'C:/Users/Damien/Dev/Destruna_dev'

if file_path not in sys.path:
    sys.path.append(file_path)


from jb_destruna.blender.tools.batch_character_import import batch_character_importer as bci
reload(bci)



don_cache = bci.CharacterAnimCache('don_teen_mocap:geoCollection')
c_filepath = 'C:/Users/Damien/Desktop/cache'

don_cache.set_cache_directory(c_filepath)



obj = don_cache.apply_anim_cache('my_don_cache_09.abc')
print(obj)
print(obj.object_paths) 
print(obj.object_paths.items()) 
print('$'*100)
bpy.ops.obj.reload()
print(obj)
print(obj.object_paths) 
print(obj.object_paths.items()) 

"""
