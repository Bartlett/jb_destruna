import bpy
import os
from jb_destruna.blender.tools.batch_character_import import batch_character_importer

reload(batch_character_importer)


print('loaded3')





def enum_list_from_filepath(self, context):
    folder_list_key = "cache_folder_file_list"

    if bpy.context.object.get(folder_list_key, None):
        char_dict = bpy.context.object[folder_list_key]
        return [(str(i), c, '') for i, c in enumerate(char_dict)]
    else:
        return [('', '', '')]

def item_cb(self, context):
    return [(c.name, c.name, "") for c in bpy.data.collections]


class OBJECT_OT_operator_1(bpy.types.Operator):
    """Operator 1"""
    bl_label = "ATTACH cache"
    bl_idname = "object.operator_1"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        print('CLEAR Character_link_dict')
        bpy.context.object['Character_link_dict'] = None
        return {'FINISHED'}

class OBJECT_OT_operator_2(bpy.types.Operator):
    """Operator 1"""
    bl_label = "DETACH cache file"
    bl_idname = "object.operator_2"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        #print('OP 2')
        print('>>', bpy.context.object['Character_link_dict'].items())
        # list_num = 10
        # print('LINK ITEMS', LINK_ITEMS)
        return {'FINISHED'}


def update_filepath(self, context):
    raw_input = context.scene.my_tool.my_string

    if os.path.exists(raw_input):
        print('path exists', raw_input)
        append_cache_folder_contents_to_list(raw_input)
    else:
        print('Not a path :(', raw_input)
        clear_cache_folder_contents_list()




def validate_filepath(filepath):
    pass

class MySettings(bpy.types.PropertyGroup):

    my_string : bpy.props.StringProperty(
        name="Cache folder",
        description="Add the file path to the relevant cache folder",
        default="Please add cache file path",
        maxlen=1024,
        update=update_filepath,
    )

    character_object_enum: bpy.props.EnumProperty(
        name="Character",
        description="Apply Data to attribute.",
        items=enum_list_from_character_dict,
    )

    cache_files_enum: bpy.props.EnumProperty(
        name="Choose file",
        description="Apply Data to attribute.",
        items=enum_list_from_filepath,
    )

def store_CharacterAnimCache_object(cache_obj):

    char_link_key = "CharacterAnimCache_objects"

    if not bpy.context.object.get(char_link_key, None):
        bpy.context.object[char_link_key] = dict()

    # Fetch storage dict
    char_anim_cache_obj_dict = bpy.context.object[char_link_key]

    # Fetch name of collection to use as key
    obj_name = cache_obj.collection_name

    # Store cache object in dictionary
    char_anim_cache_obj_dict[obj_name] = cache_obj

    # Restore dictionary to data
    bpy.context.object[char_link_key] = char_anim_cache_obj_dict

def build_enums_from_CharacterAnimCache_objects(self, context):

    char_link_key = "CharacterAnimCache_objects"

    if bpy.context.object.get(char_link_key, None):
        char_dict = bpy.context.object[char_link_key]
        return [(str(i), c, '') for i, c in enumerate(char_dict)]
    else:
        return [('', '', '')]



def append_cache_folder_contents_to_list(filepath):

    folder_list_key = "cache_folder_file_list"

    if not bpy.context.object.get(folder_list_key, None):
        bpy.context.object[folder_list_key] = dict()

    char_dict = bpy.context.object[folder_list_key]

    # FILTER
    raw_folder_contents = os.listdir(filepath)
    filtered_folder_contents = [f for f in raw_folder_contents if os.path.splitext(f)[1] == '.abc']

    bpy.context.object[folder_list_key] = filtered_folder_contents

def clear_cache_folder_contents_list():

    folder_list_key = "cache_folder_file_list"

    if not bpy.context.object.get(folder_list_key, None):
        bpy.context.object[folder_list_key] = dict()

    bpy.context.object[folder_list_key] = []

class SimpleOperator(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "object.simple_operator"
    bl_label = "Select collection of character geo"
    bl_property = "my_enum"

    my_enum : bpy.props.EnumProperty(items=item_cb)

    def execute(self, context):

        if create_character_link(self.my_enum):
            self.report({'INFO'}, "Character link for: {} created".format(self.my_enum))

        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        wm.invoke_search_popup(self)
        return {'FINISHED'}

def create_character_link(collection_name):


    # TODO: validate request

    # Create instance of CharacterAnimCache
    cc_anim_cache_obj = batch_character_importer.CharacterAnimCache(collection_name)

    store_CharacterAnimCache_object(cc_anim_cache_obj)

def validate_link(collection_name):
        pass


# MAIN
class ADDONNAME_PT_TemplatePanel(bpy.types.Panel, MySettings):
    bl_label = "CC Importer"
    bl_idname = "ADDONNAME_PT_TemplatePanel"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_category = "Destruna"

    def draw(self, context):

        my_tool = context.scene.my_tool

        layout = self.layout
        layout.label(text='1) Select character to receive cache')
        box = layout.box()
        box.operator("object.simple_operator", icon="LINKED")
        box.prop(my_tool, "character_object_enum")

        layout.label(text='2) Select cache file to apply')
        box = layout.box()
        box.prop(my_tool, "my_string")
        box.prop(my_tool, "cache_files_enum")

        layout.operator('object.operator_1')
        layout.operator('object.operator_2')

class ADDONAME_OT_TemplateOperator(bpy.types.Operator):
    bl_label = "Create Character Link"
    bl_idname = "wm.template_operator"

    text: bpy.props.StringProperty(name="Enter Text:")

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def execute(self, context):
        return {'FINISHED'}


# classes = [ADDONNAME_PT_TemplatePanel, ADDONAME_OT_TemplateOperator, SimpleOperator, MySettings]
#
#
# def register():
#     for cls in classes:
#         bpy.utils.register_class(cls)
#
#
# def unregister():
#     for cls in classes:
#         bpy.utils.unregister_class(cls)
#
#
# if __name__ == "__main__":
#     register()

classes = (
    ADDONNAME_PT_TemplatePanel,
    ADDONAME_OT_TemplateOperator,
    SimpleOperator,
    OBJECT_OT_operator_1,
    OBJECT_OT_operator_2,
    MySettings,
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    bpy.types.Scene.my_tool = bpy.props.PointerProperty(type=MySettings)

def unregister():
    del bpy.types.Scene.my_tool
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)

if __name__ == "__main__":
    register()


'''
import sys
from importlib import reload
import bpy

file_path = 'C:/Users/Damien/Dev/Destruna_dev'

if file_path not in sys.path:
    sys.path.append(file_path)

from jb_destruna.blender.tools.batch_character_import import cc_importer_UI
reload(cc_importer_UI)

cc_importer_UI.register()
'''
