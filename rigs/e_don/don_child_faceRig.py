from jb_destruna.maya.core import build
from jb_destruna.maya.core import face
from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.internal import control
from jb_destruna.maya.internal import cpom
from jb_destruna.maya.internal import curve
from jb_destruna.maya.internal import deformer
from jb_destruna.maya.internal import follicle
from jb_destruna.maya.internal import mesh
from jb_destruna.maya.internal import skin
from jb_destruna.maya.internal import wrap
from jb_destruna.maya.core import tweak
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility import misc
from jb_destruna.maya.utility.misc import pn
import maya.cmds as mc
import pymel.core as pm

for imported_module in [
    face,
    tweak,
    mesh,
    blendshape,
    follicle,
    build,
    wrap,
    deformer,
]:
    reload(imported_module)


class FaceRig(face.FaceRig):

    ASSET = 'don'

    FACE_RIG_GEO = 'face_rig_geo'
    FACE_REGION_GEO = 'face_region_geo'
    FACE_REGION_DRIVER = 'face_region_geo_driver'
    FACE_REGION_TRIDRIVER = 'face_region_geo_tridriver'
    FACE_TWEAKS_GEO = 'face_tweaks_geo'
    FACE_TWEAKS_DRIVER = 'face_tweaks_geo_driver'

    EYELASH_L_GEO = 'eyeLash_L_geo'
    EYELASH_R_GEO = 'eyeLash_R_geo'
    EYE_L_GEO = 'eye_L_geo'
    EYE_R_GEO = 'eye_R_geo'
    EYEBROW_L_GEO = 'eyebrow_L_geo'
    EYEBROW_R_GEO = 'eyebrow_R_geo'
    HEAD_GEO = 'head_geo'
    SCLERA_L_GEO = 'sclera_L_geo'
    SCLERA_R_GEO = 'sclera_R_geo'
    TEETHLOWER_GEO = 'teethLower_geo'
    TEETHUPPER_GEO = 'teethUpper_geo'
    TONGUE_GEO = 'tongue_geo'

    FACE_GEO_LIST = [
        EYELASH_L_GEO,
        EYELASH_R_GEO,
        EYE_L_GEO,
        EYE_R_GEO,
        EYEBROW_L_GEO,
        EYEBROW_R_GEO,
        HEAD_GEO,
        SCLERA_L_GEO,
        SCLERA_R_GEO,
        TEETHLOWER_GEO,
        TEETHUPPER_GEO,
        TONGUE_GEO,
    ]

    HEAD_CONSTRAINED_GRP = 'Head_constrained_grp'

    FACE_BS_GEO = 'face_bs_geo'

    TWEAKS = 'Tweaks'
    TWEAKS_CTL_GRP = TWEAKS+'_controls'
    TWEAKS_JNT_GRP = TWEAKS+'_joints'

    FACE_RIG_GEO_BASE = 'face_rig_geo_base'
    FACE_GEO_WRAPPOSE = 'face_geo_wrapPose'

    HEAD_CONSTRAINED_POSITION = [-0.0, 144.08, -4.356]

    def __init__(self):
        super(FaceRig, self).__init__()

        self.who = self.ASSET

        self.imports = [
            # GEO (ASSET)
            {'filepath': 'E:/destruna/assets/don_child/face/imports/face_geo_v002.mb', 'group': self.DELETE_GRP},

            # SCENE (CURRENT BS RIG)
            {'filepath': 'E:/destruna/assets/don_child/face/imports/face_asbs_v001.mb', 'group': self.DELETE_GRP},

            # SCENE (CTL LABELS)
            {'filepath': 'E:/destruna/assets/lona/face/imports/ctl_box_labels_v001.mb', 'group': self.DELETE_GRP},

            # SCENE (CTL CURVES)
            {'filepath': 'E:/destruna/assets/don_child/face/imports/ctl_crvs_v001.mb', 'group': self.DELETE_GRP},

            # GEO (REGION TRI DRIVER)
            {'filepath': 'E:/destruna/assets/don_child/face/imports/region_tridriver_v001.mb', 'group': self.DO_NOT_TOUCH_GRP},

            # SCENE (REGION CONTROL CURVES)
            {'filepath': 'E:/destruna/assets/don_child/face/imports/region_controls_v001.mb', 'group': self.DO_NOT_TOUCH_GRP},

            # SCENE (WRAPPERS)
            {'filepath': 'E:/destruna/assets/don_child/face/imports/face_wrappers_v002.mb', 'group': self.DO_NOT_TOUCH_GRP},
        ]

        self.groups.extend([
            {'g': self.BROW_STUFF, 'p': self.DO_NOT_TOUCH_GRP, 'v': True},
            {'g': self.EYE_STUFF, 'p': self.DO_NOT_TOUCH_GRP, 'v': True},
            {'g': self.LASH_STUFF, 'p': self.DO_NOT_TOUCH_GRP, 'v': True},
        ])

        self.region_fol_dict = {
            self.REGION_CTL_BROW_L: 17,
            self.REGION_CTL_BROW_R: 7,
            self.REGION_CTL_CHEEK_L: 15,
            self.REGION_CTL_CHEEK_R: 2,
            self.REGION_CTL_CHIN: 0,
            self.REGION_CTL_EYE_L: 16,
            self.REGION_CTL_EYE_R: 4,
            self.REGION_CTL_MOUTH: 1,
            self.REGION_CTL_NOSE: 3,
        }

        self.tweak_controls_setup = [
            {
                'name': 'Tweaks',
                'parent': self.RIG_GRP,
                'tweak_data': [
                    # mouth ===================
                    # upper
                    {'prefix': 'Tweak', 'description': 'mouthUpperA', 'side': 'M', 'orient': True, 'vtx': 'face_tweaks_geo_driver.vtx[2849]'},
                    {'prefix': 'Tweak', 'description': 'mouthUpperB', 'side': 'L', 'orient': True, 'vtx': 'face_tweaks_geo_driver.e[5551]'},
                    {'prefix': 'Tweak', 'description': 'mouthUpperC', 'side': 'L', 'orient': True, 'vtx': 'face_tweaks_geo_driver.vtx[4287]'},
                    {'prefix': 'Tweak', 'description': 'mouthUpperB', 'side': 'R', 'orient': True, 'vtx': 'face_tweaks_geo_driver.e[8166]'},
                    {'prefix': 'Tweak', 'description': 'mouthUpperC', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[4941]'},

                    # corners
                    {'prefix': 'Tweak', 'description': 'mouthCorner', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[4004]'},
                    {'prefix': 'Tweak', 'description': 'mouthCorner', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[4658]'},

                    # lower
                    {'prefix': 'Tweak', 'description': 'mouthLowerA', 'side': 'M', 'orient': True, 'vtx': 'face_tweaks_geo_driver.vtx[2821]'},
                    {'prefix': 'Tweak', 'description': 'mouthLowerB', 'side': 'L', 'orient': True, 'vtx': 'face_tweaks_geo_driver.e[7396]'},
                    {'prefix': 'Tweak', 'description': 'mouthLowerC', 'side': 'L', 'orient': True, 'vtx': 'face_tweaks_geo_driver.vtx[2833]'},
                    {'prefix': 'Tweak', 'description': 'mouthLowerB', 'side': 'R', 'orient': True, 'vtx': 'face_tweaks_geo_driver.e[10013]'},
                    {'prefix': 'Tweak', 'description': 'mouthLowerC', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1539]'},

                    # # LHS
                    {'prefix': 'Tweak', 'description': 'eyeInnerCorner', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[2989]'},
                    {'prefix': 'Tweak', 'description': 'eyeOuterCorner', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[659]'},
                    #
                    {'prefix': 'Tweak', 'description': 'eyeUpperA', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[5762]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperB', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[5770]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperC', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[5776]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperD', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[5774]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperE', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[107]'},
                    # #
                    {'prefix': 'Tweak', 'description': 'eyeLowerA', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[260]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerB', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[246]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerC', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[807]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerD', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[1307]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerE', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[4052]'},
                    #
                    # # RHS
                    {'prefix': 'Tweak', 'description': 'eyeInnerCorner', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1695]'},
                    {'prefix': 'Tweak', 'description': 'eyeOuterCorner', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[3298]'},
                    # #
                    {'prefix': 'Tweak', 'description': 'eyeUpperA', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8379]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperB', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8387]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperC', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8393]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperD', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8391]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperE', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[790]'},
                    # #
                    {'prefix': 'Tweak', 'description': 'eyeLowerA', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[943]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerB', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[929]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerC', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[3450]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerD', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[3906]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerE', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[4706]'},
                ]
            }
        ]

        self.wrapDeformer_create = [
            {
                'wrapped': 'eyebrow_L_geo',
                'wrapper': 'brow_wrapper',
                'base': 'brow_wrapperBase',
                'baseParent': self.BROW_STUFF,
            },
            {
                'wrapped': 'eyebrow_R_geo',
                'wrapper': 'brow_wrapper',
                'base': 'brow_wrapperBase',
                'baseParent': self.BROW_STUFF,
            },
        ]

        self.skin_to_joint = {
            'Main_upperTeeth_joint': [
                'teethUpper_geo'
            ],
            'Main_lowerTeeth_joint': [
                'teethLower_geo'
            ],
            'eye_jnt_L': [
                'eye_L_geo',
                'sclera_L_geo',
            ],
            'eye_jnt_R': [
                'eye_R_geo',
                'sclera_R_geo',
            ],
        }

        self.skin_bind = [
            {'geo': 'tongue_geo', 'filepath': 'E:/destruna/assets/don/face/imports/tongue_geo_skinCluster_v001.wts'}
        ]

    def set_up_face_rig_geo(self):
        mesh.copy(self.FACE_BS_GEO, n=self.FACE_RIG_GEO)
        pm.parent(self.FACE_RIG_GEO, self.DO_NOT_TOUCH_GRP)

    def setup_head_constrained_groups(self):

        head_constrained_grp = pm.group(em=True, n=self.HEAD_CONSTRAINED_GRP)
        head_constrained_grp.t.set(self.HEAD_CONSTRAINED_POSITION)

        # jaw_constrained_grp = pm.group(em=True, n='Jaw_constrained_grp')
        # jaw_constrained_grp.t.set([-0.0, 132.663, 2.124])
        # jaw_constrained_grp.r.set([-90.0, -51.321, -90.0])

        pm.parent('ctrlBoxOffset', self.HEAD_CONSTRAINED_GRP)

    def adjust_labels(self):

        # parent labels to ctl box.
        label_list = ['label_brow', 'label_cheek', 'label_eye', 'label_mouth', 'label_nose']
        pm.parent(label_list, 'ctrlBox')


        # brow ctls
        for ctl in ['ctrlBoxBrow_L', 'ctrlBoxBrow_R']:
            pn(ctl).ty.set(6.63)
        pm.xform('label_brow', t=[-24.122, 316.648, 0.0], os=True)

        # eye ctls
        for ctl in ['ctrlBoxEye_L', 'ctrlBoxEye_R']:
            pn(ctl).ty.set(3.636)
        pm.xform('label_eye', t=[-14.603, 195.748, 0.0], os=True)


        # cheek ctls
        for ctl in ['ctrlBoxCheek_L', 'ctrlBoxCheek_R']:
            pn(ctl).ty.set(0.631)
        pm.xform('label_cheek', t=[-26.257, 75.97, 0.0], os=True)

        # nose ctls
        for ctl in ['ctrlBoxNose_L', 'ctrlBoxNose_R']:
            pn(ctl).ty.set(-1.259)

        pm.xform('label_nose', t=[-21.9, 0.143, 0.0], os=True)
        pm.xform('label_mouth', t=[-30.41, -75.158, 0.0], os=True)

        # pn('ctrlBoxMouth_M').ty.set(-2.752)
        #
        # # mouth corner ctls
        # for ctl in ['ctrlBoxMouthCorner_L', 'ctrlBoxMouthCorner_R']:
        #     pn(ctl).ty.set(-6.008)
        #
        # pn('ctrlBoxPhonemes_M').ty.set(-4.977)
        # pn('ctrlBoxEmotions_M').ty.set(-7.637)
        #
        # adjust box shape.
        for cv in ['ctrlBoxShape.cv[1]', 'ctrlBoxShape.cv[0]', 'ctrlBoxShape.cv[4]']:
            pm.move(cv, [0, 3.5, 0], r=True, os=True)
        #
        # for cv in ['ctrlBoxShape.cv[2]', 'ctrlBoxShape.cv[3]']:
        #     pm.move(cv, [0, -0.597392, 0], r=True, os=True)

    def setup_blendshape_rig(self):

        # restructure a bit
        pm.parent('Main', self.RIG_GRP)
        ctl_grp = pm.rename('FaceMotionSystem', 'Main_controls')
        jnt_grp = pm.rename('FaceDeformationSystem', 'Main_joints')
        dnt_grp = pm.group(em=True, n='Main_doNotTouch', p='Main')
        pm.hide(dnt_grp)

        # deformer renames
        # pm.rename('face_rig_geo_smooth', self.FACE_BS_GEO+'_deltaSmooth')
        # pm.rename('face_rig_geo_deltaMush', self.FACE_BS_GEO+'_deltaMush')
        # pm.rename('asFaceBS', self.FACE_BS_GEO+'_blendShape')

        # deleting guides
        pm.delete('FaceFitSkeleton')

        pm.parent(self.FACE_BS_GEO, dnt_grp)

    def setup_mouth_controls(self):

        # setup floating controls.

        # unlock joints grp
        joints_grp = pn('Main_joints')
        joints_grp.v.set(l=False)
        joints_grp.v.set(True)

        # delete unused stuff.
        unused = ['LidCurves_L', 'LidCurves_R', 'LidSetup', 'LipSetup']
        pm.delete(unused)

        # UPPER TEETH ==================================================================================================
        teeth_upper_ctl = pn('upperTeeth_M')
        teeth_upper_ctl.rename('Main_CTL_upperTeeth')

        # use mouth crv shapes to make the mouth shapes better.
        pm.parent('upper_teeth_crv', teeth_upper_ctl)
        pm.makeIdentity('upper_teeth_crv', t=True, r=True, s=True, apply=True)
        pm.parent('upper_teeth_crv', self.DELETE_GRP)
        pm.connectAttr(pn('upper_teeth_crv').local, teeth_upper_ctl.create, f=True)
        teeth_upper_ctl.create.disconnect()

        # override color (red)
        teeth_upper_ctl.getShape().overrideEnabled.set(True)
        teeth_upper_ctl.getShape().overrideColor.set(13)

        # setup head constrained bypass
        pm.parent('upperTeethOffset_M', self.HEAD_CONSTRAINED_GRP)
        # pm.delete(['upperTeethJoint_M_parentConstraint1', 'upperTeethJoint_M_scaleConstraint1'])

        pm.select(cl=True)

        teeth_upper_jnt = pm.joint(n='Main_upperTeeth_joint')
        # teeth_upper_jnt.rename('Main_upperTeeth_joint')
        teeth_upper_jnt.radius.set(1)

        upper_teeth_zero = pm.group(em=True, n=teeth_upper_jnt+'_ZERO', p='Main_joints')
        pm.delete(pm.parentConstraint(teeth_upper_ctl, upper_teeth_zero, mo=False))
        pm.parent(teeth_upper_jnt, upper_teeth_zero)

        for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
            pm.connectAttr(teeth_upper_ctl.attr(atr), teeth_upper_jnt.attr(atr), f=True)

        # ==============================================================================================================
        # JAW SDK GRP
        jaw_driven_grp = pm.group(em=True, n='jaw_driven_grp', p='Main_joints')
        pm.parentConstraint('JawFollow_M', jaw_driven_grp, mo=False)

        # LOWER TEETH ==================================================================================================
        teeth_lower_ctl = pn('lowerTeeth_M')
        teeth_lower_ctl.rename('Main_CTL_lowerTeeth')

        # use mouth crv shapes to make the mouth shapes better.
        pm.parent('lower_teeth_crv', teeth_lower_ctl)
        pm.makeIdentity('lower_teeth_crv', t=True, r=True, s=True, apply=True)
        pm.parent('lower_teeth_crv', self.DELETE_GRP)
        pm.connectAttr(pn('lower_teeth_crv').local, teeth_lower_ctl.create, f=True)
        teeth_lower_ctl.create.disconnect()

        # override color (red)
        teeth_lower_ctl.getShape().overrideEnabled.set(True)
        teeth_lower_ctl.getShape().overrideColor.set(13)

        # setup head constrained bypass
        pm.parent('lowerTeethOffset_M', self.HEAD_CONSTRAINED_GRP)
        pm.delete(['lowerTeethJoint_M_parentConstraint1', 'lowerTeethJoint_M_scaleConstraint1'])

        teeth_lower_jnt = pn('lowerTeethJoint_M')
        teeth_lower_jnt.rename('Main_lowerTeeth_joint')
        teeth_lower_jnt.radius.set(1)

        teeth_lower_zero = pm.group(em=True, n=teeth_lower_jnt+'_ZERO', p=jaw_driven_grp)
        pm.delete(pm.parentConstraint(teeth_lower_ctl, teeth_lower_zero, mo=False))
        pm.parent(teeth_lower_jnt, teeth_lower_zero)

        for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
            pm.connectAttr(teeth_lower_ctl.attr(atr), teeth_lower_jnt.attr(atr), f=True)

        # TONGUE =======================================================================================================
        pm.parent('Tongue0Offset_M', self.HEAD_CONSTRAINED_GRP)
        pm.delete('Tongue0Joint_M')

        tongue_ctl_0 = pn('Tongue0_M')
        tongue_ctl_1 = pn('Tongue1_M')
        tongue_ctl_2 = pn('Tongue2_M')
        tongue_ctl_3 = pn('Tongue3_M')

        tongue_ctl_0.rename('Main_CTL_tongueA')
        tongue_ctl_1.rename('Main_CTL_tongueB')
        tongue_ctl_2.rename('Main_CTL_tongueC')
        tongue_ctl_3.rename('Main_CTL_tongueD')

        joint_list = list()
        for i, ctl in enumerate([tongue_ctl_0, tongue_ctl_1, tongue_ctl_2, tongue_ctl_3]):
            pm.select(cl=True)

            jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_joint')
            joint_list.append(jnt)
            jnt_zero = misc.zero_grp(jnt)

            pm.delete(pm.parentConstraint(ctl.getParent(), jnt_zero, mo=False))

            for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                pm.connectAttr(ctl.attr(atr), jnt.attr(atr), f=True)

            if i == 0:
                pm.parent(jnt_zero, jaw_driven_grp)
            else:

                pm.parent(jnt_zero, joint_list[i-1])

    def setup_eye_stuff(self):

        eye_ctl_grp = pm.group(em=True, n='Eye_controls')
        pm.parent(eye_ctl_grp, 'Head_constrained_grp')

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            # angle driver
            eye_driver = pm.group(em=True, n='Eye_driver_'+side+'_GRP')
            driver_pos = [4.386*side_mult, 105.087, 0.997]  # this you match to body rig eye L joint pos
            driver_rot = [180.0, -90.0, 0]

            pm.xform(eye_driver, t=driver_pos, ws=True)
            pm.xform(eye_driver, ro=driver_rot, os=True)

            eye_driver_zero = misc.zero_grp(eye_driver)

            driver = pm.group(em=True, n=eye_driver+'_driver')
            pm.parent(driver, eye_driver)
            pm.xform(driver, t=[4.587*side_mult, 105.118, 4.92],  ws=True)  # ws center of iris/pupil

            # # aim driven
            eye_driven = pm.group(em=True, n='Eye_driven_'+side+'_GRP')
            driven_pos = [2.832*side_mult, 105.661, -0.26]  # implied point around which the anim iris rotates.
            pm.xform(eye_driven, t=driven_pos, ws=True)
            eye_driven_zero = misc.zero_grp(eye_driven)

            aim = pm.aimConstraint(
                driver, eye_driven,
                aimVector=[0, 0, 1],
                upVector=[0, 1, 0],
                worldUpVector=[0, 1, 0],
                worldUpType='scene',
            )

            # eye_driven
            eye_driven_loc = pm.spaceLocator(n='Eye_driven_'+side)
            pm.hide(eye_driven_loc.getShape())
            pm.parent(eye_driven_loc, driver, r=True)

            eye_ctl_driven = pm.group(em=True, n='Eye_control_driven_'+side)
            pm.parent(eye_ctl_driven, eye_ctl_grp)
            for source_atr, target_atr in zip(['worldPosition[0].worldPositionX', 'worldPosition[0].worldPositionY', 'worldPosition[0].worldPositionZ'], ['tx', 'ty', 'tz']):
                pm.connectAttr(eye_driven_loc.getShape().attr(source_atr), eye_ctl_driven.attr(target_atr))

            ctl = control.create(
                'Main_CTL_iris_'+side,
                shape='circle',
                colorIdx=17,
                rotation=[90, 0, 0],
                zeroGroup=True
            )

            pm.delete(pm.parentConstraint(driver, ctl.getParent(), mo=False))
            pm.parent(ctl.getParent(), eye_ctl_driven)

            pm.xform(ctl.getParent(), ro=[0, 20*side_mult, 0], r=True, os=True)
            pm.xform(ctl.getShape().cv[:], s=[1.7, 1.7, 1.7], r=True, os=True)
            pm.xform(ctl.getShape().cv[:], t=[0, 0, 0.283559], r=True, os=True)


            # jnt
            pm.select(cl=True)
            jnt_offset = pm.group(em=True, n='eye_jnt_'+side+'_OFFSET')
            jnt_zero = pm.group(em=True, n='eye_jnt_'+side+'_ZERO')
            jnt = pm.joint(n='eye_jnt_'+side)
            pm.parent(jnt_offset, eye_driven, r=True)
            pm.parent(jnt_zero, jnt_offset, r=True)
            pm.delete(pm.parentConstraint(driver, jnt_offset, mo=False))
            pm.delete(pm.parentConstraint(driver, jnt_zero, mo=False))
            pm.parent(eye_driver_zero, self.EYE_STUFF)
            pm.parent(eye_driven_zero, 'Main_joints')

            for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                pm.connectAttr(ctl.getParent().attr(atr), jnt_zero.attr(atr))
                pm.connectAttr(ctl.attr(atr), jnt.attr(atr), f=True)

            # disconnect old sdks
            for sdk in [
                'asFaceBS_ctrlEyetranslateYNeg_'+side,
                'asFaceBS_ctrlEyetranslateXNeg_'+side,
                'asFaceBS_ctrlEyetranslateXPos_'+side,
                'asFaceBS_ctrlEyetranslateYPos_'+side,
            ]:
                pm.delete(sdk)

            # set new ones.
            face_bs = pn('face_bs_geo_blendShape')

            xpos_atr = 'ctrlEyetranslateXPos_'+side
            xneg_atr = 'ctrlEyetranslateXNeg_'+side
            ypos_atr = 'ctrlEyetranslateYPos_'+side
            yneg_atr = 'ctrlEyetranslateYNeg_'+side

            # xpos
            pm.setDrivenKeyframe(face_bs.attr(xpos_atr), v=0.0, cd=eye_driver.ry, dv=0.0)
            pm.setDrivenKeyframe(face_bs.attr(xpos_atr), v=1.0, cd=eye_driver.ry, dv=-60.0)

            # xneg
            pm.setDrivenKeyframe(face_bs.attr(xneg_atr), v=0.0, cd=eye_driver.ry, dv=0.0)
            pm.setDrivenKeyframe(face_bs.attr(xneg_atr), v=1.0, cd=eye_driver.ry, dv=60)

            # ypos
            pm.setDrivenKeyframe(face_bs.attr(ypos_atr), v=0.0, cd=eye_driver.rz, dv=0.0)
            pm.setDrivenKeyframe(face_bs.attr(ypos_atr), v=1.0, cd=eye_driver.rz, dv=-20.0)

            # yneg
            pm.setDrivenKeyframe(face_bs.attr(yneg_atr), v=0.0, cd=eye_driver.rz, dv=0.0)
            pm.setDrivenKeyframe(face_bs.attr(yneg_atr), v=1.0, cd=eye_driver.rz, dv=20.0)

            if side == 'L':
                ry = [-20, 60]
            else:
                ry = [-60, 20]

            pm.transformLimits(eye_driven, erx=[1, 1], rx=[-40, 40])
            pm.transformLimits(eye_driven, ery=[1, 1], ry=ry)

            pn('ctrlEye_'+side).tx.set(l=True)
            pn('ctrlEye_' + side).ty.set(l=True)

    def setup_region_controls(self):

        # groups
        top_grp = pm.group(em=True, n='Region', p=self.RIG_GRP)
        ctl_grp = pm.group(em=True, n='Region_controls', p=top_grp)
        jnt_grp = pm.group(em=True, n='Region_joints', p=top_grp)
        dnt_grp = pm.group(em=True, n='Region_doNotTouch', p=top_grp)
        constrain_to_head = pm.group(em=True, n='Region_constrainToHead')
        fol_grp = pm.group(em=True, n='Region_follicle_group', p=dnt_grp)
        pm.hide(dnt_grp)
        pm.parent(constrain_to_head, self.HEAD_CONSTRAINED_GRP)

        pm.delete(self.FACE_REGION_DRIVER)
        wrap.create(
            self.FACE_REGION_TRIDRIVER,
            self.FACE_BS_GEO,
            base=self.FACE_BS_GEO + 'Base',
            baseParent=self.DO_NOT_TOUCH_GRP
        )
        smooth = mesh.smooth(self.FACE_REGION_TRIDRIVER, divisions=1)

        pm.parent(self.FACE_REGION_TRIDRIVER, dnt_grp)

        # base jnt
        base_jnt_zero = pm.group(em=True, n='Region_base_jnt_ZERO', p=jnt_grp)
        jnt = pm.joint(n='Region_base_jnt')

        # create control follicles

        ctl_fol_map = dict()
        for ctl, vtx_idx in self.region_fol_dict.items():

            geo = self.FACE_REGION_TRIDRIVER
            ctl = pn(ctl)

            ctl.getShape().overrideEnabled.set(True)
            ctl.getShape().overrideColor.set(29)

            # create follicle
            fol = follicle.onSelected(selection=geo + '.vtx[' + str(vtx_idx) + ']', n=ctl + '_fol')
            fol.getShape().v.set(False)
            pm.parent(fol, fol_grp)
            fol.r.disconnect()
            fol.r.set([0, 0, 0])

            fol_offset = pm.spaceLocator(n=fol + '_OFFSET')
            pm.parent(fol_offset, fol)
            fol_offset.getShape().v.set(False)

            # setup ctl stuff
            ctl_zero = misc.zero_grp(ctl)
            pm.makeIdentity(ctl, n=0, s=1, r=1, t=1, apply=True, pn=1)
            pm.parent(ctl_zero, constrain_to_head)
            pm.move(ctl.cv[:], [0, 0, 0.3], r=True, os=True)

            pm.delete(pm.parentConstraint(ctl, fol_offset, mo=False))

            pm.connectAttr(fol_offset.getShape().worldPosition[0].worldPositionX, ctl_zero.tx, f=True)
            pm.connectAttr(fol_offset.getShape().worldPosition[0].worldPositionY, ctl_zero.ty, f=True)
            pm.connectAttr(fol_offset.getShape().worldPosition[0].worldPositionZ, ctl_zero.tz, f=True)

            # setup joint stuff
            jnt_zero = pm.group(em=True, n=ctl + '_jnt_ZERO')
            jnt = pm.joint(n=ctl + '_jnt')

            pm.parent(jnt_zero, jnt_grp)

            for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                pm.connectAttr(ctl_zero + '.' + atr, jnt_zero + '.' + atr)
                pm.connectAttr(ctl + '.' + atr, jnt + '.' + atr)

            ctl_fol_map[str(ctl)] = str(fol)

        # load skinweights for region controls.
        skin.import_from_file(
            geo=self.FACE_REGION_GEO,
            filepath='E:/destruna/assets/don_child/face/imports/face_region_geo_skinCluster_v001.wts'
        )
        pm.select(self.FACE_REGION_GEO)
        pm.skinPercent(self.FACE_REGION_GEO+'_skinCluster', normalize=True)
        skin.connect_prebind_matrixes('face_region_geo_skinCluster')

        # deforming on face controls
        stuck_grp = pm.group(em=True, n='Region_stuck_on_controls', p=ctl_grp)
        pm.hide(stuck_grp)

        for ctl in self.region_fol_dict.keys():
            ctl = pn(ctl)

            # fol base position
            fol = pn(ctl_fol_map[str(ctl)])
            base_grp = pm.group(em=True, n=fol+'_base_pos', p=fol.getParent())
            pm.delete(pm.parentConstraint(fol, base_grp, mo=False))

            ctl_def = curve.copy(curve=ctl, n=ctl.replace('_CTL_', '_') + '_def')
            pm.parent(ctl_def, stuck_grp)
            wrap.create(ctl_def, self.FACE_RIG_GEO, base=self.FACE_RIG_GEO + 'Base', baseParent=dnt_grp)

            bs = blendshape.apply(ctl_def, ctl, dv=1)
            # bs.origin.set(0)

            clus, clus_handle = pm.cluster(ctl_def, n=ctl_def + '_cluster')
            pm.parent(clus_handle, stuck_grp)
            pm.hide(ctl_def.getShape())

            # subtract position of control from original position
            mult = pm.createNode('multiplyDivide', n=ctl + '_mult')
            mult.operation.set(2)
            pm.connectAttr(ctl.t, mult.input1, f=True)
            mult.input2.set(-1, -1, -1)


            # fol offset pma
            fol_offset_pma = pm.createNode('plusMinusAverage', n=ctl+'_pma1')
            pm.connectAttr(base_grp.t, fol_offset_pma.input3D[0], f=True)
            pm.connectAttr(fol.getShape().outTranslate, fol_offset_pma.input3D[1], f=True)
            fol_offset_pma.operation.set(2)

            # position of control to fol offset
            ctl_pos_add = pm.createNode('plusMinusAverage', n=ctl+'_addFinal')
            pm.connectAttr(mult.output, ctl_pos_add.input3D[0], f=True)
            pm.connectAttr(fol_offset_pma.output3D, ctl_pos_add.input3D[1], f=True)

            pm.connectAttr(ctl_pos_add.output3D, clus_handle.t, f=True)

        pm.parent(self.FACE_REGION_GEO, self.DO_NOT_TOUCH_GRP)

        # eye hack to connect region controls to eye stuff
        for side in 'LR':

            # create locator to get a ws output of the region control
            region_joint = 'Region_CTL_eye_'+side+'_jnt'
            region_loc = pm.spaceLocator(n=region_joint+'_loc')
            pm.hide(region_loc.getShape())
            pm.parent(region_loc, region_joint, r=True)

            # create group driven by locator.
            eye_region_driven = pm.group(em=True, n='Eye_regionDriven_'+side+'_GRP')
            pm.delete(pm.parentConstraint(region_loc, eye_region_driven, mo=False))

            for source_atr, target_atr in zip(['worldPosition[0].worldPositionX', 'worldPosition[0].worldPositionY', 'worldPosition[0].worldPositionZ'], ['tx', 'ty', 'tz']):
                pm.connectAttr(region_loc.getShape().attr(source_atr), eye_region_driven.attr(target_atr), f=True)

            # constrain to tweak.
            pm.parentConstraint(eye_region_driven, 'Eye_driver_'+side+'_GRP_ZERO', skipRotate=['x', 'y', 'z'], mo=True)

            pm.parent(eye_region_driven, self.EYE_STUFF)

    def setup_tweak_controls(self):

        # create controls and follicles.
        self.create_tweaks_controls_setup(self.tweak_controls_setup)

        # base jnt
        base_jnt_zero = pm.group(em=True, n='Tweaks_base_jnt_ZERO', p='Tweaks_joints')
        jnt = pm.joint(n='Tweaks_base_jnt')

        # parenting
        pm.parent('Tweaks', self.RIG_GRP)
        dnt_grp = pm.group(em=True, n='Tweaks_doNotTouch', p='Tweaks')
        pm.hide(dnt_grp)

        # skin bind
        build.skin_bind({'geo': self.FACE_TWEAKS_GEO, 'filepath': 'E:/destruna/assets/don_child/face/imports/face_tweaks_geo_skinCluster_v001.wts'})
        skin.connect_prebind_matrixes(self.FACE_TWEAKS_GEO+'_skinCluster')
        pm.select(self.FACE_TWEAKS_GEO, r=True)
        pm.skinPercent(self.FACE_TWEAKS_GEO+'_skinCluster', normalize=True)

        # parent branched geo
        pm.parent(self.FACE_TWEAKS_GEO, dnt_grp)
        pm.parent(self.FACE_TWEAKS_DRIVER, dnt_grp)

        pm.parent('Tweaks_constrainToHead', self.HEAD_CONSTRAINED_GRP)

    def create_tweaks_controls_setup(self, data):
        """
        This function creates groups of tweak controls on a mesh

        Args:
            data: [{
                'name': (str)    name of joint group
                'tweak_data': [
                    {
                        'prefix': 'Eye'
                        'description': 'tweakLowerA'
                        'side': 'L'
                        'vtx': 'face_tweak.vtx[1]'
                    },
                ]
            }]
        """
        tweaks_build_data = build.check_data_type(function_name='tweaks_controls_setup', data=data)

        # if there is data
        if tweaks_build_data:

            # iterate through list
            for tweak_group in tweaks_build_data:

                # joint
                tweak_data = tweak_group['tweak_data']
                name = tweak_group['name']

                # group creation
                top_grp = pm.group(em=True, n=name)
                control_group = pm.group(em=True, n=name + '_controls', p=top_grp)
                joint_group = pm.group(em=True, n=name + '_joints', p=top_grp)
                constrain_to_head = pm.group(em=True, n=name + '_constrainToHead')

                # iterating through tweak data to create ctls etc.
                for d in tweak_data:

                    prefix = d['prefix']
                    description = d['description']
                    side = d['side']
                    vtx = d['vtx']
                    orient = d['orient']

                    # follicle creation
                    fol_name = '_'.join([prefix, description, side, 'fol'])
                    print [vtx, pn(vtx)]
                    fol = follicle.onSelected(selection=vtx, n=fol_name)
                    # if not orient:
                    fol.r.disconnect()
                    fol.r.set([0, 0, 0])
                    pm.hide(fol.getShape())

                    pm.parent(fol, control_group)

                    # create group to be driven by fol under head joint
                    ctl_parent_name = '_'.join([prefix, description, side, 'driven'])
                    ctl_parent = pm.group(em=True, n=ctl_parent_name, p=constrain_to_head)
                    pm.connectAttr(fol.t, ctl_parent.t)

                    # control creation
                    tweak_data = tweak.build_control_at_component(
                        component=vtx,
                        prefix=prefix,
                        description=description,
                        suffix=side,
                        shape='circle',
                        size=0.2,
                        rotation=[90, 0, 0],
                        orientToSurface=orient
                    )
                    ctl = pn(tweak_data['ctl'])
                    ctl.getShape().overrideEnabled.set(1)
                    ctl.getShape().overrideColor.set(31)

                    pm.xform(ctl.cv[:], t=[0, 0, .25], r=True, os=True)
                    pm.parent(tweak_data['ctl_zero'], ctl_parent)
                    pm.parent(tweak_data['jnt_zero'], fol)

                    pm.delete(tweak_data['jnt_zero'] + '_parentConstraint1')
                    pm.delete(tweak_data['jnt_zero'] + '_scaleConstraint1')
                    pm.delete(tweak_data['jnt'] + '_parentConstraint1')
                    pm.delete(tweak_data['jnt'] + '_scaleConstraint1')

                    for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                        pm.connectAttr(tweak_data['ctl_zero'] + '.' + atr, tweak_data['jnt_zero'] + '.' + atr)
                        pm.connectAttr(tweak_data['ctl'] + '.' + atr, tweak_data['jnt'] + '.' + atr)

    def prepare_brow(self):

        # functions for control setup
        self.brow_ctls()

        # skin weights
        skin.import_from_file(filepath='E:/destruna/assets/don_child/face/imports/brow_wrapper_skinCluster_v001.wts', geo='brow_wrapper')
        mesh.smooth(geo='brow_wrapper', divisions=1)
        #
        # create shrinkwrap
        shrinkwrap = deformer.create_shrinkwrap(wrapped='brow_wrapper', wrapper='head_geo')

    def brow_ctls(self):

        ctl_data_list = [
            {'name': 'Main_CTL_browA_<s>', 'translate': [1.579, 108.182, 8.096]},
            {'name': 'Main_CTL_browB_<s>', 'translate': [3.483, 109.433, 7.716]},
            {'name': 'Main_CTL_browC_<s>', 'translate': [5.453, 110.058, 6.883]},
            {'name': 'Main_CTL_browD_<s>', 'translate': [7.272, 110.456, 5.567]},
            {'name': 'Main_CTL_browE_<s>', 'translate': [8.695, 109.706, 3.835]},
        ]

        # groups
        constrain_to_head = pm.group(em=True, n='Brows_constrainToHead')
        pm.parent(constrain_to_head, 'Head_constrained_grp')
        brow_joints = pm.group(em=True, n='Brow_joints_grp', p='Main_joints')

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            # create follicles
            fol_list = list()
            for x, data in enumerate(ctl_data_list):

                ctl_name = data['name'].replace('<s>', side)
                translate = [data['translate'][0] * side_mult, data['translate'][1], data['translate'][2]]

                uval, vval = cpom.closest_uv_on_geo_from_position(position=translate, geo='face_rig_geo')
                fol = follicle.create(geo='face_rig_geo', n=ctl_name+'_fol', setUV=True, uvVal=(uval, vval))
                fol_list.append(fol)
                fol.r.disconnect()
                fol.r.set(0, 0, 0)
                fol.getShape().v.set(0)

            pm.parent(fol_list, brow_joints)

            # create control
            ctl_list = list()
            loc_list = list()
            for x, data in enumerate(ctl_data_list):

                ctl_name = data['name'].replace('<s>', side)
                translate = [data['translate'][0]*side_mult, data['translate'][1], data['translate'][2]]

                ctl = control.create(
                    ctl_name,
                    shape='circle',
                    colorIdx=17,
                    zeroGroup=True,
                    translation=[0, 5, 0],
                    rotation=[90, 0, 0],
                    size=0.1
                )

                ctl_zero = ctl.getParent()
                pm.xform(ctl_zero, t=translate, os=True)
                ctl_list.append(ctl)

                constrained_grp = pm.group(em=True, n=ctl_name+'_driven')
                fol = fol_list[x]

                pm.connectAttr(fol.t, constrained_grp.t)
                pm.parent(ctl_zero, constrained_grp)
                pm.parent(constrained_grp, constrain_to_head)

                loc = pm.spaceLocator(n=ctl_name.replace('_CTL_', '_')+'_loc')
                loc.getShape().v.set(False)
                loc_list.append(loc)
                pm.parent(loc, fol)
                for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                    pm.connectAttr(ctl.attr(atr), loc.attr(atr))

            for x, loc in enumerate(loc_list):

                if x == 0:
                    aim_loc = loc_list[x+1]
                    aim_vector = [1 * side_mult, 0, 0]
                else:
                    aim_loc = loc_list[x-1]
                    aim_vector = [1 * side_mult, 0, 0]

                pm.select(loc, r=True)
                jnt = pm.joint(n=loc.replace('_loc', '_jnt'))
                print jnt
                pm.parent(jnt, loc)

                fol = fol_list[x]

                pm.aimConstraint(
                    aim_loc, jnt,
                    aimVector=aim_vector,
                    upVector=[0, 0, 1],
                    worldUpType='objectRotation',
                    worldUpVector=[0, 0, 1],
                    worldUpObject=fol
                )

    def prepare_lash(self):

        lash_curve_edges = {
            'L': ['head_geo.e[248]', 'head_geo.e[249]', 'head_geo.e[286]', 'head_geo.e[287]', 'head_geo.e[658]', 'head_geo.e[659]', 'head_geo.e[790]', 'head_geo.e[791]', 'head_geo.e[798]', 'head_geo.e[799]', 'head_geo.e[804]', 'head_geo.e[805]', 'head_geo.e[826]', 'head_geo.e[827]', 'head_geo.e[833]', 'head_geo.e[842]', 'head_geo.e[843]', 'head_geo.e[846]', 'head_geo.e[847]', 'head_geo.e[1314]', 'head_geo.e[1315]'],
            'R': ['head_geo.e[2918]', 'head_geo.e[2919]', 'head_geo.e[2954]', 'head_geo.e[2955]', 'head_geo.e[3298]', 'head_geo.e[3299]', 'head_geo.e[3428]', 'head_geo.e[3429]', 'head_geo.e[3432]', 'head_geo.e[3433]', 'head_geo.e[3438]', 'head_geo.e[3439]', 'head_geo.e[3462]', 'head_geo.e[3463]', 'head_geo.e[3468]', 'head_geo.e[3478]', 'head_geo.e[3479]', 'head_geo.e[3486]', 'head_geo.e[3487]', 'head_geo.e[3914]', 'head_geo.e[3915]'],
        }

        for side in 'LR':

            # create wire curve
            wire_curve_edges = lash_curve_edges[side]
            pm.select(wire_curve_edges, r=True)
            wire_crv, polyEdgeToCurve = pm.polyToCurve(n='eye_lash_'+side+'_crv', form=2, degree=3, conformToSmoothMeshPreview=1)
            pm.parent(wire_crv, self.LASH_STUFF)
            eye_lash_wrapper = 'eye_lash_'+side+'_wrapper'

            # create wire deformer
            deformer.create_wire(geo=eye_lash_wrapper, curve=wire_crv, dropoff_distance=2.2, rotation=0.5)
            pm.parent(eye_lash_wrapper, wire_crv+'BaseWire', self.LASH_STUFF)

            # mesh smooth
            wrapper_smooth = mesh.smooth(eye_lash_wrapper, divisions=1, keepBorder=False)

            # wrap to wrapper
            eye_lash_geo = pn('eyeLash_'+side+'_geo')
            wrap.create(eye_lash_geo, eye_lash_wrapper, baseParent=self.LASH_STUFF)

            deformer.create_deltamush(eye_lash_geo, smoothing_iterations=3, smoothing_step=0.75, distance_weight=1)


    def do_skin_to_joint(self):

        for joint, geo_list in self.skin_to_joint.items():
            for geo in geo_list:
                pm.skinCluster(joint, geo, n=geo+'_skinCluster', tsb=True)

    # ==================================================================================================================
    def go(self):
        print '='*80
        print 'Starting '+str(self.who)+' Rig Build.'
        mc.file(new=True, f=True)
        self.build_contents()
        print 'Finished!'

    def build_contents(self):

        build.imports(self.imports)
        build.groups(self.groups)

        self.set_up_face_rig_geo()
        self.setup_head_constrained_groups()
        pm.parent(self.FACE_GEO_LIST, self.GEO_GRP)
        pm.parent(self.FACE_RIG_GEO, self.DO_NOT_TOUCH_GRP)
        self.adjust_labels()
        self.setup_blendshape_rig()
        self.setup_mouth_controls()
        self.setup_eye_stuff()

        # build region branch with controls
        mesh.branch_geo(self.FACE_BS_GEO, n=self.FACE_REGION_GEO)
        self.setup_region_controls()

        # build tweak branch with controls
        mesh.branch_geo(self.FACE_REGION_GEO, n=self.FACE_TWEAKS_GEO)
        self.setup_tweak_controls()

        # pm.parent(self.FACE_GEO, self.GEO)
        blendshape.apply(self.FACE_TWEAKS_GEO, self.FACE_RIG_GEO, dv=1)

        self.prepare_brow()
        self.prepare_lash()

        # GEO ATTACHMENTS (this stuff will be lona specific) ===========================================================
        # establish wrap to face rig.
        blendshape.apply(self.FACE_RIG_GEO, self.HEAD_GEO, dv=1)

        build.wrapDeformer_create(self.wrapDeformer_create)
        self.do_skin_to_joint()
        # pm.parent('face_geo_wrapped', self.DNT)
        # self.upper_lash_attach()
        #
        build.skin_bind(self.skin_bind)
        pm.delete([
            'AllSet1',
            'Sets',
            'AllSet',
            'FaceAllSet',
            'FaceAreas',
            'FaceControlSet',
            'eyeLidArea',
            'lipArea',
            'lipFalloffArea',
        ])
        self.do_cleanup()


def detach_and_delete_bodyrig():

    # prep freshly skinCluster -> blendshape AS rig.
    pm.parent('FaceGroup', w=True)

    pm.delete([
        'FaceMotionSystem_orientConstraint1',
        'FaceMotionSystem_pointConstraint1',

        'Customcontrols',
        'Regionscontrols',
        'Aimcontrols',

        'SideReverse_L',
        'SideReverse_R',

        'EyeAimFollowHead_parentConstraint1',
        'FaceDeformationFollowHead_orientConstraint1',
        'FaceDeformationFollowHead_pointConstraint1',
        'LidWireWS_parentConstraint1',
        'LidWireWS_scaleConstraint1',
        'LipFollicles_parentConstraint1',
        'MainAndHeadScaleMultiplyDivide',
    ])

    pm.parent('FaceJoint_M', 'FaceDeformationSystem')
    pm.delete('Group')
    pm.rename('FaceGroup', 'Main')

    pm.rename('head_geo', 'face_bs_geo')
    pm.rename('asFaceBS', 'face_bs_geo_blendShape')

    pm.delete([
        'eyeLash_L_geo',
        'eyeLash_R_geo',
        'eye_L_geo',
        'eye_R_geo',
        'eye_ring_L_geo',
        'eye_ring_R_geo',
        'eye_tmp_L',
        'eye_tmp_R',
        'eyebrow_L_geo',
        'eyebrow_R_geo',
        'teethLower_geo',
        'teethUpper_geo',
        'tongue_geo',
    ])








"""
from jb_destruna.rigs.e_don import don_child_faceRig
reload(don_child_faceRig)
face = don_child_faceRig.FaceRig()
face.go()
"""