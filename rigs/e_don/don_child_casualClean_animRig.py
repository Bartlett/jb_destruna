from jb_destruna.maya.core import build
import maya.cmds as mc
import pymel.core as pm

from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import skin
from jb_destruna.maya.internal import control

from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn
from jb_destruna.maya.utility.misc import zero_grp
from jb_destruna.tools import path as path_tools
from jb_destruna.maya.rig_modules import fk_chain as fk
from jb_destruna.maya.internal import wrap

reload(path_tools)

from jb_destruna.rigs.e_don.import_paths import don_import_paths as imp

reload(build)
reload(rig)


class Rig(rig.Rig):

    ASSET = 'don'
    VARIANT = 'child_clean'

    FACE_NS = 'face'

    IMPORT_LIST = ['office', 'jordan', 'damien']
    IMPORT_KEY = IMPORT_LIST[2]

    # BODY
    BODY_GEO = 'body_geo'
    HAIR_GEO = 'hair_geo'

    # FACE
    FACE_GEO = 'head_geo'
    FACE_GEO_EDIT_SHAPE_1 = 'head_geo_revised'
    PUPIL_GEO_L = 'eye_L_geo'
    PUPIL_GEO_R = 'eye_R_geo'
    SCLERA_GEO_L = 'sclera_L_geo'
    SCLERA_GEO_R = 'sclera_R_geo'
    EYELASH_L_GEO = 'eyeLash_L_geo'
    EYELASH_R_GEO = 'eyeLash_R_geo'
    EYEBROW_GEO_L = 'eyebrow_L_geo'
    EYEBROW_GEO_R = 'eyebrow_R_geo'
    TOPTEETH_GEO = 'teethUpper_geo'
    BOTTOMTEETH_GEO = 'teethLower_geo'
    TONGUE_GEO = 'tongue_geo'

    # CLOTHES
    CLOTHES_TOP_GEO = 'top_geo'
    CLOTHES_BELT_GEO = 'belt_geo'
    CLOTHES_PANTS_GEO = 'pants_geo'
    CLOTHES_SCARF_GEO = 'scaf_geo'
    CLOTHES_GLOVES_GEO = 'gloves_geo'
    ClOTHES_ARMBAND_GEO = 'armband_geo'
    CLOTHES_SHOES_GEO = 'shoes_geo'
    CLOTHES_FOOTWRAP_GEO = 'footWrap_geo'


    # skin geo
    TOP_SKIN_GEO = 'top_skin_geo'
    PANTS_SKIN_GEO = 'pants_skin_geo'
    BELT_SKIN_GEO = 'belt_skin_geo'

    FACE_GEO_LIST = [
        FACE_GEO,
        PUPIL_GEO_L,
        PUPIL_GEO_R,
        SCLERA_GEO_L,
        SCLERA_GEO_R,
        EYELASH_L_GEO,
        EYELASH_R_GEO,
        EYEBROW_GEO_L,
        EYEBROW_GEO_R,
        TOPTEETH_GEO,
        BOTTOMTEETH_GEO,
        TONGUE_GEO,
    ]

    GEO_LIST = [
        BODY_GEO,
        HAIR_GEO,
        CLOTHES_TOP_GEO,
        CLOTHES_BELT_GEO,
        CLOTHES_PANTS_GEO,
        CLOTHES_SCARF_GEO,
        CLOTHES_GLOVES_GEO,
        ClOTHES_ARMBAND_GEO,
        CLOTHES_SHOES_GEO,
        CLOTHES_FOOTWRAP_GEO,
    ]

    GEO_LIST += FACE_GEO_LIST


    TASSEL_A_BASE_CURVE = 'tassel_a_baseCurve'
    TASSEL_A_UP_CURVE = 'tassel_a_upCurve'

    TASSEL_B_BASE_CURVE = 'tassel_b_baseCurve'
    TASSEL_B_UP_CURVE = 'tassel_b_upCurve'

    def __init__(self):
        super(Rig, self).__init__()

        self.who = self.ASSET

        #iii
        self.imports = [
            # GEO
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/01_model/don_child_casualClean_model_24_DMR_MASTER.mb',
             'group': self.DELETE_GRP},

            # LOW
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/skin_geo/donChild_skinGeo_02.mb',
                'group': self.DELETE_GRP},

            # GUIDES
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/guides/armband/don_child_casualClean_armband_guides_01.mb',
             'group': self.DELETE_GRP},

            # GUIDES
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/tassel_guide_curves/tassel_curves_03.mb',
                'group': self.DELETE_GRP},

            # AS RIG
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/asRig/don_child_casualClean_asRig_06.mb'},

            # AS FACE RIG
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/asFaceRig/don_child_faceRig_v002.mb',
             'namespace': self.FACE_NS},

        ]


        sk_path = 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_mocap/02_rig/imports/skinClusters/'

        self.skin_weight_imports = {

            'body_geo': sk_path + 'body_geo/body_geo_003_skinCluster.json',
            'head_geo': sk_path + 'head_geo/head_geo_001_skinCluster.json',

            'hair_geo': sk_path + 'hair_geo/hair_geo_001_skinCluster.json',

            'Eye_L': sk_path + 'eye_L_geo/eye_L_geo_001_skinCluster.json',
            'Eye_R': sk_path + 'eye_R_geo/eye_R_geo_001_skinCluster.json',

            'eyebrow_L_geo': sk_path + 'eyebrow_L_geo/eyebrow_L_geo_001_skinCluster.json',
            'eyebrow_R_geo': sk_path + 'eyebrow_R_geo/eyebrow_R_geo_001_skinCluster.json',

            'sclera_L_geo': sk_path + 'sclera_L_geo/sclera_L_geo_001_skinCluster.json',
            'sclera_R_geo': sk_path + 'sclera_R_geo/sclera_R_geo_001_skinCluster.json',

            'earplug_geo': sk_path + 'earplug_geo/earplug_geo_001_skinCluster.json',
            'earring_geo': sk_path + 'earring_geo/earring_geo_001_skinCluster.json',

            'belt_geo': sk_path + 'belt_geo/belt_geo_002_skinCluster.json'
        }

        skin_weight_prefix = 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/skinClusters/'

        self.skin_bind = [

            # BODY
            {'geo': self.BODY_GEO, 'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/skinClusters/body_geo/body_geo_skinCluster_07.json',
             'import_module':'skin_ng'},
            {'geo': self.HAIR_GEO, 'filepath': skin_weight_prefix + self.HAIR_GEO + '/hair_geo_skincluster_01.json',
             'import_module': 'skin_ng'},

            # # FACE
            {'geo': self.FACE_GEO, 'filepath': skin_weight_prefix + self.FACE_GEO + '/head_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            #
            {'geo': self.PUPIL_GEO_L, 'filepath': skin_weight_prefix + self.PUPIL_GEO_L + '/eye_L_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.PUPIL_GEO_R, 'filepath': skin_weight_prefix + self.PUPIL_GEO_R + '/eye_R_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_L_GEO, 'filepath': skin_weight_prefix + self.EYELASH_L_GEO + '/eyeLash_L_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_R_GEO, 'filepath': skin_weight_prefix + self.EYELASH_R_GEO + '/eyeLash_R_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYEBROW_GEO_L, 'filepath': skin_weight_prefix + self.EYEBROW_GEO_L + '/eyebrow_L_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYEBROW_GEO_R, 'filepath': skin_weight_prefix + self.EYEBROW_GEO_R + '/eyebrow_R_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.TOPTEETH_GEO, 'filepath': skin_weight_prefix + self.TOPTEETH_GEO + '/teethUpper_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.BOTTOMTEETH_GEO, 'filepath': skin_weight_prefix + self.BOTTOMTEETH_GEO + '/teethLower_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.TONGUE_GEO, 'filepath': skin_weight_prefix + self.TONGUE_GEO + '/tongue_geo_skincluster_01.json',
             'import_module': 'skin_ng'},

            {'geo': self.ClOTHES_ARMBAND_GEO,
             'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/skinClusters/armband_geo/armband_geo_skinCluster_03.json',
             'import_module': 'skin_ng'},
            {'geo': self.CLOTHES_SCARF_GEO,
             'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/skinClusters/scaf_geo/scaf_geo_skin_cluster_04.json',
             'import_module': 'skin_ng'},
            {'geo': self.CLOTHES_PANTS_GEO,
             'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/skinClusters/pants_geo/pants_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.CLOTHES_GLOVES_GEO,
             'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/skinClusters/gloves_geo/gloves_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.CLOTHES_FOOTWRAP_GEO,
             'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/skinClusters/footWrap_geo/footWrap_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.CLOTHES_SHOES_GEO,
             'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/skinClusters/shoes_geo/shoes_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},

            # SKIN GEO
            {'geo': self.TOP_SKIN_GEO,
             'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/skinClusters/top_skin_geo/top_skin_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            # {'geo': self.PANTS_SKIN_GEO,
            #  'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/skinClusters/pants_skin_geo/pants_skin_geo_skinCluster_01.json',
            #  'import_module': 'skin_ng'},
            {'geo': self.BELT_SKIN_GEO,
             'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_child_casualClean/02_rig/imports/skinClusters/belt_skin_geo/belt_skin_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
        ]

        self.skin_copy = [
            {'source': self.TOP_SKIN_GEO, 'target': self.CLOTHES_TOP_GEO},
            # {'source': self.PANTS_SKIN_GEO, 'target': self.CLOTHES_PANTS_GEO},
            {'source': self.BELT_SKIN_GEO, 'target': self.CLOTHES_BELT_GEO},

        ]

        self.groups = [
            {'g': self.TOP_GRP, 'p': None, 'v': True},
            {'g': self.RIG_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.GEO_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.DNT_GRP, 'p': self.TOP_GRP, 'v': False},
        ]

    def modify_as_rig(self):

        # good stuff happens here.
        super(Rig, self).modify_as_rig()

        # localized stuff happens here --------------
        pn('Master_CTL_Main').bendVis.set(0)

        # lock vis ctls
        for ctl in self.AS_CTL_LIST_NEW:
            pn(ctl).v.set(k=False, cb=False, l=True)

        # attributes on master
        pm.setAttr('Master_CTL_Main.jointVis', 0)
        attribute.add('Master_CTL_Main', n='geoVis', type='bool', dv=1, cb=True, k=False)
        attribute.add('Master_CTL_Main', n='geoLock', type='bool', dv=1, cb=True, k=False)

        pm.connectAttr('Master_CTL_Main.geoLock', 'Geometry.overrideEnabled')
        pm.setAttr('Geometry.overrideDisplayType', 2)

        pm.connectAttr('Master_CTL_Main.geoVis', 'Geometry.v')

        pm.move('Spine_CTL_HipSwingerOffset_M', [0, -10, -20], r=True, os=True)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            pm.move('Arm_CTL_FKScapula_'+side+'.cv[:]', [-5*side_mult, 0, 0], r=True, os=True)
            pm.scale('Arm_CTL_FKShoulder_'+side+'.cv[:]', [0.7, 0.7, 0.7], r=True, os=True)
            pm.scale('Arm_CTL_FKWrist_' + side + '.cv[:]', [0.8, 0.8, 0.8], r=True, os=True)


    def build_hair_controls_old(self):

        hair_guides_data = {
            'fringe_L': {
                'guides': ['Hair_CTL_fringe_0_L_guide', 'Hair_CTL_fringe_1_L_guide', 'Hair_CTL_fringe_2_L_guide'],
                'scales': [6, 5, 3],
                'colorIdx': 18,
            },
            'sideA_L': {
                'guides': ['Hair_CTL_sideA_0_L_guide', 'Hair_CTL_sideA_1_L_guide', 'Hair_CTL_sideA_2_L_guide'],
                'scales': [5, 5, 4],
                'colorIdx': 18,
            },
            'cheek_L': {
                'guides': ['Hair_CTL_cheek_0_L_guide', 'Hair_CTL_cheek_1_L_guide', 'Hair_CTL_cheek_2_L_guide', 'Hair_CTL_cheek_3_L_guide'],
                'scales': [3, 2, 2, 2],
                'colorIdx': 18,
            },
            'tail': {
                'guides': ['Hair_CTL_tail_0_guide', 'Hair_CTL_tail_1_guide', 'Hair_CTL_tail_2_guide', 'Hair_CTL_tail_3_guide', 'Hair_CTL_tail_4_guide', 'Hair_CTL_tail_5_guide', 'Hair_CTL_tail_6_guide'],
                'scales': [8, 9, 10, 11, 10, 8, 4],
                'colorIdx': 23,
            },
            'fringeA_R': {
                'guides': ['Hair_CTL_fringeA_0_R_guide', 'Hair_CTL_fringeA_1_R_guide', 'Hair_CTL_fringeA_2_R_guide', 'Hair_CTL_fringeA_3_R_guide'],
                'scales': [6, 5, 3, 3],
                'colorIdx': 20,
            },
            'fringeB_R': {
                'guides': ['Hair_CTL_fringeB_0_R_guide', 'Hair_CTL_fringeB_1_R_guide', 'Hair_CTL_fringeB_2_R_guide'],
                'scales': [6, 6, 4],
                'colorIdx': 20,
            },
            'sideA_R': {
                'guides': ['Hair_CTL_sideA_0_R_guide', 'Hair_CTL_sideA_1_R_guide', 'Hair_CTL_sideA_2_R_guide'],
                'scales': [4, 4, 3],
                'colorIdx': 20,
            },
            'sideB_R': {
                'guides': ['Hair_CTL_sideB_0_R_guide', 'Hair_CTL_sideB_1_R_guide', 'Hair_CTL_sideB_2_R_guide', 'Hair_CTL_sideB_3_R_guide'],
                'scales': [6, 7, 5, 3],
                'colorIdx': 20,
            },
            'sideC_R': {
                'guides': ['Hair_CTL_sideC_0_R_guide', 'Hair_CTL_sideC_1_R_guide', 'Hair_CTL_sideC_2_R_guide', 'Hair_CTL_sideC_3_R_guide'],
                'scales': [5, 4, 4, 2],
                'colorIdx': 20,
            },
            'cheek_R': {
                'guides': ['Hair_CTL_cheek_0_R_guide', 'Hair_CTL_cheek_1_R_guide', 'Hair_CTL_cheek_2_R_guide', 'Hair_CTL_cheek_3_R_guide'],
                'scales': [3, 3, 2, 2],
                'colorIdx': 20,
            }
        }

        # Heirarchy
        jumper_grp = pm.group(em=True, n='Hair', p='Master_CTL_Main')
        jumper_ctl_grp = pm.group(em=True, n='Hair_controls', p=jumper_grp)
        jumper_jnt_grp = pm.group(em=True, n='Hair_joints', p=jumper_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', jumper_jnt_grp.v)

        for label, guide_data in hair_guides_data.items():

            guides = guide_data.get('guides')
            scales = guide_data.get('scales')
            color_idx = guide_data.get('colorIdx')

            ctl_list = list()
            jnt_list = list()
            for i, guide in enumerate(guides):

                # control
                ctl = control.create(
                    guide.replace('_guide', ''),
                    shape='square',
                    colorIdx=color_idx,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)
                pm.delete(pm.parentConstraint(guide, ctl.getParent(), mo=False))
                pm.scale(ctl.cv[:], [scales[i], 1, scales[i]], r=True, os=True)
                ctl.v.set(k=False, cb=False, l=True)

                # joint
                pm.select(cl=True)
                jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_jnt')
                jnt_list.append(jnt)
                pm.parentConstraint(ctl, jnt, mo=False)
                pm.scaleConstraint(ctl, jnt, mo=False)

                # parenting
                if i == 0:
                    pm.parent(ctl.getParent(), jumper_ctl_grp)
                    pm.parentConstraint('Head_Head_joint_M', ctl.getParent(), mo=True)
                    pm.parent(jnt, jumper_jnt_grp)
                else:
                    pm.parent(ctl.getParent(), ctl_list[i-1])
                    pm.parent(jnt, jnt_list[i-1])

    def build_hair_controls(self):

        hair_guides_data = {
            # 'fringe_L': {
            #     'guides': ['Hair_CTL_fringe_0_L_guide', 'Hair_CTL_fringe_1_L_guide', 'Hair_CTL_fringe_2_L_guide'],
            #     'scales': [6, 5, 3],
            #     'colorIdx': 18,
            # },
            # 'sideA_L': {
            #     'guides': ['Hair_CTL_sideA_0_L_guide', 'Hair_CTL_sideA_1_L_guide', 'Hair_CTL_sideA_2_L_guide'],
            #     'scales': [5, 5, 4],
            #     'colorIdx': 18,
            # },
            # 'cheek_L': {
            #     'guides': ['Hair_CTL_cheek_0_L_guide', 'Hair_CTL_cheek_1_L_guide', 'Hair_CTL_cheek_2_L_guide', 'Hair_CTL_cheek_3_L_guide'],
            #     'scales': [3, 2, 2, 2],
            #     'colorIdx': 18,
            # },
            'strand_L': {
                'guides': ['Hair_CTL_0_L_guide',
                           'Hair_CTL_1_L_guide',
                           'Hair_CTL_2_L_guide',
                           'Hair_CTL_3_L_guide',
                           'Hair_CTL_4_L_guide',
                           'Hair_CTL_5_L_guide'],
                'scales': [5, 5, 5, 5, 3, 3],
                'colorIdx': 18,
            },

            'strand_R': {
                'guides': ['Hair_CTL_0_R_guide',
                           'Hair_CTL_1_R_guide',
                           'Hair_CTL_2_R_guide',
                           'Hair_CTL_3_R_guide',
                           'Hair_CTL_4_R_guide',
                           'Hair_CTL_5_R_guide'],
                'scales': [5, 5, 5, 5, 3, 3],
                'colorIdx': 18,
            },
        }

        # Heirarchy
        hair_grp = pm.group(em=True, n='Hair', p='Master_CTL_Main')
        hair_ctl_grp = pm.group(em=True, n='Hair_controls', p=hair_grp)
        hair_jnt_grp = pm.group(em=True, n='Hair_joints', p=hair_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', hair_jnt_grp.v)

        for label, guide_data in hair_guides_data.items():

            guides = guide_data.get('guides')
            scales = guide_data.get('scales')
            color_idx = guide_data.get('colorIdx')

            ctl_list = list()
            jnt_list = list()
            for i, guide in enumerate(guides):

                # control
                ctl = control.create(
                    guide.replace('_guide', ''),
                    shape='square',
                    colorIdx=color_idx,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)
                pm.delete(pm.parentConstraint(guide, ctl.getParent(), mo=False))
                pm.scale(ctl.cv[:], [scales[i], 1, scales[i]], r=True, os=True)
                ctl.v.set(k=False, cb=False, l=True)

                # joint
                pm.select(cl=True)
                jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_jnt')
                jnt_list.append(jnt)
                pm.parentConstraint(ctl, jnt, mo=False)
                pm.scaleConstraint(ctl, jnt, mo=False)

                # parenting
                if i == 0:
                    pm.parent(ctl.getParent(), hair_ctl_grp)
                    pm.parentConstraint('Head_Head_joint_M', ctl.getParent(), mo=True)
                    pm.parent(jnt, hair_jnt_grp)
                else:
                    pm.parent(ctl.getParent(), ctl_list[i-1])
                    pm.parent(jnt, jnt_list[i-1])

    def wrap_scleras(self):

        wrap.create(
            self.SCLERA_GEO_L,
            self.PUPIL_GEO_L
        )

        wrap.create(
            self.SCLERA_GEO_R,
            self.PUPIL_GEO_R
        )

    def install_armband_control(self):

        module_name = 'Armband'

        armband_chains = [
            {
            'chain_name': 'stripA',
            'guides': ['stripA_0{}_guide'.format(i + 1) for i in range(3)],
            'ctl_shape': 'box',
            'ctl_col': 17,
            'ctl_scale': 1,
        },
        {
            'chain_name': 'stripB',
            'guides': ['stripB_0{}_guide'.format(i + 1) for i in range(3)],
            'ctl_shape': 'box',
            'ctl_col': 17,
            'ctl_scale': 1,
        }
        ]

        chain = fk.FkChain(module_name=module_name, chains=armband_chains)
        chain.build()

        mc.parent(module_name, self.MASTER_CONTROL)


        # Post clean ups
        for side in ['A', 'B']:
            mc.parentConstraint('Arm_ShoulderPart2_joint_L', 'Armband_strip{}_CTLS_constrained'.format(side), mo=1)
            mc.connectAttr(self.MASTER_CONTROL + '.jointVis', 'Armband_JNT_strip{}_0.visibility'.format(side))


    def build_jumper_controls(self):

        jumper_grp = pm.group(em=True, n='Jumper', p='Master_CTL_Main')
        jumper_ctl_grp = pm.group(em=True, n='Jumper_controls', p=jumper_grp)
        jumper_jnt_grp = pm.group(em=True, n='Jumper_joints', p=jumper_grp)

        pm.connectAttr('Master_CTL_Main.jointVis', jumper_jnt_grp.v)

        # Jumper_CTL_torsoA
        jumper_ctl_torsoA = control.create(
            'Jumper_CTL_mainA',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )

        jumper_ctl_torsoA_zero = jumper_ctl_torsoA.getParent()
        jumper_ctl_torsoA_zero.t.set([0.38, 91.31, -6.81])
        jumper_ctl_torsoA_zero.r.set([17.41, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoA.cv[:], [14.59, 10.43, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoA_zero, jumper_ctl_grp)

        # Jumper_CTL_torsoB
        jumper_ctl_torsoB = control.create(
            'Jumper_CTL_mainB',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoB_zero = jumper_ctl_torsoB.getParent()
        jumper_ctl_torsoB_zero.t.set([0.38, 81.36, -9.54])
        jumper_ctl_torsoB_zero.r.set([12.34, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoB.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoB_zero, jumper_ctl_torsoA)

        # Jumper_CTL_torsoC
        jumper_ctl_torsoC = control.create(
            'Jumper_CTL_mainC',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoC_zero = jumper_ctl_torsoC.getParent()
        jumper_ctl_torsoC_zero.t.set([0.38, 74.88, -10.75])
        jumper_ctl_torsoC_zero.r.set([10.6, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoC.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoC_zero, jumper_ctl_torsoB)

        # Jumper_CTL_torsoD
        jumper_ctl_torsoD = control.create(
            'Jumper_CTL_mainD',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoD_zero = jumper_ctl_torsoD.getParent()
        jumper_ctl_torsoD_zero.t.set([0.38, 68.17, -11.68])
        jumper_ctl_torsoD_zero.r.set([11.39, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoD.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoD_zero, jumper_ctl_torsoC)

        # Jumper_CTL_torsoE
        jumper_ctl_torsoE = control.create(
            'Jumper_CTL_mainE',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoE_zero = jumper_ctl_torsoE.getParent()
        jumper_ctl_torsoE_zero.t.set([0.38, 61.4, -12.94])
        jumper_ctl_torsoE_zero.r.set([10.09, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoE.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoE_zero, jumper_ctl_torsoD)

        # Jumper_CTL_knot
        jumper_ctl_knot = control.create(
            'Jumper_CTL_knot',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_knot_zero = jumper_ctl_knot.getParent()
        jumper_ctl_knot_zero.t.set([-1.02, 82.77, 12.47])
        pm.scale(jumper_ctl_knot.cv[:], [10.34, 10.34, 8.1], r=True, os=True)
        pm.parent(jumper_ctl_knot_zero, jumper_ctl_grp)
        pm.parentConstraint('Spine_RootPart2_joint_M', jumper_ctl_knot_zero, mo=True)

        # Jumper_CTL_tie_L
        jumper_ctl_tie_L = control.create(
            'Jumper_CTL_tie_L',
            shape='box',
            colorIdx=18,
            zeroGroup=True
        )
        jumper_ctl_tie_L_zero = jumper_ctl_tie_L.getParent()
        jumper_ctl_tie_L_zero.t.set([3.73, 77.83, 13.0])
        jumper_ctl_tie_L_zero.r.set([-5.16, 0.0, 33.22])
        pm.scale(jumper_ctl_tie_L.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tie_L_zero, jumper_ctl_knot)

        # Jumper_CTL_tieA_R
        jumper_ctl_tieA_R = control.create(
            'Jumper_CTL_tieA_R',
            shape='box',
            colorIdx=20,
            zeroGroup=True
        )
        jumper_ctl_tieA_R_zero = jumper_ctl_tieA_R.getParent()
        jumper_ctl_tieA_R.t.set([-4.02, 77.91, 11.93])
        jumper_ctl_tieA_R.r.set([-5.16, 0.0, -33.22])
        pm.scale(jumper_ctl_tieA_R.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tieA_R_zero, jumper_ctl_knot)

        # Jumper_CTL_tieB_R
        jumper_ctl_tieB_R = control.create(
            'Jumper_CTL_tieB_R',
            shape='box',
            colorIdx=20,
            zeroGroup=True
        )
        jumper_ctl_tieB_R_zero = jumper_ctl_tieB_R.getParent()
        jumper_ctl_tieB_R.t.set([-6.39, 74.28, 12.01])
        jumper_ctl_tieB_R.r.set([-5.16, 0.0, -33.22])
        pm.scale(jumper_ctl_tieB_R.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tieB_R_zero, jumper_ctl_tieA_R)

        # joints

        # knot
        pm.select(jumper_jnt_grp, r=True)
        jumper_knot_jnt = pm.joint(n='Jumper_knot_joint')

        pm.parentConstraint(jumper_ctl_knot, jumper_knot_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_knot, jumper_knot_jnt, mo=False)

        pm.select(jumper_knot_jnt, r=True)
        jumper_tie_L_jnt = pm.joint(n='Jumper_tie_joint_L')
        pm.parentConstraint(jumper_ctl_tie_L, jumper_tie_L_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tie_L, jumper_tie_L_jnt, mo=False)

        pm.select(jumper_knot_jnt, r=True)
        jumper_tieA_R_jnt = pm.joint(n='Jumper_tieA_joint_R')
        jumper_tieB_R_jnt = pm.joint(n='Jumper_tieB_joint_R')

        pm.parentConstraint(jumper_ctl_tieA_R, jumper_tieA_R_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tieA_R, jumper_tieA_R_jnt, mo=False)

        pm.parentConstraint(jumper_ctl_tieB_R, jumper_tieB_R_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tieB_R, jumper_tieB_R_jnt, mo=False)

        # torso
        pm.select(jumper_jnt_grp, r=True)
        jumper_torso_A_jnt = pm.joint(n='Jumper_mainA_joint')
        jumper_torso_B_jnt = pm.joint(n='Jumper_mainB_joint')
        jumper_torso_C_jnt = pm.joint(n='Jumper_mainC_joint')
        jumper_torso_D_jnt = pm.joint(n='Jumper_mainD_joint')
        jumper_torso_E_jnt = pm.joint(n='Jumper_mainE_joint')

        pm.parentConstraint('Spine_RootPart2_joint_M', jumper_ctl_torsoA_zero, mo=True)

        pm.parentConstraint(jumper_ctl_torsoA, jumper_torso_A_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoA, jumper_torso_A_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoB, jumper_torso_B_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoB, jumper_torso_B_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoC, jumper_torso_C_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoC, jumper_torso_C_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoD, jumper_torso_D_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoD, jumper_torso_D_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoE, jumper_torso_E_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoE, jumper_torso_E_jnt, mo=False)

        ctl_list = [
            jumper_ctl_knot,
            jumper_ctl_tie_L,
            jumper_ctl_tieA_R,
            jumper_ctl_tieB_R,

            jumper_ctl_torsoA,
            jumper_ctl_torsoB,
            jumper_ctl_torsoC,
            jumper_ctl_torsoD,
            jumper_ctl_torsoE,
        ]

        for ctl in ctl_list:
            ctl.v.set(cb=False, k=False, l=True)

    def build_pouch_controls(self):

        # ctl
        pouch_grp = pm.group(em=True, n='Pouch', p='Master_CTL_Main')
        pouch_ctl_grp = pm.group(em=True, n='Pouch_controls', p=pouch_grp)
        pouch_jnt_grp = pm.group(em=True, n='Pouch_joints', p=pouch_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', pouch_jnt_grp.v)

        pouch_ctl_leg = control.create(
            'Pouch_CTL_main',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )

        pouch_ctl_leg_zero = pouch_ctl_leg.getParent()
        pouch_ctl_leg_zero.t.set([-16.023, 78.879, 1.179])
        pouch_ctl_leg_zero.r.set([123.889, -89.963, -116.98])
        pm.scale(pouch_ctl_leg.cv[:], [10, 5, 5], r=True, os=True)
        pm.parentConstraint('Leg_HipPart1_joint_R', pouch_ctl_leg_zero, mo=True)
        pm.parent(pouch_ctl_leg_zero, pouch_ctl_grp)

        # jnt
        pm.select(pouch_ctl_leg, r=True)
        pouch_jnt = pm.joint(n='Pouch_joint')
        pm.parentConstraint(pouch_ctl_leg, pouch_jnt, mo=False)
        pm.scaleConstraint(pouch_ctl_leg, pouch_jnt, mo=False)

    def build_boot_controls(self):

        boots_grp = pm.group(em=True, n='Boots', p='Master_CTL_Main')
        boots_ctl_grp = pm.group(em=True, n='Boots_controls', p=boots_grp)
        boots_jnt_grp = pm.group(em=True, n='Boots_joints', p=boots_grp)

        pm.connectAttr('Master_CTL_Main.jointVis', boots_jnt_grp.v)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1
            color = 20 if side == 'R' else 18
            # bootA
            boot_ctl_a = control.create(
                'Boot_CTL_cuffA_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_a_zero = boot_ctl_a.getParent()
            boot_ctl_a_zero.t.set([6.9*side_mult, 14.18, -2.16])
            pm.scale(boot_ctl_a.cv[:], [9.26, 3.27, 9.81], r=True, os=True)
            pm.parent(boot_ctl_a_zero, boots_ctl_grp)

            # bootB
            boot_ctl_b = control.create(
                'Boot_CTL_cuffB_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_b_zero = boot_ctl_b.getParent()
            boot_ctl_b_zero.t.set([7.42*side_mult, 18.68, -1.98])
            boot_ctl_b_zero.r.set([5.88, 0.0, 0.0])
            pm.scale(boot_ctl_b.cv[:], [11.93, 3.27, 14.77], r=True, os=True)
            pm.parent(boot_ctl_b_zero, boot_ctl_a)

            # bootC
            boot_ctl_c = control.create(
                'Boot_CTL_cuffC_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_c_zero = boot_ctl_c.getParent()
            boot_ctl_c_zero.t.set([7.42*side_mult, 23.49, -1.33])
            boot_ctl_c_zero.r.set([11.05, 0.0, 0.0])
            pm.scale(boot_ctl_c.cv[:], [13.52, 3.27, 18.93], r=True, os=True)
            pm.parent(boot_ctl_c_zero, boot_ctl_b)

            pm.select(boots_jnt_grp, r=True)
            boot_a_jnt = pm.joint(n='Boot_cuffA_joint_'+side)
            boot_b_jnt = pm.joint(n='Boot_cuffA_joint_'+side)
            boot_c_jnt = pm.joint(n='Boot_cuffA_joint_'+side)

            pm.parentConstraint('KneePart2_'+side, boot_ctl_a_zero, mo=True)

            pm.parentConstraint(boot_ctl_a, boot_a_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_a, boot_a_jnt, mo=False)
            pm.parentConstraint(boot_ctl_b, boot_b_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_b, boot_b_jnt, mo=False)
            pm.parentConstraint(boot_ctl_c, boot_c_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_c, boot_c_jnt, mo=False)

            for ctl in [
                boot_ctl_a,
                boot_ctl_b,
                boot_ctl_c,
            ]:
                ctl.v.set(cb=False, k=False, l=True)

    def build_torso_controls(self):

        # breast ctl
        torso_grp = pm.group(em=True, n='Breasts', p='Master_CTL_Main')
        torso_ctl_grp = pm.group(em=True, n='Breasts_controls', p=torso_grp)
        torso_jnt_grp = pm.group(em=True, n='Breasts_joints', p=torso_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', torso_jnt_grp.v)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1
            color = 20 if side == 'R' else 18

            # bootA
            breast_ctl_main = control.create(
                'Torso_CTL_breast_'+side,
                shape='ball',
                colorIdx=color,
                zeroGroup=True,
            )
            breast_ctl_main_zero = breast_ctl_main.getParent()
            breast_ctl_main_zero.t.set([5.38 * side_mult, 122.23, 7.33])
            breast_ctl_main_zero.r.set([-14.66, 24.6 * side_mult,  -3.14])
            pm.scale(breast_ctl_main.cv[:], [6.2, 6.2, 6.2], r=True, os=True)
            pm.parent(breast_ctl_main_zero, torso_ctl_grp)
            pm.parentConstraint('Spine_Chest_joint_M', breast_ctl_main_zero, mo=True)
            breast_ctl_main.v.set(cb=False, k=False, l=True)

            # breast_ctl_main_zero = breast_ctl_main.getParent()
            # breast_ctl_main_zero.t.set([6.686*side_mult, 122.304, 8.0])
            # breast_ctl_main_zero.r.set([12.301, 12*side_mult, -0.422])
            # pm.scale(breast_ctl_main.cv[:], [6.2, 6.2, 6.2], r=True, os=True)
            # pm.parent(breast_ctl_main_zero, torso_ctl_grp)
            # pm.parentConstraint('Spine_Chest_joint_M', breast_ctl_main_zero, mo=True)
            # breast_ctl_main.v.set(cb=False, k=False, l=True)


            # jnt
            pm.select(torso_jnt_grp, r=True)
            breast_jnt = pm.joint(n='Torso_breast_joint_'+side)
            #pm.delete(pm.parentConstraint(breast_ctl_main, breast_jnt, mo=False))
            #pm.delete(pm.scaleConstraint(breast_ctl_main, breast_jnt, mo=False))
            pm.parentConstraint(breast_ctl_main, breast_jnt, mo=False)
            pm.scaleConstraint(breast_ctl_main, breast_jnt, mo=False)

    def build_earring_controls(self):

        root = 'Earring_root_guide'

        hair_guides_data = {
            'earring': {
                'guides': ['Earring_CTL_main_01_guide', 'Earring_CTL_main_02_guide'],
                'scales': [0.9, 0.9],
                'colorIdx': 18,
            }
        }

        # Heirarchy
        jumper_grp = pm.group(em=True, n='Earring', p='Master_CTL_Main')
        jumper_ctl_grp = pm.group(em=True, n='Earring_controls', p=jumper_grp)
        jumper_jnt_grp = pm.group(em=True, n='Earring_joints', p=jumper_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', jumper_jnt_grp.v)


        for label, guide_data in hair_guides_data.items():

            print label, guide_data

            guides = guide_data.get('guides')
            scales = guide_data.get('scales')
            color_idx = guide_data.get('colorIdx')

            ctl_list = list()
            jnt_list = list()
            for i, guide in enumerate(guides):

                print 'guide >>', guide

                # control
                ctl = control.create(
                    guide.replace('_guide', ''),
                    shape='box',
                    colorIdx=color_idx,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)
                pm.delete(pm.parentConstraint(guide, ctl.getParent(), mo=False))
                pm.scale(ctl.cv[:], [scales[i], 1, scales[i]], r=True, os=True)
                ctl.v.set(k=False, cb=False, l=True)

                # joint
                pm.select(cl=True)
                jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_jnt')
                jnt_list.append(jnt)
                pm.parentConstraint(ctl, jnt, mo=False)
                pm.scaleConstraint(ctl, jnt, mo=False)

                # parenting
                if i == 0:
                    pm.parent(ctl.getParent(), jumper_ctl_grp)
                    pm.parentConstraint('Head_Head_joint_M', ctl.getParent(), mo=True)
                    pm.parent(jnt, jumper_jnt_grp)
                else:
                    pm.parent(ctl.getParent(), ctl_list[i-1])
                    pm.parent(jnt, jnt_list[i-1])

                jnt.jo.set([0, 0, 0])

        # Root joint
        pm.select(cl=True)
        root_joint = root
        jnt = pm.joint(n='Earring_root')
        jnt_list.append(jnt)

        pm.delete(pm.parentConstraint(root_joint, jnt, mo=False))
        pm.delete(pm.scaleConstraint(root_joint, jnt, mo=False))

        pm.parentConstraint('Head_CTL_FKHead_M', jnt, mo=True)
        pm.scaleConstraint('Head_CTL_FKHead_M', jnt, mo=True)

        pm.parent(jnt, jumper_jnt_grp)

    def build_sunglasses_controls(self):

        root = 'Earring_root_guide'

        hair_guides_data = {
            'earring': {
                'guides': ['Sunglasses_CTL_main_01_guide', 'Sunglasses_CTL_arm_L_guide', 'Sunglasses_CTL_arm_R_guide'],
                'scales': [[20, 9,1], [3,3,3], [3,3,3]],
                'colorIdx': 18,
            }
        }

        # Heirarchy
        jumper_grp = pm.group(em=True, n='Sunglasses', p='Master_CTL_Rig')
        jumper_ctl_grp = pm.group(em=True, n='Sunglasses_controls', p=jumper_grp)
        jumper_jnt_grp = pm.group(em=True, n='Sunglasses_joints', p=jumper_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', jumper_jnt_grp.v)


        for label, guide_data in hair_guides_data.items():

            guides = guide_data.get('guides')
            scales = guide_data.get('scales')
            color_idx = guide_data.get('colorIdx')

            ctl_list = list()
            jnt_list = list()
            for i, guide in enumerate(guides):

                print 'guide >>', guide

                # control
                ctl = control.create(
                    guide.replace('_guide', ''),
                    shape='box',
                    colorIdx=color_idx,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)
                pm.delete(pm.parentConstraint(guide, ctl.getParent(), mo=False))
                pm.scale(ctl.cv[:], [scales[i][0], scales[i][1], scales[i][2]], r=True, os=True)
                ctl.v.set(k=False, cb=False, l=True)

                # joint
                pm.select(cl=True)
                jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_jnt')
                jnt_list.append(jnt)
                pm.parentConstraint(ctl, jnt, mo=False)
                pm.scaleConstraint(ctl, jnt, mo=False)

                pm.parent(jnt, jumper_jnt_grp)

        pm.parent('Sunglasses_CTL_main_01_ZERO', jumper_ctl_grp)
        pm.parent('Sunglasses_CTL_arm_L_ZERO', 'Sunglasses_CTL_main_01')
        pm.parent('Sunglasses_CTL_arm_R_ZERO', 'Sunglasses_CTL_main_01')


        # create constraint switch

        world_grp = pm.group(em=1, p=jumper_grp, n='Sunglasses_world_driver_grp')
        constrained_grp = pm.group(em=1, p=jumper_grp, n='Sunglasses_const_driver_grp')
        pm.parentConstraint('Head_Head_joint_M', constrained_grp, mo=1)

        zg = zero_grp('Sunglasses_CTL_main_01_ZERO')
        pm.rename(zg, 'Sunglasses_head_constrained_grp')
        const = pm.parentConstraint(world_grp, constrained_grp, 'Sunglasses_head_constrained_grp', mo=1)
        attribute.add('Master_CTL_Main', n='glasses_vis', type='int', dv=1, cb=True, k=False, min=0, max=1)
        attribute.add('Master_CTL_Main', n='glasses_constrain', type='float', dv=1.0, cb=True, k=False, min=0, max=1)

        rev = mc.createNode('reverse')

        mc.connectAttr('Master_CTL_Main.glasses_constrain',
                       'Sunglasses_head_constrained_grp_parentConstraint1.Sunglasses_const_driver_grpW1', f=1)

        mc.connectAttr('Master_CTL_Main.glasses_constrain',
                       rev + '.input.inputX', f=1)

        mc.connectAttr(rev + '.output.outputX',
                       'Sunglasses_head_constrained_grp_parentConstraint1.Sunglasses_world_driver_grpW0', f=1)

        mc.connectAttr('Master_CTL_Main.glasses_vis', 'sunglasses_geo.visibility', f=1)
        mc.connectAttr('Master_CTL_Main.glasses_vis', 'Sunglasses.visibility', f=1)

    def setup_facerig(self):

        ns = self.FACE_NS+':'

        #face_rig_geo = ns + 'face_rig_geo'


        # parent face rig.
        pm.parent(ns+'faceRig', 'Master_CTL_Main')
        pn(ns+'faceRig').inheritsTransform.set(False)

        pm.parent(ns+'Head_constrained_grp', 'Master_CTL_Main')

        # constrain controls to head.
        pm.parentConstraint('Head_Head_joint_M', ns+'Head_constrained_grp', mo=True)
        pm.parentConstraint('Head_Head_joint_M', ns+'Head_constrained_grp', mo=True)

        for geo in self.FACE_GEO_LIST:
            blendshape.apply(ns+geo, geo, dv=1)

        # hide face geo
        pm.hide(ns+'Geo')

        for side in 'LR':
            for atr in ['rx', 'ry', 'rz']:
                pm.connectAttr(pn('Head_Eye_joint_'+side).attr(atr), pn('face:Eye_driver_'+side+'_GRP').attr(atr))

        hide_list = [
            ns+'Tweaks_controls',
            ns+'Main_joints',
            ns+'Region_joints',

        ]

        for h in hide_list:
            pn(h).v.set(0)

    def build_tassel(self):

        attribute.add('Master_CTL_Main', n='tasselA_CTL', type='enum', en=['PATH', 'FK'], dv=1)
        attribute.add('Master_CTL_Main', n='tasselB_CTL', type='enum', en=['PATH', 'FK'], dv=1)

        tassel_dict = {

            'tasselA': [self.TASSEL_A_BASE_CURVE, self.TASSEL_A_UP_CURVE],
            'tasselB': [self.TASSEL_B_BASE_CURVE, self.TASSEL_B_UP_CURVE]
        }

        for tassel in tassel_dict:

            base_guide_curve = tassel_dict[tassel][0]
            up_guide_curve = tassel_dict[tassel][1]

            path_joint_list, controls = path_tools.build(base_guide_curve,
                       up_guide_curve,
                       node=3,
                       joint_num=7,
                       prefix=tassel)

            mc.parentConstraint('Spine_Spine1_joint_M', '{}_path_master_control'.format(tassel), mo=1)
            mc.setAttr('{}_path_master_control'.format(tassel) + '.visibility', 0) #            tasselB_mainCurveCTL_1
            mc.setAttr('{}_mainCurveCTL_0'.format(tassel) + '.visibility', 0)

            mc.parent(controls[1][-1], controls[1][-2])
            mc.setAttr(controls[1][-1] + '.visibility', 0)

            pm.scale(pn(controls[1][1]).cv[:], [2, 2, 2], r=True, os=True)
            pm.scale(pn(controls[1][-2]).cv[:], [2, 2, 5], r=True, os=True)
            pm.scale(pn(controls[1][-3]).cv[:], [2, 2, 5], r=True, os=True)

            output_joint_list = []
            fk_joint_list = []

            for chain, j_list in zip(['outJoint', 'fkJoint'], [output_joint_list, fk_joint_list]):

                for old_jnt in path_joint_list:
                    mc.select(cl=1)
                    jnt_name = old_jnt.replace('pathJoint', chain)
                    new_jnt = mc.joint(n=jnt_name)

                    j_list.append(new_jnt)
                    con = mc.parentConstraint(old_jnt, new_jnt)
                    mc.delete(con)


            # Blend the path chain and fk chain onto the output chain using blend colors node
            for out, fk, path in zip(output_joint_list, fk_joint_list, path_joint_list):

                blend_translation_node = mc.createNode('blendColors')
                blend_rotation_node = mc.createNode('blendColors')
                decomp_fk_node = mc.createNode('decomposeMatrix')

                mc.connectAttr(fk + '.worldMatrix[0]', decomp_fk_node + '.inputMatrix')


                mc.connectAttr('Master_CTL_Main.tassel{}_CTL'.format(tassel[-1]), blend_translation_node + '.blender')
                mc.connectAttr('Master_CTL_Main.tassel{}_CTL'.format(tassel[-1]), blend_rotation_node + '.blender')

                mc.connectAttr(decomp_fk_node + '.outputTranslate', blend_translation_node + '.color1')
                mc.connectAttr(path + '.translate', blend_translation_node + '.color2')
                mc.connectAttr(blend_translation_node + '.output', out + '.translate')

                mc.connectAttr(decomp_fk_node + '.outputRotate', blend_rotation_node + '.color1')
                mc.connectAttr(path + '.rotate', blend_rotation_node + '.color2')
                mc.connectAttr(blend_rotation_node + '.output', out + '.rotate')

            # Build FK CTLS
            rig_grp = mc.group(em=1, w=1, n=tassel + '_grp')
            out_joints_grp = mc.group(em=1, w=1, n=tassel + '_output_joints_grp')
            fk_joints_grp = mc.group(em=1, w=1, n=tassel + '_fk_joints_grp')
            ctls_grp = mc.group(em=1, w=1, n=tassel + '_ctls_grp')

            ctl_list = []

            for i, jnt in enumerate(fk_joint_list):

                guide = mc.group(em=1, w=1, n=jnt.replace('fkJoint', 'fk_CTL'))
                mc.delete(mc.parentConstraint(jnt, guide))

                ctl = control.create(
                    guide,
                    shape='ball',
                    colorIdx=17,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)

                pm.scale(ctl.cv[:], [2, 2, 2], r=True, os=True)
                pm.parentConstraint(ctl, jnt, mo=False)
                pm.scaleConstraint(ctl, jnt, mo=False)


                if i == 0:
                    pm.parent(ctl.getParent(), ctls_grp)
                    pm.parentConstraint('Spine_Spine1_joint_M', ctl.getParent(), mo=True)
                    pm.parent(jnt, fk_joints_grp)
                else:
                    pm.parent(ctl.getParent(), ctl_list[i-1])
                    #pm.parent(jnt, jnt_list[i-1])
                    pm.parent(jnt, fk_joints_grp)

            mc.parent(output_joint_list, out_joints_grp)

            mc.parent(out_joints_grp,
                      ctls_grp,
                      fk_joints_grp,
                      '{}_path_rig'.format(tassel),
                      rig_grp)

            # Connection ctl vis switch
            rev_node = mc.createNode('reverse') #
            mc.connectAttr('Master_CTL_Main.tassel{}_CTL'.format(tassel[-1]), rev_node + '.inputX')
            mc.connectAttr(rev_node + '.outputX', '{}_level_1_controls_grp'.format(tassel) + '.visibility')

            mc.connectAttr('Master_CTL_Main.tassel{}_CTL'.format(tassel[-1]), '{}_ctls_grp'.format(tassel) + '.visibility')

            # Clean up
            mc.setAttr(out_joints_grp + '.visibility', 0)
            mc.setAttr(fk_joints_grp + '.visibility', 0)
            mc.setAttr('{}_driven_grp'.format(tassel) + '.visibility', 0)
            mc.parent('{}_grp'.format(tassel), 'Master_CTL_Rig')

    def build_pants_leg_ctls(self):

        prefix = 'PantLeg'

        hip_default = 'Leg_Hip_joint_{}'
        hip_1_default ='Leg_HipPart1_joint_{}'
        hip_2_default = 'Leg_HipPart2_joint_{}'
        knee_default = 'Leg_Knee_joint_{}'

        control_zero_grps = []

        for side, side_mult in zip(["L", "R"], [1, -1]):

            hip_jnt = hip_default.format(side)
            hip1_jnt = hip_1_default.format(side)
            hip2_jnt = hip_2_default.format(side)
            knee_jnt = knee_default.format(side)

            mc.select(cl=1)
            pantLeg_knee_jnt =mc.joint(n= prefix + '_Knee_joint_{}'.format(side))
            mc.delete(mc.parentConstraint(knee_jnt, pantLeg_knee_jnt))
            mc.parent(pantLeg_knee_jnt, hip_jnt)

            for j, constraint in zip([hip1_jnt, hip2_jnt],[[2,1], [1,2]]):
                mc.select(cl=1)
                new_j = mc.joint(n=j.replace('Leg', prefix))
                mc.delete(mc.parentConstraint(j, new_j))
                #TODO :


                mc.parent(new_j, pantLeg_knee_jnt)

                con = mc.pointConstraint(pantLeg_knee_jnt, hip_jnt, new_j)[0]
                mc.setAttr(con + '.PantLeg_Knee_joint_{}W0'.format(side), constraint[0])
                mc.setAttr(con + '.Leg_Hip_joint_{}W1'.format(side), constraint[1])

                mc.aimConstraint(hip_jnt, new_j, aimVector=[side_mult, 0, 0], upVector = [0, 1, 0, ],
                                 worldUpType='objectrotation', worldUpVector = [0, 1, 0, ],
                                 worldUpObject=j)


            # Create Control

            ctl_guide = mc.group(n=prefix + 'CTL_{}'.format(side), em=1, w=1)
            mc.delete(mc.parentConstraint(knee_jnt, ctl_guide))
            ctl = control.create(
                ctl_guide,
                shape='ball',
                colorIdx=17,
                zeroGroup=True,
            )
            pm.scale(ctl.cv[:], [6, 6, 6], r=True, os=True)
            pm.parentConstraint(ctl, pantLeg_knee_jnt)
            pm.parentConstraint(knee_jnt, ctl.getParent())
            control_zero_grps.append(ctl.getParent())

        pm.group(control_zero_grps, p='Master_CTL_Main', n='Pants_Leg_offset')

    def post_edits(self):
        mc.setAttr("Master_CTL_Main.geoLock", 0)
        mc.setAttr("Master_CTL_Main.jointVis", 1)


    # ==================================================================================================================
    def go(self):
        print '='*80
        print 'Starting '+str(self.who)+' Rig Build.'
        mc.file(new=True, f=True)
        self.build_contents()
        print 'Finished!'

    def build_contents(self):
        build.imports(self.imports)
        pm.rename('Group', self.RIG_GRP)
        build.groups(self.groups)
        pm.parent(self.GEO_LIST, self.GEO_GRP)
        self.modify_as_rig()

        self.build_pants_leg_ctls()

        self.install_armband_control()

        self.build_tassel()
        self.setup_facerig()

        build.skin_bind(self.skin_bind)
        build.skin_copy(self.skin_copy)

        pm.delete(self.DELETE_GRP)

        #self.post_edits()

"""
#        DON           #
#     CASUAL CHILD     # 

from jb_destruna.rigs.e_don import don_child_casualClean_animRig
reload(don_child_casualClean_animRig)
rig = don_child_casualClean_animRig.Rig()
rig.go()
"""