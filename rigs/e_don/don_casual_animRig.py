from jb_destruna.maya.core import build
import maya.cmds as mc
import pymel.core as pm

from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import control
from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.internal import mesh
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn
from jb_destruna.maya.utility.misc import zero_grp
from jb_destruna.maya.internal import wrap
from jb_destruna.tools import path as path_tools

from jb_destruna.rigs.e_don.import_paths import don_import_paths as imp

reload(build)
reload(rig)


class Rig(rig.Rig):

    ASSET = 'don'
    VARIANT = 'casual'

    FACE_NS = 'face'

    IMPORT_LIST = ['office', 'jordan', 'damien']
    IMPORT_KEY = IMPORT_LIST[2]

    # BODY
    BODY_GEO = 'body_geo'
    HAIR_GEO = 'hair_geo'

    # FACE
    FACE_GEO = 'head_geo'
    PUPIL_GEO_L = 'eye_L_geo'
    PUPIL_GEO_R = 'eye_R_geo'
    SCLERA_GEO_L = 'sclera_L_geo'
    SCLERA_GEO_R = 'sclera_R_geo'
    EYELASH_L_GEO = 'eyeLash_L_geo'
    EYELASH_R_GEO = 'eyeLash_R_geo'
    EYEBROW_GEO_L = 'eyebrow_L_geo'
    EYEBROW_GEO_R = 'eyebrow_R_geo'
    TOPTEETH_GEO = 'teethUpper_geo'
    BOTTOMTEETH_GEO = 'teethLower_geo'
    TONGUE_GEO = 'tongue_geo'

    SHIRT_GEO = 'shirt_geo'
    PANTS_GEO = 'pants_geo'
    SHOES_GEO_L = 'shoes_L_geo'
    SHOES_GEO_R = 'shoes_R_geo'

    ACCESORY_ARMBAND = 'armBand_geo'
    ACCESORY_EARPLUG = 'earplug_geo'
    ACCESSORY_GLASSES = 'sunglasses_geo'
    ACCESSORY_LENSE = 'sunglassesLenses_geo'
    ACCESORY_EARING = 'earring_geo'

    SKIN_GEO_SHIRT = 'shirt_geo_SKINGEO'
    SKIN_GEO_PANTS = 'pants_geo_SKINGEO'
    SKIN_GEO_SHOES = 'shoes_geo_SKINGEO'

    FACE_GEO_LIST = [
        FACE_GEO,
        PUPIL_GEO_L,
        PUPIL_GEO_R,
        EYELASH_L_GEO,
        EYELASH_R_GEO,
        EYEBROW_GEO_L,
        EYEBROW_GEO_R,
        TOPTEETH_GEO,
        BOTTOMTEETH_GEO,
        TONGUE_GEO,
    ]

    GEO_LIST = [
        BODY_GEO,
        SHIRT_GEO,
        PANTS_GEO,
        SHOES_GEO_L,
        SHOES_GEO_R,
        HAIR_GEO,
        SCLERA_GEO_L,
        SCLERA_GEO_R,
        ACCESORY_EARPLUG,
        ACCESSORY_GLASSES,
        ACCESSORY_LENSE,
        ACCESORY_EARING,
    ]
    GEO_LIST += FACE_GEO_LIST

    def __init__(self):
        super(Rig, self).__init__()

        self.who = self.ASSET

        self.imports = [
            # GEO
            {'filepath': 'D:/_Active_Projects\Destruna/masterAssets_forRef/characters/don/3D/don_teen_casualClean/01_model/don_teen_casualClean_model_13_DFO_MASTER.mb',
             'group': self.DELETE_GRP},

            #LOW GEO
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_casualClean/02_rig/imports/skin_geo/don_casual_06_skingeo.obj',
             'group': self.DELETE_GRP},

            # GUIDES
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_casualClean/02_rig/imports/guides/earring_guides_06.mb',
             'group': self.DELETE_GRP},

            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_casualClean/02_rig/imports/guides/glasses/sunglasses_guides_04.mb',
             'group': self.DELETE_GRP},

            # HAIR RIG

            # HAIR CURVES
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_mocap/02_rig/imports/guides/hair_guides/don_hair_guides_01.mb',
                'group': self.DELETE_GRP},
            # MULLET LOCS
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_mocap/02_rig/imports/lattice_guide/mullet/don_mullet_lattice_guide_01.mb',
                'group': self.DELETE_GRP},


            # AS RIG
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_casualClean/02_rig/imports/asRig/Don_casual_asRig_008.mb'},

            # AS FACE RIG
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_casualClean/02_rig/imports/AS_face_rig/don_faceRig_v004.mb',
             'namespace': self.FACE_NS},
        ]

        skin_weight_prefix = 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_casualClean/02_rig/imports/skinClusters/'

        self.skin_bind = [

            # BODY
            {'geo': self.BODY_GEO, 'filepath': skin_weight_prefix +  self.BODY_GEO + '/body_geo_skincluster_02.json',
             'import_module':'skin_ng'},
            {'geo': self.HAIR_GEO, 'filepath': skin_weight_prefix + self.HAIR_GEO + '/hair_geo_skincluster_04.json',
             'import_module': 'skin_ng'},

            # FACE
            {'geo': self.FACE_GEO, 'filepath': skin_weight_prefix + self.FACE_GEO + '/head_geo_skincluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.PUPIL_GEO_L, 'filepath': skin_weight_prefix + self.PUPIL_GEO_L + '/eye_L_geo_skincluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.PUPIL_GEO_R, 'filepath': skin_weight_prefix + self.PUPIL_GEO_R + '/eye_R_geo_skincluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_L_GEO, 'filepath': skin_weight_prefix + self.EYELASH_L_GEO + '/eyeLash_L_geo_skincluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_R_GEO, 'filepath': skin_weight_prefix + self.EYELASH_R_GEO + '/eyeLash_R_geo_skincluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYEBROW_GEO_L, 'filepath': skin_weight_prefix + self.EYEBROW_GEO_L + '/eyebrow_L_geo_skincluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYEBROW_GEO_R, 'filepath': skin_weight_prefix + self.EYEBROW_GEO_R + '/eyebrow_R_geo_skincluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.TOPTEETH_GEO, 'filepath': skin_weight_prefix + self.TOPTEETH_GEO + '/teethUpper_geo_skincluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.BOTTOMTEETH_GEO, 'filepath': skin_weight_prefix + self.BOTTOMTEETH_GEO + '/teethLower_geo_skincluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.TONGUE_GEO, 'filepath': skin_weight_prefix + self.TONGUE_GEO + '/tongue_geo_skincluster_02.json',
             'import_module': 'skin_ng'},

            # ACCESSORY
            # {'geo': self.ACCESORY_ARMBAND, 'filepath': skin_weight_prefix + self.ACCESORY_ARMBAND + '/armBand_geo_skincluster_02.json',
            #  'import_module':'skin_ng'},
            {'geo': self.ACCESORY_EARING, 'filepath': skin_weight_prefix + self.ACCESORY_EARING + '/earring_geo_skincluster_03.json',
             'import_module':'skin_ng'},
            {'geo': self.ACCESORY_EARPLUG, 'filepath': skin_weight_prefix + self.ACCESORY_EARPLUG + '/earplug_geo_skincluster_02.json',
             'import_module':'skin_ng'},
            {'geo': self.ACCESSORY_GLASSES, 'filepath': skin_weight_prefix + self.ACCESSORY_GLASSES + '/sunglasses_geo_skinCluster_04.json',
             'import_module': 'skin_ng'},

            # SKIN COPY GEO
            {'geo': self.SKIN_GEO_SHIRT, 'filepath': skin_weight_prefix + self.SKIN_GEO_SHIRT + '/shirt_geo_SKINGEO_skincluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.SKIN_GEO_PANTS, 'filepath': skin_weight_prefix + self.SKIN_GEO_PANTS + '/pants_geo_SKINGEO_skincluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.SKIN_GEO_SHOES, 'filepath': skin_weight_prefix + self.SKIN_GEO_SHOES + '/shoes_geo_SKINGEO_skincluster_02.json',
             'import_module': 'skin_ng'},

        ]

        self.skin_copy = [
            {'source': self.SKIN_GEO_SHIRT, 'target': self.SHIRT_GEO},
            {'source': self.SKIN_GEO_PANTS, 'target': self.PANTS_GEO},
            {'source': self.SKIN_GEO_SHOES, 'target': self.SHOES_GEO_L},
            {'source': self.SKIN_GEO_SHOES, 'target': self.SHOES_GEO_R},
        ]

        self.groups = [
            {'g': self.TOP_GRP, 'p': None, 'v': True},
            {'g': self.RIG_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.GEO_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.DNT_GRP, 'p': self.TOP_GRP, 'v': False},
        ]

        self.hair_rig_dict = [
            {'name': 'hairA', 'base': 'hair_baseGuideA', 'up': 'hair_upGuideA', 'col': 2, 'CTL_ScaleRange': [2, 6],
             'j_num': 4, 'CTL_offset': -0.6},
            {'name': 'hairB', 'base': 'hair_baseGuideB', 'up': 'hair_upGuideB', 'col': 4, 'CTL_ScaleRange': [3, 6],
             'j_num': 4, 'CTL_offset': 0.25},
            {'name': 'hairC', 'base': 'hair_baseGuideC', 'up': 'hair_upGuideC', 'col': 6, 'CTL_ScaleRange': [4, 6],
             'j_num': 4, 'CTL_offset': 0.3},
            {'name': 'hairD', 'base': 'hair_baseGuideD', 'up': 'hair_upGuideD', 'col': 17, 'CTL_ScaleRange': [5, 10],
             'j_num': 4, 'CTL_offset': 0.5},
            {'name': 'hairE', 'base': 'hair_baseGuideE', 'up': 'hair_upGuideE', 'col': 16, 'CTL_ScaleRange': [4, 6],
             'j_num': 4, 'CTL_offset': 0.25},
            {'name': 'hairF', 'base': 'hair_baseGuideF', 'up': 'hair_upGuideF', 'col': 18, 'CTL_ScaleRange': [4, 6],
             'j_num': 4, 'CTL_offset': 0.25},
            {'name': 'hairG', 'base': 'hair_baseGuideG', 'up': 'hair_upGuideG', 'col': 22, 'CTL_ScaleRange': [2, 19],
             'j_num': 4, 'CTL_offset': 0.5},
            {'name': 'hairH', 'base': 'hair_baseGuideH', 'up': 'hair_upGuideH', 'col': 6, 'CTL_ScaleRange': [3, 7],
             'j_num': 4, 'CTL_offset': 0.5},
            {'name': 'hairI', 'base': 'hair_baseGuideI', 'up': 'hair_upGuideI', 'col': 19, 'CTL_ScaleRange': [2, 6],
             'j_num': 4, 'CTL_offset': 0.25},
            {'name': 'hairJ', 'base': 'hair_baseGuideJ', 'up': 'hair_upGuideJ', 'col': 1, 'CTL_ScaleRange': [4, 6],
             'j_num': 4, 'CTL_offset': 0.25},
            {'name': 'hairK', 'base': 'hair_baseGuideK', 'up': 'hair_upGuideK', 'col': 22, 'CTL_ScaleRange': [4, 6],
             'j_num': 4, 'CTL_offset': 0.25},
            {'name': 'hairL', 'base': 'hair_baseGuideL', 'up': 'hair_upGuideL', 'col': 17, 'CTL_ScaleRange': [4, 6],
             'j_num': 4, 'CTL_offset': 0.25},
            {'name': 'hairM', 'base': 'hair_baseGuideM', 'up': 'hair_upGuideM', 'col': 6, 'CTL_ScaleRange': [4, 6],
             'j_num': 4, 'CTL_offset': 0.25},
            {'name': 'hairN', 'base': 'hair_baseGuideN', 'up': 'hair_upGuideN', 'col': 9, 'CTL_ScaleRange': [4, 6],
             'j_num': 6, 'CTL_offset': 0.25},
            {'name': 'hairO', 'base': 'hair_baseGuideO', 'up': 'hair_upGuideO', 'col': 19, 'CTL_ScaleRange': [2, 4],
             'j_num': 4, 'CTL_offset': 0.25},
            {'name': 'hairP', 'base': 'hair_baseGuideP', 'up': 'hair_upGuideP', 'col': 7, 'CTL_ScaleRange': [2, 5],
             'j_num': 4, 'CTL_offset': 0.25},
        ]

    def modify_as_rig(self):

        # good stuff happens here.
        super(Rig, self).modify_as_rig()

        # localized stuff happens here --------------
        pn('Master_CTL_Main').bendVis.set(0)

        # lock vis ctls
        for ctl in self.AS_CTL_LIST_NEW:
            pn(ctl).v.set(k=False, cb=False, l=True)

        # attributes on master
        pm.setAttr('Master_CTL_Main.jointVis', 0)
        attribute.add('Master_CTL_Main', n='geoVis', type='bool', dv=1, cb=True, k=False)
        attribute.add('Master_CTL_Main', n='geoLock', type='bool', dv=1, cb=True, k=False)

        pm.connectAttr('Master_CTL_Main.geoLock', 'Geometry.overrideEnabled')
        pm.setAttr('Geometry.overrideDisplayType', 2)

        pm.connectAttr('Master_CTL_Main.geoVis', 'Geometry.v')

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            pm.move('Arm_CTL_FKScapula_'+side+'.cv[:]', [-5*side_mult, 0, 0], r=True, os=True)
            pm.scale('Arm_CTL_FKShoulder_'+side+'.cv[:]', [0.7, 0.7, 0.7], r=True, os=True)
            pm.scale('Arm_CTL_FKWrist_' + side + '.cv[:]', [0.8, 0.8, 0.8], r=True, os=True)

    def build_hair_rig(self):

        # Create hair main main control to hold attributes

        h_main_ctl_name = 'Hair_CTL_Main'
        h_main_ctl_trans = mc.group(em=1, w=1, n=h_main_ctl_name)

        h_main_ctl = control.create(
            h_main_ctl_trans,
            shape='hair',
            colorIdx=20,
            zeroGroup=True
        )

        pm.delete(pm.parentConstraint(self.FACE_NS + ':ctrlBox', h_main_ctl.getParent()))  # Position Ctl
        pm.move(h_main_ctl.getParent(), [2.2, 9.852, 0], r=True, os=True)

        pm.parentConstraint(self.FACE_NS + ':ctrlBox', h_main_ctl.getParent(), mo=1)

        self.lock_attribues_for(h_main_ctl, attrs=['translate', 'rotate', 'scale'])
        attribute.add(h_main_ctl.name(), n='visible', type='enum', en=['False', 'True'], dv=0)

        rig_top_group = pm.group(h_main_ctl.getParent(), p='Master_CTL_Rig')

        for hair_rig_data in self.hair_rig_dict:

            h_prefix  = hair_rig_data['name']
            base_crv  = hair_rig_data['base']
            up_crv    = hair_rig_data['up']
            ctl_col   = hair_rig_data['col']
            jnt_num   = hair_rig_data['j_num']

            scale_min_val = hair_rig_data['CTL_ScaleRange'][0]
            scale_max_val = hair_rig_data['CTL_ScaleRange'][1]
            ctl_offset = hair_rig_data['CTL_offset']

            toggle_attribute = h_prefix + '_visiblity'
            attribute.add(h_main_ctl, n=toggle_attribute, type='enum', en=['Disabled', 'FK', 'IK', 'Both'], dv=1)

            path_jnts, path_ctls = path_tools.build(  # Build Path rig from guide curves
                base_crv,
                up_crv,
                node=3,
                joint_num=jnt_num,
                prefix=h_prefix,
                shape='pivot',
                col=ctl_col
            )

            fk_ctls = self.fk_chain_from_path(  # Using path rig as guide, build FK chain
                'Head_Head_joint_M',  # Attach FK to head joint
                path_ctls[1],
                ctl_col,
                toggle_attribute,
            )

            fk_vis_plug, path_vis_plug = self.create_vis_attr_logic(h_main_ctl, toggle_attribute)

            #       Scale controls with custom range along the length of the hair
            scale_factor = scale_min_val
            scale_factor_list = []

            for node in range(len(fk_ctls)):  # Generate a scale factor for every ctl

                scale_factor_list.append(scale_factor)
                scale_factor += (scale_max_val - scale_factor) / (float((len(fk_ctls))))

            path_ctls_no_tangent = path_ctls[1][1:-1]  # exclude first and last path ctl as they have no fk counterpart

            for i_fk_ctl, i_path_ctl, i_scale_f in zip(fk_ctls, path_ctls_no_tangent, reversed(scale_factor_list)):
                # Reverse scale_factor_list to match Fk chain direction

                pm.scale(i_fk_ctl + '.cv[:]', [0.3, 1.0, 0.3], r=True, os=True)
                pm.move(i_fk_ctl + '.cv[:]', [0.0, ctl_offset, 0.0], r=True, os=True)

                pm.scale(i_fk_ctl + '.cv[:]', [i_scale_f, i_scale_f * 1.2, i_scale_f], r=True, os=True)
                pm.scale(i_path_ctl + '.cv[:]', [i_scale_f, i_scale_f * 1.2, i_scale_f], r=True, os=True)

                pm.connectAttr(fk_vis_plug, i_fk_ctl + '.visibility')
                pm.connectAttr(path_vis_plug, i_path_ctl + '.visibility')

            pm.connectAttr(path_vis_plug, path_ctls[1][-1] + '.visibility')

            path_top_grp = h_prefix + '_path_rig'
            fk_top_grp = h_prefix + '_FkCTL_1_ZERO'

            pm.parent(path_top_grp, fk_top_grp, rig_top_group)

            # Post edit
            for j in path_jnts:
                mc.rename(j, j.replace('path', 'out'))

            mc.parentConstraint('Head_Head_joint_M', h_prefix + '_path_master_control', mo=1)


        self.build_mullet_rig(rig_top_group)

    def fk_chain_from_path(self, main_control, path_controls, colour, toggle_attribute):

        fk_ctls_list = []

        path_ctl_guides = [p for p in path_controls[1:-1]] # exclude first and last path control for fk generation,
                                                           # they're useful for tangency on path but excessive on fk

        for index, path_ctl in enumerate(path_ctl_guides):

            path_ctl_guide  = pn(path_ctl)

            fk_ctl_name     = path_ctl.replace('mainCurveCTL', 'FkCTL')
            fk_ctl_trans    = mc.group(n=fk_ctl_name, em=1, w=1)
            fk_ctl          = control.create(
                                fk_ctl_trans,
                                shape='box',
                                colorIdx=colour,
                                zeroGroup=True,
            )
            fk_ctls_list.append(fk_ctl)

            pm.delete(pm.parentConstraint(path_ctl, fk_ctl.getParent()))   # Position FK on Path
            pm.parentConstraint(fk_ctl, path_ctl_guide.getParent())        # Constrain FK >> Path
            pm.scaleConstraint(fk_ctl, path_ctl_guide.getParent())

            if index == 0:                                # Constrain first FK control to head joint
                pm.parentConstraint(main_control, fk_ctl.getParent(), mo=1)
                pm.scaleConstraint(main_control,  fk_ctl.getParent(), mo=1)

            elif index==(len(path_ctl_guides)-1):         # Constrain additional path tangency control to last FK
                pm.parent(fk_ctl.getParent(), fk_ctls_list[index - 1])
                pm.parentConstraint(fk_ctl, pn(path_controls[-1]).getParent(), mo=1)

            else:                                         # Parent zero grp to previous FK for all other FK
                pm.parent(fk_ctl.getParent(), fk_ctls_list[index - 1])

        return fk_ctls_list

    def create_vis_attr_logic(self, main_control,toggle_attribute):

        ik_vis_condition    =  mc.createNode('condition', n='ik_vis_conditionNode')
        fk_vis_condition_a  =  mc.createNode('condition', n='fk_vis_a_conditionNode')
        fk_vis_condition_b  =  mc.createNode('condition', n='fk_vis_b_conditionNode')

        mc.connectAttr(main_control + '.' + toggle_attribute, ik_vis_condition + '.firstTerm')
        mc.setAttr(ik_vis_condition + '.operation', 3)  # Greater or equal
        mc.setAttr(ik_vis_condition + '.secondTerm', 2)
        mc.setAttr(ik_vis_condition + '.colorIfTrueR', 1)
        mc.setAttr(ik_vis_condition + '.colorIfFalseR', 0)
        ik_vis_plug = ik_vis_condition + '.outColorR'

        mc.connectAttr(main_control + '.' + toggle_attribute, fk_vis_condition_a + '.firstTerm')
        mc.setAttr(fk_vis_condition_a + '.operation', 2)  # Greater than
        mc.setAttr(fk_vis_condition_a + '.secondTerm', 0)
        mc.setAttr(fk_vis_condition_a + '.colorIfTrueR', 1)
        mc.setAttr(fk_vis_condition_a + '.colorIfFalseR', 0)

        mc.connectAttr(main_control + '.' + toggle_attribute, fk_vis_condition_b + '.firstTerm')
        mc.setAttr(fk_vis_condition_b + '.operation', 1)   # Not equal to
        mc.setAttr(fk_vis_condition_b + '.secondTerm', 2)
        mc.connectAttr(fk_vis_condition_a + '.outColorR', fk_vis_condition_b + '.colorIfTrueR')
        mc.setAttr(fk_vis_condition_b + '.colorIfFalseR', 0)
        fk_vis_plug = fk_vis_condition_b + '.outColorR'

        return fk_vis_plug, ik_vis_plug

    def build_mullet_rig(self, top_grp):

        mullet_geo = 'mullet_geo'
        mullet_guide_mesh = 'mullet_lattice_guide'

        mesh.copy(self.HAIR_GEO, n=mullet_geo)


        self.create_lattice(mullet_guide_mesh, mullet_geo, name='Mullet')
        #m.parentConstraint('Head_Head_joint_M', 'MulletLatticeGroup', mo=1)
        pm.parentConstraint('Head_Head_joint_M', mullet_geo, mo=1)

        bs_name = 'mullet_blendshape'
        blendshape.apply(mullet_geo,
                         self.HAIR_GEO,
                         dv=1,
                         name=bs_name)

        blendshape.import_weights_from_file(
            filepath='D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_mocap/02_rig/imports/blendshape_weights/mullet/don_mullet_bs_weights_02.json',
            geo=self.HAIR_GEO,
            blendshape=bs_name
        )

        # Install clusters

        cluser_set_list = [
            {'upper_L': ['MulletLattice.pt[2][0][1]', 'MulletLattice.pt[2][1][1]', 'MulletLattice.pt[3][1][1]', 'MulletLattice.pt[3][0][1]', 'MulletLattice.pt[3][1][0]', 'MulletLattice.pt[2][1][0]', 'MulletLattice.pt[3][0][0]', 'MulletLattice.pt[2][0][0]']},
            {'upper_R': ['MulletLattice.pt[1][1][1]', 'MulletLattice.pt[1][1][0]', 'MulletLattice.pt[0][1][0]', 'MulletLattice.pt[0][1][1]', 'MulletLattice.pt[1][0][1]', 'MulletLattice.pt[1][0][0]', 'MulletLattice.pt[0][0][1]', 'MulletLattice.pt[0][0][0]']},
            {'lower_L': ['MulletLattice.pt[2][1][2]', 'MulletLattice.pt[3][1][2]', 'MulletLattice.pt[2][1][3]', 'MulletLattice.pt[3][1][3]', 'MulletLattice.pt[2][0][2]', 'MulletLattice.pt[3][0][2]', 'MulletLattice.pt[2][0][3]', 'MulletLattice.pt[3][0][3]']},
            {'lower_R': ['MulletLattice.pt[1][1][2]', 'MulletLattice.pt[1][1][3]', 'MulletLattice.pt[1][0][2]', 'MulletLattice.pt[1][0][3]', 'MulletLattice.pt[0][1][2]', 'MulletLattice.pt[0][1][3]', 'MulletLattice.pt[0][0][2]', 'MulletLattice.pt[0][0][3]']},
        ]

        root_clusters = ['MulletLattice.pt[0][2][0]', 'MulletLattice.pt[1][2][0]', 'MulletLattice.pt[2][2][0]', 'MulletLattice.pt[3][2][0]', 'MulletLattice.pt[0][2][1]', 'MulletLattice.pt[1][2][1]', 'MulletLattice.pt[2][2][1]', 'MulletLattice.pt[3][2][1]', 'MulletLattice.pt[0][2][2]', 'MulletLattice.pt[1][2][2]', 'MulletLattice.pt[2][2][2]', 'MulletLattice.pt[3][2][2]', 'MulletLattice.pt[0][2][3]', 'MulletLattice.pt[1][2][3]', 'MulletLattice.pt[2][2][3]', 'MulletLattice.pt[3][2][3]']

        control_clusters_dict = dict()

        cluster_grp = mc.group(n=mullet_geo + '_clusters', em=1, w=1)
        mc.setAttr(cluster_grp + '.visibility', 0)

        for control_set in cluser_set_list:
            for name, lattice_points in control_set.items():
                c = mc.cluster(lattice_points, n=name)
                control_clusters_dict[name] = c
                mc.parent(c, cluster_grp)

        root_cluster = mc.cluster(root_clusters, n='root_cluster')
        mc.parent(root_cluster, cluster_grp)
        mc.parentConstraint('Head_Head_joint_M', root_cluster, mo=1)

        control_grp = mc.group(n=mullet_geo + '_controls', em=1, w=1)

        for ctl_name in control_clusters_dict:

            cluster = control_clusters_dict[ctl_name]
            m_ctl_trans = mc.group(em=1, w=1, n=ctl_name + '_ctl')

            m_ctl_control = control.create(
                m_ctl_trans,
                shape='box',
                colorIdx=20,
                zeroGroup=True
            )

            pm.delete(pm.parentConstraint(cluster, m_ctl_control.getParent()))
            mc.delete(mc.orientConstraint(mullet_guide_mesh, m_ctl_trans))
            mc.parentConstraint(m_ctl_trans, cluster, mo=1)
            mc.scaleConstraint(m_ctl_trans, cluster, mo=1

                               )
            pm.parent(m_ctl_control.getParent(), control_grp)
            pm.parentConstraint('Head_Head_joint_M', m_ctl_control.getParent(), mo=1)

        pm.setAttr('MulletLatticeGroup.visibility', 0)

        pm.parent(control_grp, 'MulletLatticeGroup', cluster_grp, top_grp)
        mc.parent(mullet_geo, self.DNT_GRP)

        mc.parentConstraint('Head_Head_joint_M', 'MulletBase', mo=1)

    def wrap_scleras(self):

        wrap.create(
            self.SCLERA_GEO_L,
            self.PUPIL_GEO_L
        )

        wrap.create(
            self.SCLERA_GEO_R,
            self.PUPIL_GEO_R
        )

    def build_jumper_controls(self):

        jumper_grp = pm.group(em=True, n='Jumper', p='Master_CTL_Main')
        jumper_ctl_grp = pm.group(em=True, n='Jumper_controls', p=jumper_grp)
        jumper_jnt_grp = pm.group(em=True, n='Jumper_joints', p=jumper_grp)

        pm.connectAttr('Master_CTL_Main.jointVis', jumper_jnt_grp.v)

        # Jumper_CTL_torsoA
        jumper_ctl_torsoA = control.create(
            'Jumper_CTL_mainA',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )

        jumper_ctl_torsoA_zero = jumper_ctl_torsoA.getParent()
        jumper_ctl_torsoA_zero.t.set([0.38, 91.31, -6.81])
        jumper_ctl_torsoA_zero.r.set([17.41, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoA.cv[:], [14.59, 10.43, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoA_zero, jumper_ctl_grp)

        # Jumper_CTL_torsoB
        jumper_ctl_torsoB = control.create(
            'Jumper_CTL_mainB',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoB_zero = jumper_ctl_torsoB.getParent()
        jumper_ctl_torsoB_zero.t.set([0.38, 81.36, -9.54])
        jumper_ctl_torsoB_zero.r.set([12.34, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoB.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoB_zero, jumper_ctl_torsoA)

        # Jumper_CTL_torsoC
        jumper_ctl_torsoC = control.create(
            'Jumper_CTL_mainC',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoC_zero = jumper_ctl_torsoC.getParent()
        jumper_ctl_torsoC_zero.t.set([0.38, 74.88, -10.75])
        jumper_ctl_torsoC_zero.r.set([10.6, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoC.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoC_zero, jumper_ctl_torsoB)

        # Jumper_CTL_torsoD
        jumper_ctl_torsoD = control.create(
            'Jumper_CTL_mainD',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoD_zero = jumper_ctl_torsoD.getParent()
        jumper_ctl_torsoD_zero.t.set([0.38, 68.17, -11.68])
        jumper_ctl_torsoD_zero.r.set([11.39, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoD.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoD_zero, jumper_ctl_torsoC)

        # Jumper_CTL_torsoE
        jumper_ctl_torsoE = control.create(
            'Jumper_CTL_mainE',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoE_zero = jumper_ctl_torsoE.getParent()
        jumper_ctl_torsoE_zero.t.set([0.38, 61.4, -12.94])
        jumper_ctl_torsoE_zero.r.set([10.09, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoE.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoE_zero, jumper_ctl_torsoD)

        # Jumper_CTL_knot
        jumper_ctl_knot = control.create(
            'Jumper_CTL_knot',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_knot_zero = jumper_ctl_knot.getParent()
        jumper_ctl_knot_zero.t.set([-1.02, 82.77, 12.47])
        pm.scale(jumper_ctl_knot.cv[:], [10.34, 10.34, 8.1], r=True, os=True)
        pm.parent(jumper_ctl_knot_zero, jumper_ctl_grp)
        pm.parentConstraint('Spine_RootPart2_joint_M', jumper_ctl_knot_zero, mo=True)

        # Jumper_CTL_tie_L
        jumper_ctl_tie_L = control.create(
            'Jumper_CTL_tie_L',
            shape='box',
            colorIdx=18,
            zeroGroup=True
        )
        jumper_ctl_tie_L_zero = jumper_ctl_tie_L.getParent()
        jumper_ctl_tie_L_zero.t.set([3.73, 77.83, 13.0])
        jumper_ctl_tie_L_zero.r.set([-5.16, 0.0, 33.22])
        pm.scale(jumper_ctl_tie_L.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tie_L_zero, jumper_ctl_knot)

        # Jumper_CTL_tieA_R
        jumper_ctl_tieA_R = control.create(
            'Jumper_CTL_tieA_R',
            shape='box',
            colorIdx=20,
            zeroGroup=True
        )
        jumper_ctl_tieA_R_zero = jumper_ctl_tieA_R.getParent()
        jumper_ctl_tieA_R.t.set([-4.02, 77.91, 11.93])
        jumper_ctl_tieA_R.r.set([-5.16, 0.0, -33.22])
        pm.scale(jumper_ctl_tieA_R.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tieA_R_zero, jumper_ctl_knot)

        # Jumper_CTL_tieB_R
        jumper_ctl_tieB_R = control.create(
            'Jumper_CTL_tieB_R',
            shape='box',
            colorIdx=20,
            zeroGroup=True
        )
        jumper_ctl_tieB_R_zero = jumper_ctl_tieB_R.getParent()
        jumper_ctl_tieB_R.t.set([-6.39, 74.28, 12.01])
        jumper_ctl_tieB_R.r.set([-5.16, 0.0, -33.22])
        pm.scale(jumper_ctl_tieB_R.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tieB_R_zero, jumper_ctl_tieA_R)

        # joints

        # knot
        pm.select(jumper_jnt_grp, r=True)
        jumper_knot_jnt = pm.joint(n='Jumper_knot_joint')

        pm.parentConstraint(jumper_ctl_knot, jumper_knot_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_knot, jumper_knot_jnt, mo=False)

        pm.select(jumper_knot_jnt, r=True)
        jumper_tie_L_jnt = pm.joint(n='Jumper_tie_joint_L')
        pm.parentConstraint(jumper_ctl_tie_L, jumper_tie_L_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tie_L, jumper_tie_L_jnt, mo=False)

        pm.select(jumper_knot_jnt, r=True)
        jumper_tieA_R_jnt = pm.joint(n='Jumper_tieA_joint_R')
        jumper_tieB_R_jnt = pm.joint(n='Jumper_tieB_joint_R')

        pm.parentConstraint(jumper_ctl_tieA_R, jumper_tieA_R_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tieA_R, jumper_tieA_R_jnt, mo=False)

        pm.parentConstraint(jumper_ctl_tieB_R, jumper_tieB_R_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tieB_R, jumper_tieB_R_jnt, mo=False)

        # torso
        pm.select(jumper_jnt_grp, r=True)
        jumper_torso_A_jnt = pm.joint(n='Jumper_mainA_joint')
        jumper_torso_B_jnt = pm.joint(n='Jumper_mainB_joint')
        jumper_torso_C_jnt = pm.joint(n='Jumper_mainC_joint')
        jumper_torso_D_jnt = pm.joint(n='Jumper_mainD_joint')
        jumper_torso_E_jnt = pm.joint(n='Jumper_mainE_joint')

        pm.parentConstraint('Spine_RootPart2_joint_M', jumper_ctl_torsoA_zero, mo=True)

        pm.parentConstraint(jumper_ctl_torsoA, jumper_torso_A_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoA, jumper_torso_A_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoB, jumper_torso_B_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoB, jumper_torso_B_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoC, jumper_torso_C_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoC, jumper_torso_C_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoD, jumper_torso_D_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoD, jumper_torso_D_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoE, jumper_torso_E_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoE, jumper_torso_E_jnt, mo=False)

        ctl_list = [
            jumper_ctl_knot,
            jumper_ctl_tie_L,
            jumper_ctl_tieA_R,
            jumper_ctl_tieB_R,

            jumper_ctl_torsoA,
            jumper_ctl_torsoB,
            jumper_ctl_torsoC,
            jumper_ctl_torsoD,
            jumper_ctl_torsoE,
        ]

        for ctl in ctl_list:
            ctl.v.set(cb=False, k=False, l=True)

    def build_pouch_controls(self):

        # ctl
        pouch_grp = pm.group(em=True, n='Pouch', p='Master_CTL_Main')
        pouch_ctl_grp = pm.group(em=True, n='Pouch_controls', p=pouch_grp)
        pouch_jnt_grp = pm.group(em=True, n='Pouch_joints', p=pouch_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', pouch_jnt_grp.v)

        pouch_ctl_leg = control.create(
            'Pouch_CTL_main',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )

        pouch_ctl_leg_zero = pouch_ctl_leg.getParent()
        pouch_ctl_leg_zero.t.set([-16.023, 78.879, 1.179])
        pouch_ctl_leg_zero.r.set([123.889, -89.963, -116.98])
        pm.scale(pouch_ctl_leg.cv[:], [10, 5, 5], r=True, os=True)
        pm.parentConstraint('Leg_HipPart1_joint_R', pouch_ctl_leg_zero, mo=True)
        pm.parent(pouch_ctl_leg_zero, pouch_ctl_grp)

        # jnt
        pm.select(pouch_ctl_leg, r=True)
        pouch_jnt = pm.joint(n='Pouch_joint')
        pm.parentConstraint(pouch_ctl_leg, pouch_jnt, mo=False)
        pm.scaleConstraint(pouch_ctl_leg, pouch_jnt, mo=False)

    def build_sunglasses_controls(self):

        root = 'Earring_root_guide'

        hair_guides_data = {
            'earring': {
                'guides': ['Sunglasses_CTL_main_01_guide', 'Sunglasses_CTL_arm_L_guide', 'Sunglasses_CTL_arm_R_guide'],
                'scales': [[20, 9, 1], [3, 3, 3], [3, 3, 3]],
                'colorIdx': 18,
            }
        }

        # Heirarchy
        jumper_grp = pm.group(em=True, n='Sunglasses', p='Master_CTL_Rig')
        jumper_ctl_grp = pm.group(em=True, n='Sunglasses_controls', p=jumper_grp)
        jumper_jnt_grp = pm.group(em=True, n='Sunglasses_joints', p=jumper_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', jumper_jnt_grp.v)

        for label, guide_data in hair_guides_data.items():

            guides = guide_data.get('guides')
            scales = guide_data.get('scales')
            color_idx = guide_data.get('colorIdx')

            ctl_list = list()
            jnt_list = list()
            for i, guide in enumerate(guides):

                # control
                ctl = control.create(
                    guide.replace('_guide', ''),
                    shape='box',
                    colorIdx=color_idx,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)
                pm.delete(pm.parentConstraint(guide, ctl.getParent(), mo=False))
                pm.scale(ctl.cv[:], [scales[i][0], scales[i][1], scales[i][2]], r=True, os=True)
                ctl.v.set(k=False, cb=False, l=True)

                if guide != 'Sunglasses_CTL_main_01_guide':
                    # joint
                    pm.select(cl=True)
                    jnt = pm.joint(n=str(ctl).replace('_CTL_', '_') + '_jnt')
                    jnt_list.append(jnt)
                    pm.parentConstraint(ctl, jnt, mo=False)
                    pm.scaleConstraint(ctl, jnt, mo=False)

                    pm.parent(jnt, jumper_jnt_grp)

                else:

                    rp_position_offset_grp = pm.group(em=1, w=1, n='Sunglasses_CTL_main_01_offset')
                    pm.delete(pm.parentConstraint(ctl, rp_position_offset_grp))
                    pm.select(cl=True)
                    jnt = pm.joint(n=str(ctl).replace('_CTL_', '_') + '_jnt')
                    jnt_list.append(jnt)
                    pm.parentConstraint(rp_position_offset_grp, jnt, mo=False)
                    pm.scaleConstraint(rp_position_offset_grp, jnt, mo=False)

                    pm.parent(jnt, jumper_jnt_grp)

        pm.parent('Sunglasses_CTL_main_01_ZERO', jumper_ctl_grp)
        pm.parent('Sunglasses_CTL_arm_L_ZERO', 'Sunglasses_CTL_main_01')
        pm.parent('Sunglasses_CTL_arm_R_ZERO', 'Sunglasses_CTL_main_01')

        # Create Rotate pivot switch
        frame_CTL = 'Sunglasses_CTL_main_01'
        frame_CTL_rp = control.create(
            'Frame_CTL_main_rotateOffset',
            shape='pivot',
            colorIdx=8,
            zeroGroup=True
        )
        pm.scale(frame_CTL_rp.cv[:], [3, 3, 3], r=True, os=True)

        pm.delete(pm.parentConstraint(frame_CTL, frame_CTL_rp + '_ZERO'))
        pm.parent(frame_CTL_rp + '_ZERO', frame_CTL)
        pm.parent(rp_position_offset_grp, frame_CTL)
        pm.parent('Sunglasses_CTL_arm_L_ZERO', 'Sunglasses_CTL_arm_R_ZERO', rp_position_offset_grp)

        pm.connectAttr(frame_CTL_rp + '.translate', frame_CTL + '.rotatePivot')
        attribute.add(frame_CTL, n='rotatePivot_vis', type='bool', dv=0, cb=True, k=False)
        pm.connectAttr(frame_CTL + '.rotatePivot_vis', frame_CTL_rp + '.visibility', f=1)

        # Parent lens
        mc.parentConstraint('Sunglasses_CTL_main_01_offset', self.ACCESSORY_LENSE, mo=1)
        mc.scaleConstraint('Sunglasses_CTL_main_01_offset', self.ACCESSORY_LENSE, mo=1)

        # create constraint switch

        world_grp = pm.group(em=1, p=jumper_grp, n='Sunglasses_world_driver_grp')
        constrained_grp = pm.group(em=1, p=jumper_grp, n='Sunglasses_const_driver_grp')
        pm.parentConstraint('Head_Head_joint_M', constrained_grp, mo=1)

        zg = zero_grp('Sunglasses_CTL_main_01_ZERO')
        pm.rename(zg, 'Sunglasses_head_constrained_grp')
        const = pm.parentConstraint(world_grp, constrained_grp, 'Sunglasses_head_constrained_grp', mo=1)
        attribute.add('Master_CTL_Main', n='glasses_vis', type='int', dv=1, cb=True, k=False, min=0, max=1)
        attribute.add('Master_CTL_Main', n='glasses_constrain', type='float', dv=1.0, cb=True, k=False, min=0, max=1)

        rev = mc.createNode('reverse')

        mc.connectAttr('Master_CTL_Main.glasses_constrain',
                       'Sunglasses_head_constrained_grp_parentConstraint1.Sunglasses_const_driver_grpW1', f=1)

        mc.connectAttr('Master_CTL_Main.glasses_constrain',
                       rev + '.input.inputX', f=1)

        mc.connectAttr(rev + '.output.outputX',
                       'Sunglasses_head_constrained_grp_parentConstraint1.Sunglasses_world_driver_grpW0', f=1)

        mc.connectAttr('Master_CTL_Main.glasses_vis', self.ACCESSORY_GLASSES + '.visibility', f=1)
        mc.connectAttr('Master_CTL_Main.glasses_vis', self.ACCESSORY_LENSE + '.visibility', f=1)
        mc.connectAttr('Master_CTL_Main.glasses_vis', 'Sunglasses.visibility', f=1)

    def build_boot_controls(self):

        boots_grp = pm.group(em=True, n='Boots', p='Master_CTL_Main')
        boots_ctl_grp = pm.group(em=True, n='Boots_controls', p=boots_grp)
        boots_jnt_grp = pm.group(em=True, n='Boots_joints', p=boots_grp)

        pm.connectAttr('Master_CTL_Main.jointVis', boots_jnt_grp.v)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1
            color = 20 if side == 'R' else 18
            # bootA
            boot_ctl_a = control.create(
                'Boot_CTL_cuffA_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_a_zero = boot_ctl_a.getParent()
            boot_ctl_a_zero.t.set([6.9*side_mult, 14.18, -2.16])
            pm.scale(boot_ctl_a.cv[:], [9.26, 3.27, 9.81], r=True, os=True)
            pm.parent(boot_ctl_a_zero, boots_ctl_grp)

            # bootB
            boot_ctl_b = control.create(
                'Boot_CTL_cuffB_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_b_zero = boot_ctl_b.getParent()
            boot_ctl_b_zero.t.set([7.42*side_mult, 18.68, -1.98])
            boot_ctl_b_zero.r.set([5.88, 0.0, 0.0])
            pm.scale(boot_ctl_b.cv[:], [11.93, 3.27, 14.77], r=True, os=True)
            pm.parent(boot_ctl_b_zero, boot_ctl_a)

            # bootC
            boot_ctl_c = control.create(
                'Boot_CTL_cuffC_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_c_zero = boot_ctl_c.getParent()
            boot_ctl_c_zero.t.set([7.42*side_mult, 23.49, -1.33])
            boot_ctl_c_zero.r.set([11.05, 0.0, 0.0])
            pm.scale(boot_ctl_c.cv[:], [13.52, 3.27, 18.93], r=True, os=True)
            pm.parent(boot_ctl_c_zero, boot_ctl_b)

            pm.select(boots_jnt_grp, r=True)
            boot_a_jnt = pm.joint(n='Boot_cuffA_joint_'+side)
            boot_b_jnt = pm.joint(n='Boot_cuffA_joint_'+side)
            boot_c_jnt = pm.joint(n='Boot_cuffA_joint_'+side)

            pm.parentConstraint('KneePart2_'+side, boot_ctl_a_zero, mo=True)

            pm.parentConstraint(boot_ctl_a, boot_a_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_a, boot_a_jnt, mo=False)
            pm.parentConstraint(boot_ctl_b, boot_b_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_b, boot_b_jnt, mo=False)
            pm.parentConstraint(boot_ctl_c, boot_c_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_c, boot_c_jnt, mo=False)

            for ctl in [
                boot_ctl_a,
                boot_ctl_b,
                boot_ctl_c,
            ]:
                ctl.v.set(cb=False, k=False, l=True)

    def build_torso_controls(self):

        # breast ctl
        torso_grp = pm.group(em=True, n='Breasts', p='Master_CTL_Main')
        torso_ctl_grp = pm.group(em=True, n='Breasts_controls', p=torso_grp)
        torso_jnt_grp = pm.group(em=True, n='Breasts_joints', p=torso_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', torso_jnt_grp.v)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1
            color = 20 if side == 'R' else 18

            # bootA
            breast_ctl_main = control.create(
                'Torso_CTL_breast_'+side,
                shape='ball',
                colorIdx=color,
                zeroGroup=True,
            )
            breast_ctl_main_zero = breast_ctl_main.getParent()
            breast_ctl_main_zero.t.set([5.38 * side_mult, 122.23, 7.33])
            breast_ctl_main_zero.r.set([-14.66, 24.6 * side_mult,  -3.14])
            pm.scale(breast_ctl_main.cv[:], [6.2, 6.2, 6.2], r=True, os=True)
            pm.parent(breast_ctl_main_zero, torso_ctl_grp)
            pm.parentConstraint('Spine_Chest_joint_M', breast_ctl_main_zero, mo=True)
            breast_ctl_main.v.set(cb=False, k=False, l=True)

            # breast_ctl_main_zero = breast_ctl_main.getParent()
            # breast_ctl_main_zero.t.set([6.686*side_mult, 122.304, 8.0])
            # breast_ctl_main_zero.r.set([12.301, 12*side_mult, -0.422])
            # pm.scale(breast_ctl_main.cv[:], [6.2, 6.2, 6.2], r=True, os=True)
            # pm.parent(breast_ctl_main_zero, torso_ctl_grp)
            # pm.parentConstraint('Spine_Chest_joint_M', breast_ctl_main_zero, mo=True)
            # breast_ctl_main.v.set(cb=False, k=False, l=True)


            # jnt
            pm.select(torso_jnt_grp, r=True)
            breast_jnt = pm.joint(n='Torso_breast_joint_'+side)
            #pm.delete(pm.parentConstraint(breast_ctl_main, breast_jnt, mo=False))
            #pm.delete(pm.scaleConstraint(breast_ctl_main, breast_jnt, mo=False))
            pm.parentConstraint(breast_ctl_main, breast_jnt, mo=False)
            pm.scaleConstraint(breast_ctl_main, breast_jnt, mo=False)

    def build_earring_controls(self):

        root = 'Earring_root_guide'

        hair_guides_data = {
            'earring': {
                'guides': ['Earring_CTL_main_01_guide', 'Earring_CTL_main_02_guide'],
                'scales': [0.9, 0.9],
                'colorIdx': 18,
            }
        }

        # Heirarchy
        jumper_grp = pm.group(em=True, n='Earring', p='Master_CTL_Main')
        jumper_ctl_grp = pm.group(em=True, n='Earring_controls', p=jumper_grp)
        jumper_jnt_grp = pm.group(em=True, n='Earring_joints', p=jumper_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', jumper_jnt_grp.v)


        for label, guide_data in hair_guides_data.items():

            print label, guide_data

            guides = guide_data.get('guides')
            scales = guide_data.get('scales')
            color_idx = guide_data.get('colorIdx')

            ctl_list = list()
            jnt_list = list()
            for i, guide in enumerate(guides):

                print 'guide >>', guide

                # control
                ctl = control.create(
                    guide.replace('_guide', ''),
                    shape='box',
                    colorIdx=color_idx,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)
                pm.delete(pm.parentConstraint(guide, ctl.getParent(), mo=False))
                pm.scale(ctl.cv[:], [scales[i], 1, scales[i]], r=True, os=True)
                ctl.v.set(k=False, cb=False, l=True)

                # joint
                pm.select(cl=True)
                jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_jnt')
                jnt_list.append(jnt)
                pm.parentConstraint(ctl, jnt, mo=False)
                pm.scaleConstraint(ctl, jnt, mo=False)

                # parenting
                if i == 0:
                    pm.parent(ctl.getParent(), jumper_ctl_grp)
                    pm.parentConstraint('Head_Head_joint_M', ctl.getParent(), mo=True)
                    pm.parent(jnt, jumper_jnt_grp)
                else:
                    pm.parent(ctl.getParent(), ctl_list[i-1])
                    pm.parent(jnt, jnt_list[i-1])

                jnt.jo.set([0, 0, 0])

        # Root joint
        pm.select(cl=True)
        root_joint = root
        jnt = pm.joint(n='Earring_root')
        jnt_list.append(jnt)

        pm.delete(pm.parentConstraint(root_joint, jnt, mo=False))
        pm.delete(pm.scaleConstraint(root_joint, jnt, mo=False))

        pm.parentConstraint('Head_CTL_FKHead_M', jnt, mo=True)
        pm.scaleConstraint('Head_CTL_FKHead_M', jnt, mo=True)

        pm.parent(jnt, jumper_jnt_grp)

    def create_lattice(self, guide_mesh, deformerGeo, name=None):

        d = float(3)

        # POSITION & SCALE
        position_tuple = mc.xform(guide_mesh, rp=1, ws=1, q=1)
        rotation_tuple = mc.xform(guide_mesh, ro=1, ws=1, q=1)
        scale_tuple = mc.xform(guide_mesh, s=1, ws=1, q=1)

        scale_div_ten = lambda x, y, z: [x/d, y/d, z/d]

        lattice = mc.lattice(
            deformerGeo,
            name=name,
            pos=position_tuple,
            ro=rotation_tuple,
            s=scale_tuple,
            dv=[4,3, 4],   #scale_div_ten(*scale_tuple),
            cp=1,
        )

        base = name + 'Base'

        for p, attr in zip(position_tuple, ['X', 'Y', 'Z']):
            mc.setAttr(base + '.translate{}'.format(attr), p)

        for p, attr in zip(rotation_tuple, ['X', 'Y', 'Z']):
            mc.setAttr(base + '.rotate{}'.format(attr), p)

        for s, attr in zip(scale_tuple, ['X', 'Y', 'Z']):
            mc.setAttr(base + '.scale{}'.format(attr), s)

        mc.select(cl=1)

        return lattice[1], lattice[2]

    def lock_attribues_for(self, node, attrs=[]):

        for dim in ['X', 'Y', 'Z']:
            for a in attrs:
                mc.setAttr('{0}.{1}{2}'.format(node,a,dim), channelBox=False, k=0, l=0)

        mc.setAttr('{}.visibility'.format(node), channelBox=False, k=0, l=0)

    def setup_facerig(self):

        ns = self.FACE_NS+':'

        #face_rig_geo = ns + 'face_rig_geo'


        # parent face rig.
        pm.parent(ns+'faceRig', 'Master_CTL_Main')
        pn(ns+'faceRig').inheritsTransform.set(False)

        pm.parent(ns+'Head_constrained_grp', 'Master_CTL_Main')

        # constrain controls to head.
        pm.parentConstraint('Head_Head_joint_M', ns+'Head_constrained_grp', mo=True)
        pm.parentConstraint('Head_Head_joint_M', ns+'Head_constrained_grp', mo=True)

        for geo in self.FACE_GEO_LIST:
            blendshape.apply(ns+geo, geo, dv=1)

        # hide face geo
        pm.hide(ns+'Geo')

        for side in 'LR':
            for atr in ['rx', 'ry', 'rz']:
                pm.connectAttr(pn('Head_Eye_joint_'+side).attr(atr), pn('face:Eye_driver_'+side+'_GRP').attr(atr))

        hide_list = [
            ns+'Tweaks_controls',
            ns+'Main_joints',
            ns+'Region_joints',

        ]

        for h in hide_list:
            pn(h).v.set(0)

    def import_fix(self):

        mc.delete('head_geo')
        mc.rename('head_geo_revised', 'head_geo')

    def post_edits(self):
        mc.setAttr("Master_CTL_Main.geoLock", 0)
        mc.setAttr("Master_CTL_Main.jointVis", 1)

    # ==================================================================================================================
    def go(self):
        print '='*80
        print 'Starting '+str(self.who)+' Rig Build.'
        mc.file(new=True, f=True)
        self.build_contents()
        print 'Finished!'

    def build_contents(self):

        build.imports(self.imports)
        self.import_fix()

        pm.rename('Group', self.RIG_GRP)
        build.groups(self.groups)
        pm.parent(self.GEO_LIST, self.GEO_GRP)
        self.modify_as_rig()

        self.setup_facerig()

        self.build_earring_controls()
        self.build_sunglasses_controls()
        self.build_hair_rig()

        self.wrap_scleras()

        build.skin_bind(self.skin_bind)
        build.skin_copy(self.skin_copy)
        pm.delete(self.DELETE_GRP)

        #self.post_edits()
"""
#        DON           #
#       CASUAL         # 

from jb_destruna.rigs.e_don import don_casual_animRig
reload(don_casual_animRig)
rig = don_casual_animRig.Rig()
rig.go()
"""