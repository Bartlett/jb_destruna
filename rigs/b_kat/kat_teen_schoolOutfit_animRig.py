from jb_destruna.maya.core import build
import maya.cmds as mc
import pymel.core as pm

from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import skin
from jb_destruna.maya.internal import control
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn

reload(build)
reload(rig)

#


class Rig(rig.Rig):

    ASSET = 'kat'
    VARIANT = 'kat_teen_schoolOutfit'

    #Eye_geo_L
    #Eye_geo_R
    #Accessory_Mask_geo
    #HairFront_geo
    #HairBase_geo
    #MocapSuit_Torn_geo


    FACE_GEO = 'Face_geo'
    HAIR_FRONT_GEO = 'HairFront_geo'
    HAIR_BASE_GEO = 'HairBase_geo'
    HAIR_LONG = 'School_Hair_geo'
    BODY_GEO = 'School_Body_geo'
    PUPIL_GEO_L = 'Eye_geo_L'
    #SCLERA_GEO_L = 'Eye_Sclehra_geo_L'
    #SCLERA_GEO_R = 'Eye_Sclera_geo_R'
    EYEBROW = 'Eyebrows_Geo'
    PUPIL_GEO_R = 'Eye_geo_R'
    ACCESORY_HAT = 'School_Hat_geo'
    CLOTHES = 'School_Clothes_geo'
    # TOPTEETH_GEO = 'topTeeth_geo'

    GEO_LIST = [
        FACE_GEO,
        HAIR_FRONT_GEO,
        HAIR_BASE_GEO,
        HAIR_LONG,
        BODY_GEO,
        PUPIL_GEO_L,
        EYEBROW,
        CLOTHES,
        # SCLERA_GEO_L,
        # SCLERA_GEO_R,
        PUPIL_GEO_R,
    ]

    def __init__(self):
        super(Rig, self).__init__()

        self.who = self.ASSET

        self.imports = [
            # GEO
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/kat/3D/kat_teen_schoolOutfit/01_model/kat_teen_schoolOutfit_model_01_DMR_MASTER.mb',
             'group': self.DELETE_GRP},

            # # LOW GEO
            # # {
            # #     'filepath': '//BIG-RIG\Google Drive\Destruna\masterAssets_forRef\characters\kat/3D\kat_teen_allOutfits/02_rig/02_elements\skinning\kat_skin_mesh.mb'},
            #
            # AS RIG
            {'filepath': 'D:\_Active_Projects\Destruna\masterAssets_forRef\characters/kat/3D\kat_teen_allOutfits/02_rig/03_imports/asRig\Kat_teen_asRig_021'
                         ''
                         ''
                         '.mb'},

            # Skirt ctl guides
            {'filepath': 'D:\_Active_Projects\Destruna\masterAssets_forRef\characters\kat/3D\kat_teen_schoolOutfit/02_rig/03_imports/guides/skirt_guides.mb', 'group': self.DELETE_GRP},

            # # Hair ctl guides
            # {'filepath': '//BIG-RIG\Google Drive\Destruna\masterAssets_forRef\characters\kat/3D\kat_teen_allOutfits/02_rig/03_imports\guides/hair_guides_aa_02.mb', 'group': self.DELETE_GRP},
        ]

        sk_path = 'D:\_Active_Projects\\Destruna\masterAssets_forRef\characters\kat/3D\{}/02_rig/03_imports/skinClusters/'.format(self.VARIANT)

        self.skin_weight_imports = {

            'body_geo': sk_path + 'School_Body_geo_SK/School_Body_001_skinCluster.json',
            'face_geo': sk_path + 'face_geo_SK/face_001_skinCluster.json',

            'HairBase': sk_path + 'hair/HairBase_SK/HairBase_001_skinCluster.json',
            'HairFront': sk_path + 'hair/HairFront_SK/HairFront_001_skinCluster.json',
            'HairLong': sk_path + 'hair/schoolOutfitSchool_Hair_SK/schoolOutfitSchool_Hair_001_skinCluster.json',

            'Eye_L': sk_path + 'eyes/Eye_L_SK/Eye_L_001_skinCluster.json',
            'Eye_R': sk_path + 'eyes/Eye_R_SK/Eye_R_001_skinCluster.json',

            'Clothes': sk_path + 'School_Clothes_SK/School_Clothes_003_skinCluster.json',
            'AccessoryBelt': sk_path + 'MocapSuit_AccessoryBelt_SK/MocapSuit_AccessoryBelt_001_skinCluster.json',
            'Pouch': sk_path + 'MocapSuit_Pouch_SK/MocapSuit_Pouch_002_skinCluster.json',
            'Accessory_Mask': sk_path + 'Accessory_Mask_SK/Accessory_Mask_001_skinCluster.json'

        }

        self.groups = [
            {'g': self.TOP_GRP, 'p': None, 'v': True},
            {'g': self.RIG_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.GEO_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.DNT_GRP, 'p': self.TOP_GRP, 'v': False},
        ]

    def modify_as_rig(self):

        # good stuff happens here.
        super(Rig, self).modify_as_rig()

        # localized stuff happens here --------------
        pn('Master_CTL_Main').bendVis.set(0)

        # lock vis ctls
        for ctl in self.AS_CTL_LIST_NEW:
            pn(ctl).v.set(k=False, cb=False, l=True)

        # attributes on master
        pm.setAttr('Master_CTL_Main.jointVis', 0)
        attribute.add('Master_CTL_Main', n='geoVis', type='bool', dv=1, cb=True, k=False)
        attribute.add('Master_CTL_Main', n='geoLock', type='bool', dv=1, cb=True, k=False)

        pm.connectAttr('Master_CTL_Main.geoLock', 'Geometry.overrideEnabled')
        pm.setAttr('Geometry.overrideDisplayType', 2)

        pm.connectAttr('Master_CTL_Main.geoVis', 'Geometry.v')

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            pm.move('Arm_CTL_FKScapula_'+side+'.cv[:]', [-5*side_mult, 0, 0], r=True, os=True)
            pm.scale('Arm_CTL_FKShoulder_'+side+'.cv[:]', [0.7, 0.7, 0.7], r=True, os=True)
            pm.scale('Arm_CTL_FKWrist_' + side + '.cv[:]', [0.8, 0.8, 0.8], r=True, os=True)


    def build_hair_controls_old(self):

        hair_guides_data = {
            'fringe_L': {
                'guides': ['Hair_CTL_fringe_0_L_guide', 'Hair_CTL_fringe_1_L_guide', 'Hair_CTL_fringe_2_L_guide'],
                'scales': [6, 5, 3],
                'colorIdx': 18,
            },
            'sideA_L': {
                'guides': ['Hair_CTL_sideA_0_L_guide', 'Hair_CTL_sideA_1_L_guide', 'Hair_CTL_sideA_2_L_guide'],
                'scales': [5, 5, 4],
                'colorIdx': 18,
            },
            'cheek_L': {
                'guides': ['Hair_CTL_cheek_0_L_guide', 'Hair_CTL_cheek_1_L_guide', 'Hair_CTL_cheek_2_L_guide', 'Hair_CTL_cheek_3_L_guide'],
                'scales': [3, 2, 2, 2],
                'colorIdx': 18,
            },
            'tail': {
                'guides': ['Hair_CTL_tail_0_guide', 'Hair_CTL_tail_1_guide', 'Hair_CTL_tail_2_guide', 'Hair_CTL_tail_3_guide', 'Hair_CTL_tail_4_guide', 'Hair_CTL_tail_5_guide', 'Hair_CTL_tail_6_guide'],
                'scales': [8, 9, 10, 11, 10, 8, 4],
                'colorIdx': 23,
            },
            'fringeA_R': {
                'guides': ['Hair_CTL_fringeA_0_R_guide', 'Hair_CTL_fringeA_1_R_guide', 'Hair_CTL_fringeA_2_R_guide', 'Hair_CTL_fringeA_3_R_guide'],
                'scales': [6, 5, 3, 3],
                'colorIdx': 20,
            },
            'fringeB_R': {
                'guides': ['Hair_CTL_fringeB_0_R_guide', 'Hair_CTL_fringeB_1_R_guide', 'Hair_CTL_fringeB_2_R_guide'],
                'scales': [6, 6, 4],
                'colorIdx': 20,
            },
            'sideA_R': {
                'guides': ['Hair_CTL_sideA_0_R_guide', 'Hair_CTL_sideA_1_R_guide', 'Hair_CTL_sideA_2_R_guide'],
                'scales': [4, 4, 3],
                'colorIdx': 20,
            },
            'sideB_R': {
                'guides': ['Hair_CTL_sideB_0_R_guide', 'Hair_CTL_sideB_1_R_guide', 'Hair_CTL_sideB_2_R_guide', 'Hair_CTL_sideB_3_R_guide'],
                'scales': [6, 7, 5, 3],
                'colorIdx': 20,
            },
            'sideC_R': {
                'guides': ['Hair_CTL_sideC_0_R_guide', 'Hair_CTL_sideC_1_R_guide', 'Hair_CTL_sideC_2_R_guide', 'Hair_CTL_sideC_3_R_guide'],
                'scales': [5, 4, 4, 2],
                'colorIdx': 20,
            },
            'cheek_R': {
                'guides': ['Hair_CTL_cheek_0_R_guide', 'Hair_CTL_cheek_1_R_guide', 'Hair_CTL_cheek_2_R_guide', 'Hair_CTL_cheek_3_R_guide'],
                'scales': [3, 3, 2, 2],
                'colorIdx': 20,
            }
        }

        # Heirarchy
        jumper_grp = pm.group(em=True, n='Hair', p='Master_CTL_Main')
        jumper_ctl_grp = pm.group(em=True, n='Hair_controls', p=jumper_grp)
        jumper_jnt_grp = pm.group(em=True, n='Hair_joints', p=jumper_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', jumper_jnt_grp.v)

        for label, guide_data in hair_guides_data.items():

            guides = guide_data.get('guides')
            scales = guide_data.get('scales')
            color_idx = guide_data.get('colorIdx')

            ctl_list = list()
            jnt_list = list()
            for i, guide in enumerate(guides):

                # control
                ctl = control.create(
                    guide.replace('_guide', ''),
                    shape='square',
                    colorIdx=color_idx,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)
                pm.delete(pm.parentConstraint(guide, ctl.getParent(), mo=False))
                pm.scale(ctl.cv[:], [scales[i], 1, scales[i]], r=True, os=True)
                ctl.v.set(k=False, cb=False, l=True)

                # joint
                pm.select(cl=True)
                jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_jnt')
                jnt_list.append(jnt)
                pm.parentConstraint(ctl, jnt, mo=False)
                pm.scaleConstraint(ctl, jnt, mo=False)

                # parenting
                if i == 0:
                    pm.parent(ctl.getParent(), jumper_ctl_grp)
                    pm.parentConstraint('Head_Head_joint_M', ctl.getParent(), mo=True)
                    pm.parent(jnt, jumper_jnt_grp)
                else:
                    pm.parent(ctl.getParent(), ctl_list[i-1])
                    pm.parent(jnt, jnt_list[i-1])

    def build_hair_controls(self):

        hair_guides_data = {
            # 'fringe_L': {
            #     'guides': ['Hair_CTL_fringe_0_L_guide', 'Hair_CTL_fringe_1_L_guide', 'Hair_CTL_fringe_2_L_guide'],
            #     'scales': [6, 5, 3],
            #     'colorIdx': 18,
            # },
            # 'sideA_L': {
            #     'guides': ['Hair_CTL_sideA_0_L_guide', 'Hair_CTL_sideA_1_L_guide', 'Hair_CTL_sideA_2_L_guide'],
            #     'scales': [5, 5, 4],
            #     'colorIdx': 18,
            # },
            # 'cheek_L': {
            #     'guides': ['Hair_CTL_cheek_0_L_guide', 'Hair_CTL_cheek_1_L_guide', 'Hair_CTL_cheek_2_L_guide', 'Hair_CTL_cheek_3_L_guide'],
            #     'scales': [3, 2, 2, 2],
            #     'colorIdx': 18,
            # },
            'strand_L': {
                'guides': ['Hair_CTL_0_L_guide',
                           'Hair_CTL_1_L_guide',
                           'Hair_CTL_2_L_guide',
                           'Hair_CTL_3_L_guide',
                           'Hair_CTL_4_L_guide',
                           'Hair_CTL_5_L_guide'],
                'scales': [5, 5, 5, 5, 3, 3],
                'colorIdx': 18,
            },

            'strand_R': {
                'guides': ['Hair_CTL_0_R_guide',
                           'Hair_CTL_1_R_guide',
                           'Hair_CTL_2_R_guide',
                           'Hair_CTL_3_R_guide',
                           'Hair_CTL_4_R_guide',
                           'Hair_CTL_5_R_guide'],
                'scales': [5, 5, 5, 5, 3, 3],
                'colorIdx': 18,
            },
        }

        # Heirarchy
        hair_grp = pm.group(em=True, n='Hair', p='Master_CTL_Main')
        hair_ctl_grp = pm.group(em=True, n='Hair_controls', p=hair_grp)
        hair_jnt_grp = pm.group(em=True, n='Hair_joints', p=hair_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', hair_jnt_grp.v)

        for label, guide_data in hair_guides_data.items():

            guides = guide_data.get('guides')
            scales = guide_data.get('scales')
            color_idx = guide_data.get('colorIdx')

            ctl_list = list()
            jnt_list = list()
            for i, guide in enumerate(guides):

                # control
                ctl = control.create(
                    guide.replace('_guide', ''),
                    shape='square',
                    colorIdx=color_idx,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)
                pm.delete(pm.parentConstraint(guide, ctl.getParent(), mo=False))
                pm.scale(ctl.cv[:], [scales[i], 1, scales[i]], r=True, os=True)
                ctl.v.set(k=False, cb=False, l=True)

                # joint
                pm.select(cl=True)
                jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_jnt')
                jnt_list.append(jnt)
                pm.parentConstraint(ctl, jnt, mo=False)
                pm.scaleConstraint(ctl, jnt, mo=False)

                # parenting
                if i == 0:
                    pm.parent(ctl.getParent(), hair_ctl_grp)
                    pm.parentConstraint('Head_Head_joint_M', ctl.getParent(), mo=True)
                    pm.parent(jnt, hair_jnt_grp)
                else:
                    pm.parent(ctl.getParent(), ctl_list[i-1])
                    pm.parent(jnt, jnt_list[i-1])


    def build_jumper_controls(self):

        jumper_grp = pm.group(em=True, n='Jumper', p='Master_CTL_Main')
        jumper_ctl_grp = pm.group(em=True, n='Jumper_controls', p=jumper_grp)
        jumper_jnt_grp = pm.group(em=True, n='Jumper_joints', p=jumper_grp)

        pm.connectAttr('Master_CTL_Main.jointVis', jumper_jnt_grp.v)

        # Jumper_CTL_torsoA
        jumper_ctl_torsoA = control.create(
            'Jumper_CTL_mainA',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )

        jumper_ctl_torsoA_zero = jumper_ctl_torsoA.getParent()
        jumper_ctl_torsoA_zero.t.set([0.38, 91.31, -6.81])
        jumper_ctl_torsoA_zero.r.set([17.41, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoA.cv[:], [14.59, 10.43, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoA_zero, jumper_ctl_grp)

        # Jumper_CTL_torsoB
        jumper_ctl_torsoB = control.create(
            'Jumper_CTL_mainB',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoB_zero = jumper_ctl_torsoB.getParent()
        jumper_ctl_torsoB_zero.t.set([0.38, 81.36, -9.54])
        jumper_ctl_torsoB_zero.r.set([12.34, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoB.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoB_zero, jumper_ctl_torsoA)

        # Jumper_CTL_torsoC
        jumper_ctl_torsoC = control.create(
            'Jumper_CTL_mainC',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoC_zero = jumper_ctl_torsoC.getParent()
        jumper_ctl_torsoC_zero.t.set([0.38, 74.88, -10.75])
        jumper_ctl_torsoC_zero.r.set([10.6, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoC.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoC_zero, jumper_ctl_torsoB)

        # Jumper_CTL_torsoD
        jumper_ctl_torsoD = control.create(
            'Jumper_CTL_mainD',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoD_zero = jumper_ctl_torsoD.getParent()
        jumper_ctl_torsoD_zero.t.set([0.38, 68.17, -11.68])
        jumper_ctl_torsoD_zero.r.set([11.39, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoD.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoD_zero, jumper_ctl_torsoC)

        # Jumper_CTL_torsoE
        jumper_ctl_torsoE = control.create(
            'Jumper_CTL_mainE',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoE_zero = jumper_ctl_torsoE.getParent()
        jumper_ctl_torsoE_zero.t.set([0.38, 61.4, -12.94])
        jumper_ctl_torsoE_zero.r.set([10.09, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoE.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoE_zero, jumper_ctl_torsoD)

        # Jumper_CTL_knot
        jumper_ctl_knot = control.create(
            'Jumper_CTL_knot',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_knot_zero = jumper_ctl_knot.getParent()
        jumper_ctl_knot_zero.t.set([-1.02, 82.77, 12.47])
        pm.scale(jumper_ctl_knot.cv[:], [10.34, 10.34, 8.1], r=True, os=True)
        pm.parent(jumper_ctl_knot_zero, jumper_ctl_grp)
        pm.parentConstraint('Spine_RootPart2_joint_M', jumper_ctl_knot_zero, mo=True)

        # Jumper_CTL_tie_L
        jumper_ctl_tie_L = control.create(
            'Jumper_CTL_tie_L',
            shape='box',
            colorIdx=18,
            zeroGroup=True
        )
        jumper_ctl_tie_L_zero = jumper_ctl_tie_L.getParent()
        jumper_ctl_tie_L_zero.t.set([3.73, 77.83, 13.0])
        jumper_ctl_tie_L_zero.r.set([-5.16, 0.0, 33.22])
        pm.scale(jumper_ctl_tie_L.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tie_L_zero, jumper_ctl_knot)

        # Jumper_CTL_tieA_R
        jumper_ctl_tieA_R = control.create(
            'Jumper_CTL_tieA_R',
            shape='box',
            colorIdx=20,
            zeroGroup=True
        )
        jumper_ctl_tieA_R_zero = jumper_ctl_tieA_R.getParent()
        jumper_ctl_tieA_R.t.set([-4.02, 77.91, 11.93])
        jumper_ctl_tieA_R.r.set([-5.16, 0.0, -33.22])
        pm.scale(jumper_ctl_tieA_R.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tieA_R_zero, jumper_ctl_knot)

        # Jumper_CTL_tieB_R
        jumper_ctl_tieB_R = control.create(
            'Jumper_CTL_tieB_R',
            shape='box',
            colorIdx=20,
            zeroGroup=True
        )
        jumper_ctl_tieB_R_zero = jumper_ctl_tieB_R.getParent()
        jumper_ctl_tieB_R.t.set([-6.39, 74.28, 12.01])
        jumper_ctl_tieB_R.r.set([-5.16, 0.0, -33.22])
        pm.scale(jumper_ctl_tieB_R.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tieB_R_zero, jumper_ctl_tieA_R)

        # joints

        # knot
        pm.select(jumper_jnt_grp, r=True)
        jumper_knot_jnt = pm.joint(n='Jumper_knot_joint')

        pm.parentConstraint(jumper_ctl_knot, jumper_knot_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_knot, jumper_knot_jnt, mo=False)

        pm.select(jumper_knot_jnt, r=True)
        jumper_tie_L_jnt = pm.joint(n='Jumper_tie_joint_L')
        pm.parentConstraint(jumper_ctl_tie_L, jumper_tie_L_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tie_L, jumper_tie_L_jnt, mo=False)

        pm.select(jumper_knot_jnt, r=True)
        jumper_tieA_R_jnt = pm.joint(n='Jumper_tieA_joint_R')
        jumper_tieB_R_jnt = pm.joint(n='Jumper_tieB_joint_R')

        pm.parentConstraint(jumper_ctl_tieA_R, jumper_tieA_R_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tieA_R, jumper_tieA_R_jnt, mo=False)

        pm.parentConstraint(jumper_ctl_tieB_R, jumper_tieB_R_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tieB_R, jumper_tieB_R_jnt, mo=False)

        # torso
        pm.select(jumper_jnt_grp, r=True)
        jumper_torso_A_jnt = pm.joint(n='Jumper_mainA_joint')
        jumper_torso_B_jnt = pm.joint(n='Jumper_mainB_joint')
        jumper_torso_C_jnt = pm.joint(n='Jumper_mainC_joint')
        jumper_torso_D_jnt = pm.joint(n='Jumper_mainD_joint')
        jumper_torso_E_jnt = pm.joint(n='Jumper_mainE_joint')

        pm.parentConstraint('Spine_RootPart2_joint_M', jumper_ctl_torsoA_zero, mo=True)

        pm.parentConstraint(jumper_ctl_torsoA, jumper_torso_A_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoA, jumper_torso_A_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoB, jumper_torso_B_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoB, jumper_torso_B_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoC, jumper_torso_C_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoC, jumper_torso_C_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoD, jumper_torso_D_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoD, jumper_torso_D_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoE, jumper_torso_E_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoE, jumper_torso_E_jnt, mo=False)

        ctl_list = [
            jumper_ctl_knot,
            jumper_ctl_tie_L,
            jumper_ctl_tieA_R,
            jumper_ctl_tieB_R,

            jumper_ctl_torsoA,
            jumper_ctl_torsoB,
            jumper_ctl_torsoC,
            jumper_ctl_torsoD,
            jumper_ctl_torsoE,
        ]

        for ctl in ctl_list:
            ctl.v.set(cb=False, k=False, l=True)

    def build_pouch_controls(self):

        # ctl
        pouch_grp = pm.group(em=True, n='Pouch', p='Master_CTL_Main')
        pouch_ctl_grp = pm.group(em=True, n='Pouch_controls', p=pouch_grp)
        pouch_jnt_grp = pm.group(em=True, n='Pouch_joints', p=pouch_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', pouch_jnt_grp.v)

        pouch_ctl_leg = control.create(
            'Pouch_CTL_main',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )

        pouch_ctl_leg_zero = pouch_ctl_leg.getParent()
        pouch_ctl_leg_zero.t.set([-16.023, 78.879, 1.179])
        pouch_ctl_leg_zero.r.set([123.889, -89.963, -116.98])
        pm.scale(pouch_ctl_leg.cv[:], [10, 5, 5], r=True, os=True)
        pm.parentConstraint('Leg_HipPart1_joint_R', pouch_ctl_leg_zero, mo=True)
        pm.parent(pouch_ctl_leg_zero, pouch_ctl_grp)

        # jnt
        pm.select(pouch_ctl_leg, r=True)
        pouch_jnt = pm.joint(n='Pouch_joint')
        pm.parentConstraint(pouch_ctl_leg, pouch_jnt, mo=False)
        pm.scaleConstraint(pouch_ctl_leg, pouch_jnt, mo=False)

    def build_boot_controls(self):

        boots_grp = pm.group(em=True, n='Boots', p='Master_CTL_Main')
        boots_ctl_grp = pm.group(em=True, n='Boots_controls', p=boots_grp)
        boots_jnt_grp = pm.group(em=True, n='Boots_joints', p=boots_grp)

        pm.connectAttr('Master_CTL_Main.jointVis', boots_jnt_grp.v)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1
            color = 20 if side == 'R' else 18
            # bootA
            boot_ctl_a = control.create(
                'Boot_CTL_cuffA_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_a_zero = boot_ctl_a.getParent()
            boot_ctl_a_zero.t.set([6.9*side_mult, 14.18, -2.16])
            pm.scale(boot_ctl_a.cv[:], [9.26, 3.27, 9.81], r=True, os=True)
            pm.parent(boot_ctl_a_zero, boots_ctl_grp)

            # bootB
            boot_ctl_b = control.create(
                'Boot_CTL_cuffB_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_b_zero = boot_ctl_b.getParent()
            boot_ctl_b_zero.t.set([7.42*side_mult, 18.68, -1.98])
            boot_ctl_b_zero.r.set([5.88, 0.0, 0.0])
            pm.scale(boot_ctl_b.cv[:], [11.93, 3.27, 14.77], r=True, os=True)
            pm.parent(boot_ctl_b_zero, boot_ctl_a)

            # bootC
            boot_ctl_c = control.create(
                'Boot_CTL_cuffC_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_c_zero = boot_ctl_c.getParent()
            boot_ctl_c_zero.t.set([7.42*side_mult, 23.49, -1.33])
            boot_ctl_c_zero.r.set([11.05, 0.0, 0.0])
            pm.scale(boot_ctl_c.cv[:], [13.52, 3.27, 18.93], r=True, os=True)
            pm.parent(boot_ctl_c_zero, boot_ctl_b)

            pm.select(boots_jnt_grp, r=True)
            boot_a_jnt = pm.joint(n='Boot_cuffA_joint_'+side)
            boot_b_jnt = pm.joint(n='Boot_cuffA_joint_'+side)
            boot_c_jnt = pm.joint(n='Boot_cuffA_joint_'+side)

            pm.parentConstraint('KneePart2_'+side, boot_ctl_a_zero, mo=True)

            pm.parentConstraint(boot_ctl_a, boot_a_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_a, boot_a_jnt, mo=False)
            pm.parentConstraint(boot_ctl_b, boot_b_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_b, boot_b_jnt, mo=False)
            pm.parentConstraint(boot_ctl_c, boot_c_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_c, boot_c_jnt, mo=False)

            for ctl in [
                boot_ctl_a,
                boot_ctl_b,
                boot_ctl_c,
            ]:
                ctl.v.set(cb=False, k=False, l=True)

    def build_torso_controls(self):

        # breast ctl
        torso_grp = pm.group(em=True, n='Breasts', p='Master_CTL_Main')
        torso_ctl_grp = pm.group(em=True, n='Breasts_controls', p=torso_grp)
        torso_jnt_grp = pm.group(em=True, n='Breasts_joints', p=torso_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', torso_jnt_grp.v)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1
            color = 20 if side == 'R' else 18

            # bootA
            breast_ctl_main = control.create(
                'Torso_CTL_breast_'+side,
                shape='ball',
                colorIdx=color,
                zeroGroup=True,
            )
            breast_ctl_main_zero = breast_ctl_main.getParent()
            breast_ctl_main_zero.t.set([5.38 * side_mult, 122.23, 7.33])
            breast_ctl_main_zero.r.set([-14.66, 24.6 * side_mult,  -3.14])
            pm.scale(breast_ctl_main.cv[:], [6.2, 6.2, 6.2], r=True, os=True)
            pm.parent(breast_ctl_main_zero, torso_ctl_grp)
            pm.parentConstraint('Spine_Chest_joint_M', breast_ctl_main_zero, mo=True)
            breast_ctl_main.v.set(cb=False, k=False, l=True)

            # breast_ctl_main_zero = breast_ctl_main.getParent()
            # breast_ctl_main_zero.t.set([6.686*side_mult, 122.304, 8.0])
            # breast_ctl_main_zero.r.set([12.301, 12*side_mult, -0.422])
            # pm.scale(breast_ctl_main.cv[:], [6.2, 6.2, 6.2], r=True, os=True)
            # pm.parent(breast_ctl_main_zero, torso_ctl_grp)
            # pm.parentConstraint('Spine_Chest_joint_M', breast_ctl_main_zero, mo=True)
            # breast_ctl_main.v.set(cb=False, k=False, l=True)


            # jnt
            pm.select(torso_jnt_grp, r=True)
            breast_jnt = pm.joint(n='Torso_breast_joint_'+side)
            #pm.delete(pm.parentConstraint(breast_ctl_main, breast_jnt, mo=False))
            #pm.delete(pm.scaleConstraint(breast_ctl_main, breast_jnt, mo=False))
            pm.parentConstraint(breast_ctl_main, breast_jnt, mo=False)
            pm.scaleConstraint(breast_ctl_main, breast_jnt, mo=False)
    def build_skirt(self):

        waist = 'WAIST'
        waist_aim = 'WAIST_AIM'
        hem = 'HEM'
        node = 10
        mc.spaceLocator(n='up_aim')
        mc.select(cl=1)

        for i in range(node):
            for side, sideMult in zip(['left', 'right'], [1, -1]):
                up = 'up_aim'

                point = mc.pointOnCurve(waist, pr=(i / float(node - 1)))
                point = [point[0] * sideMult, point[1], point[2]]
                waist_joint = mc.joint(p=point)

                up_point = mc.pointOnCurve(waist_aim, pr=(i / float(node - 1)))

                for attr, value in zip(['X', 'Y', 'Z'], up_point):
                    mc.setAttr(up + '.translate{}'.format(attr), value)

                mc.select(cl=1)

                point = mc.pointOnCurve(hem, pr=(i / float(node - 1)))
                point = [point[0] * sideMult, point[1], point[2]]
                hem_joint = mc.joint(p=point)

                mc.delete(mc.aimConstraint(waist_aim, waist_joint, aimVector=[0, 0, 1], upVector=[0, 1, 0],
                                               worldUpType='Scene'))

                mc.parent(hem_joint, waist_joint)
                mc.parent(waist_joint, 'Spine_Root_joint_M')
                mc.select(cl=1)

                # print cmds.pointOnCurve(waist, pr=0.5)

    def assign_skinClusters(self):

        # Set body geo skin cluster
        skin.import_from_file(
            self.skin_weight_imports['body_geo'],
            self.BODY_GEO)
        skin.import_from_file(
            self.skin_weight_imports['face_geo'],
            self.FACE_GEO)
        skin.import_from_file(
            self.skin_weight_imports['Eye_L'],
            self.PUPIL_GEO_L)
        skin.import_from_file(
            self.skin_weight_imports['Eye_R'],
            self.PUPIL_GEO_R)

        # Clothes

        skin.import_from_file(
            self.skin_weight_imports['Clothes'],
            self.CLOTHES)


        # Skin Hair
        skin.import_from_file(
            self.skin_weight_imports['HairBase'],
            self.HAIR_BASE_GEO)
        skin.import_from_file(
            self.skin_weight_imports['HairFront'],
            self.HAIR_FRONT_GEO)
        skin.import_from_file(
            self.skin_weight_imports['HairLong'],
            self.HAIR_LONG)

        # # Accessory
        # skin.import_from_file(
        #     self.skin_weight_imports['AccessoryBelt'],
        #     self.ACCESORY_BELT)
        # skin.import_from_file(
        #     self.skin_weight_imports['Pouch'],
        #     self.ACCESORY_POUCH)
        # skin.import_from_file(
        #     self.skin_weight_imports['Accessory_Mask'],
        #     self.ACCESORY_MASK)

    def post_edits(self):
        mc.setAttr("Master_CTL_Main.geoLock", 0)
        mc.setAttr("Master_CTL_Main.jointVis", 1)


    # ==================================================================================================================
    def go(self):
        print '='*80
        print 'Starting '+str(self.who)+' Rig Build.'
        mc.file(new=True, f=True)
        self.build_contents()
        print 'Finished!'

    def build_contents(self):
        build.imports(self.imports)

        pm.rename('Group', self.RIG_GRP)
        build.groups(self.groups)

        pm.parent(self.GEO_LIST, self.GEO_GRP)
        self.modify_as_rig()
        self.build_skirt()

        #self.build_jumper_controls()

        #self.build_pouch_controls()

        #self.build_hair_controls()

        #self.build_boot_controls()
        self.build_torso_controls()

        self.assign_skinClusters()

        pm.delete(self.DELETE_GRP)

        self.post_edits()



"""
######    SCHOOL     ###### 

from jb_destruna.rigs.b_kat import kat_teen_schoolOutfit_animRig
reload(kat_teen_schoolOutfit_animRig)

rig = kat_teen_schoolOutfit_animRig.Rig()
rig.go()
"""