from jb_destruna.maya.core import build
from jb_destruna.maya.core import face
from jb_destruna.maya.core import tweak
from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.internal import control
from jb_destruna.maya.internal import cpom
from jb_destruna.maya.internal import curve
from jb_destruna.maya.internal import deformer
from jb_destruna.maya.internal import follicle
from jb_destruna.maya.internal import skin
from jb_destruna.maya.internal import skin_ng
from jb_destruna.maya.internal import mesh
from jb_destruna.maya.internal import wrap
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility import misc
from jb_destruna.maya.utility.misc import pn
import pymel.core as pm


class FaceRig(face.FaceRig):

    ASSET_NAME = 'kat'

    FACE_RIG_GEO = 'face_rig_geo'
    FACE_REGION_GEO = 'face_region_geo'
    FACE_REGION_DRIVER = 'face_region_geo_driver'
    FACE_REGION_TRIDRIVER = 'face_region_geo_tridriver'
    FACE_TWEAKS_GEO = 'face_tweaks_geo'
    FACE_TWEAKS_DRIVER = 'face_tweaks_geo_driver'

    ACCESSORY_MASK_GEO = 'Accessory_Mask_geo'
    BROWS_GEO = 'brows_geo'
    FACE_GEO = 'face_geo'
    IRIS_L_GEO = 'iris_L_geo'
    IRIS_R_GEO = 'iris_R_geo'
    LASHCLOSED_L_GEO = 'lashClosed_L_geo'
    LASHCLOSED_R_GEO = 'lashClosed_R_geo'
    LASH_L_GEO = 'lash_L_geo'
    LASH_R_GEO = 'lash_R_geo'
    SCLERA_L_GEO = 'sclera_L_geo'
    SCLERA_R_GEO = 'sclera_R_geo'
    TEETHLOWER_GEO = 'teethLower_geo'
    TEETHUPPER_GEO = 'teethUpper_geo'
    TONGUE_GEO = 'tongue_geo'

    FACE_GEO_LIST = [
        ACCESSORY_MASK_GEO,
        BROWS_GEO,
        FACE_GEO,
        IRIS_L_GEO,
        IRIS_R_GEO,
        LASHCLOSED_L_GEO,
        LASHCLOSED_R_GEO,
        LASH_L_GEO,
        LASH_R_GEO,
        SCLERA_L_GEO,
        SCLERA_R_GEO,
        TEETHLOWER_GEO,
        TEETHUPPER_GEO,
        TONGUE_GEO,
    ]

    HEAD_CONSTRAINED_GRP = 'Head_constrained_grp'

    FACE_BS_GEO = 'face_bs_geo'

    TWEAKS = 'Tweaks'
    TWEAKS_CTL_GRP = TWEAKS+'_controls'
    TWEAKS_JNT_GRP = TWEAKS+'_joints'

    FACE_RIG_GEO_BASE = 'face_rig_geo_base'
    FACE_GEO_WRAPPOSE = 'face_geo_wrapPose'

    HEAD_CONSTRAINED_POSITION = [-0.0, 147.069, -4.468]

    def __init__(self):
        super(FaceRig, self).__init__()

        self.imports = [
            # FACE GEO
            {'filepath': 'E:/destruna/assets/kat/face/imports/face_geo_v004.mb', 'group': self.DELETE_GRP},

            # SCENE (CURRENT BS RIG)
            {'filepath': 'E:/destruna/assets/kat/face/imports/face_bs_v006.mb', 'group': self.DELETE_GRP},

            # SCENE (CTL LABELS)
            {'filepath': 'E:/destruna/assets/lona/face/imports/ctl_box_labels_v001.mb', 'group': self.DELETE_GRP},

            # GEO (REGION TRI DRIVER)
            {'filepath': 'E:/destruna/assets/kat/face/imports/region_tridriver_v002.mb', 'group': self.DO_NOT_TOUCH_GRP},

            # SCENE (CTL CURVES)
            {'filepath': 'E:/destruna/assets/kat/face/imports/ctl_crvs_v003.mb', 'group': self.DELETE_GRP},

            # SCENE (REGION CONTROL CURVES)
            {'filepath': 'E:/destruna/assets/kat/face/imports/region_controls_v002.mb', 'group': self.DO_NOT_TOUCH_GRP},

            # SCENE (WRAPPER GEO)
            {'filepath': 'E:/destruna/assets/kat/face/imports/wrapper_geo_v003.mb', 'group': self.DELETE_GRP},

            # SCENE MASK BS GEO
            {'filepath': 'E:/destruna/assets/kat/face/imports/mask_shapes_v002.mb', 'group': self.DO_NOT_TOUCH_GRP},
        ]

        self.groups.extend([
            {'g': self.BROW_STUFF, 'p': self.DO_NOT_TOUCH_GRP, 'v': True},
            {'g': self.EYE_STUFF, 'p': self.DO_NOT_TOUCH_GRP, 'v': True},
            {'g': self.LASH_STUFF, 'p': self.DO_NOT_TOUCH_GRP, 'v': True},
        ])

        self.region_fol_dict = {
            self.REGION_CTL_BROW_L: 17,
            self.REGION_CTL_BROW_R: 7,
            self.REGION_CTL_CHEEK_L: 15,
            self.REGION_CTL_CHEEK_R: 2,
            self.REGION_CTL_CHIN: 0,
            self.REGION_CTL_EYE_L: 16,
            self.REGION_CTL_EYE_R: 4,
            self.REGION_CTL_MOUTH: 1,
            self.REGION_CTL_NOSE: 3,
        }

        self.tweak_controls_setup = [
            {
                'name': 'Tweaks',
                'parent': self.RIG_GRP,
                'tweak_data': [
                    # mouth ===================
                    # upper
                    {'prefix': 'Tweak', 'description': 'mouthUpperA', 'side': 'M', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[163]'},
                    {'prefix': 'Tweak', 'description': 'mouthUpperB', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[852]'},
                    {'prefix': 'Tweak', 'description': 'mouthUpperC', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1595]'},
                    {'prefix': 'Tweak', 'description': 'mouthUpperB', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[5809]'},
                    {'prefix': 'Tweak', 'description': 'mouthUpperC', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[2737]'},

                    # corners
                    {'prefix': 'Tweak', 'description': 'mouthCorner', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1560]'},
                    {'prefix': 'Tweak', 'description': 'mouthCorner', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[2707]'},

                    # lower
                    {'prefix': 'Tweak', 'description': 'mouthLowerA', 'side': 'M', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[902]'},
                    {'prefix': 'Tweak', 'description': 'mouthLowerB', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[903]'},
                    {'prefix': 'Tweak', 'description': 'mouthLowerC', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[866]'},
                    {'prefix': 'Tweak', 'description': 'mouthLowerB', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[5859]'},
                    {'prefix': 'Tweak', 'description': 'mouthLowerC', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[5817]'},

                    # # LHS
                    {'prefix': 'Tweak', 'description': 'eyeInnerCorner', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[3794]'},
                    {'prefix': 'Tweak', 'description': 'eyeOuterCorner', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[3800]'},
                    #
                    {'prefix': 'Tweak', 'description': 'eyeUpperA', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1218]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperB', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[1391]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperC', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[3789]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperD', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[1395]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperE', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[3796]'},
                    # #
                    {'prefix': 'Tweak', 'description': 'eyeLowerA', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[2302]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerB', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[10513]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerC', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[3396]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerD', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[3776]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerE', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[3786]'},
                    #
                    # # RHS
                    {'prefix': 'Tweak', 'description': 'eyeInnerCorner', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[3834]'},
                    {'prefix': 'Tweak', 'description': 'eyeOuterCorner', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[3840]'},
                    # #
                    {'prefix': 'Tweak', 'description': 'eyeUpperA', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1238]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperB', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[6340]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperC', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[3829]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperD', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[6344]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperE', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[3836]'},
                    # #
                    {'prefix': 'Tweak', 'description': 'eyeLowerA', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[7218]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerB', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[10611]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerC', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8309]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerD', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[3816]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerE', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[3826]'},
                ]
            }
        ]

        self.ctl_data_list = [
            {'name': 'Main_CTL_browA_<s>', 'translate': [1.524, 155.378, 7.571]},
            {'name': 'Main_CTL_browB_<s>', 'translate': [3.413, 155.76, 7.147]},
            {'name': 'Main_CTL_browC_<s>', 'translate': [5.293, 155.881, 6.447]},
            {'name': 'Main_CTL_browD_<s>', 'translate': [7.1, 155.804, 5.297]},
            {'name': 'Main_CTL_browE_<s>', 'translate': [8.556, 155.336, 3.883]},
        ]

        self.wrapDeformer_create = [
            {
                'wrapped': 'brows_geo',
                'wrapper': 'brow_wrapper',
                'base': 'brow_wrapperBase',
                'baseParent': self.BROW_STUFF,
            },
        ]

        self.skin_to_joint = {
            'Main_upperTeeth_joint': [
                'teethUpper_geo'
            ],
            'Main_lowerTeeth_joint': [
                'teethLower_geo'
            ],
            'eye_jnt_L': [
                'iris_L_geo'
            ],
            'eye_jnt_R': [
                'iris_R_geo'
            ],
            'Region_CTL_eye_L_jnt': [
                'sclera_L_geo'
            ],
            'Region_CTL_eye_R_jnt': [
                'sclera_R_geo'
            ],
        }

        self.skin_bind = [
            {'geo': 'tongue_geo', 'filepath': 'E:/destruna/assets/kat/face/imports/tongue_geo_skinCluster_v001.nwts', 'import_module': 'skin_ng'}
        ]


    def initial_setup_steps(self):
        """
        This is just a utility function. nothing too crazy happening.

        - duplicate face blendshape geo
        - create and position head_constrained_grp
        - parent geo into appropriate groups
        - parent/rename some AS face rig elements
        - delete face rig guides
        """
        # duplicate face blendshape geo
        mesh.copy(self.FACE_BS_GEO, n=self.FACE_RIG_GEO)
        #pm.parent(self.FACE_RIG_GEO, self.DO_NOT_TOUCH_GRP)

        # create and position head_constrained_grp
        head_constrained_grp = pm.group(em=True, n=self.HEAD_CONSTRAINED_GRP)
        head_constrained_grp.t.set(self.HEAD_CONSTRAINED_POSITION)
        pm.parent('ctrlBoxOffset', self.HEAD_CONSTRAINED_GRP)

        # parent geo into appropriate groups
        pm.parent(self.FACE_GEO_LIST, self.GEO_GRP)
        # pm.parent(self.FACE_RIG_GEO, self.DO_NOT_TOUCH_GRP)

        # parent/rename some AS face rig elements
        pm.parent('Main', self.RIG_GRP)
        ctl_grp = pm.rename('FaceMotionSystem', 'Main_controls')
        jnt_grp = pm.rename('FaceDeformationSystem', 'Main_joints')
        dnt_grp = pm.group(em=True, n='Main_doNotTouch', p='Main')
        pm.hide(dnt_grp)
        # pm.parent(self.FACE_BS_GEO, dnt_grp)

        # delete face rig guides
        pm.delete('FaceFitSkeleton')

    def setup_mouth_controls(self):
        """
        This function hacks the resulting nodes from the AS face build and makes it useable for my needs.
        For this to work, proper preparation is important using the detach_and_delete_bodyrig() function.
        I would also advise against changing anything in here, as its pretty delicate (badly written)
        - unlock joints grp
        - delete unused stuff
        - setup upper teeth floating controls
        - setup jaw driven grp
        - setup lower teeth floating controls
        - setup tongue controls

        """
        # unlock joints grp
        joints_grp = pn('Main_joints')
        joints_grp.v.set(l=False)
        joints_grp.v.set(True)

        # delete unused stuff.
        unused = ['LidCurves_L', 'LidCurves_R', 'LidSetup', 'LipSetup']
        pm.delete(unused)

        # UPPER TEETH ==================================================================================================
        teeth_upper_ctl = pn('upperTeeth_M')
        teeth_upper_ctl.rename('Main_CTL_upperTeeth')

        # use mouth crv shapes to make the mouth shapes better.
        pm.parent('upper_teeth_crv', teeth_upper_ctl)
        pm.makeIdentity('upper_teeth_crv', t=True, r=True, s=True, apply=True)
        pm.parent('upper_teeth_crv', self.DELETE_GRP)
        pm.connectAttr(pn('upper_teeth_crv').local, teeth_upper_ctl.create, f=True)
        teeth_upper_ctl.create.disconnect()

        # override color (red)
        teeth_upper_ctl.getShape().overrideEnabled.set(True)
        teeth_upper_ctl.getShape().overrideColor.set(13)

        # setup head constrained bypass
        pm.parent('upperTeethOffset_M', self.HEAD_CONSTRAINED_GRP)
        # pm.delete(['upperTeethJoint_M_parentConstraint1', 'upperTeethJoint_M_scaleConstraint1'])

        pm.select(cl=True)

        teeth_upper_jnt = pm.joint(n='Main_upperTeeth_joint')
        # teeth_upper_jnt.rename('Main_upperTeeth_joint')
        teeth_upper_jnt.radius.set(1)

        upper_teeth_zero = pm.group(em=True, n=teeth_upper_jnt+'_ZERO', p='Main_joints')
        pm.delete(pm.parentConstraint(teeth_upper_ctl, upper_teeth_zero, mo=False))
        pm.parent(teeth_upper_jnt, upper_teeth_zero)

        for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
            pm.connectAttr(teeth_upper_ctl.attr(atr), teeth_upper_jnt.attr(atr), f=True)

        # ==============================================================================================================
        # JAW SDK GRP
        jaw_driven_grp = pm.group(em=True, n='jaw_driven_grp', p='Main_joints')
        pm.parentConstraint('JawFollow_M', jaw_driven_grp, mo=False)

        # LOWER TEETH ==================================================================================================
        teeth_lower_ctl = pn('lowerTeeth_M')
        teeth_lower_ctl.rename('Main_CTL_lowerTeeth')

        # use mouth crv shapes to make the mouth shapes better.
        pm.parent('lower_teeth_crv', teeth_lower_ctl)
        pm.makeIdentity('lower_teeth_crv', t=True, r=True, s=True, apply=True)
        pm.parent('lower_teeth_crv', self.DELETE_GRP)
        pm.connectAttr(pn('lower_teeth_crv').local, teeth_lower_ctl.create, f=True)
        teeth_lower_ctl.create.disconnect()

        # override color (red)
        teeth_lower_ctl.getShape().overrideEnabled.set(True)
        teeth_lower_ctl.getShape().overrideColor.set(13)

        # setup head constrained bypass
        pm.parent('lowerTeethOffset_M', self.HEAD_CONSTRAINED_GRP)
        pm.delete(['lowerTeethJoint_M_parentConstraint1', 'lowerTeethJoint_M_scaleConstraint1'])

        teeth_lower_jnt = pn('lowerTeethJoint_M')
        teeth_lower_jnt.rename('Main_lowerTeeth_joint')
        teeth_lower_jnt.radius.set(1)

        teeth_lower_zero = pm.group(em=True, n=teeth_lower_jnt+'_ZERO', p=jaw_driven_grp)
        pm.delete(pm.parentConstraint(teeth_lower_ctl, teeth_lower_zero, mo=False))
        pm.parent(teeth_lower_jnt, teeth_lower_zero)

        for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
            pm.connectAttr(teeth_lower_ctl.attr(atr), teeth_lower_jnt.attr(atr), f=True)

        # TONGUE =======================================================================================================
        pm.parent('Tongue0Offset_M', self.HEAD_CONSTRAINED_GRP)
        pm.delete('Tongue0Joint_M')

        tongue_ctl_0 = pn('Tongue0_M')
        tongue_ctl_1 = pn('Tongue1_M')
        tongue_ctl_2 = pn('Tongue2_M')
        tongue_ctl_3 = pn('Tongue3_M')

        tongue_ctl_0.rename('Main_CTL_tongueA')
        tongue_ctl_1.rename('Main_CTL_tongueB')
        tongue_ctl_2.rename('Main_CTL_tongueC')
        tongue_ctl_3.rename('Main_CTL_tongueD')

        joint_list = list()
        for i, ctl in enumerate([tongue_ctl_0, tongue_ctl_1, tongue_ctl_2, tongue_ctl_3]):
            pm.select(cl=True)

            jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_joint')
            joint_list.append(jnt)
            jnt_zero = misc.zero_grp(jnt)

            pm.delete(pm.parentConstraint(ctl.getParent(), jnt_zero, mo=False))

            for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                pm.connectAttr(ctl.attr(atr), jnt.attr(atr), f=True)

            if i == 0:
                pm.parent(jnt_zero, jaw_driven_grp)
            else:

                pm.parent(jnt_zero, joint_list[i-1])

    def setup_eye_controls(self):
        """
        This function prepares eye controls for detached setup.
        """
        eye_ctl_grp = pm.group(em=True, n='Eye_controls')
        pm.parent(eye_ctl_grp, 'Head_constrained_grp')

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            # angle driver
            eye_driver = pm.group(em=True, n='Eye_driver_'+side+'_GRP')
            driver_pos = [5.583*side_mult, 151.552, 2.008]  # this you match to body rig eye L joint pos
            driver_rot = [180.0, -90.0, 0]

            pm.xform(eye_driver, t=driver_pos, ws=True)
            pm.xform(eye_driver, ro=driver_rot, os=True)

            eye_driver_zero = misc.zero_grp(eye_driver)

            driver = pm.group(em=True, n=eye_driver+'_driver')
            pm.parent(driver, eye_driver)
            pm.xform(driver, t=[4.964*side_mult, 151.179, 4.264],  ws=True)  # ws center of iris/pupil

            # # aim driven
            eye_driven = pm.group(em=True, n='Eye_driven_'+side+'_GRP')
            driven_pos = [1.215*side_mult, 151.471, -3.238]  # implied point around which the anim iris rotates.
            pm.xform(eye_driven, t=driven_pos, ws=True)
            eye_driven_zero = misc.zero_grp(eye_driven)

            aim = pm.aimConstraint(
                driver, eye_driven,
                aimVector=[0, 0, 1],
                upVector=[0, 1, 0],
                worldUpVector=[0, 1, 0],
                worldUpType='scene',
            )

            # eye_driven
            eye_driven_loc = pm.spaceLocator(n='Eye_driven_'+side)
            pm.hide(eye_driven_loc.getShape())
            pm.parent(eye_driven_loc, driver, r=True)

            eye_ctl_driven = pm.group(em=True, n='Eye_control_driven_'+side)
            pm.parent(eye_ctl_driven, eye_ctl_grp)
            for source_atr, target_atr in zip(['worldPosition[0].worldPositionX', 'worldPosition[0].worldPositionY', 'worldPosition[0].worldPositionZ'], ['tx', 'ty', 'tz']):
                pm.connectAttr(eye_driven_loc.getShape().attr(source_atr), eye_ctl_driven.attr(target_atr))

            ctl = control.create(
                'Main_CTL_iris_'+side,
                shape='circle',
                colorIdx=17,
                rotation=[90, 0, 0],
                zeroGroup=True
            )

            pm.delete(pm.parentConstraint(driver, ctl.getParent(), mo=False))
            pm.parent(ctl.getParent(), eye_ctl_driven)

            pm.xform(ctl.getParent(), ro=[0, 20*side_mult, 0], r=True, os=True)

            # jnt
            pm.select(cl=True)
            jnt_offset = pm.group(em=True, n='eye_jnt_'+side+'_OFFSET')
            jnt_zero = pm.group(em=True, n='eye_jnt_'+side+'_ZERO')
            jnt = pm.joint(n='eye_jnt_'+side)
            pm.parent(jnt_offset, eye_driven, r=True)
            pm.parent(jnt_zero, jnt_offset, r=True)
            pm.delete(pm.parentConstraint(driver, jnt_offset, mo=False))
            pm.delete(pm.parentConstraint(driver, jnt_zero, mo=False))
            pm.parent(eye_driver_zero, self.EYE_STUFF)
            pm.parent(eye_driven_zero, 'Main_joints')

            for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                pm.connectAttr(ctl.getParent().attr(atr), jnt_zero.attr(atr))
                pm.connectAttr(ctl.attr(atr), jnt.attr(atr), f=True)

            # disconnect old sdks
            for sdk in [
                'asFaceBS_ctrlEyetranslateYNeg_'+side,
                'asFaceBS_ctrlEyetranslateXNeg_'+side,
                'asFaceBS_ctrlEyetranslateXPos_'+side,
                'asFaceBS_ctrlEyetranslateYPos_'+side,
            ]:
                pm.delete(sdk)

            # set new ones.
            face_bs = pn('face_bs_geo_blendShape')

            xpos_atr = 'ctrlEyetranslateXPos_'+side
            xneg_atr = 'ctrlEyetranslateXNeg_'+side
            ypos_atr = 'ctrlEyetranslateYPos_'+side
            yneg_atr = 'ctrlEyetranslateYNeg_'+side

            # xpos
            pm.setDrivenKeyframe(face_bs.attr(xpos_atr), v=0.0, cd=eye_driver.ry, dv=0.0)
            pm.setDrivenKeyframe(face_bs.attr(xpos_atr), v=1.0, cd=eye_driver.ry, dv=-60.0)

            # xneg
            pm.setDrivenKeyframe(face_bs.attr(xneg_atr), v=0.0, cd=eye_driver.ry, dv=0.0)
            pm.setDrivenKeyframe(face_bs.attr(xneg_atr), v=1.0, cd=eye_driver.ry, dv=60)

            # ypos
            pm.setDrivenKeyframe(face_bs.attr(ypos_atr), v=0.0, cd=eye_driver.rz, dv=0.0)
            pm.setDrivenKeyframe(face_bs.attr(ypos_atr), v=1.0, cd=eye_driver.rz, dv=-20.0)

            # yneg
            pm.setDrivenKeyframe(face_bs.attr(yneg_atr), v=0.0, cd=eye_driver.rz, dv=0.0)
            pm.setDrivenKeyframe(face_bs.attr(yneg_atr), v=1.0, cd=eye_driver.rz, dv=20.0)

            if side == 'L':
                ry = [-20, 60]
            else:
                ry = [-60, 20]

            pm.transformLimits(eye_driven, erx=[1, 1], rx=[-40, 40])
            pm.transformLimits(eye_driven, ery=[1, 1], ry=ry)

            pn('ctrlEye_'+side).tx.set(l=True)
            pn('ctrlEye_' + side).ty.set(l=True)

    def setup_region_controls(self):
        """
        This function contains all the steps involved in creating the region controls.
        - setup face region geo and connect
        - create groups
        - create base jnt
        - create control follicles
        - create controls
        - create joints
        - apply skincluster + prebind matrix connection
        - on-face controls hackery. (this was hard)
        - connect region control to eye setup so it follows eye region control.

        """
        # setup face region geo and connect
        mesh.branch_geo(self.FACE_BS_GEO, n=self.FACE_REGION_GEO)

        # groups
        top_grp = pm.group(em=True, n='Region', p=self.RIG_GRP)
        ctl_grp = pm.group(em=True, n='Region_controls', p=top_grp)
        jnt_grp = pm.group(em=True, n='Region_joints', p=top_grp)
        dnt_grp = pm.group(em=True, n='Region_doNotTouch', p=top_grp)
        constrain_to_head = pm.group(em=True, n='Region_constrainToHead')
        fol_grp = pm.group(em=True, n='Region_follicle_group', p=dnt_grp)
        pm.hide(dnt_grp)
        pm.parent(constrain_to_head, self.HEAD_CONSTRAINED_GRP)

        pm.delete(self.FACE_REGION_DRIVER)
        wrap.create(
            self.FACE_REGION_TRIDRIVER,
            self.FACE_BS_GEO,
            base=self.FACE_BS_GEO + 'Base',
            baseParent=self.DO_NOT_TOUCH_GRP
        )
        smooth = mesh.smooth(self.FACE_REGION_TRIDRIVER, divisions=1)

        pm.parent(self.FACE_REGION_TRIDRIVER, dnt_grp)

        # base jnt
        base_jnt_zero = pm.group(em=True, n='Region_base_jnt_ZERO', p=jnt_grp)
        jnt = pm.joint(n='Region_base_jnt')

        # create control follicles

        ctl_fol_map = dict()
        for ctl, vtx_idx in self.region_fol_dict.items():

            geo = self.FACE_REGION_TRIDRIVER
            ctl = pn(ctl)

            ctl.getShape().overrideEnabled.set(True)
            ctl.getShape().overrideColor.set(29)

            # create follicle
            fol = follicle.onSelected(selection=geo + '.vtx[' + str(vtx_idx) + ']', n=ctl + '_fol')
            fol.getShape().v.set(False)
            pm.parent(fol, fol_grp)
            fol.r.disconnect()
            fol.r.set([0, 0, 0])

            fol_offset = pm.spaceLocator(n=fol + '_OFFSET')
            pm.parent(fol_offset, fol)
            fol_offset.getShape().v.set(False)

            # setup ctl stuff
            ctl_zero = misc.zero_grp(ctl)
            pm.makeIdentity(ctl, n=0, s=1, r=1, t=1, apply=True, pn=1)
            pm.parent(ctl_zero, constrain_to_head)
            pm.move(ctl.cv[:], [0, 0, 0.3], r=True, os=True)

            pm.delete(pm.parentConstraint(ctl, fol_offset, mo=False))

            pm.connectAttr(fol_offset.getShape().worldPosition[0].worldPositionX, ctl_zero.tx, f=True)
            pm.connectAttr(fol_offset.getShape().worldPosition[0].worldPositionY, ctl_zero.ty, f=True)
            pm.connectAttr(fol_offset.getShape().worldPosition[0].worldPositionZ, ctl_zero.tz, f=True)

            # setup joint stuff
            jnt_zero = pm.group(em=True, n=ctl + '_jnt_ZERO')
            jnt = pm.joint(n=ctl + '_jnt')

            pm.parent(jnt_zero, jnt_grp)

            for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                pm.connectAttr(ctl_zero + '.' + atr, jnt_zero + '.' + atr)
                pm.connectAttr(ctl + '.' + atr, jnt + '.' + atr)

            ctl_fol_map[str(ctl)] = str(fol)

        # load skinweights for region controls.
        skin_ng.import_from_file(
            geo=self.FACE_REGION_GEO,
            filepath='E:/destruna/assets/kat/face/imports/face_region_geo_skinCluster_v001.nwts'
        )
        pm.select(self.FACE_REGION_GEO)
        pm.skinPercent(self.FACE_REGION_GEO+'_skinCluster', normalize=True)
        skin.connect_prebind_matrixes('face_region_geo_skinCluster')

        # deforming on face controls
        stuck_grp = pm.group(em=True, n='Region_stuck_on_controls', p=ctl_grp)
        pm.hide(stuck_grp)

        for ctl in self.region_fol_dict.keys():
            ctl = pn(ctl)

            # fol base position
            fol = pn(ctl_fol_map[str(ctl)])
            base_grp = pm.group(em=True, n=fol+'_base_pos', p=fol.getParent())
            pm.delete(pm.parentConstraint(fol, base_grp, mo=False))

            ctl_def = curve.copy(curve=ctl, n=ctl.replace('_CTL_', '_') + '_def')
            pm.parent(ctl_def, stuck_grp)
            wrap.create(ctl_def, self.FACE_RIG_GEO, base=self.FACE_RIG_GEO + 'Base', baseParent=dnt_grp)

            bs = blendshape.apply(ctl_def, ctl, dv=1)
            # bs.origin.set(0)

            clus, clus_handle = pm.cluster(ctl_def, n=ctl_def + '_cluster')
            pm.parent(clus_handle, stuck_grp)
            pm.hide(ctl_def.getShape())

            # subtract position of control from original position
            mult = pm.createNode('multiplyDivide', n=ctl + '_mult')
            mult.operation.set(2)
            pm.connectAttr(ctl.t, mult.input1, f=True)
            mult.input2.set(-1, -1, -1)


            # fol offset pma
            fol_offset_pma = pm.createNode('plusMinusAverage', n=ctl+'_pma1')
            pm.connectAttr(base_grp.t, fol_offset_pma.input3D[0], f=True)
            pm.connectAttr(fol.getShape().outTranslate, fol_offset_pma.input3D[1], f=True)
            fol_offset_pma.operation.set(2)

            # position of control to fol offset
            ctl_pos_add = pm.createNode('plusMinusAverage', n=ctl+'_addFinal')
            pm.connectAttr(mult.output, ctl_pos_add.input3D[0], f=True)
            pm.connectAttr(fol_offset_pma.output3D, ctl_pos_add.input3D[1], f=True)

            pm.connectAttr(ctl_pos_add.output3D, clus_handle.t, f=True)

        pm.parent(self.FACE_REGION_GEO, self.DO_NOT_TOUCH_GRP)

        # eye hack to connect region controls to eye stuff
        for side in 'LR':

            # create locator to get a ws output of the region control
            region_joint = 'Region_CTL_eye_'+side+'_jnt'
            region_loc = pm.spaceLocator(n=region_joint+'_loc')
            pm.hide(region_loc.getShape())
            pm.parent(region_loc, region_joint, r=True)

            # create group driven by locator.
            eye_region_driven = pm.group(em=True, n='Eye_regionDriven_'+side+'_GRP')
            pm.delete(pm.parentConstraint(region_loc, eye_region_driven, mo=False))

            for source_atr, target_atr in zip(['worldPosition[0].worldPositionX', 'worldPosition[0].worldPositionY', 'worldPosition[0].worldPositionZ'], ['tx', 'ty', 'tz']):
                pm.connectAttr(region_loc.getShape().attr(source_atr), eye_region_driven.attr(target_atr), f=True)

            # constrain to tweak.
            pm.parentConstraint(eye_region_driven, 'Eye_driver_'+side+'_GRP_ZERO', skipRotate=['x', 'y', 'z'], mo=True)

            pm.parent(eye_region_driven, self.EYE_STUFF)

    def create_tweaks_controls_setup(self, data):
        """
        This function creates groups of tweak controls on a mesh

        Args:
            data: [{
                'name': (str)    name of joint group
                'tweak_data': [
                    {
                        'prefix': 'Eye'
                        'description': 'tweakLowerA'
                        'side': 'L'
                        'vtx': 'face_tweak.vtx[1]'
                    },
                ]
            }]
        """
        tweaks_build_data = build.check_data_type(function_name='tweaks_controls_setup', data=data)

        # if there is data
        if tweaks_build_data:

            # iterate through list
            for tweak_group in tweaks_build_data:

                # joint
                tweak_data = tweak_group['tweak_data']
                name = tweak_group['name']

                # group creation
                top_grp = pm.group(em=True, n=name)
                control_group = pm.group(em=True, n=name + '_controls', p=top_grp)
                joint_group = pm.group(em=True, n=name + '_joints', p=top_grp)
                constrain_to_head = pm.group(em=True, n=name + '_constrainToHead')

                # iterating through tweak data to create ctls etc.
                for d in tweak_data:

                    prefix = d['prefix']
                    description = d['description']
                    side = d['side']
                    vtx = d['vtx']
                    orient = d['orient']

                    # follicle creation
                    fol_name = '_'.join([prefix, description, side, 'fol'])
                    print [vtx, pn(vtx)]
                    fol = follicle.onSelected(selection=vtx, n=fol_name)
                    # if not orient:
                    fol.r.disconnect()
                    fol.r.set([0, 0, 0])
                    # pm.hide(fol.getShape())

                    pm.parent(fol, control_group)

                    # create group to be driven by fol under head joint
                    ctl_parent_name = '_'.join([prefix, description, side, 'driven'])
                    ctl_parent = pm.group(em=True, n=ctl_parent_name, p=constrain_to_head)
                    pm.connectAttr(fol.t, ctl_parent.t)

                    # control creation
                    tweak_data = tweak.build_control_at_component(
                        component=vtx,
                        prefix=prefix,
                        description=description,
                        suffix=side,
                        shape='circle',
                        size=0.2,
                        rotation=[90, 0, 0],
                        orientToSurface=orient
                    )
                    ctl = pn(tweak_data['ctl'])
                    ctl.getShape().overrideEnabled.set(1)
                    ctl.getShape().overrideColor.set(31)

                    pm.xform(ctl.cv[:], t=[0, 0, .25], r=True, os=True)
                    pm.parent(tweak_data['ctl_zero'], ctl_parent)
                    pm.parent(tweak_data['jnt_zero'], fol)

                    pm.delete(tweak_data['jnt_zero'] + '_parentConstraint1')
                    pm.delete(tweak_data['jnt_zero'] + '_scaleConstraint1')
                    pm.delete(tweak_data['jnt'] + '_parentConstraint1')
                    pm.delete(tweak_data['jnt'] + '_scaleConstraint1')

                    for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                        pm.connectAttr(tweak_data['ctl_zero'] + '.' + atr, tweak_data['jnt_zero'] + '.' + atr)
                        pm.connectAttr(tweak_data['ctl'] + '.' + atr, tweak_data['jnt'] + '.' + atr)

    def setup_tweak_controls(self):
        """
        This function creates the tweaks layer of controls.
        - setup face tweaks geo
        - create controls + follicles
        - create base joints
        - skincluster + prebind matrix connections
        - parenting.
        """

        # # setup face tweaks geo and connect
        mesh.branch_geo(self.FACE_REGION_GEO, n=self.FACE_TWEAKS_GEO)

        # create controls and follicles.
        self.create_tweaks_controls_setup(self.tweak_controls_setup)

        # base jnt
        base_jnt_zero = pm.group(em=True, n='Tweaks_base_jnt_ZERO', p='Tweaks_joints')
        jnt = pm.joint(n='Tweaks_base_jnt')

        # parenting
        pm.parent('Tweaks', self.RIG_GRP)
        dnt_grp = pm.group(em=True, n='Tweaks_doNotTouch', p='Tweaks')
        pm.hide(dnt_grp)

        # skin bind
        skin_ng.import_from_file(
            geo=self.FACE_TWEAKS_GEO,
            filepath='E:/destruna/assets/kat/face/imports/face_tweaks_geo_skinCluster_v001.nwts'
        )
        skin.connect_prebind_matrixes(self.FACE_TWEAKS_GEO+'_skinCluster')
        pm.select(self.FACE_TWEAKS_GEO, r=True)
        pm.skinPercent(self.FACE_TWEAKS_GEO+'_skinCluster', normalize=True)

        # parent branched geo
        pm.parent(self.FACE_TWEAKS_GEO, dnt_grp)
        pm.parent(self.FACE_TWEAKS_DRIVER, dnt_grp)

        pm.parent('Tweaks_constrainToHead', self.HEAD_CONSTRAINED_GRP)

    def prepare_brow(self):

        # functions for control setup
        self.brow_ctls()

        # skin weights
        skin_ng.import_from_file(filepath='E:/destruna/assets/kat/face/imports/brow_wrapper_skinCluster_v001.nwts', geo='brow_wrapper')
        mesh.smooth(geo='brow_wrapper', divisions=1)
        #
        # create shrinkwrap
        shrinkwrap = deformer.create_shrinkwrap(wrapped='brow_wrapper', wrapper='face_rig_geo')
        pm.parent('brow_wrapper', 'DoNotTouch')

    def brow_ctls(self):

        # groups
        constrain_to_head = pm.group(em=True, n='Brows_constrainToHead')
        pm.parent(constrain_to_head, 'Head_constrained_grp')
        brow_joints = pm.group(em=True, n='Brow_joints_grp')
        pm.parent(brow_joints, 'Main_joints')

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            # create follicles
            fol_list = list()
            for x, data in enumerate(self.ctl_data_list):

                ctl_name = data['name'].replace('<s>', side)
                translate = [data['translate'][0] * side_mult, data['translate'][1], data['translate'][2]]

                uval, vval = cpom.closest_uv_on_geo_from_position(position=translate, geo='face_rig_geo')
                fol = follicle.create(geo='face_rig_geo', n=ctl_name+'_fol', setUV=True, uvVal=(uval, vval))
                fol_list.append(fol)
                fol.r.disconnect()
                fol.r.set(0, 0, 0)

            pm.parent(fol_list, brow_joints)

            # create control
            ctl_list = list()
            loc_list = list()
            for x, data in enumerate(self.ctl_data_list):

                ctl_name = data['name'].replace('<s>', side)
                translate = [data['translate'][0]*side_mult, data['translate'][1], data['translate'][2]]

                ctl = control.create(
                    ctl_name,
                    shape='circle',
                    colorIdx=17,
                    zeroGroup=True,
                    translation=[0, 5, 0],
                    rotation=[90, 0, 0],
                    size=0.1
                )

                ctl_zero = ctl.getParent()
                pm.xform(ctl_zero, t=translate, os=True)
                ctl_list.append(ctl)

                constrained_grp = pm.group(em=True, n=ctl_name+'_driven')
                fol = fol_list[x]

                pm.connectAttr(fol.t, constrained_grp.t)
                pm.parent(ctl_zero, constrained_grp)
                pm.parent(constrained_grp, constrain_to_head)

                loc = pm.spaceLocator(n=ctl_name.replace('_CTL_', '_')+'_loc')
                loc.getShape().v.set(False)
                loc_list.append(loc)
                pm.parent(loc, fol)
                for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                    pm.connectAttr(ctl.attr(atr), loc.attr(atr))

            for x, loc in enumerate(loc_list):

                if x == 0:
                    aim_loc = loc_list[x+1]
                    aim_vector = [1 * side_mult, 0, 0]
                else:
                    aim_loc = loc_list[x-1]
                    aim_vector = [1 * side_mult, 0, 0]

                pm.select(loc, r=True)
                jnt = pm.joint(n=loc.replace('_loc', '_jnt'))
                print jnt
                pm.parent(jnt, loc)

                fol = fol_list[x]

                pm.aimConstraint(
                    aim_loc, jnt,
                    aimVector=aim_vector,
                    upVector=[0, 0, 1],
                    worldUpType='objectRotation',
                    worldUpVector=[0, 0, 1],
                    worldUpObject=fol
                )

    def prepare_lash(self):

        lash_curve_edges = {
            'L': ['face_geoShape.e[10030]', 'face_geoShape.e[10033]', 'face_geoShape.e[1089]', 'face_geoShape.e[1093]', 'face_geoShape.e[1315]', 'face_geoShape.e[1318]', 'face_geoShape.e[1325]', 'face_geoShape.e[1328]', 'face_geoShape.e[1335]', 'face_geoShape.e[1338]', 'face_geoShape.e[1369]', 'face_geoShape.e[1373]', 'face_geoShape.e[1381]', 'face_geoShape.e[1389]', 'face_geoShape.e[1392]', 'face_geoShape.e[1397]', 'face_geoShape.e[1401]', 'face_geoShape.e[1407]', 'face_geoShape.e[1413]'],
            'R': ['face_geoShape.e[10137:10138]', 'face_geoShape.e[6038]', 'face_geoShape.e[6046]', 'face_geoShape.e[6260]', 'face_geoShape.e[6267]', 'face_geoShape.e[6269]', 'face_geoShape.e[6276]', 'face_geoShape.e[6279]', 'face_geoShape.e[6286]', 'face_geoShape.e[6314]', 'face_geoShape.e[6322]', 'face_geoShape.e[6325]', 'face_geoShape.e[6334]', 'face_geoShape.e[6339]', 'face_geoShape.e[6342]', 'face_geoShape.e[6350]', 'face_geoShape.e[6355]', 'face_geoShape.e[6357]'],
        }

        for side in 'LR':

            # create wire curve
            wire_curve_edges = lash_curve_edges[side]
            pm.select(wire_curve_edges, r=True)
            wire_crv, polyEdgeToCurve = pm.polyToCurve(n='eye_lash_'+side+'_crv', form=2, degree=3, conformToSmoothMeshPreview=1)
            pm.parent(wire_crv, self.LASH_STUFF)
            eye_lash_wrapper = 'eye_lash_'+side+'_wrapper'

            # create wire deformer
            deformer.create_wire(geo=eye_lash_wrapper, curve=wire_crv, dropoff_distance=10, rotation=0.5)
            pm.parent(eye_lash_wrapper, wire_crv+'BaseWire', self.LASH_STUFF)

            # mesh smooth
            wrapper_smooth = mesh.smooth(eye_lash_wrapper, divisions=1, keepBorder=False)

            # wrap to wrapper
            eye_lash_geo = pn('lash_'+side+'_geo')
            wrap.create(eye_lash_geo, eye_lash_wrapper, baseParent=self.LASH_STUFF)

            deformer.create_deltamush(eye_lash_geo, smoothing_iterations=3, smoothing_step=0.75, distance_weight=1)

    def do_skin_to_joint(self):

        for joint, geo_list in self.skin_to_joint.items():
            for geo in geo_list:
                pm.skinCluster(joint, geo, n=geo+'_skinCluster', tsb=True)

    def adjust_labels(self):

        # box position
        pm.xform('ctrlBoxOffset', t=[16, 3, 8], r=False, os=True)

        # parent labels to ctl box.
        label_list = ['label_brow', 'label_cheek', 'label_eye', 'label_mouth', 'label_nose']
        pm.parent(label_list, 'ctrlBox')

        # brow ctls
        for ctl in ['ctrlBoxBrow_L', 'ctrlBoxBrow_R']:
            pn(ctl).ty.set(6.677)
        pm.xform('label_brow', t=[-91.389, 314.379, 0.0], os=True)

        # eye ctls
        for ctl in ['ctrlBoxEye_L', 'ctrlBoxEye_R']:
            pn(ctl).ty.set(3.721)
        pm.xform('label_eye', t=[-91.086, 196.421, 0.0], os=True)


        # cheek ctls
        for ctl in ['ctrlBoxCheek_L', 'ctrlBoxCheek_R']:
            pn(ctl).ty.set(0.765)
        pm.xform('label_cheek', t=[-90.639, 78.569, 0.0], os=True)

        # nose ctls
        for ctl in ['ctrlBoxNose_L', 'ctrlBoxNose_R']:
            pn(ctl).ty.set(-1.138)

        pm.xform('label_nose', t=[-91.406, 2.234, 0.0], os=True)
        pm.xform('label_mouth', t=[-91.39, -74.941, 0.0], os=True)

        # pn('ctrlBoxMouth_M').ty.set(-2.752)
        #
        # # mouth corner ctls
        # for ctl in ['ctrlBoxMouthCorner_L', 'ctrlBoxMouthCorner_R']:
        #     pn(ctl).ty.set(-6.008)
        #
        # pn('ctrlBoxPhonemes_M').ty.set(-4.977)
        # pn('ctrlBoxEmotions_M').ty.set(-7.637)
        #
        # adjust box shape.
        for cv in ['ctrlBoxShape.cv[1]', 'ctrlBoxShape.cv[0]', 'ctrlBoxShape.cv[4]']:
            pm.move(cv, [0, 2.5, 0], r=True, os=True)
        #
        # for cv in ['ctrlBoxShape.cv[2]', 'ctrlBoxShape.cv[3]']:
        #     pm.move(cv, [0, -0.597392, 0], r=True, os=True)

    def prepare_mask(self):

        # create mask control
        mask_ctl = control.create(
            'Mask_CTL_settings',
            shape='pyramid',
            colorIdx=17,
            zeroGroup=True,
        )
        mask_ctl.getParent().t.set(16, 160, 3.532)
        pm.parent(mask_ctl.getParent(), 'ctrlBox')

        for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v']:
            mask_ctl.attr(atr).set(k=False, cb=False, lock=True)

        for tgt in [
            'mask_squint_L',
            'mask_squint_R',
            'mask_wide_L',
            'mask_wide_R',
        ]:
            blendshape.apply(tgt, self.ACCESSORY_MASK_GEO)

        bs = pn(self.ACCESSORY_MASK_GEO+'_blendShape')
        for side in 'LR':
            attribute.add(mask_ctl, n='eyeSquint_'+side, type='float', min=-1, max=1, k=True, cb=True)

            pm.setDrivenKeyframe(bs.attr('mask_squint_'+side), v=0, cd=mask_ctl.attr('eyeSquint_'+side), dv=0)
            pm.setDrivenKeyframe(bs.attr('mask_squint_' + side), v=1, cd=mask_ctl.attr('eyeSquint_' + side), dv=1)

            pm.setDrivenKeyframe(bs.attr('mask_wide_'+side), v=0, cd=mask_ctl.attr('eyeSquint_'+side), dv=0)
            pm.setDrivenKeyframe(bs.attr('mask_wide_' + side), v=1, cd=mask_ctl.attr('eyeSquint_' + side), dv=-1)

    def build_contents(self):

        build.imports(self.imports)
        build.groups(self.groups)

        self.initial_setup_steps()

        self.adjust_labels()
        self.setup_mouth_controls()
        self.setup_eye_controls()
        self.setup_region_controls()
        self.setup_tweak_controls()

        # pm.parent(self.HEAD_GEO, self.GEO)
        blendshape.apply(self.FACE_TWEAKS_GEO, self.FACE_RIG_GEO, dv=1)

        self.prepare_brow()
        self.prepare_lash()
        self.prepare_mask()

        # # GEO ATTACHMENTS (this stuff will be lona specific) ===========================================================
        # # establish wrap to face rig.
        blendshape.apply(self.FACE_RIG_GEO, self.FACE_GEO, dv=1)

        build.wrapDeformer_create(self.wrapDeformer_create)

        self.do_skin_to_joint()
        # pm.parent('face_geo_wrapped', self.DNT)
        # # self.upper_lash_attach()
        # #
        build.skin_bind(self.skin_bind)
        pm.parent(self.FACE_RIG_GEO, self.FACE_BS_GEO, self.DO_NOT_TOUCH_GRP)

        self.do_cleanup()

def detach_and_delete_bodyrig():

    # prep freshly skinCluster -> blendshape AS rig.
    pm.parent('FaceGroup', w=True)

    pm.delete([
        'FaceMotionSystem_orientConstraint1',
        'FaceMotionSystem_pointConstraint1',

        'Customcontrols',
        'Regionscontrols',
        'Aimcontrols',

        'SideReverse_L',
        'SideReverse_R',

        'EyeAimFollowHead_parentConstraint1',
        'FaceDeformationFollowHead_orientConstraint1',
        'FaceDeformationFollowHead_pointConstraint1',
        'LidWireWS_parentConstraint1',
        'LidWireWS_scaleConstraint1',
        'LipFollicles_parentConstraint1',
        'MainAndHeadScaleMultiplyDivide',
    ])

    pm.parent('FaceJoint_M', 'FaceDeformationSystem')
    pm.delete('Group')
    pm.rename('FaceGroup', 'Main')

    pm.rename('face_geo', 'face_bs_geo')
    pm.rename('asFaceBS', 'face_bs_geo_blendShape')

    pm.delete([
        'brows_geo',
        'iris_L_geo',
        'iris_R_geo',
        'lash_L_geo',
        'lash_R_geo',
        'sclera_L_geo',
        'sclera_R_geo',
        'teethLower_geo',
        'teethUpper_geo',
        'tongue_geo',
    ])

def rip_eye_sdks_out_of_face():

    eye_sdk_detached = pm.group(em=True, n='eye_sdk_detached')
    holder_grp = pm.group(em=True, n='attr_holder', p=eye_sdk_detached)
    atr_list = list()

    for side in 'LR':

        side_rev = pm.group(em=True, n='side_reverse_'+side, p=eye_sdk_detached)

        pm.delete(pm.parentConstraint('SideReverse_'+side, side_rev, mo=False))
        pm.delete(pm.scaleConstraint('SideReverse_'+side, side_rev, mo=False))

        pm.parent('EyeOffset_'+side, side_rev)
        pm.parent('EyeRegionOffset_'+side, side_rev)
        pm.delete('EyeAim_'+side+'_aimConstraint1')

        pm.parent('EyeJoint_'+side, eye_sdk_detached)
        atr_list.extend([
            'ctrlEye_'+side+'.translateX',
            'ctrlEye_'+side+'.translateY',
            'ctrlEye_' + side + '.squint',
        ])

    atr_list.extend([
        'ctrlEmotions_M.angry',
        'ctrlEmotions_M.contempt',
        'ctrlEmotions_M.disgust',
        'ctrlEmotions_M.fear',
        'ctrlEmotions_M.happy',
        'ctrlEmotions_M.sad',
        'ctrlEmotions_M.surprise',
    ])

    for atr in atr_list:

        atr_name = atr.replace('.', '__dot__').replace('[', '__b0__').replace(']', '__b1__')

        cnx_list = pm.listConnections(atr, s=False, d=True, p=True)
        attribute.add(holder_grp, n=atr_name, type='float', dv=0)

        for cnx in cnx_list:
            pm.connectAttr(holder_grp.attr(atr_name), cnx, f=True)



"""
from jb_destruna.rigs.b_kat import kat_faceRig
reload(kat_faceRig)
kat_faceRig.detach_and_delete_bodyrig()

from jb_destruna.rigs.b_kat import kat_faceRig
reload(kat_faceRig)
face = kat_faceRig.FaceRig()
face.go()
"""