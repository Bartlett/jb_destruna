from jb_destruna.maya.core import build
import maya.cmds as mc
import pymel.core as pm

from jb_destruna.maya.internal import skin
from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.internal import control
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn
from jb_destruna.maya.internal import mesh
from jb_destruna.maya.internal import wrap
from jb_destruna.tools import hair_rig


from jb_destruna.rigs.b_kat.import_paths import kat_mocapTorn_import_paths as kti

reload(build)
reload(rig)
reload(hair_rig)

class Rig(rig.Rig):

    ASSET = 'kat'

    FACE_NS = 'face'

    FACE_GEO = 'face_geo'
    HAIR_FRONT_GEO = 'HairFront_geo'
    HAIR_BASE_GEO = 'HairBase_geo'
    HAIR_LONG = 'MocapSuit_Hair_geo'
    BODY_GEO = 'MocapSuit_Torn_geo'
    BODY_GEO_ALT = 'MocapSuit_Pristine_geo'
    PUPIL_GEO_L = 'iris_L_geo'
    PUPIL_GEO_R = 'iris_R_geo'
    SCLERA_GEO_L = 'sclera_L_geo'
    SCLERA_GEO_R = 'sclera_R_geo'
    TOPTEETH_GEO = 'teethUpper_geo'
    BOTTOMTEETH_GEO = 'teethLower_geo'
    TONGUE_GEO = 'tongue_geo'

    EARPLUGS_GEO = 'katEarplug_geo'
    EYEBROWS_GEO = 'brows_geo'
    EYELASH_L_GEO = 'lash_L_geo'
    EYELASH_R_GEO = 'lash_R_geo'
    EYELASH_CLOSED_L_GEO = 'lashClosed_L_geo'
    EYELASH_CLOSED_R_GEO = 'lashClosed_R_geo'

    ACCESORY_SCRUNCHIE_GEO = 'MocapSuit_AccessoryScrunchie_geo'
    ACCESORY_ARMBAND = 'wristband_geo'
    ACCESORY_BELT = 'MocapSuit_AccessoryBelt_geo'
    ACCESORY_POUCH = 'MocapSuit_Pouch_geo'

    ACCESORY_MASK_GEO = 'Accessory_Mask_geo'

    ACCESORY_MASK_SMOOSH_GEO = 'Accessory_Mask_Smoosh_geo'  # Geo to combine the face rig and Smoosh on a blendshape

    EYEMASK_LOW_L = 'EyeMask_L_low_geo'
    EYEMASK_LOW_R = 'EyeMask_R_low_geo'

    GEO_LIST = [
        HAIR_FRONT_GEO,
        HAIR_BASE_GEO,
        HAIR_LONG,
        BODY_GEO,
        BODY_GEO_ALT,
        ACCESORY_BELT,
        ACCESORY_POUCH,
        ACCESORY_ARMBAND,
        ACCESORY_SCRUNCHIE_GEO,
        EARPLUGS_GEO,
    ]

    FACE_GEO_LIST = [
        FACE_GEO,
        BOTTOMTEETH_GEO,
        EYEBROWS_GEO,
        PUPIL_GEO_L,
        PUPIL_GEO_R,
        ACCESORY_MASK_GEO,
        SCLERA_GEO_L,
        SCLERA_GEO_R,
        TONGUE_GEO,
        TOPTEETH_GEO,
        EYELASH_L_GEO,
        EYELASH_R_GEO,
    ]

    GEO_LIST += FACE_GEO_LIST

    def __init__(self):
        super(Rig, self).__init__()

        self.who = self.ASSET

        self.imports = [
            # GEO
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/kat/3D/kat_teen_mocapTorn/01_model/kat_teen_mocapTorn_model_14_DFO_MASTER.mb',
             'group': self.DELETE_GRP},

            # AS BODY RIG
            {'filepath': 'D:/_Active_Projects\Destruna\masterAssets_forRef\characters\kat/3D\kat_teen_allOutfits/02_rig/03_imports/asRig\Kat_teen_asRig_023.mb'},

            # AS FACE RIG
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/kat/3D/kat_teen_mocapTorn/02_rig/03_imports/AS_faceRig/kat_faceRig_v003.mb',
             'group': self.DELETE_GRP, 'namespace': self.FACE_NS},

            # Toe ctl guides
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/kat/3D/kat_teen_mocapTorn/02_rig/03_imports/toe_guides/toe_guides_01.mb',
             'group': self.DELETE_GRP},

            # Scrunchie_guides
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/kat/3D/kat_teen_mocapTorn/02_rig/03_imports/mask_guides/Scrunchie_guides/Scrunchie_guides_02.mb',
                'group': self.DELETE_GRP},

            # Mask ctl guides
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/kat/3D/kat_teen_mocapTorn/02_rig/03_imports/mask_guides/mask_guides_02.mb',
                'group': self.DELETE_GRP},

            # Hair guides
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/kat/3D/kat_teen_mocapTorn/02_rig/03_imports/hair_guides/kat_teen_mocapTorn_hair_guides_07.mb',
             'group': self.DELETE_GRP},

            # Smoosh Mask guides
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/kat/3D/kat_teen_mocapTorn/02_rig/03_imports/mask_guides/smoosh_mask/kat_smoosh_mask_guides_04.mb',
                'group': self.DELETE_GRP},

            # Smoosh Mask lattice guides
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/kat/3D/kat_teen_mocapTorn/02_rig/03_imports/mask_guides/lattice_guides/Eyemask_lattice_guide_01_geo.mb',
             'group': self.DELETE_GRP},
        ]

        sk_path = kti.KAT_IMPORT_PATHS['damien']['skin']

        self.skin_weight_imports = {

            'body_geo': sk_path + 'MocapSuit_Torn_SK/MocapSuit_Torn_010'
                                  '_skinCluster.json',
            'face_geo': sk_path + 'Face_geo_SK/Face_001_skinCluster.json',

            'HairBase': sk_path + 'hair/HairBase_SK/HairBase_001_skinCluster.json',
            'HairFront': sk_path + 'hair/HairFront_SK/HairFront_001_skinCluster.json',
            'HairLong': sk_path + 'hair/MocapSuit_Hair_SK/MocapSuit_Hair_002_skinCluster.json',

            'Eye_L': sk_path + 'eyes/Eye_L_SK/Eye_L_001_skinCluster.json',
            'Eye_R': sk_path + 'eyes/Eye_R_SK/Eye_R_001_skinCluster.json',

            'AccessoryBelt': sk_path + 'MocapSuit_AccessoryBelt_SK/MocapSuit_AccessoryBelt_002_skinCluster.json',
            'Pouch': sk_path + 'MocapSuit_Pouch_SK/MocapSuit_Pouch_002_skinCluster.json',
            'Accessory_Mask': sk_path + 'Accessory_Mask_SK/Accessory_Mask_001_skinCluster.json'

        }
        skin_weight_prefix = 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/kat/3D/kat_teen_mocapTorn/02_rig/03_imports/skinClusters/'
        self.skin_bind = [

            # BODY
            {'geo': self.BODY_GEO, 'filepath': skin_weight_prefix + self.BODY_GEO + '/MocapSuit_Torn_geo_skinCluster_09.json',
             'import_module': 'skin_ng'},
            {'geo': self.BODY_GEO_ALT,
             'filepath': skin_weight_prefix + self.BODY_GEO_ALT + '/MocapSuit_Pristine_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.HAIR_FRONT_GEO, 'filepath': skin_weight_prefix + self.HAIR_FRONT_GEO + '/HairFront_geo_skinCluster_05.json',
             'import_module': 'skin_ng'},
            {'geo': self.HAIR_BASE_GEO, 'filepath': skin_weight_prefix + self.HAIR_BASE_GEO + '/HairBase_geo_skinCluster_03.json',
             'import_module': 'skin_ng'},
            {'geo': self.HAIR_LONG, 'filepath': skin_weight_prefix + self.HAIR_LONG + '/MocapSuit_Hair_geo_skinCluster_05.json',
             'import_module': 'skin_ng'},

            # FACE
            {'geo': self.FACE_GEO, 'filepath': skin_weight_prefix + self.FACE_GEO + '/Face_geo_skinCluster_05.json',
             'import_module': 'skin_ng'},
            # {'geo': self.PUPIL_GEO_L, 'filepath': skin_weight_prefix + self.PUPIL_GEO_L + '/Eye_geo_L_skinCluster_03.json',
            #  'import_module': 'skin_ng'},

            {'geo': self.PUPIL_GEO_L, 'filepath': skin_weight_prefix + '/Eye_geo_L/Eye_geo_L_skinCluster_05.json',
             'import_module': 'skin_ng'},
            {'geo': self.PUPIL_GEO_R, 'filepath': skin_weight_prefix + '/Eye_geo_R/Eye_geo_R_skinCluster_05.json',
             'import_module': 'skin_ng'},

            {'geo': self.EYEBROWS_GEO, 'filepath': skin_weight_prefix + self.EYEBROWS_GEO + '/brows_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},

            {'geo': self.EYELASH_L_GEO, 'filepath': skin_weight_prefix + self.EYELASH_L_GEO+ '/lash_L_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_R_GEO, 'filepath': skin_weight_prefix + self.EYELASH_R_GEO + '/lash_R_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},

            {'geo': self.TOPTEETH_GEO, 'filepath': skin_weight_prefix + self.TOPTEETH_GEO + '/teethUpper_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.BOTTOMTEETH_GEO, 'filepath': skin_weight_prefix + self.BOTTOMTEETH_GEO + '/teethLower_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.TONGUE_GEO, 'filepath': skin_weight_prefix + self.TONGUE_GEO + '/tongue_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},

            {'geo': self.ACCESORY_BELT, 'filepath': skin_weight_prefix + self.ACCESORY_BELT + '/MocapSuit_AccessoryBelt_geo_skinCluster_05.json',
             'import_module': 'skin_ng'},
            {'geo': self.ACCESORY_MASK_GEO, 'filepath': skin_weight_prefix + self.ACCESORY_MASK_GEO + '/Accessory_Mask_geo_skinCluster_10.json',
             'import_module': 'skin_ng'},
            {'geo': self.ACCESORY_POUCH, 'filepath': skin_weight_prefix + self.ACCESORY_POUCH + '/MocapSuit_Pouch_geo_skinCluster_03.json',
             'import_module': 'skin_ng'},
            {'geo': self.ACCESORY_SCRUNCHIE_GEO, 'filepath': skin_weight_prefix + self.ACCESORY_SCRUNCHIE_GEO + '/MocapSuit_AccessoryScrunchie_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EARPLUGS_GEO, 'filepath': skin_weight_prefix + self.EARPLUGS_GEO + '/katEarplug_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
        ]


        self.groups = [
            {'g': self.TOP_GRP, 'p': None, 'v': True},
            {'g': self.RIG_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.GEO_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.DNT_GRP, 'p': self.TOP_GRP, 'v': False},
        ]

        self.duplicate_geo_dict = [
            {'source': self.ACCESORY_MASK_GEO, 'dupe_name': self.ACCESORY_MASK_SMOOSH_GEO},
        ]

        self.hair_data = [
            {
                'name': name,
                'base_crv': name.format('base'),
                'up_crv': name.format('up'),
                'ctl_size': 2,
                'ctl_count': 5,
                'joint_count': 7,
            } for name in [

                'bun_{}A',
                'bun_{}B',
                'bun_{}C',
                'bun_{}D',
                'bun_{}E',

            ]
        ]

        self.hair_data += [
            {
                'name': name,
                'base_crv': name.format('base'),
                'up_crv': name.format('up'),
                'ctl_size': 4,
                'ctl_count': 5,
                'joint_count': 7,
            } for name in [

                'long_{}F',
                'long_{}G',
                'long_{}H',
                'long_{}I',
                'long_{}J',
                'long_{}K',

            ]
        ]

        self.hair_data += [
            {
                'name': name,
                'base_crv': name.format('base'),
                'up_crv': name.format('up'),
                'ctl_size': 2,
                'ctl_count': 5,
                'joint_count': 7,
            } for name in [

                'fringe_{}L',
                'fringe_{}M',
                'fringe_{}N',
                'fringe_{}O',
                'fringe_{}P',
                'fringe_{}Q',
                'fringe_{}R',
                'fringe_{}S',
                'fringe_{}T',
                'fringe_{}U',
                'fringe_{}V',
                'fringe_{}W',
                'fringe_{}X',
                'fringe_{}Y',
            ]
        ]

    def modify_as_rig(self):

        # good stuff happens here.
        super(Rig, self).modify_as_rig()

        # localized stuff happens here --------------
        pn('Master_CTL_Main').bendVis.set(0)

        # lock vis ctls
        for ctl in self.AS_CTL_LIST_NEW:
            pn(ctl).v.set(k=False, cb=False, l=True)

        # attributes on master
        pm.setAttr('Master_CTL_Main.jointVis', 0)
        attribute.add('Master_CTL_Main', n='geoVis', type='bool', dv=1, cb=True, k=False)
        attribute.add('Master_CTL_Main', n='geoLock', type='bool', dv=1, cb=True, k=False)

        pm.connectAttr('Master_CTL_Main.geoLock', 'Geometry.overrideEnabled')
        pm.setAttr('Geometry.overrideDisplayType', 2)

        pm.connectAttr('Master_CTL_Main.geoVis', 'Geometry.v')

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            pm.move('Arm_CTL_FKScapula_'+side+'.cv[:]', [-5*side_mult, 0, 0], r=True, os=True)
            pm.scale('Arm_CTL_FKShoulder_'+side+'.cv[:]', [0.7, 0.7, 0.7], r=True, os=True)
            pm.scale('Arm_CTL_FKWrist_' + side + '.cv[:]', [0.8, 0.8, 0.8], r=True, os=True)

    def wrap_scleras(self):

        wrap.create(
            self.SCLERA_GEO_L,
            self.PUPIL_GEO_L
        )

        wrap.create(
            self.SCLERA_GEO_R,
            self.PUPIL_GEO_R
        )

    def build_toe_controls(self):

        hair_guides_data = {

            'Toe': {
                'guides': ['Toe_CTL_0_L_guide', 'Toe_CTL_1_L_guide',],
                'scales': [4, 3],
                'colorIdx': 19,
            }}

        # Heirarchy
        hair_grp = pm.group(em=True, n='Toe', p='Master_CTL_Main')
        hair_ctl_grp = pm.group(em=True, n='Toe_controls', p=hair_grp)
        hair_jnt_grp = pm.group(em=True, n='Toe_joints', p=hair_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', hair_jnt_grp.v)

        for label, guide_data in hair_guides_data.items():

            guides = guide_data.get('guides')
            scales = guide_data.get('scales')
            color_idx = guide_data.get('colorIdx')

            ctl_list = list()
            jnt_list = list()
            for i, guide in enumerate(guides):

                # control
                ctl = control.create(
                    guide.replace('_guide', ''),
                    shape='box',
                    colorIdx=color_idx,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)
                pm.delete(pm.parentConstraint(guide, ctl.getParent(), mo=False))
                pm.scale(ctl.cv[:], [scales[i], scales[i], 1], r=True, os=True)
                ctl.v.set(k=False, cb=False, l=True)

                # joint
                pm.select(cl=True)
                jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_jnt')
                jnt_list.append(jnt)
                pm.parentConstraint(ctl, jnt, mo=False)
                pm.scaleConstraint(ctl, jnt, mo=False)

                # parenting
                if i == 0:
                    pm.parent(ctl.getParent(), hair_ctl_grp)
                    pm.parentConstraint('ToesEnd_L', ctl.getParent(), mo=True)
                    pm.parent(jnt, hair_jnt_grp)
                else:
                    pm.parent(ctl.getParent(), ctl_list[i-1])
                    pm.parent(jnt, jnt_list[i-1])

    def build_mask_controls(self):


        mask_top_grp = 'Mask'
        mask_jnt_grp = 'Mask_joints_grp'
        mask_ctl_grp = 'Mask_Controls_grp'
        mask_util_grp = 'Mask_utility_grp'
        mask_geo_grp = 'Mask_geo_grp'

        build.groups([
            {'g': mask_top_grp, 'p': self.MASTER_CONTROL, 'v': True},
            {'g': mask_jnt_grp, 'p': mask_top_grp, 'v': True},
            {'g': mask_ctl_grp, 'p': mask_top_grp, 'v': True},
            {'g': mask_util_grp, 'p': mask_top_grp, 'v': False},
            {'g': mask_geo_grp, 'p': mask_util_grp, 'v': False},
        ])
        # Groups Post edit
        mc.setAttr(mask_util_grp + '.inheritsTransform', 0)
        mc.parentConstraint('Head_Head_joint_M', mask_jnt_grp, mo=1)
        mc.parentConstraint('Head_Head_joint_M', mask_ctl_grp, mo=1)
        mc.scaleConstraint('Head_Head_joint_M', mask_ctl_grp, mo=1)
        mc.connectAttr(self.MASTER_CONTROL + '.jointVis', mask_jnt_grp + '.visibility')
        mc.parentConstraint('Head_Head_joint_M', mask_ctl_grp, mo=1)

        lattice_guide_mesh_set = lambda side: 'EyeMask_{}_lattice_guide'.format(side)

        joint_guides_set = lambda side : [

            {'n':'EyeMask_{}_mid_guide'.format(side),   'p': mask_jnt_grp},
            {'n':'EyeMask_{}_high_guide'.format(side),  'p': 'EyeMask_{}_mid_guide'.format(side)},
            {'n':'EyeMask_{}_low_guide'.format(side),   'p': 'EyeMask_{}_mid_guide'.format(side)},
            {'n':'EyeMask_{}_inner_guide'.format(side), 'p': 'EyeMask_{}_mid_guide'.format(side)},
            {'n':'EyeMask_{}_outer_guide'.format(side), 'p': 'EyeMask_{}_mid_guide'.format(side),}
        ]

        deformer_geo_list = []
        lattice_list = []

        # Build joints from guides
        for side in ['L', 'R']:

            # Create Deformation geo
            eyeMask_Smoosh_geo = 'eyeMask_Smoosh_{}_geo'.format(side)
            mesh.copy(self.ACCESORY_MASK_GEO, n=eyeMask_Smoosh_geo)
            deformer_geo_list.append(eyeMask_Smoosh_geo)

            # Build Joints
            for guide in joint_guides_set(side):

                joint_name    =  guide['n'].replace('guide', 'joint')
                joint_parent  =  guide['p'].replace('guide', 'joint') if guide['p'] else None

                mc.select(cl=1)

                mc.joint(n=joint_name)
                mc.delete(mc.parentConstraint(guide['n'], joint_name))

                if joint_parent:
                    mc.parent(joint_name, joint_parent)


                # Build Controls

                ctl_name = joint_name.replace('joint', 'CTL')
                ctl = mc.group(em=1,w=1, n=ctl_name)
                mc.delete(mc.parentConstraint(joint_name, ctl))

                shape = 'box' if '_mid_' not in joint_name else 'ball'
                shape_scale = 1 if '_mid_' not in joint_name else 2

                ctl = control.create(
                    ctl,
                    shape=shape,
                    colorIdx=17,
                    zeroGroup=True,
                )

                pm.scale(ctl + '.cv[:]', [shape_scale, shape_scale, shape_scale], r=True, os=True)

                pm.parentConstraint(ctl, joint_name)
                pm.scaleConstraint(ctl, joint_name)

                if joint_parent:
                    ctl_parent = joint_parent.replace('joint', 'CTL') if joint_parent != mask_jnt_grp else mask_ctl_grp
                    pm.parent(ctl.getParent(), ctl_parent)


            # Create Lattice
            lattice_n = 'EyeMask_{}_'.format(side)
            lattice_guide_mesh = lattice_guide_mesh_set(side)
            lattice, base = self.create_lattice(lattice_guide_mesh, eyeMask_Smoosh_geo, name=lattice_n)
            lattice_list.append(lattice)

        mc.parent('EyeMask_L_LatticeGroup', 'EyeMask_R_LatticeGroup', mask_util_grp)

        # Skin low geo
        skin_prefix = 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/kat/3D/kat_teen_mocapTorn/02_rig/03_imports/skinClusters/smoosh_mask/'
        print(skin_prefix + 'EyeMask_L_low_geo/EyeMask_L_low_geo_skinCluster_01.json')
        build.skin_bind([
            {'geo': self.EYEMASK_LOW_L, 'filepath': skin_prefix + 'EyeMask_L_low_geo/EyeMask_L_low_geo_skinCluster_02.json', 'import_module': 'skin_ng'},
            {'geo': self.EYEMASK_LOW_R, 'filepath': skin_prefix + 'EyeMask_R_low_geo/EyeMask_R_low_geo_skinCluster_02.json', 'import_module': 'skin_ng'},
        ])

        for geo, lat in zip([self.EYEMASK_LOW_L, self.EYEMASK_LOW_R], lattice_list):
            print('LATTICE COPY', geo, lat)
            skin.copy(geo, lat)


        mesh.copy(self.ACCESORY_MASK_GEO, n=self.ACCESORY_MASK_SMOOSH_GEO)

        eyeMask_bs_name = 'eyeMask_Smoosh_blendShape'

        blendshape.apply(
            deformer_geo_list[0],
            self.ACCESORY_MASK_SMOOSH_GEO,
            n=eyeMask_bs_name,
            dv=1,
        )

        blendshape.apply(
            deformer_geo_list[1],
            self.ACCESORY_MASK_SMOOSH_GEO,
            n=eyeMask_bs_name,
            dv=1,
        )

        blendshape.import_weights_from_file(
            filepath='D:/_Active_Projects/Destruna/masterAssets_forRef/characters/kat/3D/kat_teen_mocapTorn/02_rig/03_imports/blendshape_weights/mask/mask_bs_weights_02.json',
            geo=self.ACCESORY_MASK_SMOOSH_GEO,
            blendshape=eyeMask_bs_name

        )


        #     Insert blendshape to combine smoosh and face rig


        # Re-path  face rig blendsshape result into intermediate geo
        in_mesh, bs_node = [str(i) for i in mc.listConnections('Accessory_Mask_geoShape', type='blendShape', c=1)]
        # mc.disconnectAttr(bs_node, in_mesh)  # TODO DELETE OLD BLENDSHAPE NODE
        # mc.delete(bs_node.split('.')[0])

        blendshape.apply(
            self.ACCESORY_MASK_SMOOSH_GEO,
            self.ACCESORY_MASK_GEO,
            n=bs_node,
            dv=1,
        )

        mc.parent(self.EYEMASK_LOW_R, self.EYEMASK_LOW_L, self.ACCESORY_MASK_SMOOSH_GEO, deformer_geo_list,
                  mask_geo_grp)

        hair_guides_data = {

            'EarL': {
                'guides': ['Mask_ear_CTL_L_guide'],
                'scales': [5],
                'colorIdx': 19,
            },
            'EarR': {
                'guides': ['Mask_ear_CTL_R_guide'],
                'scales': [5],
                'colorIdx': 19,
            },
        }

        # Heirarchy
        hair_grp = pm.group(em=True, n='Mask', p='Master_CTL_Main')
        hair_ctl_grp = pm.group(em=True, n='Mask_controls', p=hair_grp)
        hair_jnt_grp = pm.group(em=True, n='Mask_joints', p=hair_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', hair_jnt_grp.v)

        for label, guide_data in hair_guides_data.items():

            guides = guide_data.get('guides')
            scales = guide_data.get('scales')
            color_idx = guide_data.get('colorIdx')

            ctl_list = list()
            jnt_list = list()
            for i, guide in enumerate(guides):

                # control
                ctl = control.create(
                    guide.replace('_guide', ''),
                    shape='ball',
                    colorIdx=color_idx,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)
                pm.delete(pm.parentConstraint(guide, ctl.getParent(), mo=False))
                pm.scale(ctl.cv[:], [scales[i], scales[i], scales[i]], r=True, os=True)
                ctl.v.set(k=False, cb=False, l=True)

                # joint
                pm.select(cl=True)
                jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_jnt')
                jnt_list.append(jnt)
                pm.parentConstraint(ctl, jnt, mo=False)
                pm.scaleConstraint(ctl, jnt, mo=False)

                # parenting
                if i == 0:
                    pm.parent(ctl.getParent(), hair_ctl_grp)
                    pm.parentConstraint('Head_Head_joint_M', ctl.getParent(), mo=True)
                    pm.parent(jnt, hair_jnt_grp)
                else:
                    pm.parent(ctl.getParent(), ctl_list[i-1])
                    pm.parent(jnt, jnt_list[i-1])

        # Vis attribute
        attribute.add(self.FACE_NS + ':Mask_CTL_settings', n='Mask_Vis', type='enum', en=['hide', 'show'], dv=1)
        mc.connectAttr(self.FACE_NS + ':Mask_CTL_settings.Mask_Vis', self.ACCESORY_MASK_GEO + '.visibility')
        mc.connectAttr(self.FACE_NS + ':Mask_CTL_settings.Mask_Vis', hair_ctl_grp + '.visibility')
        mc.connectAttr(self.FACE_NS + ':Mask_CTL_settings.Mask_Vis', 'Mask_Controls_grp.visibility')


        # Post edit
        mc.select(cl=1)
        jnt = mc.joint(n='mask_root_jnt')
        mc.parent(jnt, mask_util_grp)

    def build_jumper_controls(self):

        jumper_grp = pm.group(em=True, n='Jumper', p='Master_CTL_Main')
        jumper_ctl_grp = pm.group(em=True, n='Jumper_controls', p=jumper_grp)
        jumper_jnt_grp = pm.group(em=True, n='Jumper_joints', p=jumper_grp)

        pm.connectAttr('Master_CTL_Main.jointVis', jumper_jnt_grp.v)

        # Jumper_CTL_torsoA
        jumper_ctl_torsoA = control.create(
            'Jumper_CTL_mainA',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )

        jumper_ctl_torsoA_zero = jumper_ctl_torsoA.getParent()
        jumper_ctl_torsoA_zero.t.set([0.38, 91.31, -6.81])
        jumper_ctl_torsoA_zero.r.set([17.41, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoA.cv[:], [14.59, 10.43, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoA_zero, jumper_ctl_grp)

        # Jumper_CTL_torsoB
        jumper_ctl_torsoB = control.create(
            'Jumper_CTL_mainB',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoB_zero = jumper_ctl_torsoB.getParent()
        jumper_ctl_torsoB_zero.t.set([0.38, 81.36, -9.54])
        jumper_ctl_torsoB_zero.r.set([12.34, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoB.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoB_zero, jumper_ctl_torsoA)

        # Jumper_CTL_torsoC
        jumper_ctl_torsoC = control.create(
            'Jumper_CTL_mainC',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoC_zero = jumper_ctl_torsoC.getParent()
        jumper_ctl_torsoC_zero.t.set([0.38, 74.88, -10.75])
        jumper_ctl_torsoC_zero.r.set([10.6, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoC.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoC_zero, jumper_ctl_torsoB)

        # Jumper_CTL_torsoD
        jumper_ctl_torsoD = control.create(
            'Jumper_CTL_mainD',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoD_zero = jumper_ctl_torsoD.getParent()
        jumper_ctl_torsoD_zero.t.set([0.38, 68.17, -11.68])
        jumper_ctl_torsoD_zero.r.set([11.39, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoD.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoD_zero, jumper_ctl_torsoC)

        # Jumper_CTL_torsoE
        jumper_ctl_torsoE = control.create(
            'Jumper_CTL_mainE',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoE_zero = jumper_ctl_torsoE.getParent()
        jumper_ctl_torsoE_zero.t.set([0.38, 61.4, -12.94])
        jumper_ctl_torsoE_zero.r.set([10.09, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoE.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoE_zero, jumper_ctl_torsoD)

        # Jumper_CTL_knot
        jumper_ctl_knot = control.create(
            'Jumper_CTL_knot',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_knot_zero = jumper_ctl_knot.getParent()
        jumper_ctl_knot_zero.t.set([-1.02, 82.77, 12.47])
        pm.scale(jumper_ctl_knot.cv[:], [10.34, 10.34, 8.1], r=True, os=True)
        pm.parent(jumper_ctl_knot_zero, jumper_ctl_grp)
        pm.parentConstraint('Spine_RootPart2_joint_M', jumper_ctl_knot_zero, mo=True)

        # Jumper_CTL_tie_L
        jumper_ctl_tie_L = control.create(
            'Jumper_CTL_tie_L',
            shape='box',
            colorIdx=18,
            zeroGroup=True
        )
        jumper_ctl_tie_L_zero = jumper_ctl_tie_L.getParent()
        jumper_ctl_tie_L_zero.t.set([3.73, 77.83, 13.0])
        jumper_ctl_tie_L_zero.r.set([-5.16, 0.0, 33.22])
        pm.scale(jumper_ctl_tie_L.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tie_L_zero, jumper_ctl_knot)

        # Jumper_CTL_tieA_R
        jumper_ctl_tieA_R = control.create(
            'Jumper_CTL_tieA_R',
            shape='box',
            colorIdx=20,
            zeroGroup=True
        )
        jumper_ctl_tieA_R_zero = jumper_ctl_tieA_R.getParent()
        jumper_ctl_tieA_R.t.set([-4.02, 77.91, 11.93])
        jumper_ctl_tieA_R.r.set([-5.16, 0.0, -33.22])
        pm.scale(jumper_ctl_tieA_R.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tieA_R_zero, jumper_ctl_knot)

        # Jumper_CTL_tieB_R
        jumper_ctl_tieB_R = control.create(
            'Jumper_CTL_tieB_R',
            shape='box',
            colorIdx=20,
            zeroGroup=True
        )
        jumper_ctl_tieB_R_zero = jumper_ctl_tieB_R.getParent()
        jumper_ctl_tieB_R.t.set([-6.39, 74.28, 12.01])
        jumper_ctl_tieB_R.r.set([-5.16, 0.0, -33.22])
        pm.scale(jumper_ctl_tieB_R.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tieB_R_zero, jumper_ctl_tieA_R)

        # joints

        # knot
        pm.select(jumper_jnt_grp, r=True)
        jumper_knot_jnt = pm.joint(n='Jumper_knot_joint')

        pm.parentConstraint(jumper_ctl_knot, jumper_knot_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_knot, jumper_knot_jnt, mo=False)

        pm.select(jumper_knot_jnt, r=True)
        jumper_tie_L_jnt = pm.joint(n='Jumper_tie_joint_L')
        pm.parentConstraint(jumper_ctl_tie_L, jumper_tie_L_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tie_L, jumper_tie_L_jnt, mo=False)

        pm.select(jumper_knot_jnt, r=True)
        jumper_tieA_R_jnt = pm.joint(n='Jumper_tieA_joint_R')
        jumper_tieB_R_jnt = pm.joint(n='Jumper_tieB_joint_R')

        pm.parentConstraint(jumper_ctl_tieA_R, jumper_tieA_R_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tieA_R, jumper_tieA_R_jnt, mo=False)

        pm.parentConstraint(jumper_ctl_tieB_R, jumper_tieB_R_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tieB_R, jumper_tieB_R_jnt, mo=False)

        # torso
        pm.select(jumper_jnt_grp, r=True)
        jumper_torso_A_jnt = pm.joint(n='Jumper_mainA_joint')
        jumper_torso_B_jnt = pm.joint(n='Jumper_mainB_joint')
        jumper_torso_C_jnt = pm.joint(n='Jumper_mainC_joint')
        jumper_torso_D_jnt = pm.joint(n='Jumper_mainD_joint')
        jumper_torso_E_jnt = pm.joint(n='Jumper_mainE_joint')

        pm.parentConstraint('Spine_RootPart2_joint_M', jumper_ctl_torsoA_zero, mo=True)

        pm.parentConstraint(jumper_ctl_torsoA, jumper_torso_A_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoA, jumper_torso_A_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoB, jumper_torso_B_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoB, jumper_torso_B_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoC, jumper_torso_C_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoC, jumper_torso_C_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoD, jumper_torso_D_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoD, jumper_torso_D_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoE, jumper_torso_E_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoE, jumper_torso_E_jnt, mo=False)

        ctl_list = [
            jumper_ctl_knot,
            jumper_ctl_tie_L,
            jumper_ctl_tieA_R,
            jumper_ctl_tieB_R,

            jumper_ctl_torsoA,
            jumper_ctl_torsoB,
            jumper_ctl_torsoC,
            jumper_ctl_torsoD,
            jumper_ctl_torsoE,
        ]

        for ctl in ctl_list:
            ctl.v.set(cb=False, k=False, l=True)

    def build_pouch_controls(self):

        # ctl
        pouch_grp = pm.group(em=True, n='Pouch', p='Master_CTL_Main')
        pouch_ctl_grp = pm.group(em=True, n='Pouch_controls', p=pouch_grp)
        pouch_jnt_grp = pm.group(em=True, n='Pouch_joints', p=pouch_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', pouch_jnt_grp.v)

        pouch_ctl_leg = control.create(
            'Pouch_CTL_main',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )

        pouch_ctl_leg_zero = pouch_ctl_leg.getParent()
        pouch_ctl_leg_zero.t.set([-16.023, 78.879, 1.179])
        pouch_ctl_leg_zero.r.set([123.889, -89.963, -116.98])
        pm.scale(pouch_ctl_leg.cv[:], [10, 5, 5], r=True, os=True)
        pm.parentConstraint('Leg_HipPart1_joint_R', pouch_ctl_leg_zero, mo=True)
        pm.parent(pouch_ctl_leg_zero, pouch_ctl_grp)

        # jnt
        pm.select(pouch_ctl_leg, r=True)
        pouch_jnt = pm.joint(n='Pouch_joint')
        pm.parentConstraint(pouch_ctl_leg, pouch_jnt, mo=False)
        pm.scaleConstraint(pouch_ctl_leg, pouch_jnt, mo=False)

    def create_lattice(self, guide_mesh, deformerGeo, name=None):

        d = float(2)

        # POSITION & SCALE
        position_tuple = mc.xform(guide_mesh, rp=1, ws=1, q=1)
        rotation_tuple = mc.xform(guide_mesh, ro=1, ws=1, q=1)
        scale_tuple = mc.xform(guide_mesh, s=1, ws=1, q=1)

        scale_div_ten = lambda x, y, z: [x/d, y/d, z/d]

        lattice = mc.lattice(
            deformerGeo,
            name=name,
            pos=position_tuple,
            ro=rotation_tuple,
            s=scale_tuple,
            dv=[9, 6, 2],   #scale_div_ten(*scale_tuple),
            cp=1,
        )

        base = name + 'Base'

        for p, attr in zip(position_tuple, ['X', 'Y', 'Z']):
            mc.setAttr(base + '.translate{}'.format(attr), p)

        for p, attr in zip(rotation_tuple, ['X', 'Y', 'Z']):
            mc.setAttr(base + '.rotate{}'.format(attr), p)

        for s, attr in zip(scale_tuple, ['X', 'Y', 'Z']):
            mc.setAttr(base + '.scale{}'.format(attr), s)

        mc.select(cl=1)

        return lattice[1], lattice[2]

    def build_boot_controls(self):

        boots_grp = pm.group(em=True, n='Boots', p='Master_CTL_Main')
        boots_ctl_grp = pm.group(em=True, n='Boots_controls', p=boots_grp)
        boots_jnt_grp = pm.group(em=True, n='Boots_joints', p=boots_grp)

        pm.connectAttr('Master_CTL_Main.jointVis', boots_jnt_grp.v)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1
            color = 20 if side == 'R' else 18
            # bootA
            boot_ctl_a = control.create(
                'Boot_CTL_cuffA_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_a_zero = boot_ctl_a.getParent()
            boot_ctl_a_zero.t.set([6.9*side_mult, 14.18, -2.16])
            pm.scale(boot_ctl_a.cv[:], [9.26, 3.27, 9.81], r=True, os=True)
            pm.parent(boot_ctl_a_zero, boots_ctl_grp)

            # bootB
            boot_ctl_b = control.create(
                'Boot_CTL_cuffB_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_b_zero = boot_ctl_b.getParent()
            boot_ctl_b_zero.t.set([7.42*side_mult, 18.68, -1.98])
            boot_ctl_b_zero.r.set([5.88, 0.0, 0.0])
            pm.scale(boot_ctl_b.cv[:], [11.93, 3.27, 14.77], r=True, os=True)
            pm.parent(boot_ctl_b_zero, boot_ctl_a)

            # bootC
            boot_ctl_c = control.create(
                'Boot_CTL_cuffC_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_c_zero = boot_ctl_c.getParent()
            boot_ctl_c_zero.t.set([7.42*side_mult, 23.49, -1.33])
            boot_ctl_c_zero.r.set([11.05, 0.0, 0.0])
            pm.scale(boot_ctl_c.cv[:], [13.52, 3.27, 18.93], r=True, os=True)
            pm.parent(boot_ctl_c_zero, boot_ctl_b)

            pm.select(boots_jnt_grp, r=True)
            boot_a_jnt = pm.joint(n='Boot_cuffA_joint_'+side)
            boot_b_jnt = pm.joint(n='Boot_cuffA_joint_'+side)
            boot_c_jnt = pm.joint(n='Boot_cuffA_joint_'+side)

            pm.parentConstraint('KneePart2_'+side, boot_ctl_a_zero, mo=True)

            pm.parentConstraint(boot_ctl_a, boot_a_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_a, boot_a_jnt, mo=False)
            pm.parentConstraint(boot_ctl_b, boot_b_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_b, boot_b_jnt, mo=False)
            pm.parentConstraint(boot_ctl_c, boot_c_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_c, boot_c_jnt, mo=False)

            for ctl in [
                boot_ctl_a,
                boot_ctl_b,
                boot_ctl_c,
            ]:
                ctl.v.set(cb=False, k=False, l=True)

    def build_scrunchie_controls(self):

        scrunchie_guides_data = [

            {   'name': 'Scrunchie_M',
                'guides': ['Scrunchie_Main_M_guide',],
                'scales': [5],
                'colorIdx': 18,
                'shape': 'ball',
            },

            {   'name': 'Scrunchie_L',
                'guides': ['Scrunchie_Main_L_01_guide',
                           'Scrunchie_Main_L_02_guide',
                           'Scrunchie_Main_L_03_guide',],
                'scales': [5, 5, 5,],
                'colorIdx': 18,
                'shape': 'box',
            },

            {   'name': 'Scrunchie_R',
                'guides': ['Scrunchie_Main_R_01_guide',
                           'Scrunchie_Main_R_02_guide',
                           'Scrunchie_Main_R_03_guide',],
                'scales': [5, 5, 5],
                'colorIdx': 13,
                'shape': 'box',
            },
        ]

        # Heirarchy
        scrunchie_grp = pm.group(em=True, n='Scrunchie', p='Master_CTL_Main')
        scrunchie_ctl_grp = pm.group(em=True, n='Scrunchie_controls', p=scrunchie_grp)
        scrunchie_jnt_grp = pm.group(em=True, n='Scrunchie_joints', p=scrunchie_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', scrunchie_jnt_grp.v)

        for guide_data in scrunchie_guides_data:

            name   = guide_data['name']
            guides = guide_data.get('guides')
            scales = guide_data.get('scales')
            color_idx = guide_data.get('colorIdx')

            ctl_list = list()
            jnt_list = list()
            for i, guide in enumerate(guides):

                # control
                ctl = control.create(
                    guide.replace('_guide', '_CTL'),
                    shape=guide_data['shape'],
                    colorIdx=color_idx,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)
                pm.delete(pm.parentConstraint(guide, ctl.getParent(), mo=False))
                pm.scale(ctl.cv[:], [scales[i], scales[i], scales[i]], r=True, os=True)
                ctl.v.set(k=False, cb=False, l=True)

                # joint
                pm.select(cl=True)
                jnt = pm.joint(n=str(ctl).replace('_CTL_', '_') + '_jnt')
                jnt_list.append(jnt)
                pm.parentConstraint(ctl, jnt, mo=False)
                pm.scaleConstraint(ctl, jnt, mo=False)

                # parenting
                if i == 0 and name != 'Scrunchie_M':
                    pm.parent(ctl.getParent(), scrunchie_ctl_grp)
                    pm.parentConstraint('Scrunchie_Main_M_CTL', ctl.getParent(), mo=True)
                    pm.scaleConstraint('Scrunchie_Main_M_CTL', ctl.getParent(), mo=True)
                    pm.parent(jnt, scrunchie_jnt_grp)

                elif i==0:
                    pm.parent(ctl.getParent(), scrunchie_ctl_grp)
                    pm.parentConstraint('Head_Head_joint_M', ctl.getParent(), mo=True)
                    pm.scaleConstraint('Head_Head_joint_M', ctl.getParent(), mo=True)
                    pm.parent(jnt, scrunchie_jnt_grp)

                else:
                    pm.parent(ctl.getParent(), ctl_list[i - 1])
                    pm.parent(jnt, jnt_list[i - 1])

    def build_torso_controls(self):

        # breast ctl
        torso_grp = pm.group(em=True, n='Breasts', p='Master_CTL_Main')
        torso_ctl_grp = pm.group(em=True, n='Breasts_controls', p=torso_grp)
        torso_jnt_grp = pm.group(em=True, n='Breasts_joints', p=torso_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', torso_jnt_grp.v)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1
            color = 20 if side == 'R' else 18

            # bootA
            breast_ctl_main = control.create(
                'Torso_CTL_breast_'+side,
                shape='ball',
                colorIdx=color,
                zeroGroup=True,
            )
            breast_ctl_main_zero = breast_ctl_main.getParent()
            breast_ctl_main_zero.t.set([5.38 * side_mult, 122.23, 7.33])
            breast_ctl_main_zero.r.set([-14.66, 24.6 * side_mult,  -3.14])
            pm.scale(breast_ctl_main.cv[:], [6.2, 6.2, 6.2], r=True, os=True)
            pm.parent(breast_ctl_main_zero, torso_ctl_grp)
            pm.parentConstraint('Spine_Chest_joint_M', breast_ctl_main_zero, mo=True)
            breast_ctl_main.v.set(cb=False, k=False, l=True)

            # breast_ctl_main_zero = breast_ctl_main.getParent()
            # breast_ctl_main_zero.t.set([6.686*side_mult, 122.304, 8.0])
            # breast_ctl_main_zero.r.set([12.301, 12*side_mult, -0.422])
            # pm.scale(breast_ctl_main.cv[:], [6.2, 6.2, 6.2], r=True, os=True)
            # pm.parent(breast_ctl_main_zero, torso_ctl_grp)
            # pm.parentConstraint('Spine_Chest_joint_M', breast_ctl_main_zero, mo=True)
            # breast_ctl_main.v.set(cb=False, k=False, l=True)


            # jnt
            pm.select(torso_jnt_grp, r=True)
            breast_jnt = pm.joint(n='Torso_breast_joint_'+side)
            #pm.delete(pm.parentConstraint(breast_ctl_main, breast_jnt, mo=False))
            #pm.delete(pm.scaleConstraint(breast_ctl_main, breast_jnt, mo=False))
            pm.scaleConstraint(breast_ctl_main, breast_jnt, mo=False)
            pm.parentConstraint(breast_ctl_main, breast_jnt, mo=False)

    def install_outfit_toggle(self):

        attribute.add_separator('Master_CTL_Main')
        attribute.add('Master_CTL_Main', n='Mocap_Outfit', type='enum', en=['pristine', 'torn'], dv=0)

        rev_node = mc.createNode('reverse')
        mc.connectAttr('Master_CTL_Main.Mocap_Outfit', self.BODY_GEO + ".visibility")
        mc.connectAttr('Master_CTL_Main.Mocap_Outfit', 'Toe_CTL_0_L_ZERO' + ".visibility")
        mc.connectAttr('Master_CTL_Main.Mocap_Outfit', rev_node + '.input.inputX')
        mc.connectAttr(rev_node + '.output.outputX', self.BODY_GEO_ALT + ".visibility")

    def build_hair_controls(self):

        # instance hair_class + build
        hair_class = hair_rig.HairRig()
        hair_class.build_data = self.hair_data
        hair_class.build()

        # install into AS rig.
        pm.parent(hair_class.TOP_GRP, 'Master_CTL_Main')
        pm.parentConstraint('Head_Head_joint_M', hair_class.PREFIX + '_headConstrainedGroup', mo=True)
        pm.scaleConstraint('Head_Head_joint_M', hair_class.PREFIX + '_headConstrainedGroup', mo=True)

        pm.connectAttr('Master_CTL_Main.jointVis', 'Hair_joints.v')

        pm.connectAttr('Master_CTL_Main.sy', 'Hair_connectors.rigScale')

        attribute.add(self.MASTER_CONTROL, n='hairCTL_vis', type='bool', dv=1, cb=True, k=False)
        mc.connectAttr(self.MASTER_CONTROL + '.hairCTL_vis', 'Hair_controls.visibility')

    def duplicate_geo(self, dupe_dict):

        for copy_set in dupe_dict:
            source = copy_set['source']
            dupe_name = copy_set['dupe_name']
            mesh.copy(source, n=dupe_name)

    def setup_facerig(self):

        ns = self.FACE_NS + ':'

        # face_rig_geo = ns + 'face_rig_geo'

        # parent face rig.
        pm.parent(ns + 'faceRig', 'Master_CTL_Main')
        pn(ns + 'faceRig').inheritsTransform.set(False)

        pm.parent(ns + 'Head_constrained_grp', 'Master_CTL_Main')

        # constrain controls to head.
        pm.parentConstraint('Head_Head_joint_M', ns + 'Head_constrained_grp', mo=True)
        pm.parentConstraint('Head_Head_joint_M', ns + 'Head_constrained_grp', mo=True)

        for geo in self.FACE_GEO_LIST:
            blendshape.apply(ns + geo, geo, dv=1)

        # hide face geo
        pm.hide(ns + 'Geo')

        for side in 'LR':
            for atr in ['rx', 'ry', 'rz']:
                pm.connectAttr(pn('Head_Eye_joint_' + side).attr(atr), pn('face:Eye_driver_' + side + '_GRP').attr(atr))

        hide_list = [
            ns + 'Tweaks_controls',
            ns + 'Main_joints',
            ns + 'Region_joints',

        ]

        for h in hide_list:
            pn(h).v.set(0)

    def attach_wristband(self):

        driver_joint = 'Arm_ElbowPart2_joint_R'
        driven_mesh = self.ACCESORY_ARMBAND

        mc.parentConstraint(driver_joint, driven_mesh, mo=1)
        mc.scaleConstraint(driver_joint, driven_mesh, mo=1)

    def post_edits(self):
        mc.setAttr("Master_CTL_Main.geoLock", 0)
        mc.setAttr("Master_CTL_Main.jointVis", 1)

    # ==================================================================================================================
    def go(self):
        print '='*80
        print 'Starting '+str(self.who)+' Rig Build.'
        mc.file(new=True, f=True)
        self.build_contents()
        print 'Finished!'

    def build_contents(self):

        build.imports(self.imports)
        pm.rename('Group', self.RIG_GRP)
        build.groups(self.groups)
        pm.parent(self.GEO_LIST, self.GEO_GRP)

        self.modify_as_rig()
        self.build_pouch_controls()
        self.build_hair_controls()
        self.build_torso_controls()
        self.build_toe_controls()
        self.attach_wristband()
        self.build_scrunchie_controls()

        self.setup_facerig()
        self.wrap_scleras()
        self.build_mask_controls()

        build.skin_bind(self.skin_bind)

        self.install_outfit_toggle()

        pm.delete(self.DELETE_GRP)

        #self.post_edits()
"""
 
"""
