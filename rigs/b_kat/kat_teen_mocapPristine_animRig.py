from jb_destruna.maya.core import build
import maya.cmds as mc
import pymel.core as pm

from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import skin
from jb_destruna.maya.internal import control
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn

import kat_teen_mocapTorn_animRig as mocapTorn

reload(build)
reload(rig)


class Rig(mocapTorn.Rig):

    ASSET = 'kat'

    #Eye_geo_L
    #Eye_geo_R
    #Accessory_Mask_geo
    #HairFront_geo
    #HairBase_geo
    #MocapSuit_Torn_geo

    # THE FOLLOWING TO BE SPLIT INTO OTHER VAIRANTS0
    #              MocapSuit_Hair_geo
    #              MocapSuit_Pouch_geo

    # *****Eyebrows_Geo***** not accounted for
    #Face_geo

    # BOTTOMTEETH_GEO = 'bottomTeeth_geo'
    # EYEBROW_GEO_L = 'eyebrow_geo_l'
    # EYEBROW_GEO_R = 'eyebrow_geo_r'MocapSuit_Hair_geo
    FACE_GEO = 'Face_geo'
    HAIR_FRONT_GEO = 'HairFront_geo'
    HAIR_BASE_GEO = 'HairBase_geo'
    HAIR_LONG = 'MocapSuit_Hair_geo'
    BODY_GEO = 'MocapSuit_Torn_geo'
    PUPIL_GEO_L = 'Eye_geo_L'
    SCLERA_GEO_L = 'Eye_Sclera_geo_L'
    SCLERA_GEO_R = 'Eye_Sclera_geo_R'
    PUPIL_GEO_R = 'Eye_geo_R'
    ACCESORY_MASK = 'Accessory_Mask_geo'
    ACCESORY_BELT = 'MocapSuit_AccessoryBelt_geo'
    ACCESORY_POUCH = 'MocapSuit_Pouch_geo'
    # TOPTEETH_GEO = 'topTeeth_geo'


    GEO_LIST = [
        FACE_GEO,
        HAIR_FRONT_GEO,
        HAIR_BASE_GEO,
        HAIR_LONG,
        BODY_GEO,
        PUPIL_GEO_L,
        SCLERA_GEO_L,
        SCLERA_GEO_R,
        PUPIL_GEO_R,
        ACCESORY_MASK,
        ACCESORY_BELT,
        ACCESORY_POUCH,

    ]