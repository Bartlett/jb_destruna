"""
# general AS rig tweaks for build script
    - build main heirarcy
    - default build color is crap
    - some ctls are in a bad position by default (jaw, clavs)
    - attr locking
    - figure out message array connection system for custom selection tools.


"""

# DONE: LONA: prep geo import file
# TODO: run cleanup on lona mesh.
# DONE: LONA: fitted asRig
# DONE: LONA: roll extra controls into an importable format.
# IN PROGRESS: LONA: write build script
# TODO: LONA: (tiedJumper) skinweights
# TODO: LONA: Face Rig.
# TODO: Face Rig Transfer Methodology.
# TODO: Download Rig Shaders


"""
# LONA CUSTOMIZATIONS

# custom ctls from sweater
    - breasts DONE
    - hair fk ctls DONE

# custom body correctives
    - hoodie puff
    - shoulder puff L
    - shoulder puff R
    - torso puff
    - overall puff
    - wrist shrink L
    - wrist shrink R
    - neck thick
    - hand scale R
    - hand scale L
"""

# TODO: KAT: prep geo import file
# TODO: KAT: fitted asRig
# TODO: KAT: roll extra controls into an importable format.
# TODO: KAT: write build script

# TODO: PEKAPOT: prep geo import file
# TODO: PEKAPOT: fitted asRig
# TODO: PEKAPOT: roll extra controls into an importable format.
# TODO: PEKAPOT: write build script

# TODO: PEKAPOT: prep geo import file
# TODO: JACK: fitted asRig
# TODO: JACK: roll extra controls into an importable format.
# TODO: JACK: write build script
