from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import control
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn

from jb_destruna.rigs.c_pekabot import pekabot_base_animRig
import maya.cmds as mc
import pymel.core as pm
#reload(build)
#reload(rig)


class Rig(pekabot_base_animRig.Rig):

    ASSET = 'pekabot_guns'

    # Gun Geo
    GUN_BASE_L = 'turretBase_L'
    GUN_A_PIVOT_L = 'turretSectionA_pivot_L'
    GUN_A_CYLINDER_L = 'turretSectionA_cylinder_L'
    GUN_B_PIVOT_L = 'turretSectionB_pivot_L'
    GUN_B_CYLINDER_L = 'turretSectionB_cylinder_L'
    GUN_B_POLE_L = 'turretSectionA_pole_L'
    GUN_C_PIVOT_L = 'turretSectionC_pivot_L'
    GUN_C_END_L = 'turretSectionC_end_L'

    GUN_BASE_R = 'turretBase_R'
    GUN_A_PIVOT_R = 'turretSectionA_pivot_R'
    GUN_A_CYLINDER_R = 'turretSectionA_cylinder_R'
    GUN_B_PIVOT_R = 'turretSectionB_pivot_R'
    GUN_B_CYLINDER_R = 'turretSectionB_cylinder_R'
    GUN_B_POLE_R = 'turretSectionA_pole_R'
    GUN_C_PIVOT_R = 'turretSectionC_pivot_R'
    GUN_C_END_R = 'turretSectionC_end_R'

    GUN_GEO_DICT = {

        'L': {
            'GUN_BASE': GUN_BASE_L,
            'GUN_A_PIVOT': GUN_A_PIVOT_L,
            'GUN_A_CYLINDER': GUN_A_CYLINDER_L,
            'GUN_B_PIVOT': GUN_B_PIVOT_L,
            'GUN_B_CYLINDER': GUN_B_CYLINDER_L,
            'GUN_B_POLE': GUN_B_POLE_L,
            'GUN_C_PIVOT': GUN_C_PIVOT_L,
            'GUN_C_END': GUN_C_END_L
        },

        'R': {
            'GUN_BASE': GUN_BASE_R,
            'GUN_A_PIVOT': GUN_A_PIVOT_R,
            'GUN_A_CYLINDER': GUN_A_CYLINDER_R,
            'GUN_B_PIVOT': GUN_B_PIVOT_R,
            'GUN_B_CYLINDER': GUN_B_CYLINDER_R,
            'GUN_B_POLE': GUN_B_POLE_R,
            'GUN_C_PIVOT': GUN_C_PIVOT_R,
            'GUN_C_END': GUN_C_END_R
        },
    }

    # GUN GUIDES
    GUN_BASE_GUIDE_L = 'guns_base_L_guide'
    GUN_MID_GUIDE_L = 'guns_mid_L_guide'
    GUN_TOP_GUIDE_L = 'guns_top_L_guide'

    GUN_BASE_GUIDE_R = 'guns_base_R_guide'
    GUN_MID_GUIDE_R = 'guns_mid_R_guide'
    GUN_TOP_GUIDE_R = 'guns_top_R_guide'

    GUN_GUIDE_DICT = {

        'L': {
            'GUN_BASE_GUIDE': GUN_BASE_GUIDE_L,
            'GUN_MID_GUIDE': GUN_MID_GUIDE_L,
            'GUN_TOP_GUIDE': GUN_TOP_GUIDE_L,
        },

        'R': {
            'GUN_BASE_GUIDE': GUN_BASE_GUIDE_R,
            'GUN_MID_GUIDE': GUN_MID_GUIDE_R,
            'GUN_TOP_GUIDE': GUN_TOP_GUIDE_R,
        },
    }



    def __init__(self):

        super(Rig, self).__init__()

        self.imports += [
            # GUNS GEO
            {
                'filepath': 'D:/_Active_Projects\Destruna\masterAssets_forRef\characters\peka_bot\gunsModel/3D/02_rig\imports/geo/peka_bot_gunsGeo_02_DMR_WIP.mb',
                #'group': self.DELETE_GRP
             },

            # GUNS GUIDES
            {
                'filepath': 'D:/_Active_Projects\Destruna\masterAssets_forRef\characters\peka_bot\gunsModel/3D/02_rig\imports/guides/peka_bot_gunsGuides_02_DMR_WIP.mb'},

        ]


        self.joint_chain_drivers = [

            'FK',
            'IK',
            'SDK'

        ]

    def build_gun_rigs(self):

        for side in ['L', 'R']:

            self.create_joint_chain(side)

    def create_joint_chain(self, side):

        for chain in self.joint_chain_drivers:
            for joint in self.GUN_GUIDE_JOINTS:

                new_j = joint.replace('_side_guide', '{0}_{1}'.format('joint', side))
                mc.delete(mc.parentConstraint())



    # ==================================================================================================================
    def go(self):
        print('='*80)
        print('Starting '+str(self.who)+' Rig Build.')
        mc.file(new=True, f=True)
        self.build_contents()
        print('Finished!')

    def build_contents(self):
        build.imports(self.imports)
        pm.rename('Group', self.RIG_GRP)
        build.groups(self.groups)
        pm.parent(self.GEO_LIST, self.GEO_GRP)
        self.modify_as_rig()

        self.build_gun_rigs()

        # build.skin_bind(self.skin_bind)
        # build.skin_copy(self.skin_copy)

        build.proxy_setup(self.proxy_setup)
        pm.delete(self.DELETE_GRP)


"""
#     PEKA-BOT    #
#      BASE       # 

from jb_destruna.rigs.c_pekabot import pekabot_base_animRig
from jb_destruna.rigs.c_pekabot import pekabot_guns_animRig
reload(pekabot_base_animRig)
reload(pekabot_guns_animRig)
rig = pekabot_guns_animRig.Rig()
rig.go()
"""