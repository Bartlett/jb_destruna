from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import control
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn

import maya.cmds as mc
import pymel.core as pm

import lona_teen_animRig
reload(lona_teen_animRig)


class Rig(lona_teen_animRig.Rig):

    ASSET = 'lona_teen_jumperOff'

    BOTTOMTEETH_GEO = 'bottomTeeth_geo'
    EYEBROW_GEO_L = 'eyebrow_geo_l'
    EYEBROW_GEO_R = 'eyebrow_geo_r'
    FACE_GEO = 'face_geo'
    HAIR_GEO = 'hair_geo'
    HOODIEKNOT_GEO = 'hoodieKnot_geo'
    HOODIE_GEO = 'hoodie_geo'
    LONABODY_NOJUMPER_GEO = 'lonaBody_noJumper_geo'
    MIDBOTTOMEYELASH_GEO_L = 'midBottomEyelash_geo_l'
    MIDBOTTOMEYELASH_GEO_R = 'midBottomEyelash_geo_r'
    PUPIL_GEO_L = 'pupil_geo_l'
    PUPIL_GEO_R = 'pupil_geo_r'
    SCLERA_GEO_L = 'sclera_geo_l'
    SCLERA_GEO_R = 'sclera_geo_r'
    TONGUE_GEO = 'tongue_geo'
    TOPTEETH_GEO = 'topTeeth_geo'
    UNDERBOTTOMEYELASH_GEO_L = 'underBottomEyelash_geo_l'
    UNDERBOTTOMEYELASH_GEO_R = 'underBottomEyelash_geo_r'
    UPPERBOTTOMEYELASH_GEO_L = 'upperBottomEyelash_geo_l'
    UPPERBOTTOMEYELASH_GEO_R = 'upperBottomEyelash_geo_r'
    UPPEREYELASH_GEO_L = 'upperEyelash_geo_l'
    UPPEREYELASH_GEO_R = 'upperEyelash_geo_r'

    GEO_LIST = [
        BOTTOMTEETH_GEO,
        EYEBROW_GEO_L,
        EYEBROW_GEO_R,
        FACE_GEO,
        HAIR_GEO,
        HOODIEKNOT_GEO,
        HOODIE_GEO,
        LONABODY_NOJUMPER_GEO,
        MIDBOTTOMEYELASH_GEO_L,
        MIDBOTTOMEYELASH_GEO_R,
        PUPIL_GEO_L,
        PUPIL_GEO_R,
        SCLERA_GEO_L,
        SCLERA_GEO_R,
        TONGUE_GEO,
        TOPTEETH_GEO,
        UNDERBOTTOMEYELASH_GEO_L,
        UNDERBOTTOMEYELASH_GEO_R,
        UPPERBOTTOMEYELASH_GEO_L,
        UPPERBOTTOMEYELASH_GEO_R,
        UPPEREYELASH_GEO_L,
        UPPEREYELASH_GEO_R,
    ]

    JACKET_SKINCOPY_GEO = 'jacket_skinCopy_geo'

    def __init__(self):
        super(Rig, self).__init__()

        self.imports = [
            # GEO
            {'filepath': 'E:/destruna/assets/lona/geo/jumperOff/lona_teen_jumperOff_model_12_DFO_MASTER_fixes.mb', 'group': self.DELETE_GRP},

            # AS RIG
            {'filepath': 'E:/destruna/assets/lona/rig/imports/asRig_v007.mb'},

            # Hair ctl guides
            {'filepath': 'E:/destruna/assets/lona/rig/imports/hair_guides_v002.mb', 'group': self.DELETE_GRP},

            # skincopy geo
            {'filepath': 'E:/destruna/assets/lona/geo/body_skinCopy_geo_v003.mb', 'group': self.DELETE_GRP},
            {'filepath': 'E:/destruna/assets/lona/geo/jacket_skinCopy_geo_v001.mb', 'group': self.DELETE_GRP},

            # AS FACE RIG
            {'filepath': 'E:/destruna/assets/lona/publishes/face_v007.mb', 'namespace': self.FACE_NS},
        ]

        self.skin_bind.extend([
            # {'geo': self.HOODIE_GEO, 'filepath': 'E:/destruna/assets/lona/rig/weights/hoodie_geo_skinCluster_001.json'},
            # {'geo': self.HOODIEKNOT_GEO, 'filepath': 'E:/destruna/assets/lona/rig/weights/hoodieKnot_geo_skinCluster_001.json'},
            {'geo': self.JACKET_SKINCOPY_GEO, 'filepath': 'E:/destruna/assets/lona/rig/weights/jacket_skinCopy_geo_skinCluster_001.json'},
        ])

        self.skin_copy = [
            {'source': self.BODY_SKINCOPY_GEO, 'target': self.LONABODY_NOJUMPER_GEO},
            {'source': self.JACKET_SKINCOPY_GEO, 'target': self.HOODIEKNOT_GEO},
            {'source': self.JACKET_SKINCOPY_GEO, 'target': self.HOODIE_GEO},

            # {'source': self.BODY_SKINCOPY_GEO, 'target': self.HOODIE_GEO},
            # {'source': self.BODY_SKINCOPY_GEO, 'target': self.HOODIEKNOT_GEO},
        ]

    def build_jumper_controls(self):

        jumper_grp = pm.group(em=True, n='Jumper', p='Master_CTL_Main')
        jumper_ctl_grp = pm.group(em=True, n='Jumper_controls', p=jumper_grp)
        jumper_jnt_grp = pm.group(em=True, n='Jumper_joints', p=jumper_grp)

        pm.connectAttr('Master_CTL_Main.jointVis', jumper_jnt_grp.v)

        # Jumper_CTL_torsoA
        jumper_ctl_torsoA = control.create(
            'Jumper_CTL_mainA',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoA_zero = jumper_ctl_torsoA.getParent()
        jumper_ctl_torsoA_zero.t.set([0.38, 91.31, -6.81])
        jumper_ctl_torsoA_zero.r.set([17.41, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoA.cv[:], [14.59, 10.43, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoA_zero, jumper_ctl_grp)

        # Jumper_CTL_torsoB
        jumper_ctl_torsoB = control.create(
            'Jumper_CTL_mainB',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoB_zero = jumper_ctl_torsoB.getParent()
        jumper_ctl_torsoB_zero.t.set([0.38, 81.36, -9.54])
        jumper_ctl_torsoB_zero.r.set([12.34, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoB.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoB_zero, jumper_ctl_torsoA)

        # Jumper_CTL_torsoC
        jumper_ctl_torsoC = control.create(
            'Jumper_CTL_mainC',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoC_zero = jumper_ctl_torsoC.getParent()
        jumper_ctl_torsoC_zero.t.set([0.38, 74.88, -10.75])
        jumper_ctl_torsoC_zero.r.set([10.6, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoC.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoC_zero, jumper_ctl_torsoB)

        # Jumper_CTL_torsoD
        jumper_ctl_torsoD = control.create(
            'Jumper_CTL_mainD',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoD_zero = jumper_ctl_torsoD.getParent()
        jumper_ctl_torsoD_zero.t.set([0.38, 68.17, -11.68])
        jumper_ctl_torsoD_zero.r.set([11.39, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoD.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoD_zero, jumper_ctl_torsoC)

        # Jumper_CTL_torsoE
        jumper_ctl_torsoE = control.create(
            'Jumper_CTL_mainE',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_torsoE_zero = jumper_ctl_torsoE.getParent()
        jumper_ctl_torsoE_zero.t.set([0.38, 61.4, -12.94])
        jumper_ctl_torsoE_zero.r.set([10.09, 0.0, 0.0])
        pm.scale(jumper_ctl_torsoE.cv[:], [14.59, 3.85, 8.25], r=True, os=True)
        pm.parent(jumper_ctl_torsoE_zero, jumper_ctl_torsoD)

        # Jumper_CTL_knot
        jumper_ctl_knot = control.create(
            'Jumper_CTL_knot',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )
        jumper_ctl_knot_zero = jumper_ctl_knot.getParent()
        jumper_ctl_knot_zero.t.set([-1.02, 82.77, 12.47])
        pm.scale(jumper_ctl_knot.cv[:], [10.34, 10.34, 8.1], r=True, os=True)
        pm.parent(jumper_ctl_knot_zero, jumper_ctl_grp)
        pm.parentConstraint('Spine_RootPart2_joint_M', jumper_ctl_knot_zero, mo=True)

        # Jumper_CTL_tie_L
        jumper_ctl_tie_L = control.create(
            'Jumper_CTL_tie_L',
            shape='box',
            colorIdx=18,
            zeroGroup=True
        )
        jumper_ctl_tie_L_zero = jumper_ctl_tie_L.getParent()
        jumper_ctl_tie_L_zero.t.set([3.73, 77.83, 13.0])
        jumper_ctl_tie_L_zero.r.set([-5.16, 0.0, 33.22])
        pm.scale(jumper_ctl_tie_L.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tie_L_zero, jumper_ctl_knot)

        # Jumper_CTL_tieA_R
        jumper_ctl_tieA_R = control.create(
            'Jumper_CTL_tieA_R',
            shape='box',
            colorIdx=20,
            zeroGroup=True
        )
        jumper_ctl_tieA_R_zero = jumper_ctl_tieA_R.getParent()
        jumper_ctl_tieA_R.t.set([-4.02, 77.91, 11.93])
        jumper_ctl_tieA_R.r.set([-5.16, 0.0, -33.22])
        pm.scale(jumper_ctl_tieA_R.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tieA_R_zero, jumper_ctl_knot)

        # Jumper_CTL_tieB_R
        jumper_ctl_tieB_R = control.create(
            'Jumper_CTL_tieB_R',
            shape='box',
            colorIdx=20,
            zeroGroup=True
        )
        jumper_ctl_tieB_R_zero = jumper_ctl_tieB_R.getParent()
        jumper_ctl_tieB_R.t.set([-6.39, 74.28, 12.01])
        jumper_ctl_tieB_R.r.set([-5.16, 0.0, -33.22])
        pm.scale(jumper_ctl_tieB_R.cv, [7.32, 2.68, 7.32], r=True, os=True)
        pm.parent(jumper_ctl_tieB_R_zero, jumper_ctl_tieA_R)

        # joints

        # knot
        pm.select(jumper_jnt_grp, r=True)
        jumper_knot_jnt = pm.joint(n='Jumper_knot_joint')

        pm.parentConstraint(jumper_ctl_knot, jumper_knot_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_knot, jumper_knot_jnt, mo=False)

        pm.select(jumper_knot_jnt, r=True)
        jumper_tie_L_jnt = pm.joint(n='Jumper_tie_joint_L')
        pm.parentConstraint(jumper_ctl_tie_L, jumper_tie_L_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tie_L, jumper_tie_L_jnt, mo=False)

        pm.select(jumper_knot_jnt, r=True)
        jumper_tieA_R_jnt = pm.joint(n='Jumper_tieA_joint_R')
        jumper_tieB_R_jnt = pm.joint(n='Jumper_tieB_joint_R')

        pm.parentConstraint(jumper_ctl_tieA_R, jumper_tieA_R_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tieA_R, jumper_tieA_R_jnt, mo=False)

        pm.parentConstraint(jumper_ctl_tieB_R, jumper_tieB_R_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_tieB_R, jumper_tieB_R_jnt, mo=False)

        # torso
        pm.select(jumper_jnt_grp, r=True)
        jumper_torso_A_jnt = pm.joint(n='Jumper_mainA_joint')
        jumper_torso_B_jnt = pm.joint(n='Jumper_mainB_joint')
        jumper_torso_C_jnt = pm.joint(n='Jumper_mainC_joint')
        jumper_torso_D_jnt = pm.joint(n='Jumper_mainD_joint')
        jumper_torso_E_jnt = pm.joint(n='Jumper_mainE_joint')

        pm.parentConstraint('Spine_RootPart2_joint_M', jumper_ctl_torsoA_zero, mo=True)

        pm.parentConstraint(jumper_ctl_torsoA, jumper_torso_A_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoA, jumper_torso_A_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoB, jumper_torso_B_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoB, jumper_torso_B_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoC, jumper_torso_C_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoC, jumper_torso_C_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoD, jumper_torso_D_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoD, jumper_torso_D_jnt, mo=False)
        pm.parentConstraint(jumper_ctl_torsoE, jumper_torso_E_jnt, mo=False)
        pm.scaleConstraint(jumper_ctl_torsoE, jumper_torso_E_jnt, mo=False)

        ctl_list = [
            jumper_ctl_knot,
            jumper_ctl_tie_L,
            jumper_ctl_tieA_R,
            jumper_ctl_tieB_R,

            jumper_ctl_torsoA,
            jumper_ctl_torsoB,
            jumper_ctl_torsoC,
            jumper_ctl_torsoD,
            jumper_ctl_torsoE,
        ]

        for ctl in ctl_list:
            ctl.v.set(cb=False, k=False, l=True)

    def finalize(self):

        for sc in pm.ls(type='skinCluster'):
            sc.deformUserNormals.set(0)



    def build_contents(self):
        build.imports(self.imports)
        pm.rename('Group', self.RIG_GRP)
        build.groups(self.groups)
        pm.parent(self.GEO_LIST, self.GEO_GRP)
        self.modify_as_rig()
        self.build_jumper_controls()
        # self.build_hair_controls()
        self.build_boot_controls()
        self.build_torso_controls()
        self.setup_facerig()
        #self.temp_shader_setup()
        build.skin_bind(self.skin_bind)
        self.do_skin_to_joint()
        build.skin_copy(self.skin_copy)
        build.proxy_setup(self.proxy_setup)
        pm.delete(self.DELETE_GRP)
        self.finalize()


"""
from jb_destruna.rigs.a_lona import lona_teen_jumperOff_animRig
reload(lona_teen_jumperOff_animRig)
rig = lona_teen_jumperOff_animRig.Rig()
rig.go()
"""