from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.internal import control
from jb_destruna.maya.internal import deformer
from jb_destruna.maya.internal import shaders
from jb_destruna.maya.internal import wrap
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn
from jb_destruna.maya.rig_modules import fk_chain as fk

import maya.cmds as mc
import pymel.core as pm
reload(build)
reload(rig)
reload(shaders)

from jb_destruna.tools import hair_rig

reload(hair_rig)

class Rig(rig.Rig):

    ASSET = 'lona_teen'

    FACE_NS = 'face'

    FACE_GEO = 'face_geo'
    UPPEREYELASH_GEO_L = 'upperEyelash_geo_l'
    UPPERBOTTOMEYELASH_GEO_L = 'upperBottomEyelash_geo_l'
    MIDBOTTOMEYELASH_GEO_L = 'midBottomEyelash_geo_l'
    UNDERBOTTOMEYELASH_GEO_L = 'underBottomEyelash_geo_l'
    UPPEREYELASH_GEO_R = 'upperEyelash_geo_r'
    UPPERBOTTOMEYELASH_GEO_R = 'upperBottomEyelash_geo_r'
    MIDBOTTOMEYELASH_GEO_R = 'midBottomEyelash_geo_r'
    UNDERBOTTOMEYELASH_GEO_R = 'underBottomEyelash_geo_r'
    EYEBROW_GEO_L = 'eyebrow_geo_l'
    EYEBROW_GEO_R = 'eyebrow_geo_r'
    TOPTEETH_GEO = 'topTeeth_geo'
    BOTTOMTEETH_GEO = 'bottomTeeth_geo'
    TONGUE_GEO = 'tongue_geo'
    SCLERA_GEO_L = 'sclera_geo_l'
    SCLERA_GEO_R = 'sclera_geo_r'
    PUPIL_GEO_L = 'pupil_geo_l'
    PUPIL_GEO_R = 'pupil_geo_r'
    HAIR_GEO = 'hair_geo'
    BODY_GEO = 'body_geo'

    GEO_LIST = [
        FACE_GEO,
        UPPEREYELASH_GEO_L,
        UPPERBOTTOMEYELASH_GEO_L,
        MIDBOTTOMEYELASH_GEO_L,
        UNDERBOTTOMEYELASH_GEO_L,
        UPPEREYELASH_GEO_R,
        UPPERBOTTOMEYELASH_GEO_R,
        MIDBOTTOMEYELASH_GEO_R,
        UNDERBOTTOMEYELASH_GEO_R,
        EYEBROW_GEO_L,
        EYEBROW_GEO_R,
        TOPTEETH_GEO,
        BOTTOMTEETH_GEO,
        TONGUE_GEO,
        SCLERA_GEO_L,
        SCLERA_GEO_R,
        PUPIL_GEO_L,
        PUPIL_GEO_R,
        HAIR_GEO,
        BODY_GEO,
    ]

    FACE_GEO_LIST = [
        BOTTOMTEETH_GEO,
        EYEBROW_GEO_L,
        EYEBROW_GEO_R,
        FACE_GEO,
        MIDBOTTOMEYELASH_GEO_L,
        MIDBOTTOMEYELASH_GEO_R,
        PUPIL_GEO_L,
        PUPIL_GEO_R,
        SCLERA_GEO_L,
        SCLERA_GEO_R,
        TONGUE_GEO,
        TOPTEETH_GEO,
        UNDERBOTTOMEYELASH_GEO_L,
        UNDERBOTTOMEYELASH_GEO_R,
        UPPERBOTTOMEYELASH_GEO_L,
        UPPERBOTTOMEYELASH_GEO_R,
        UPPEREYELASH_GEO_L,
        UPPEREYELASH_GEO_R,
    ]

    BODY_SKINCOPY_GEO = 'body_skinCopy_geo'

    def __init__(self):
        super(Rig, self).__init__()

        self.who = self.ASSET

        self.imports = [
            # GEO
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/lona/3D/lona_teen/02_rig/01_imports/lona_teen_geo_v005.mb', 'group': self.DELETE_GRP},

            # # GEO (WRAP HELPERS)
            # {'filepath': 'E:/destruna/assets/lona/face/imports/wrap_helper_geo_v001.mb', 'group': self.DNT_GRP},

            # AS RIG
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/lona/3D/lona_teen/02_rig/01_imports/AS_rig/asRig_v007.mb'},

            # AS FACE RIG
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/lona/3D/lona_teen/02_rig/01_imports/AS_faceRig/lona_faceRig_v010.mb', 'namespace': self.FACE_NS},

            # Hair ctl guides
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/lona/3D/lona_teen/02_rig/01_imports/guides/hair_guides_v005.mb', 'group': self.DELETE_GRP},

            # String ctl guides
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/lona/3D/lona_teen/02_rig/01_imports/guides/string_guides/string_guides_01.mb', 'group': self.DELETE_GRP},

            # skincopy geo
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/lona/3D/lona_teen/02_rig/01_imports/low_geo/body_skinCopy_geo_v003.mb', 'group': self.DELETE_GRP},

        ]

        self.groups = [
            {'g': self.TOP_GRP, 'p': None, 'v': True},
            {'g': self.RIG_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.GEO_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.DNT_GRP, 'p': self.TOP_GRP, 'v': False},
        ]

        skin_weight_prefix = 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/lona/3D/lona_teen/02_rig/01_imports/skinCluster/'

        self.skin_bind = [

            {'geo': self.BODY_SKINCOPY_GEO, 'filepath': skin_weight_prefix + self.BODY_SKINCOPY_GEO + '/body_skinCopy_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},


            {'geo': self.BODY_GEO, 'filepath': skin_weight_prefix + self.BODY_GEO + '/body_geo_skinCluster_03.json',
             'import_module': 'skin_ng'},

            # {'geo': self.BOTTOMTEETH_GEO,           'filepath': 'E:/destruna/assets/lona/rig/weights/bottomTeeth_geo_skinCluster_001.json'},
            # {'geo': self.EYEBROW_GEO_L,             'filepath': 'E:/destruna/assets/lona/rig/weights/eyebrow_geo_l_skinCluster_001.json'},
            # {'geo': self.EYEBROW_GEO_R,             'filepath': 'E:/destruna/assets/lona/rig/weights/eyebrow_geo_r_skinCluster_001.json'},
            # {'geo': self.FACE_GEO,                  'filepath': 'E:/destruna/assets/lona/rig/weights/face_geo_skinCluster_001.json'},

            {'geo': self.HAIR_GEO, 'filepath': skin_weight_prefix + self.HAIR_GEO + '/hair_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},

            # {'geo': self.MIDBOTTOMEYELASH_GEO_L,    'filepath': 'E:/destruna/assets/lona/rig/weights/midBottomEyelash_geo_l_skinCluster_001.json'},
            # {'geo': self.MIDBOTTOMEYELASH_GEO_R,    'filepath': 'E:/destruna/assets/lona/rig/weights/midBottomEyelash_geo_r_skinCluster_001.json'},
            # {'geo': self.PUPIL_GEO_L,               'filepath': 'E:/destruna/assets/lona/rig/weights/pupil_geo_l_skinCluster_001.json'},
            # {'geo': self.PUPIL_GEO_R,               'filepath': 'E:/destruna/assets/lona/rig/weights/pupil_geo_r_skinCluster_001.json'},
            # {'geo': self.SCLERA_GEO_L,              'filepath': 'E:/destruna/assets/lona/rig/weights/sclera_geo_l_skinCluster_001.json'},
            # {'geo': self.SCLERA_GEO_R,              'filepath': 'E:/destruna/assets/lona/rig/weights/sclera_geo_r_skinCluster_001.json'},
            # {'geo': self.TONGUE_GEO,                'filepath': 'E:/destruna/assets/lona/rig/weights/tongue_geo_skinCluster_001.json'},
            # {'geo': self.TOPTEETH_GEO,              'filepath': 'E:/destruna/assets/lona/rig/weights/topTeeth_geo_skinCluster_001.json'},
            # {'geo': self.UNDERBOTTOMEYELASH_GEO_L,  'filepath': 'E:/destruna/assets/lona/rig/weights/underBottomEyelash_geo_l_skinCluster_001.json'},
            # {'geo': self.UNDERBOTTOMEYELASH_GEO_R,  'filepath': 'E:/destruna/assets/lona/rig/weights/underBottomEyelash_geo_r_skinCluster_001.json'},
            # {'geo': self.UPPERBOTTOMEYELASH_GEO_L,  'filepath': 'E:/destruna/assets/lona/rig/weights/upperBottomEyelash_geo_l_skinCluster_001.json'},
            # {'geo': self.UPPERBOTTOMEYELASH_GEO_R,  'filepath': 'E:/destruna/assets/lona/rig/weights/upperBottomEyelash_geo_r_skinCluster_001.json'},
            # {'geo': self.UPPEREYELASH_GEO_L,        'filepath': 'E:/destruna/assets/lona/rig/weights/upperEyelash_geo_l_skinCluster_001.json'},
            # {'geo': self.UPPEREYELASH_GEO_R,        'filepath': 'E:/destruna/assets/lona/rig/weights/upperEyelash_geo_r_skinCluster_001.json'},
        ]

        self.skin_to_joint = {
            'Head_Head_joint_M': [
                self.BOTTOMTEETH_GEO,
                self.EYEBROW_GEO_L,
                self.EYEBROW_GEO_R,
                self.FACE_GEO,
                # self.HAIR_GEO,
                self.MIDBOTTOMEYELASH_GEO_L,
                self.MIDBOTTOMEYELASH_GEO_R,
                self.PUPIL_GEO_L,
                self.PUPIL_GEO_R,
                self.SCLERA_GEO_L,
                self.SCLERA_GEO_R,
                self.TONGUE_GEO,
                self.TOPTEETH_GEO,
                self.UNDERBOTTOMEYELASH_GEO_L,
                self.UNDERBOTTOMEYELASH_GEO_R,
                self.UPPERBOTTOMEYELASH_GEO_L,
                self.UPPERBOTTOMEYELASH_GEO_R,
                self.UPPEREYELASH_GEO_L,
                self.UPPEREYELASH_GEO_R,
            ]
        }

        self.skin_copy = [
             {'source': self.BODY_SKINCOPY_GEO, 'target': self.BODY_GEO},
        ]

        self.proxy_setup = [
            {'tag': self.PROXY_TAG}
        ]

        self.hair_data = [
            {
                'name': name,
                'base_crv': name + '_crv',
                'up_crv': name + '_up',
                'ctl_size': 2,
                'ctl_count': 5,
                'joint_count': 7,
            } for name in [
                'Hair_cheek_L',
                'Hair_cheek_R',
                'Hair_fringeA_R',
                'Hair_fringeB_R',
                'Hair_fringeC_R',
                'Hair_fringeD_R',
                'Hair_fringe_L',
                'Hair_sideA_L',
                'Hair_sideA_R',
                'Hair_sideB_L',
                'Hair_sideB_R',
                'Hair_sideC_R',
                'Hair_tailA',
                'Hair_tailB',
                'Hair_tailC',
                'Hair_tailD',
                'Hair_tailE',
                'Hair_tailF',
                'Hair_tailG',
                'Hair_tailH',
            ]
        ]

    def modify_as_rig(self):

        # good stuff happens here.
        super(Rig, self).modify_as_rig()

        # localized stuff happens here --------------
        pn('Master_CTL_Main').bendVis.set(0)

        # lock vis ctls
        for ctl in self.AS_CTL_LIST_NEW:
            pn(ctl).v.set(k=False, cb=False, l=True)

        # attributes on master
        pm.setAttr('Master_CTL_Main.jointVis', 0)

        attribute.add('Master_CTL_Main', n='geoVis', type='bool', dv=1, cb=True, k=False)
        attribute.add('Master_CTL_Main', n='geoLock', type='bool', dv=1, cb=True, k=False)

        pm.connectAttr('Master_CTL_Main.geoLock', 'Geometry.overrideEnabled')
        pm.setAttr('Geometry.overrideDisplayType', 2)

        pm.connectAttr('Master_CTL_Main.geoVis', 'Geometry.v')

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            pm.move('Arm_CTL_FKScapula_'+side+'.cv[:]', [-5*side_mult, 0, 0], r=True, os=True)
            pm.scale('Arm_CTL_FKShoulder_'+side+'.cv[:]', [0.7, 0.7, 0.7], r=True, os=True)
            pm.scale('Arm_CTL_FKWrist_' + side + '.cv[:]', [0.8, 0.8, 0.8], r=True, os=True)

    def do_skin_to_joint(self):

        for joint, geo_list in self.skin_to_joint.items():
            for geo in geo_list:
                pm.skinCluster(joint, geo, n=geo+'_skinCluster', tsb=True)

    def build_hair_controls(self):

        hair_guides_data = {
            'fringe_L': {
                'guides': ['Hair_CTL_fringe_0_L_guide', 'Hair_CTL_fringe_1_L_guide', 'Hair_CTL_fringe_2_L_guide', 'Hair_CTL_fringe_3_L_guide'],
                'scales': [6, 5, 3, 3],
                'colorIdx': 18,
            },
            'sideA_L': {
                'guides': ['Hair_CTL_sideA_0_L_guide', 'Hair_CTL_sideA_1_L_guide', 'Hair_CTL_sideA_2_L_guide'],
                'scales': [5, 5, 4],
                'colorIdx': 18,
            },
            'cheek_L': {
                'guides': ['Hair_CTL_cheek_0_L_guide', 'Hair_CTL_cheek_1_L_guide', 'Hair_CTL_cheek_2_L_guide', 'Hair_CTL_cheek_3_L_guide'],
                'scales': [3, 2, 2, 2],
                'colorIdx': 18,
            },
            'tail': {
                'guides': ['Hair_CTL_tail_0_guide', 'Hair_CTL_tail_1_guide', 'Hair_CTL_tail_2_guide', 'Hair_CTL_tail_3_guide', 'Hair_CTL_tail_4_guide', 'Hair_CTL_tail_5_guide'],
                'scales': [8, 9, 10, 11, 10, 8],
                'colorIdx': 23,
            },
            'fringeA_R': {
                'guides': ['Hair_CTL_fringeA_0_R_guide', 'Hair_CTL_fringeA_1_R_guide', 'Hair_CTL_fringeA_2_R_guide', 'Hair_CTL_fringeA_3_R_guide'],
                'scales': [6, 5, 3, 3],
                'colorIdx': 20,
            },
            'fringeB_R': {
                'guides': ['Hair_CTL_fringeB_0_R_guide', 'Hair_CTL_fringeB_1_R_guide', 'Hair_CTL_fringeB_2_R_guide'],
                'scales': [6, 6, 4],
                'colorIdx': 20,
            },
            'sideA_R': {
                'guides': ['Hair_CTL_sideA_0_R_guide', 'Hair_CTL_sideA_1_R_guide', 'Hair_CTL_sideA_2_R_guide'],
                'scales': [4, 4, 3],
                'colorIdx': 20,
            },
            'sideB_R': {
                'guides': ['Hair_CTL_sideB_0_R_guide', 'Hair_CTL_sideB_1_R_guide', 'Hair_CTL_sideB_2_R_guide', 'Hair_CTL_sideB_3_R_guide'],
                'scales': [6, 7, 5, 3],
                'colorIdx': 20,
            },
            'sideC_R': {
                'guides': ['Hair_CTL_sideC_0_R_guide', 'Hair_CTL_sideC_1_R_guide', 'Hair_CTL_sideC_2_R_guide', 'Hair_CTL_sideC_3_R_guide'],
                'scales': [5, 4, 4, 2],
                'colorIdx': 20,
            },
            'cheek_R': {
                'guides': ['Hair_CTL_cheek_0_R_guide', 'Hair_CTL_cheek_1_R_guide', 'Hair_CTL_cheek_2_R_guide', 'Hair_CTL_cheek_3_R_guide'],
                'scales': [3, 3, 2, 2],
                'colorIdx': 20,
            }
        }

        # Heirarchy
        jumper_grp = pm.group(em=True, n='Hair', p='Master_CTL_Main')
        jumper_ctl_grp = pm.group(em=True, n='Hair_controls', p=jumper_grp)
        jumper_jnt_grp = pm.group(em=True, n='Hair_joints', p=jumper_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', jumper_jnt_grp.v)

        for label, guide_data in hair_guides_data.items():

            guides = guide_data.get('guides')
            scales = guide_data.get('scales')
            color_idx = guide_data.get('colorIdx')

            ctl_list = list()
            jnt_list = list()
            for i, guide in enumerate(guides):

                # control
                ctl = control.create(
                    guide.replace('_guide', ''),
                    shape='square',
                    colorIdx=color_idx,
                    zeroGroup=True,
                )
                ctl_list.append(ctl)
                pm.delete(pm.parentConstraint(guide, ctl.getParent(), mo=False))
                pm.scale(ctl.cv[:], [scales[i], 1, scales[i]], r=True, os=True)
                ctl.v.set(k=False, cb=False, l=True)

                # joint
                pm.select(cl=True)
                jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_jnt')
                jnt_list.append(jnt)
                pm.parentConstraint(ctl, jnt, mo=False)
                pm.scaleConstraint(ctl, jnt, mo=False)

                # parenting
                if i == 0:
                    pm.parent(ctl.getParent(), jumper_ctl_grp)
                    pm.parentConstraint('Head_Head_joint_M', ctl.getParent(), mo=True)
                    pm.parent(jnt, jumper_jnt_grp)
                else:
                    pm.parent(ctl.getParent(), ctl_list[i-1])
                    pm.parent(jnt, jnt_list[i-1])

                jnt.jo.set([0, 0, 0])

    def build_hair_controls_1(self):

        # instance hair_class + build
        hair_class = hair_rig.HairRig()
        hair_class.build_data = self.hair_data
        hair_class.build()

        # install into AS rig.
        pm.parent(hair_class.TOP_GRP, 'Master_CTL_Main')
        pm.parentConstraint('Head_Head_joint_M', hair_class.PREFIX+'_headConstrainedGroup', mo=True)
        pm.scaleConstraint('Head_Head_joint_M', hair_class.PREFIX + '_headConstrainedGroup', mo=True)

        pm.connectAttr('Master_CTL_Main.jointVis', 'Hair_joints.v')

        pm.connectAttr('Master_CTL_Main.sy', 'Hair_connectors.rigScale')


    def build_boot_controls(self):

        boots_grp = pm.group(em=True, n='Boots', p='Master_CTL_Main')
        boots_ctl_grp = pm.group(em=True, n='Boots_controls', p=boots_grp)
        boots_jnt_grp = pm.group(em=True, n='Boots_joints', p=boots_grp)

        pm.connectAttr('Master_CTL_Main.jointVis', boots_jnt_grp.v)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1
            color = 20 if side == 'R' else 18
            # bootA
            boot_ctl_a = control.create(
                'Boot_CTL_cuffA_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_a_zero = boot_ctl_a.getParent()
            boot_ctl_a_zero.t.set([6.9*side_mult, 14.18, -2.16])
            pm.scale(boot_ctl_a.cv[:], [9.26, 3.27, 9.81], r=True, os=True)
            pm.parent(boot_ctl_a_zero, boots_ctl_grp)

            # bootB
            boot_ctl_b = control.create(
                'Boot_CTL_cuffB_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_b_zero = boot_ctl_b.getParent()
            boot_ctl_b_zero.t.set([7.42*side_mult, 18.68, -1.98])
            boot_ctl_b_zero.r.set([5.88, 0.0, 0.0])
            pm.scale(boot_ctl_b.cv[:], [11.93, 3.27, 14.77], r=True, os=True)
            pm.parent(boot_ctl_b_zero, boot_ctl_a)

            # bootC
            boot_ctl_c = control.create(
                'Boot_CTL_cuffC_'+side,
                shape='box',
                colorIdx=color,
                zeroGroup=True,
            )
            boot_ctl_c_zero = boot_ctl_c.getParent()
            boot_ctl_c_zero.t.set([7.42*side_mult, 23.49, -1.33])
            boot_ctl_c_zero.r.set([11.05, 0.0, 0.0])
            pm.scale(boot_ctl_c.cv[:], [13.52, 3.27, 18.93], r=True, os=True)
            pm.parent(boot_ctl_c_zero, boot_ctl_b)

            pm.select(boots_jnt_grp, r=True)
            boot_a_jnt = pm.joint(n='Boot_cuffA_joint_'+side)
            boot_b_jnt = pm.joint(n='Boot_cuffB_joint_'+side)
            boot_c_jnt = pm.joint(n='Boot_cuffC_joint_'+side)

            pm.parentConstraint('KneePart2_'+side, boot_ctl_a_zero, mo=True)

            pm.parentConstraint(boot_ctl_a, boot_a_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_a, boot_a_jnt, mo=False)
            pm.parentConstraint(boot_ctl_b, boot_b_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_b, boot_b_jnt, mo=False)
            pm.parentConstraint(boot_ctl_c, boot_c_jnt, mo=False)
            pm.scaleConstraint(boot_ctl_c, boot_c_jnt, mo=False)

            for ctl in [
                boot_ctl_a,
                boot_ctl_b,
                boot_ctl_c,
            ]:
                ctl.v.set(cb=False, k=False, l=True)

            mc.setAttr("Boots_joints.inheritsTransform", 0)

    def build_torso_controls(self):

        # breast ctl
        torso_grp = pm.group(em=True, n='Breasts', p='Master_CTL_Main')
        torso_ctl_grp = pm.group(em=True, n='Breasts_controls', p=torso_grp)
        torso_jnt_grp = pm.group(em=True, n='Breasts_joints', p=torso_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', torso_jnt_grp.v)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1
            color = 20 if side == 'R' else 18

            # bootA
            breast_ctl_main = control.create(
                'Torso_CTL_breast_'+side,
                shape='ball',
                colorIdx=color,
                zeroGroup=True,
            )
            breast_ctl_main_zero = breast_ctl_main.getParent()
            breast_ctl_main_zero.t.set([4.6*side_mult, 111.364, 6.846])
            breast_ctl_main_zero.r.set([-16.39, 21.86*side_mult, 0.0])
            pm.scale(breast_ctl_main.cv[:], [6.2, 6.2, 6.2], r=True, os=True)
            pm.parent(breast_ctl_main_zero, torso_ctl_grp)
            pm.parentConstraint('Spine_Chest_joint_M', breast_ctl_main_zero, mo=True)
            breast_ctl_main.v.set(cb=False, k=False, l=True)

            # jnt
            pm.select(torso_jnt_grp, r=True)
            breast_jnt = pm.joint(n='Torso_breast_joint_'+side)
            pm.parentConstraint(breast_ctl_main, breast_jnt, mo=False)
            pm.scaleConstraint(breast_ctl_main, breast_jnt, mo=False)

    def build_String_controls(self):

        module_name = 'String'

        #String_main_01_L_guide

        string_chain_list = [
            {
            'chain_name': 'mainL',
            'guides': ['String_main_0{}_L_guide'.format(i+1) for i in range(4)],
            'ctl_shape': 'box',
            'ctl_col': 9,
            'ctl_scale': 1,
            'ctl_scale_range':[1.8, 1],
            },
            {
            'chain_name': 'mainR',
            'guides': ['String_main_0{}_R_guide'.format(i + 1) for i in range(4)],
            'ctl_shape': 'box',
            'ctl_col': 13,
            'ctl_scale': 1,
            'ctl_scale_range':[1.8, 1],
            },
        ]

        collar_chain = fk.FkChain(module_name=module_name, chains=string_chain_list)
        collar_chain.build()

        mc.parent(module_name, self.MASTER_CONTROL)

        # Post clean ups

        mc.setAttr("String_joint_grp.inheritsTransform", 0)

        for jnt in mc.listRelatives('String_joint_grp', c=1):
            mc.connectAttr(self.MASTER_CONTROL + '.jointVis', '{}.visibility'.format(jnt))

        for side in ['L', 'R']:

            ctl_top_grp = 'String_main{}_CTLS_constrained'.format(side)
            mc.parentConstraint('Spine_Chest_joint_M', ctl_top_grp, mo=1)
            mc.scaleConstraint('Spine_Chest_joint_M', ctl_top_grp, mo=1)



    def setup_facerig(self):

        ns = self.FACE_NS+':'

        #face_rig_geo = ns + 'face_rig_geo'


        # parent face rig.
        pm.parent(ns+'faceRig', 'Master_CTL_Main')
        pn(ns+'faceRig').inheritsTransform.set(False)

        pm.parent(ns+'Head_constrained_grp', 'Master_CTL_Main')

        # constrain controls to head.
        pm.parentConstraint('Head_Head_joint_M', ns+'Head_constrained_grp', mo=True)
        pm.parentConstraint('Head_Head_joint_M', ns+'Head_constrained_grp', mo=True)

        for geo in self.FACE_GEO_LIST:
            blendshape.apply(ns+geo, geo, dv=1)

        # hide face geo
        pm.hide(ns+'Geo')

        for side in 'LR':
            for atr in ['rx', 'ry', 'rz']:
                pm.connectAttr(pn('Head_Eye_joint_'+side).attr(atr), pn('face:Eye_driver_'+side+'_GRP').attr(atr))

        hide_list = [
            ns+'Tweaks_controls',
            ns + 'Tweaks_joints',
            ns+'Main_joints',
            ns+'Region_joints',
        ]

        for h in hide_list:
            pn(h).v.set(0)

    def post_fix(self):

        mc.connectAttr('Master_CTL_Main.scale', 'hair_geo_deltaMush.scale', f=1)

    def temp_shader_setup(self):

        clear_shd, clear_sg = shaders.create_blinn(name='SHD_clear_mat', transparency=[1, 1, 1])
        black_shd, black_sg = shaders.create_surface(name='SHD_black_mat', outColor=[0, 0, 0])
        #purple_shd, purple_sg = shaders.create_surface(name='SHD_purple_mat', outColor=[0.451, 0.220, 0.723])
        white_shd, white_sg = shaders.create_surface(name='SHD_white_mat', outColor=[1, 1, 1])

        shd_dict = {
            black_sg: ['eyebrow_geo_l', 'eyebrow_geo_r', 'midBottomEyelash_geo_l', 'midBottomEyelash_geo_r', 'underBottomEyelash_geo_l', 'underBottomEyelash_geo_r', 'upperBottomEyelash_geo_l', 'upperBottomEyelash_geo_r', 'upperEyelash_geo_l', 'upperEyelash_geo_r', 'pupil_geo_lShape.f[:]', 'pupil_geo_rShape.f[:]'],
            clear_sg: ['sclera_geo_l', 'sclera_geo_r'],
            #purple_sg: ['pupil_geo_lShape.f[600:857]', 'pupil_geo_rShape.f[600:857]'],
            white_sg: ['face_geoShape.f[1640:1643]', 'face_geoShape.f[1825:1828]', 'face_geoShape.f[1968:1971]', 'face_geoShape.f[2141:2156]', 'face_geoShape.f[2161:2339]', 'face_geoShape.f[220:223]', 'face_geoShape.f[2439:2514]', 'face_geoShape.f[3355:3358]', 'face_geoShape.f[393:408]', 'face_geoShape.f[413:591]', 'face_geoShape.f[691:766]', 'face_geoShape.f[74:77]'],
        }

        for sg, geo_list in shd_dict.items():

            pm.sets(sg, fe=geo_list)

    # ==================================================================================================================
    def go(self):
        print '='*80
        print 'Starting '+str(self.who)+' Rig Build.'
        mc.file(new=True, f=True)
        self.build_contents()
        print 'Finished!'

    def build_contents(self):
        build.imports(self.imports)
        pm.rename('Group', self.RIG_GRP)
        build.groups(self.groups)
        pm.parent(self.GEO_LIST, self.GEO_GRP)
        self.modify_as_rig()
        self.build_String_controls()

        self.build_hair_controls_1()
        self.build_boot_controls()
        self.build_torso_controls()
        self.setup_facerig()
        #self.temp_shader_setup()
        build.skin_bind(self.skin_bind)

        self.do_skin_to_joint()
        #build.skin_copy(self.skin_copy)

        deformer.create_deltamush(geo='hair_geo')

        build.proxy_setup(self.proxy_setup)

        self.post_fix()

        pm.delete(self.DELETE_GRP)

"""
from jb_destruna.rigs.a_lona import lona_teen_animRig
reload(lona_teen_animRig)
rig = lona_teen_animRig.Rig()
rig.go()
"""