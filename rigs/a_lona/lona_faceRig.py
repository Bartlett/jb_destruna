from jb_destruna.maya.core import build
from jb_destruna.maya.core import face
from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.internal import control
from jb_destruna.maya.internal import cpom
from jb_destruna.maya.internal import curve
from jb_destruna.maya.internal import deformer
from jb_destruna.maya.internal import follicle
from jb_destruna.maya.internal import mesh
from jb_destruna.maya.internal import skin
from jb_destruna.maya.internal import wrap
from jb_destruna.maya.core import tweak
from jb_destruna.maya.utility import misc
from jb_destruna.maya.utility.misc import pn
import maya.cmds as mc
import pymel.core as pm

for imported_module in [
    tweak,
    mesh,
    blendshape,
    follicle,
    build,
    wrap,
    deformer,
]:
    reload(imported_module)


class FaceRig(face.FaceRig):

    ASSET = 'lona'
    FACE_RIG_GEO = 'face_rig_geo'
    FACE_REGION_GEO = 'face_region_geo'
    FACE_REGION_DRIVER = 'face_region_geo_driver'
    FACE_REGION_TRIDRIVER = 'face_region_geo_tridriver'
    FACE_TWEAKS_GEO = 'face_tweaks_geo'
    FACE_TWEAKS_DRIVER = 'face_tweaks_geo_driver'

    BOTTOMTEETH_GEO = 'bottomTeeth_geo'
    EYEBROW_GEO_L = 'eyebrow_geo_l'
    EYEBROW_GEO_R = 'eyebrow_geo_r'
    FACE_GEO = 'face_geo'
    MIDBOTTOMEYELASH_GEO_L = 'midBottomEyelash_geo_l'
    MIDBOTTOMEYELASH_GEO_R = 'midBottomEyelash_geo_r'
    PUPIL_GEO_L = 'pupil_geo_l'
    PUPIL_GEO_R = 'pupil_geo_r'
    SCLERA_GEO_L = 'sclera_geo_l'
    SCLERA_GEO_R = 'sclera_geo_r'
    TONGUE_GEO = 'tongue_geo'
    TOPTEETH_GEO = 'topTeeth_geo'
    UNDERBOTTOMEYELASH_GEO_L = 'underBottomEyelash_geo_l'
    UNDERBOTTOMEYELASH_GEO_R = 'underBottomEyelash_geo_r'
    UPPERBOTTOMEYELASH_GEO_L = 'upperBottomEyelash_geo_l'
    UPPERBOTTOMEYELASH_GEO_R = 'upperBottomEyelash_geo_r'
    UPPEREYELASH_GEO_L = 'upperEyelash_geo_l'
    UPPEREYELASH_GEO_R = 'upperEyelash_geo_r'

    FACE_GEO_LIST = [
        BOTTOMTEETH_GEO,
        EYEBROW_GEO_L,
        EYEBROW_GEO_R,
        FACE_GEO,
        MIDBOTTOMEYELASH_GEO_L,
        MIDBOTTOMEYELASH_GEO_R,
        PUPIL_GEO_L,
        PUPIL_GEO_R,
        SCLERA_GEO_L,
        SCLERA_GEO_R,
        TONGUE_GEO,
        TOPTEETH_GEO,
        UNDERBOTTOMEYELASH_GEO_L,
        UNDERBOTTOMEYELASH_GEO_R,
        UPPERBOTTOMEYELASH_GEO_L,
        UPPERBOTTOMEYELASH_GEO_R,
        UPPEREYELASH_GEO_L,
        UPPEREYELASH_GEO_R,
    ]

    HEAD_CONSTRAINED_GRP = 'Head_constrained_grp'

    FACE_BS_GEO = 'face_bs_geo'

    TWEAKS = 'Tweaks'
    TWEAKS_CTL_GRP = TWEAKS+'_controls'
    TWEAKS_JNT_GRP = TWEAKS+'_joints'

    FACE_RIG_GEO_BASE = 'face_rig_geo_base'
    FACE_GEO_WRAPPOSE = 'face_geo_wrapPose'

    def __init__(self):
        super(FaceRig, self).__init__()

        self.who = self.ASSET

        self.imports = [
            # GEO (ASSET)
            {'filepath': 'E:/destruna/assets/lona/geo/base/lona_teen_geo_v005.mb', 'group': self.DELETE_GRP},

            # # GEO (FACE)
            # {'filepath': 'E:/destruna/assets/lona/geo/face_rig_geo_v002.mb', 'group': self.DELETE_GRP},

            # # GEO (WRAP HELPERS)
            # {'filepath': 'E:/destruna/assets/lona/face/imports/wrap_helper_geo_v001.mb', 'group': self.DO_NOT_TOUCH_GRP},

            # SCENE (REGION CONTROL CURVES)
            {'filepath': 'E:/destruna/assets/lona/face/imports/region_controls_v003.mb', 'group': self.DO_NOT_TOUCH_GRP},

            # GEO (REGION TRI DRIVER)
            {'filepath': 'E:/destruna/assets/lona/face/imports/region_tridriver_v001.mb', 'group': self.DO_NOT_TOUCH_GRP},

            # SCENE (CURRENT BS RIG)
            {'filepath': 'E:/destruna/assets/lona/face/imports/face_asbs_v006.mb', 'group': self.DELETE_GRP},

            # SCENE (CTL LABELS)
            {'filepath': 'E:/destruna/assets/lona/face/imports/ctl_box_labels_v001.mb', 'group': self.DELETE_GRP},

            # SCENE (CTL LABELS)
            {'filepath': 'E:/destruna/assets/lona/face/imports/ctl_crvs_v002.mb', 'group': self.DELETE_GRP},

            # GEO (BROW AND LASH WRAPPERS)
            {'filepath': 'E:/destruna/assets/lona/face/imports/brow_and_lashes_wrapper_v005.mb', 'group': self.DO_NOT_TOUCH_GRP},

            # # GEO (EYE LASH CURVES)
            # {'filepath': 'E:/destruna/assets/lona/face/imports/lash_curves_v001.mb', 'group': self.DO_NOT_TOUCH_GRP},

            # GEO BLINK SHAPES
            {'filepath': 'E:/destruna/assets/lona/face/imports/blink_shapes_v002.mb', 'group': self.DELETE_GRP},
        ]

        self.groups.extend([
            {'g': self.BROW_STUFF, 'p': self.DO_NOT_TOUCH_GRP, 'v': True},
            {'g': self.EYE_STUFF, 'p': self.DO_NOT_TOUCH_GRP, 'v': True},
            {'g': self.LASH_STUFF, 'p': self.DO_NOT_TOUCH_GRP, 'v': True},
        ])

        self.tweak_controls_setup = [
            {
                'name': 'Tweaks',
                'parent': self.RIG_GRP,
                'tweak_data': [
                    # mouth ===================
                    # upper
                    {'prefix': 'Tweak', 'description': 'mouthUpperA', 'side': 'M', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[825]'},
                    {'prefix': 'Tweak', 'description': 'mouthUpperB', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1235]'},
                    {'prefix': 'Tweak', 'description': 'mouthUpperC', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1383]'},
                    {'prefix': 'Tweak', 'description': 'mouthUpperB', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[2189]'},
                    {'prefix': 'Tweak', 'description': 'mouthUpperC', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[2335]'},
                    #
                    # # corners
                    {'prefix': 'Tweak', 'description': 'mouthCorner', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[512]'},
                    {'prefix': 'Tweak', 'description': 'mouthCorner', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[2413]'},
                    #
                    # # lower
                    {'prefix': 'Tweak', 'description': 'mouthLowerA', 'side': 'M', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[834]'},
                    {'prefix': 'Tweak', 'description': 'mouthLowerB', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1257]'},
                    {'prefix': 'Tweak', 'description': 'mouthLowerC', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1260]'},
                    {'prefix': 'Tweak', 'description': 'mouthLowerB', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[2207]'},
                    {'prefix': 'Tweak', 'description': 'mouthLowerC', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[2211]'},

                    # # LHS
                    {'prefix': 'Tweak', 'description': 'eyeInnerCorner', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1073]'},
                    {'prefix': 'Tweak', 'description': 'eyeOuterCorner', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1071]'},

                    {'prefix': 'Tweak', 'description': 'eyeUpperA', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[4302]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperB', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8469]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperC', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1084]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperD', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8504]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperE', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8496]'},
                    #
                    {'prefix': 'Tweak', 'description': 'eyeLowerA', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[4303]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerB', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1079]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerC', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[4309]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerD', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8452]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerE', 'side': 'L', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8473]'},
                    #
                    # # RHS
                    {'prefix': 'Tweak', 'description': 'eyeInnerCorner', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1091]'},
                    {'prefix': 'Tweak', 'description': 'eyeOuterCorner', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1089]'},
                    # #
                    {'prefix': 'Tweak', 'description': 'eyeUpperA', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[4321]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperB', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8546]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperC', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1103]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperD', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8579]'},
                    {'prefix': 'Tweak', 'description': 'eyeUpperE', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8571]'},
                    # #
                    {'prefix': 'Tweak', 'description': 'eyeLowerA', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[4322]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerB', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[1098]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerC', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.vtx[4328]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerD', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8527]'},
                    {'prefix': 'Tweak', 'description': 'eyeLowerE', 'side': 'R', 'orient': False, 'vtx': 'face_tweaks_geo_driver.e[8550]'},
                ]
            }
        ]

        self.region_fol_dict = {
            self.REGION_CTL_BROW_L: 17,
            self.REGION_CTL_BROW_R: 7,
            self.REGION_CTL_CHEEK_L: 15,
            self.REGION_CTL_CHEEK_R: 2,
            self.REGION_CTL_CHIN: 0,
            self.REGION_CTL_EYE_L: 16,
            self.REGION_CTL_EYE_R: 4,
            self.REGION_CTL_MOUTH: 1,
            self.REGION_CTL_NOSE: 3,
        }

        self.skin_to_joint = {
            'Main_upperTeeth_joint': [
                self.TOPTEETH_GEO,
            ],
            'Main_lowerTeeth_joint': [
                self.BOTTOMTEETH_GEO,
            ],
            'eye_jnt_L': [
                self.PUPIL_GEO_L,
                self.SCLERA_GEO_L,
            ],
            'eye_jnt_R': [
                self.PUPIL_GEO_R,
                self.SCLERA_GEO_R
            ],
        }

        self.skin_bind = [
            # {'geo': self.BOTTOMTEETH_GEO, 'filepath': 'E:/destruna/assets/lona/face/imports/bottomTeeth_geo_skinCluster_v001.wts'},
            # {'geo': self.PUPIL_GEO_L, 'filepath': 'E:/destruna/assets/lona/face/imports/pupil_geo_l_skinCluster_v001.wts'},
            # {'geo': self.PUPIL_GEO_R, 'filepath': 'E:/destruna/assets/lona/face/imports/pupil_geo_r_skinCluster_v001.wts'},
            {'geo': self.TONGUE_GEO, 'filepath': 'E:/destruna/assets/lona/face/imports/tongue_geo_skinCluster_v001.wts'},
            # {'geo': self.TOPTEETH_GEO, 'filepath': 'E:/destruna/assets/lona/face/imports/topTeeth_geo_skinCluster_v001.wts'},
        ]

        self.wrapDeformer_create = [
            # {
            #     'wrapped': self.FACE_GEO,
            #     'wrapper': self.FACE_RIG_GEO,
            #     'base': self.FACE_RIG_GEO_BASE,
            #     'wrapPose': self.FACE_GEO_WRAPPOSE,
            #     'asBlendshape': True,
            #     'blendshape_weights': 'E:/destruna/assets/lona/face/imports/face_geo_wrappedBlendShape_v001.wts'
            # }
        ]
        self.wrapDeformer_create.extend({
            'wrapped': g,
            'wrapper': 'brow_and_lashes_wrapper',
            'base': 'brow_and_lashes_wrapperBase',
            'baseParent': self.DO_NOT_TOUCH_GRP
        } for g in [
            self.EYEBROW_GEO_L,
            self.EYEBROW_GEO_R,
            self.UNDERBOTTOMEYELASH_GEO_L,
            self.UNDERBOTTOMEYELASH_GEO_R,
            self.MIDBOTTOMEYELASH_GEO_L,
            self.MIDBOTTOMEYELASH_GEO_R,
            self.UPPERBOTTOMEYELASH_GEO_L,
            self.UPPERBOTTOMEYELASH_GEO_R,
        ])

    def setup_blendshape_rig(self):

        # restructure a bit
        pm.parent('Main', self.RIG_GRP)
        ctl_grp = pm.rename('FaceMotionSystem', 'Main_controls')
        jnt_grp = pm.rename('FaceDeformationSystem', 'Main_joints')
        dnt_grp = pm.group(em=True, n='Main_doNotTouch', p='Main')
        pm.hide(dnt_grp)

        # deformer renames
        # pm.rename('face_rig_geo_smooth', self.FACE_BS_GEO+'_deltaSmooth')
        # pm.rename('face_rig_geo_deltaMush', self.FACE_BS_GEO+'_deltaMush')
        # pm.rename('asFaceBS', self.FACE_BS_GEO+'_blendShape')

        # deleting guides
        pm.delete('FaceFitSkeleton')

        pm.parent(self.FACE_BS_GEO, dnt_grp)

    def do_skin_to_joint(self):

        for joint, geo_list in self.skin_to_joint.items():
            for geo in geo_list:
                pm.skinCluster(joint, geo, n=geo+'_skinCluster', tsb=True)


    def setup_head_constrained_groups(self):

        head_constrained_grp = pm.group(em=True, n=self.HEAD_CONSTRAINED_GRP)
        head_constrained_grp.t.set([0.0, 133.057, 0.433])

        # jaw_constrained_grp = pm.group(em=True, n='Jaw_constrained_grp')
        # jaw_constrained_grp.t.set([-0.0, 132.663, 2.124])
        # jaw_constrained_grp.r.set([-90.0, -51.321, -90.0])

        pm.parent('ctrlBoxOffset', self.HEAD_CONSTRAINED_GRP)

    def setup_mouth_controls(self):

        # setup floating controls.

        # unlock joints grp
        joints_grp = pn('Main_joints')
        joints_grp.v.set(l=False)
        joints_grp.v.set(True)

        # delete unused stuff.
        unused = ['LidCurves_L', 'LidCurves_R', 'LidSetup', 'LipSetup']
        pm.delete(unused)

        # UPPER TEETH ==================================================================================================
        teeth_upper_ctl = pn('upperTeeth_M')
        teeth_upper_ctl.rename('Main_CTL_upperTeeth')

        # use mouth crv shapes to make the mouth shapes better.
        pm.parent('upper_teeth_crv', teeth_upper_ctl)
        pm.makeIdentity('upper_teeth_crv', t=True, r=True, s=True, apply=True)
        pm.parent('upper_teeth_crv', self.DELETE_GRP)
        pm.connectAttr(pn('upper_teeth_crv').local, teeth_upper_ctl.create, f=True)
        teeth_upper_ctl.create.disconnect()

        # override color (red)
        teeth_upper_ctl.getShape().overrideEnabled.set(True)
        teeth_upper_ctl.getShape().overrideColor.set(13)

        # setup head constrained bypass
        pm.parent('upperTeethOffset_M', self.HEAD_CONSTRAINED_GRP)
        pm.delete(['upperTeethJoint_M_parentConstraint1', 'upperTeethJoint_M_scaleConstraint1'])

        teeth_upper_jnt = pn('upperTeethJoint_M')
        teeth_upper_jnt.rename('Main_upperTeeth_joint')
        teeth_upper_jnt.radius.set(1)

        upper_teeth_zero = pm.group(em=True, n=teeth_upper_jnt+'_ZERO', p='Main_joints')
        pm.delete(pm.parentConstraint(teeth_upper_ctl, upper_teeth_zero, mo=False))
        pm.parent(teeth_upper_jnt, upper_teeth_zero)

        for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
            pm.connectAttr(teeth_upper_ctl.attr(atr), teeth_upper_jnt.attr(atr), f=True)

        # ==============================================================================================================
        # JAW SDK GRP
        jaw_driven_grp = pm.group(em=True, n='jaw_driven_grp', p='Main_joints')
        pm.parentConstraint('JawFollow_M', jaw_driven_grp, mo=False)

        # LOWER TEETH ==================================================================================================
        teeth_lower_ctl = pn('lowerTeeth_M')
        teeth_lower_ctl.rename('Main_CTL_lowerTeeth')

        # use mouth crv shapes to make the mouth shapes better.
        pm.parent('lower_teeth_crv', teeth_lower_ctl)
        pm.makeIdentity('lower_teeth_crv', t=True, r=True, s=True, apply=True)
        pm.parent('lower_teeth_crv', self.DELETE_GRP)
        pm.connectAttr(pn('lower_teeth_crv').local, teeth_lower_ctl.create, f=True)
        teeth_lower_ctl.create.disconnect()

        # override color (red)
        teeth_lower_ctl.getShape().overrideEnabled.set(True)
        teeth_lower_ctl.getShape().overrideColor.set(13)

        # setup head constrained bypass
        pm.parent('lowerTeethOffset_M', self.HEAD_CONSTRAINED_GRP)
        pm.delete(['lowerTeethJoint_M_parentConstraint1', 'lowerTeethJoint_M_scaleConstraint1'])

        teeth_lower_jnt = pn('lowerTeethJoint_M')
        teeth_lower_jnt.rename('Main_lowerTeeth_joint')
        teeth_lower_jnt.radius.set(1)

        teeth_lower_zero = pm.group(em=True, n=teeth_lower_jnt+'_ZERO', p=jaw_driven_grp)
        pm.delete(pm.parentConstraint(teeth_lower_ctl, teeth_lower_zero, mo=False))
        pm.parent(teeth_lower_jnt, teeth_lower_zero)

        for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
            pm.connectAttr(teeth_lower_ctl.attr(atr), teeth_lower_jnt.attr(atr), f=True)

        # TONGUE =======================================================================================================
        pm.parent('Tongue0Offset_M', self.HEAD_CONSTRAINED_GRP)
        pm.delete('Tongue0Joint_M')

        tongue_ctl_0 = pn('Tongue0_M')
        tongue_ctl_1 = pn('Tongue1_M')
        tongue_ctl_2 = pn('Tongue2_M')
        tongue_ctl_3 = pn('Tongue3_M')

        tongue_ctl_0.rename('Main_CTL_tongueA')
        tongue_ctl_1.rename('Main_CTL_tongueB')
        tongue_ctl_2.rename('Main_CTL_tongueC')
        tongue_ctl_3.rename('Main_CTL_tongueD')

        joint_list = list()
        for i, ctl in enumerate([tongue_ctl_0, tongue_ctl_1, tongue_ctl_2, tongue_ctl_3]):
            pm.select(cl=True)

            jnt = pm.joint(n=str(ctl).replace('_CTL_', '_')+'_joint')
            joint_list.append(jnt)
            jnt_zero = misc.zero_grp(jnt)

            pm.delete(pm.parentConstraint(ctl.getParent(), jnt_zero, mo=False))

            for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                pm.connectAttr(ctl.attr(atr), jnt.attr(atr), f=True)

            if i == 0:
                pm.parent(jnt_zero, jaw_driven_grp)
            else:

                pm.parent(jnt_zero, joint_list[i-1])

    def do_parenting(self):
        #pm.parent(self.FACE_RIG_GEO, self.DO_NOT_TOUCH_GRP)

        # pm.parent(self.FACE_REGION_GEO, self.DNT)
        # pm.parent(self.FACE_TWEAKS_GEO, self.DNT)
        # pm.parent(self.FACE_TWEAKS_DRIVER, self.DNT)
        pass

    def setup_region_controls(self):

        # groups
        top_grp = pm.group(em=True, n='Region', p=self.RIG_GRP)
        ctl_grp = pm.group(em=True, n='Region_controls', p=top_grp)
        jnt_grp = pm.group(em=True, n='Region_joints', p=top_grp)
        dnt_grp = pm.group(em=True, n='Region_doNotTouch', p=top_grp)
        constrain_to_head = pm.group(em=True, n='Region_constrainToHead')
        fol_grp = pm.group(em=True, n='Region_follicle_group', p=dnt_grp)
        pm.hide(dnt_grp)
        pm.parent(constrain_to_head, self.HEAD_CONSTRAINED_GRP)

        pm.delete(self.FACE_REGION_DRIVER)
        wrap.create(self.FACE_REGION_TRIDRIVER, self.FACE_BS_GEO, base=self.FACE_BS_GEO+'Base', baseParent=self.DO_NOT_TOUCH_GRP)
        smooth = mesh.smooth(self.FACE_REGION_TRIDRIVER, divisions=1)

        pm.parent(self.FACE_REGION_TRIDRIVER, dnt_grp)

        # base jnt
        base_jnt_zero = pm.group(em=True, n='Region_base_jnt_ZERO', p=jnt_grp)
        jnt = pm.joint(n='Region_base_jnt')

        # create control follicles
        ctl_fol_map = dict()
        for ctl, vtx_idx in self.region_fol_dict.items():

            geo = self.FACE_REGION_TRIDRIVER
            ctl = pn(ctl)

            ctl.getShape().overrideEnabled.set(True)
            ctl.getShape().overrideColor.set(29)

            # create follicle
            fol = follicle.onSelected(selection=geo+'.vtx['+str(vtx_idx)+']', n=ctl+'_fol')
            fol.getShape().v.set(False)
            pm.parent(fol, fol_grp)
            fol.r.disconnect()
            fol.r.set([0, 0, 0])

            fol_offset = pm.spaceLocator(n=fol+'_OFFSET')
            pm.parent(fol_offset, fol)
            fol_offset.getShape().v.set(False)

            # setup ctl stuff
            ctl_zero = misc.zero_grp(ctl)
            pm.makeIdentity(ctl, n=0, s=1, r=1, t=1, apply=True, pn=1)
            pm.parent(ctl_zero, constrain_to_head)
            pm.move(ctl.cv[:], [0, 0, 0.3], r=True, os=True)

            pm.delete(pm.parentConstraint(ctl, fol_offset, mo=False))

            pm.connectAttr(fol_offset.getShape().worldPosition[0].worldPositionX, ctl_zero.tx, f=True)
            pm.connectAttr(fol_offset.getShape().worldPosition[0].worldPositionY, ctl_zero.ty, f=True)
            pm.connectAttr(fol_offset.getShape().worldPosition[0].worldPositionZ, ctl_zero.tz, f=True)

            #ctl.v.set(k=False, cb=False, l=True)

            # setup joint stuff
            jnt_zero = pm.group(em=True, n=ctl+'_jnt_ZERO')
            jnt = pm.joint(n=ctl+'_jnt')

            # pm.parentConstraint(ctl_zero, jnt_zero, mo=False)
            # pm.scaleConstraint(ctl_zero, jnt_zero, mo=False)
            #
            # pm.parentConstraint(ctl, jnt, mo=False)
            # pm.scaleConstraint(ctl, jnt, mo=False)

            pm.parent(jnt_zero, jnt_grp)

            for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                pm.connectAttr(ctl_zero + '.' + atr, jnt_zero + '.' + atr)
                pm.connectAttr(ctl + '.' + atr, jnt + '.' + atr)

            ctl_fol_map[str(ctl)] = str(fol)

        # load skinweights for region controls.
        skin.import_from_file(geo=self.FACE_REGION_GEO, filepath='E:/destruna/assets/lona/face/imports/face_region_geo_skinCluster_v004.wts')
        skin.connect_prebind_matrixes('face_region_geo_skinCluster')

        # deforming on face controls
        stuck_grp = pm.group(em=True, n='Region_stuck_on_controls', p=ctl_grp)
        pm.hide(stuck_grp)

        for ctl in self.region_fol_dict.keys():
            ctl = pn(ctl)

            # fol base position
            fol = pn(ctl_fol_map[str(ctl)])
            base_grp = pm.group(em=True, n=fol+'_base_pos', p=fol.getParent())
            pm.delete(pm.parentConstraint(fol, base_grp, mo=False))

            ctl_def = curve.copy(curve=ctl, n=ctl.replace('_CTL_', '_') + '_def')
            pm.parent(ctl_def, stuck_grp)
            wrap.create(ctl_def, self.FACE_REGION_GEO, base=self.FACE_REGION_GEO + 'Base', baseParent=dnt_grp)

            bs = blendshape.apply(ctl_def, ctl, dv=1)
            # bs.origin.set(0)

            clus, clus_handle = pm.cluster(ctl_def, n=ctl_def + '_cluster')
            pm.parent(clus_handle, stuck_grp)
            pm.hide(ctl_def.getShape())

            # subtract position of control from original position
            mult = pm.createNode('multiplyDivide', n=ctl + '_mult')
            mult.operation.set(2)
            pm.connectAttr(ctl.t, mult.input1, f=True)
            mult.input2.set(-1, -1, -1)

            # fol offset pma
            fol_offset_pma = pm.createNode('plusMinusAverage', n=ctl+'_pma1')
            pm.connectAttr(base_grp.t, fol_offset_pma.input3D[0], f=True)
            pm.connectAttr(fol.getShape().outTranslate, fol_offset_pma.input3D[1], f=True)
            fol_offset_pma.operation.set(2)

            # position of control to fol offset
            ctl_pos_add = pm.createNode('plusMinusAverage', n=ctl+'_addFinal')
            pm.connectAttr(mult.output, ctl_pos_add.input3D[0], f=True)
            pm.connectAttr(fol_offset_pma.output3D, ctl_pos_add.input3D[1], f=True)

            pm.connectAttr(ctl_pos_add.output3D, clus_handle.t, f=True)

        pm.parent(self.FACE_REGION_GEO, self.DO_NOT_TOUCH_GRP)

        # eye hack to connect region controls to eye stuff
        for side in 'LR':

            # create locator to get a ws output of the region control
            region_joint = 'Region_CTL_eye_'+side+'_jnt'
            region_loc = pm.spaceLocator(n=region_joint+'_loc')
            pm.hide(region_loc.getShape())
            pm.parent(region_loc, region_joint, r=True)

            # create group driven by locator.
            eye_region_driven = pm.group(em=True, n='Eye_regionDriven_'+side+'_GRP')
            pm.delete(pm.parentConstraint(region_loc, eye_region_driven, mo=False))

            for source_atr, target_atr in zip(['worldPosition[0].worldPositionX', 'worldPosition[0].worldPositionY', 'worldPosition[0].worldPositionZ'], ['tx', 'ty', 'tz']):
                pm.connectAttr(region_loc.getShape().attr(source_atr), eye_region_driven.attr(target_atr), f=True)

            # constrain to tweak.
            pm.parentConstraint(eye_region_driven, 'Eye_driver_'+side+'_GRP_ZERO', skipRotate=['x', 'y', 'z'], mo=True)

            pm.parent(eye_region_driven, self.EYE_STUFF)

    def create_tweaks_controls_setup(self, data):
        """
        This function creates groups of tweak controls on a mesh

        Args:
            data: [{
                'name': (str)    name of joint group
                'tweak_data': [
                    {
                        'prefix': 'Eye'
                        'description': 'tweakLowerA'
                        'side': 'L'
                        'vtx': 'face_tweak.vtx[1]'
                    },
                ]
            }]
        """
        tweaks_build_data = build.check_data_type(function_name='tweaks_controls_setup', data=data)

        # if there is data
        if tweaks_build_data:

            # iterate through list
            for tweak_group in tweaks_build_data:

                # joint
                tweak_data = tweak_group['tweak_data']
                name = tweak_group['name']

                # group creation
                top_grp = pm.group(em=True, n=name)
                control_group = pm.group(em=True, n=name + '_controls', p=top_grp)
                joint_group = pm.group(em=True, n=name + '_joints', p=top_grp)
                constrain_to_head = pm.group(em=True, n=name + '_constrainToHead')

                # iterating through tweak data to create ctls etc.
                for d in tweak_data:

                    prefix = d['prefix']
                    description = d['description']
                    side = d['side']
                    vtx = d['vtx']
                    orient = d['orient']

                    # follicle creation
                    fol_name = '_'.join([prefix, description, side, 'fol'])
                    fol = follicle.onSelected(selection=vtx, n=fol_name)
                    # if not orient:
                    fol.r.disconnect()
                    fol.r.set([0, 0, 0])
                    pm.hide(fol.getShape())

                    pm.parent(fol, control_group)

                    # create group to be driven by fol under head joint
                    ctl_parent_name = '_'.join([prefix, description, side, 'driven'])
                    ctl_parent = pm.group(em=True, n=ctl_parent_name, p=constrain_to_head)
                    pm.connectAttr(fol.t, ctl_parent.t)

                    # control creation
                    tweak_data = tweak.build_control_at_component(
                        component=vtx,
                        prefix=prefix,
                        description=description,
                        suffix=side,
                        shape='circle',
                        size=0.2,
                        rotation=[90, 0, 0],
                        orientToSurface=orient
                    )
                    ctl = pn(tweak_data['ctl'])
                    ctl.getShape().overrideEnabled.set(1)
                    ctl.getShape().overrideColor.set(31)

                    pm.xform(ctl.cv[:], t=[0, 0, .25], r=True, os=True)
                    pm.parent(tweak_data['ctl_zero'], ctl_parent)

                    pm.parent(tweak_data['jnt_zero'], fol)

                    pm.delete(tweak_data['jnt_zero'] + '_parentConstraint1')
                    pm.delete(tweak_data['jnt_zero'] + '_scaleConstraint1')
                    pm.delete(tweak_data['jnt'] + '_parentConstraint1')
                    pm.delete(tweak_data['jnt'] + '_scaleConstraint1')

                    for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                        pm.connectAttr(tweak_data['ctl_zero'] + '.' + atr, tweak_data['jnt_zero'] + '.' + atr)
                        pm.connectAttr(tweak_data['ctl'] + '.' + atr, tweak_data['jnt'] + '.' + atr)

    def setup_tweak_controls(self):

        # create controls and follicles.
        self.create_tweaks_controls_setup(self.tweak_controls_setup)

        # base jnt
        base_jnt_zero = pm.group(em=True, n='Tweaks_base_jnt_ZERO', p='Tweaks_joints')
        jnt = pm.joint(n='Tweaks_base_jnt')

        # parenting
        pm.parent('Tweaks', self.RIG_GRP)
        dnt_grp = pm.group(em=True, n='Tweaks_doNotTouch', p='Tweaks')
        pm.hide(dnt_grp)

        # skin bind
        build.skin_bind({'geo': self.FACE_TWEAKS_GEO, 'filepath': 'E:/destruna/assets/lona/face/imports/face_tweaks_geo_skinCluster_v004.wts'})
        skin.connect_prebind_matrixes(self.FACE_TWEAKS_GEO+'_skinCluster')
        pm.select(self.FACE_TWEAKS_GEO, r=True)
        pm.skinPercent(self.FACE_TWEAKS_GEO+'_skinCluster', normalize=True)

        # parent branched geo
        pm.parent(self.FACE_TWEAKS_GEO, dnt_grp)
        pm.parent(self.FACE_TWEAKS_DRIVER, dnt_grp)

        pm.parent('Tweaks_constrainToHead', self.HEAD_CONSTRAINED_GRP)

    def adjust_labels(self):

        # parent labels to ctl box.
        label_list = ['label_brow', 'label_cheek', 'label_eye', 'label_mouth', 'label_nose']
        pm.parent(label_list, 'ctrlBox')

        # brow ctls
        for ctl in ['ctrlBoxBrow_L', 'ctrlBoxBrow_R']:
            pn(ctl).ty.set(6.361)

        # eye ctls
        for ctl in ['ctrlBoxEye_L', 'ctrlBoxEye_R']:
            pn(ctl).ty.set(3.692)

        # cheek ctls
        for ctl in ['ctrlBoxCheek_L', 'ctrlBoxCheek_R']:
            pn(ctl).ty.set(0.988)

        # nose ctls
        for ctl in ['ctrlBoxNose_L', 'ctrlBoxNose_R']:
            pn(ctl).ty.set(-1.045)

        pn('ctrlBoxMouth_M').ty.set(-2.752)

        # mouth corner ctls
        for ctl in ['ctrlBoxMouthCorner_L', 'ctrlBoxMouthCorner_R']:
            pn(ctl).ty.set(-6.008)

        pn('ctrlBoxPhonemes_M').ty.set(-4.977)
        pn('ctrlBoxEmotions_M').ty.set(-7.637)

        # adjust box shape.
        for cv in ['ctrlBoxShape.cv[1]', 'ctrlBoxShape.cv[0]', 'ctrlBoxShape.cv[4]']:
            pm.move(cv, [0, 2.523, 0], r=True, os=True)

        for cv in ['ctrlBoxShape.cv[2]', 'ctrlBoxShape.cv[3]']:
            pm.move(cv, [0, -0.597392, 0], r=True, os=True)

        pm.xform('label_brow', t=[-25.133, 296.309, 0.0], os=True)
        pm.xform('label_eye', t=[-15.614, 189.546, 0.0], os=True)
        pm.xform('label_cheek', t=[-27.269, 81.395, 0.0], os=True)
        pm.xform('label_mouth', t=[-31.422, -80.973, 0.0], os=True)
        pm.xform('label_nose', t=[-22.911, 0.075, 0.0], os=True)


    def setup_eye_stuff(self):

        eye_ctl_grp = pm.group(em=True, n='Eye_controls')
        pm.parent(eye_ctl_grp, 'Head_constrained_grp')

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            # angle driver
            eye_driver = pm.group(em=True, n='Eye_driver_'+side+'_GRP')
            driver_pos = [4.988*side_mult, 138.692, 3.932] # this you match to body rig eye L joint pos
            driver_rot = [180.0, -90.0, 0]

            pm.xform(eye_driver, t=driver_pos, ws=True)
            pm.xform(eye_driver, ro=driver_rot, os=True)

            eye_driver_zero = misc.zero_grp(eye_driver)

            driver = pm.group(em=True, n=eye_driver+'_driver')
            pm.parent(driver, eye_driver)
            pm.xform(driver, t=[4.399*side_mult, 138.317, 7.565], ws=True)   # ws center of iris/pupil

            # # aim driven
            eye_driven = pm.group(em=True, n='Eye_driven_'+side+'_GRP')
            driven_pos = [3.305*side_mult, 138.692, 2.967] # implied point around which the anim iris rotates.
            pm.xform(eye_driven, t=driven_pos, ws=True)
            eye_driven_zero = misc.zero_grp(eye_driven)

            aim = pm.aimConstraint(
                driver, eye_driven,
                aimVector=[0, 0, 1],
                upVector=[0, 1, 0],
                worldUpVector=[0, 1, 0],
                worldUpType='scene',
            )

            # eye_driven
            eye_driven_loc = pm.spaceLocator(n='Eye_driven_'+side)
            pm.hide(eye_driven_loc.getShape())
            pm.parent(eye_driven_loc, driver, r=True)

            eye_ctl_driven = pm.group(em=True, n='Eye_control_driven_'+side)
            pm.parent(eye_ctl_driven, eye_ctl_grp)
            for source_atr, target_atr in zip(['worldPosition[0].worldPositionX', 'worldPosition[0].worldPositionY', 'worldPosition[0].worldPositionZ'], ['tx', 'ty', 'tz']):
                pm.connectAttr(eye_driven_loc.getShape().attr(source_atr), eye_ctl_driven.attr(target_atr))

            ctl = control.create(
                'Main_CTL_iris_'+side,
                shape='circle',
                colorIdx=17,
                rotation=[90, 0, 0],
                zeroGroup=True
            )

            pm.scale(ctl.getShape().cv[:], [3.3, 3.3, 1], r=True)

            pm.delete(pm.parentConstraint(driver, ctl.getParent(), mo=False))
            pm.parent(ctl.getParent(), eye_ctl_driven)
            pm.xform(ctl.getParent(), ro=[0, 20*side_mult, 0], r=True, os=True)

            # jnt
            pm.select(cl=True)
            jnt_offset = pm.group(em=True, n='eye_jnt_'+side+'_OFFSET')
            jnt_zero = pm.group(em=True, n='eye_jnt_'+side+'_ZERO')
            jnt = pm.joint(n='eye_jnt_'+side)
            pm.parent(jnt_offset, eye_driven, r=True)
            pm.parent(jnt_zero, jnt_offset, r=True)
            pm.delete(pm.parentConstraint(driver, jnt_offset, mo=False))
            pm.delete(pm.parentConstraint(driver, jnt_zero, mo=False))
            pm.parent(eye_driver_zero, self.EYE_STUFF)
            pm.parent(eye_driven_zero, 'Main_joints')

            for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                pm.connectAttr(ctl.getParent().attr(atr), jnt_zero.attr(atr))
                pm.connectAttr(ctl.attr(atr), jnt.attr(atr), f=True)

            # ctl.sx.set(1.5)
            # ctl.sy.set(3.3)

            # disconnect old sdks
            for sdk in [
                'asFaceBS_ctrlEyetranslateYNeg_'+side,
                'asFaceBS_ctrlEyetranslateXNeg_'+side,
                'asFaceBS_ctrlEyetranslateXPos_'+side,
                'asFaceBS_ctrlEyetranslateYPos_'+side,
            ]:
                pm.delete(sdk)

            # set new ones.
            face_bs = pn('face_bs_geo_blendShape')

            xpos_atr = 'ctrlEyetranslateXPos_'+side
            xneg_atr = 'ctrlEyetranslateXNeg_'+side
            ypos_atr = 'ctrlEyetranslateYPos_'+side
            yneg_atr = 'ctrlEyetranslateYNeg_'+side

            # xpos
            pm.setDrivenKeyframe(face_bs.attr(xpos_atr), v=0.0, cd=eye_driver.ry, dv=0.0)
            pm.setDrivenKeyframe(face_bs.attr(xpos_atr), v=1.0, cd=eye_driver.ry, dv=-60.0)

            # xneg
            pm.setDrivenKeyframe(face_bs.attr(xneg_atr), v=0.0, cd=eye_driver.ry, dv=0.0)
            pm.setDrivenKeyframe(face_bs.attr(xneg_atr), v=1.0, cd=eye_driver.ry, dv=60)

            # ypos
            pm.setDrivenKeyframe(face_bs.attr(ypos_atr), v=0.0, cd=eye_driver.rz, dv=0.0)
            pm.setDrivenKeyframe(face_bs.attr(ypos_atr), v=1.0, cd=eye_driver.rz, dv=-20.0)

            # yneg
            pm.setDrivenKeyframe(face_bs.attr(yneg_atr), v=0.0, cd=eye_driver.rz, dv=0.0)
            pm.setDrivenKeyframe(face_bs.attr(yneg_atr), v=1.0, cd=eye_driver.rz, dv=20.0)

            # if side == 'L':
            #     ry = [4, 39]
            # else:
            #     ry = [-39, -4]
            #
            # pm.transformLimits(eye_driven, erx=[1, 1], rx=[-7, 7])
            # pm.transformLimits(eye_driven, ery=[1, 1], ry=ry)

            pn('ctrlEye_'+side).tx.set(l=True)
            pn('ctrlEye_' + side).ty.set(l=True)

    def brow_ctls(self):

        ctl_data_list = [
            {'name': 'Main_CTL_browA_<s>', 'translate': [0.889, 143.398, 9.786]},
            {'name': 'Main_CTL_browB_<s>', 'translate': [2.098, 144.283, 9.528]},
            {'name': 'Main_CTL_browC_<s>', 'translate': [3.349, 144.938, 9.096]},
            {'name': 'Main_CTL_browD_<s>', 'translate': [4.486, 145.411, 8.57]},
            {'name': 'Main_CTL_browE_<s>', 'translate': [5.778, 145.783, 7.854]},
        ]

        # groups
        constrain_to_head = pm.group(em=True, n='Brows_constrainToHead')
        pm.parent(constrain_to_head, 'Head_constrained_grp')
        brow_joints = pm.group(em=True, n='Brow_joints_grp', p='Main_joints')

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            # create follicles
            fol_list = list()
            for x, data in enumerate(ctl_data_list):

                ctl_name = data['name'].replace('<s>', side)
                translate = [data['translate'][0] * side_mult, data['translate'][1], data['translate'][2]]

                uval, vval = cpom.closest_uv_on_geo_from_position(position=translate, geo='face_tweaks_geo')
                fol = follicle.create(geo='face_tweaks_geo', n=ctl_name+'_fol', setUV=True, uvVal=(uval, vval))
                fol_list.append(fol)
                fol.r.disconnect()
                fol.r.set(0, 0, 0)

            pm.parent(fol_list, brow_joints)

            # create control
            ctl_list = list()
            loc_list = list()
            for x, data in enumerate(ctl_data_list):

                ctl_name = data['name'].replace('<s>', side)
                translate = [data['translate'][0]*side_mult, data['translate'][1], data['translate'][2]]

                ctl = control.create(
                    ctl_name,
                    shape='circle',
                    colorIdx=17,
                    zeroGroup=True,
                    translation=[0, 5, 0],
                    rotation=[90, 0, 0],
                    size=0.1
                )

                ctl_zero = ctl.getParent()
                pm.xform(ctl_zero, t=translate, os=True)
                ctl_list.append(ctl)

                constrained_grp = pm.group(em=True, n=ctl_name+'_driven')
                fol = fol_list[x]

                pm.connectAttr(fol.t, constrained_grp.t)
                pm.parent(ctl_zero, constrained_grp)
                pm.parent(constrained_grp, constrain_to_head)

                loc = pm.spaceLocator(n=ctl_name.replace('_CTL_', '_')+'_loc')
                loc.getShape().v.set(False)
                loc_list.append(loc)
                pm.parent(loc, fol)
                for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                    pm.connectAttr(ctl.attr(atr), loc.attr(atr))

            print 'test'

            for x, loc in enumerate(loc_list):

                if x == 0:
                    aim_loc = loc_list[x+1]
                    aim_vector = [1 * side_mult, 0, 0]
                else:
                    aim_loc = loc_list[x-1]
                    aim_vector = [1 * side_mult, 0, 0]

                pm.select(loc, r=True)
                jnt = pm.joint(n=loc.replace('_loc', '_jnt'))
                print jnt
                pm.parent(jnt, loc)

                fol = fol_list[x]

                pm.aimConstraint(
                    aim_loc, jnt,
                    aimVector=aim_vector,
                    upVector=[0, 0, 1],
                    worldUpType='objectRotation',
                    worldUpVector=[0, 0, 1],
                    worldUpObject=fol
                )

    def lash_ctls(self):
        ctl_data_list = [
            {'name': 'Main_CTL_lowerLashA_<s>', 'translate': [6.444, 135.343, 7.369], 'rotate': [-21.539, 22.964, 9.477]},
            {'name': 'Main_CTL_lowerLashB_<s>', 'translate': [7.228, 135.655, 6.93], 'rotate': [1.142, 33.502, 30.135]},
            {'name': 'Main_CTL_lowerLashC_<s>', 'translate': [7.853, 136.106, 6.423], 'rotate': [15.782, 32.161, 51.459]},
        ]

        # groups
        constrain_to_head = pm.group(em=True, n='Lashes_constrainToHead')
        pm.parent(constrain_to_head, 'Head_constrained_grp')
        brow_joints = pm.group(em=True, n='Lashes_joints_grp', p='Main_joints')

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            # create follicles
            fol_list = list()
            for x, data in enumerate(ctl_data_list):

                ctl_name = data['name'].replace('<s>', side)
                translate = [data['translate'][0] * side_mult, data['translate'][1], data['translate'][2]]

                uval, vval = cpom.closest_uv_on_geo_from_position(position=translate, geo='face_tweaks_geo')
                fol = follicle.create(geo='face_tweaks_geo', n=ctl_name+'_fol', setUV=True, uvVal=(uval, vval))
                fol_list.append(fol)
                fol.r.disconnect()
                fol.r.set(0, 0, 0)
            pm.parent(fol_list, brow_joints)

            # create control
            ctl_list = list()
            loc_list = list()
            for x, data in enumerate(ctl_data_list):

                ctl_name = data['name'].replace('<s>', side)
                translate = [data['translate'][0]*side_mult, data['translate'][1], data['translate'][2]]
                rotate = [data['rotate'][0], data['rotate'][1] * side_mult, data['rotate'][2] * side_mult]

                ctl = control.create(
                    ctl_name,
                    shape='circle',
                    colorIdx=17,
                    zeroGroup=True,
                    translation=[0, 5, 0],
                    rotation=[90, 0, 0],
                    size=0.1
                )

                ctl_zero = ctl.getParent()
                pm.xform(ctl_zero, t=translate, os=True)
                pm.xform(ctl_zero, ro=rotate, os=True)
                ctl_list.append(ctl)

                constrained_grp = pm.group(em=True, n=ctl_name+'_driven')
                fol = fol_list[x]

                pm.connectAttr(fol.t, constrained_grp.t)
                pm.parent(ctl_zero, constrained_grp)
                pm.parent(constrained_grp, constrain_to_head)

                loc = pm.spaceLocator(n=ctl_name.replace('_CTL_', '_')+'_loc')
                loc_zero = misc.zero_grp(loc)
                pm.delete(pm.parentConstraint(ctl, loc_zero, mo=False))

                loc.getShape().v.set(False)
                loc_list.append(loc)
                pm.parent(loc_zero, fol)
                for atr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
                    pm.connectAttr(ctl.attr(atr), loc.attr(atr))

            for x, loc in enumerate(loc_list):

                if x == 0:
                    aim_loc = loc_list[x+1]
                    aim_vector = [1 * side_mult, 0, 0]
                else:
                    aim_loc = loc_list[x-1]
                    aim_vector = [1 * side_mult, 0, 0]

                pm.select(loc, r=True)
                jnt = pm.joint(n=loc.replace('_loc', '_jnt'))
                print jnt
                pm.parent(jnt, loc)

                fol = fol_list[x]

                # pm.aimConstraint(
                #     aim_loc, jnt,
                #     aimVector=aim_vector,
                #     upVector=[0, 0, 1],
                #     worldUpType='objectRotation',
                #     worldUpVector=[0, 0, 1],
                #     worldUpObject=fol
                # )

    def prepare_brow_and_lashes(self):

        # functions for control setup
        self.brow_ctls()
        self.lash_ctls()

        # skin weights
        skin.import_from_file(filepath='E:/destruna/assets/lona/face/imports/brow_and_lashes_wrapper_skinCluster_v001.wts', geo='brow_and_lashes_wrapper')
        mesh.smooth(geo='brow_and_lashes_wrapper', divisions=1)

        # create shrinkwrap
        shrinkwrap = deformer.create_shrinkwrap(wrapped='brow_and_lashes_wrapper', wrapper='face_geo')

    def upper_lash_attach(self):

        lash_curve_edges = {
            'L': ['face_geoShape.e[0:1]', 'face_geoShape.e[16:17]', 'face_geoShape.e[28:29]', 'face_geoShape.e[40:41]', 'face_geoShape.e[70:71]', 'face_geoShape.e[92:93]'],
            'R': ['face_geoShape.e[1926:1927]', 'face_geoShape.e[1940:1941]', 'face_geoShape.e[1952:1953]', 'face_geoShape.e[1964:1965]', 'face_geoShape.e[1996:1997]', 'face_geoShape.e[2014:2015]'],
        }

        for side in 'LR':

            # create wire curve
            wire_curve_edges = lash_curve_edges[side]
            pm.select(wire_curve_edges, r=True)
            wire_crv, polyEdgeToCurve = pm.polyToCurve(n='eye_lash_'+side+'_crv', form=2, degree=1, conformToSmoothMeshPreview=0)
            pm.delete(wire_crv, ch=True)
            wrap.create(wire_crv, 'face_geo', baseParent=self.LASH_STUFF)

            # pm.parent(wire_crv, self.LASH_STUFF)
            # eye_lash_wrapper = 'eye_lash_'+side+'_wrapper'

            face_geo = pn(self.FACE_GEO)
            geo = pn('upperEyelash_geo_l') if side == 'L' else pn('upperEyelash_geo_r')
            lash_wrapper = pn('lash_wrapper_'+side)
            # curve = pn('upper_lash_'+side+'_curve')

            wrapper_smooth = mesh.smooth(lash_wrapper, divisions=1, keepBorder=False)
            pm.delete(lash_wrapper, ch=True)

            # wire defore geo to curve
            wire = deformer.create_wire(geo=lash_wrapper, curve=wire_crv, rotation=1.0, dropoff_distance=20, scale=1)

            # wrap curve to face
            # pm.select(curve, face_geo, r=True)
            # pm.mel.CreateWrap()
            # wrap.create(wire_crv, face_geo)

            wrap.create(geo, lash_wrapper, baseParent=self.LASH_STUFF)
            # deltamush
            deformer.create_deltamush(geo=geo, smoothing_iterations=5, distance_weight=1.0)

            pm.parent('eye_lash_'+side+'_crv', 'eye_lash_'+side+'_crvBaseWire', 'Lash_stuff')

    def rename_geo(self):

        geo_list = list(set([g.getParent() for g in pm.ls('*_modified*', type='mesh')]))

        for g in geo_list:
            g.rename(str(g).replace('_modified', ''))

    def modify_blinks(self):

        from jb_destruna.maya.utility import attribute

        # requires.
        # - 2 x new blink targets. (flat and reversed)
        # - blink tweak ctl positions.

        for side in 'LR':

            # add attrs to eye control
            eye_ctl = 'ctrlEye_'+side
            attribute.add(eye_ctl, n='blinkCurve', type='float', dv=-0.4, min=-1, max=1, k=True, cb=True)
            attribute.add(eye_ctl, n='blinkOffsetY', type='float', dv=-0.5, min=-1, max=1, k=True, cb=True)

            # extract existing blink shape
            bs = 'face_bs_geo_blendShape'
            existing_blink_target = 'ctrlEyeblinkPos_'+side
            target_idx = blendshape.get_target_index(blendshape=bs, target=existing_blink_target)
            extracted_blink_target = pm.sculptTarget(bs, e=True, target=target_idx, regenerate=True)[0]

            # create new blink targets. (1- flat and 2- reversed curve)

            # apply as bs to extracted blink target.
            reversed_blink = extracted_blink_target+'_reversed'
            up_blink = extracted_blink_target+'_up'
            down_blink = extracted_blink_target+'_down'
            blink_bs = blendshape.apply(reversed_blink, extracted_blink_target, dv=1, alias='reverse')
            blink_bs = blendshape.apply(up_blink, extracted_blink_target, dv=1, alias='up')
            blink_bs = blendshape.apply(down_blink, extracted_blink_target, dv=1, alias='down')

            pm.setDrivenKeyframe(blink_bs.reverse, v=0, cd=eye_ctl+'.blinkCurve', dv=-1, itt='linear', ott='linear')
            pm.setDrivenKeyframe(blink_bs.reverse, v=1, cd=eye_ctl+'.blinkCurve', dv=1, itt='linear', ott='linear')

            pm.setDrivenKeyframe(blink_bs.up, v=0, cd=eye_ctl+'.blinkOffsetY', dv=0, itt='linear', ott='linear')
            pm.setDrivenKeyframe(blink_bs.up, v=1, cd=eye_ctl+'.blinkOffsetY', dv=1, itt='linear', ott='linear')

            pm.setDrivenKeyframe(blink_bs.down, v=0, cd=eye_ctl+'.blinkOffsetY', dv=0, itt='linear', ott='linear')
            pm.setDrivenKeyframe(blink_bs.down, v=1, cd=eye_ctl+'.blinkOffsetY', dv=-1, itt='linear', ott='linear')

            pm.hide(extracted_blink_target)


            # connect eye_ctl.blinkCurve to blink bs with sdk

    def generate_lid_blink_shapes(self):

        # generate lid shapes from guide curve.
        for side in 'LR':

            side_mult = -1 if side == 'R' else 1

            crv = pm.PyNode('curve1_' + side)

            # assemble pos list.
            pos_list = list()
            for cv in pm.ls(crv.cv[:], fl=True):
                pos_list.append(pm.pointPosition(cv))

            # get average height
            average_height = sum([p[1] for p in pos_list]) / len(pos_list)

            # use average height to create flattened lid curve
            flat_pos_list = [[p[0], average_height, p[2]] for p in pos_list]
            flat_crv = pm.duplicate(crv, n=crv + '_flat')[0]
            for cv, pos in zip(pm.ls(flat_crv.cv[:], fl=True), flat_pos_list):
                pm.xform(cv, t=pos)

            # use difference between average height and orig curve to create the reverse shape.
            reverse_pos_list = [[p[0], average_height - (p[1] - average_height), p[2]] for p in pos_list]
            reverse_crv = pm.duplicate(crv, n=crv + '_reverse')[0]
            for cv, pos in zip(pm.ls(reverse_crv.cv[:], fl=True), reverse_pos_list):
                pm.xform(cv, t=pos)

            # create height offset
            offset_y_crv = pm.duplicate(crv, n=crv + '_offsetY')[0]
            pm.xform(offset_y_crv, t=[0, 5, 0], r=True, os=True)

            # create side offset
            offset_x_crv = pm.duplicate(crv, n=crv + '_offsetX')[0]
            pm.xform(offset_x_crv, t=[-5 * side_mult, 0, 0], r=True, os=True)

    # ==================================================================================================================
    def go(self):
        print '='*80
        print 'Starting '+str(self.who)+' Rig Build.'
        mc.file(new=True, f=True)
        self.build_contents()
        print 'Finished!'

    def build_contents(self):

        build.imports(self.imports)

        self.rename_geo()
        build.groups(self.groups)
        self.setup_head_constrained_groups()
        pm.parent(self.FACE_GEO_LIST, self.GEO_GRP)
        # pm.parent(self.FACE_RIG_GEO, self.DO_NOT_TOUCH_GRP)
        self.adjust_labels()
        self.setup_blendshape_rig()
        self.setup_mouth_controls()
        self.setup_eye_stuff()

        # build region branch with controls
        mesh.branch_geo(self.FACE_BS_GEO, n=self.FACE_REGION_GEO)

        self.setup_region_controls()

        # build tweak branch with controls
        mesh.branch_geo(self.FACE_REGION_GEO, n=self.FACE_TWEAKS_GEO)
        self.setup_tweak_controls()

        # pm.parent(self.FACE_GEO, self.GEO)
        blendshape.apply(self.FACE_TWEAKS_GEO, self.FACE_GEO, dv=1)

        self.prepare_brow_and_lashes()
        self.modify_blinks()

        # GEO ATTACHMENTS (this stuff will be lona specific) ===========================================================
        # establish wrap to face rig.
        build.wrapDeformer_create(self.wrapDeformer_create)
        # pm.parent('face_geo_wrapped', self.DO_NOT_TOUCH_GRP)
        self.upper_lash_attach()
        self.do_skin_to_joint()
        build.skin_bind(self.skin_bind)

        self.do_cleanup()

def create_transform_in_new_pos(translate=[0.0, 12.956, 0.445], scale=[0.9, 0.9, 0.9]):

    for target in pm.selected():

        new_loc = pm.spaceLocator(n=target+'_new')
        pm.delete(pm.parentConstraint(target, new_loc, mo=False))
        pm.delete(pm.scaleConstraint(target, new_loc))

        tmp_parent = pm.group(em=True, n='tmp')
        pm.parent(new_loc, tmp_parent)

        tmp_parent.t.set(translate)
        tmp_parent.s.set(translate)

        pm.parent(new_loc, w=True)
        pm.delete(tmp_parent)


def detach_and_delete_bodyrig():

    # prep freshly skinCluster -> blendshape AS rig.
    pm.parent('FaceGroup', w=True)

    pm.delete([
        'FaceMotionSystem_orientConstraint1',
        'FaceMotionSystem_pointConstraint1',

        'Customcontrols',
        'Regionscontrols',
        'Aimcontrols',

        'SideReverse_L',
        'SideReverse_R',

        'EyeAimFollowHead_parentConstraint1',
        'FaceDeformationFollowHead_orientConstraint1',
        'FaceDeformationFollowHead_pointConstraint1',
        'LidWireWS_parentConstraint1',
        'LidWireWS_scaleConstraint1',
        'LipFollicles_parentConstraint1',
        'MainAndHeadScaleMultiplyDivide',
    ])

    pm.parent('FaceJoint_M', 'FaceDeformationSystem')
    pm.delete('Group')
    pm.rename('FaceGroup', 'Main')

    pm.rename('face_geo', 'face_bs_geo')
    pm.rename('asFaceBS', 'face_bs_geo_blendShape')

    pm.delete([
        'body_geo',
        'bottomTeeth_geo',
        'eyebrow_geo_l',
        'eyebrow_geo_r',
        'hair_geo',
        'midBottomEyelash_geo_l',
        'midBottomEyelash_geo_r',
        'pupil_geo_l',
        'pupil_geo_r',
        'sclera_geo_l',
        'sclera_geo_r',
        'tongue_geo',
        'topTeeth_geo',
        'underBottomEyelash_geo_l',
        'underBottomEyelash_geo_r',
        'upperBottomEyelash_geo_l',
        'upperBottomEyelash_geo_r',
        'upperEyelash_geo_l',
        'upperEyelash_geo_r',
    ])

"""
from jb_destruna.rigs.a_lona import lona_faceRig
reload(lona_faceRig)
face = lona_faceRig.FaceRig()
face.go()

"""