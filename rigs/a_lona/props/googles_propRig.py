from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import control
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn
from jb_destruna.maya.internal import skin
from jb_destruna.maya.internal import skin_ng



import maya.cmds as mc
import pymel.core as pm
reload(build)
reload(rig)

class Rig(rig.Rig):

    FRAME_GEO = 'frame_geo'
    BELT_GEO = 'belt_geo'

    BELT_SKIN_GEO = 'belt_skin_geo'

    GEO_LIST = [FRAME_GEO, BELT_GEO]

    def __init__(self):
        super(Rig, self).__init__()

        self.who = self.ASSET

        self.imports = [
            # GEO
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/props/lona_goggles/01_model/lona_teen_wGoggles_model_03_DMR_WIP.mb', 'group': self.DELETE_GRP},

            # PATH RIG
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/props/lona_goggles/02_rig/03_imports/Rig/path_rig_001.mb'},

            # CTL GUIDES
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/props/lona_goggles/02_rig/03_imports/guides/frame_guide_011.mb', 'group': self.DELETE_GRP},

            # SKINCOPY GEO
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/props/lona_goggles/02_rig/03_imports/skin_geo/belt_skin_geo_002.mb', 'group': self.DELETE_GRP},
        ]

        self.groups = [
            {'g': self.TOP_GRP, 'p': None, 'v': True},
            {'g': self.RIG_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.GEO_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.DNT_GRP, 'p': self.TOP_GRP, 'v': False},
        ]

        sk_path = 'D:/_Active_Projects/Destruna/masterAssets_forRef/props/lona_goggles/02_rig/03_imports/skinCluster'

        self.skin_weight_imports = {

            'belt_geo': sk_path + '/belt_SK/geo_skinGeo_001_skinCluster.json'
        }

    def post_edits(self):

        mc.setAttr('path_master_control.visibility', 0)
        mc.setAttr('level_2_controls_grp.visibility', 0)
        mc.setAttr('driven_grp.visibility', 0)
        pm.parent(self.GEO_LIST, self.GEO_GRP)
        pm.parent('path_rig', self.RIG_GRP)

        # attributes on master

        attribute.add('Master_CTL_Main', n='geoVis', type='bool', dv=1, cb=True, k=False)
        attribute.add('Master_CTL_Main', n='geoLock', type='bool', dv=1, cb=True, k=False)

        pm.connectAttr('Master_CTL_Main.geoLock', 'Geometry.overrideEnabled')
        pm.setAttr('Geometry.overrideDisplayType', 2)

        pm.connectAttr('Master_CTL_Main.geoVis', 'Geometry.v')

        for ctl in ['mainCurveCTL_6', 'mainCurveCTL_3', 'mainCurveCTL_2', 'mainCurveCTL_4', 'mainCurveCTL_5', 'mainCurveCTL_1', 'mainCurveCTL_0', 'Belt_CTL_main', 'Frame_CTL_main']:
            mc.setAttr(ctl + '.v', keyable=False, channelBox=False, lock=True)

        for ctl in ['Frame_CTL_main_rotateOffset', 'Master_CTL_Main_rotateOffset']:
            mc.setAttr(ctl + '.v', keyable=False, channelBox=False)

            for dim in ['X', 'Y', 'Z']:
                mc.setAttr(ctl + '.rotate{}'.format(dim), keyable=False, channelBox=False)
                mc.setAttr(ctl + '.scale{}'.format(dim), keyable=False, channelBox=False)

    def build_frame_control(self):

        pouch_grp = pm.group(em=True, n='Frame', p=self.RIG_GRP)
        pouch_ctl_grp = pm.group(em=True, n='Frame_controls', p=pouch_grp)

        frame_CTL = control.create(
            'Frame_CTL_main',
            shape='box',
            colorIdx=8,
            zeroGroup=True
        )

        frame_CTL_rp = control.create(
            'Frame_CTL_main_rotateOffset',
            shape='pivot',
            colorIdx=8,
            zeroGroup=True
        )

        frame_CTL_zero = frame_CTL.getParent()
        frame_CTL_zero.t.set([0, 0, 12.179])
        pm.scale(frame_CTL.cv[:], [24, 10, 5], r=True, os=True)

        frame_CTL_RP_zero = frame_CTL_rp.getParent()
        frame_CTL_RP_zero.t.set([0, 0, 12.179])
        pm.scale(frame_CTL_rp.cv[:], [3, 3, 3], r=True, os=True)
        pm.parent(frame_CTL_RP_zero, frame_CTL)

        pm.connectAttr(frame_CTL_rp + '.translate', frame_CTL + '.rotatePivot')
        attribute.add(frame_CTL, n='rotatePivot_vis', type='bool', dv=0, cb=True, k=False)
        pm.connectAttr(frame_CTL + '.rotatePivot_vis', frame_CTL_rp + '.visibility', f=1)

        frame_CTL_offset_grp = pm.group(n='Frame_CTL_main_offset', em=1, p=frame_CTL)

        # jnt
        pm.select(frame_CTL, r=True)
        pouch_jnt = pm.joint(n='Frame_CTL_main_jnt')
        pm.parentConstraint(frame_CTL_offset_grp, pouch_jnt, mo=False)
        pm.scaleConstraint(frame_CTL_offset_grp, pouch_jnt, mo=False)

        belt_ctl = control.create(
            'Belt_CTL_main',
            shape='box',
            colorIdx=23,
            zeroGroup=True
        )

        belt_ctl_leg_zero = belt_ctl.getParent()
        belt_ctl_leg_zero.t.set([-0.014, 0.976, -12.888])
        pm.scale(belt_ctl.cv[:], [20, 10, 5], r=True, os=True)

        master_ctl = control.create(
            'Master_CTL_Main',
            shape='box',
            colorIdx=17,
            zeroGroup=True
        )

        master_CTL_rp = control.create(
            'Master_CTL_Main_rotateOffset',
            shape='pivot',
            colorIdx=17,
            zeroGroup=True
        )

        master_CTL_offset_grp = pm.group(n='Master_CTL_main_offset', em=1, p=master_ctl)

        mc_zero = master_ctl.getParent()
        mc_zero.t.set([0, 0, 12.179])
        pm.scale(master_ctl.cv[:], [30, 15, 7], r=True, os=True)

        # MASTER RP
        master_CTL_rp_zero = master_CTL_rp.getParent()
        master_CTL_rp_zero.t.set([0, 0, 12.179])
        pm.scale(master_CTL_rp.cv[:], [5, 5, 5], r=True, os=True)
        pm.parent(master_CTL_rp_zero, master_ctl)
        pm.connectAttr(master_CTL_rp + '.translate', master_ctl + '.rotatePivot')
        attribute.add(master_ctl, n='rotatePivot_vis', type='bool', dv=0, cb=True, k=False)
        pm.connectAttr(master_ctl + '.rotatePivot_vis', master_CTL_rp + '.visibility', f=1)


        pm.parent(frame_CTL_zero, belt_ctl_leg_zero, mc_zero, pouch_ctl_grp)

        # CONSTRAIN
        pm.parentConstraint(master_CTL_offset_grp, frame_CTL_zero, mo=1)
        pm.parentConstraint(master_CTL_offset_grp, belt_ctl_leg_zero, mo=1)
        pm.parentConstraint(master_CTL_offset_grp, 'path_master_control', mo=1)


        for to_constrain in [self.FRAME_GEO, 'mainCurveCTL_6_ZERO', 'mainCurveCTL_0_ZERO']:
            pm.parentConstraint(frame_CTL_offset_grp, to_constrain, mo=1)

        for to_constrain in ['mainCurveCTL_2_ZERO', 'mainCurveCTL_3_ZERO', 'mainCurveCTL_4_ZERO']:
            pm.parentConstraint(belt_ctl, to_constrain, mo=1)

        pm.parentConstraint(frame_CTL_offset_grp, to_constrain, 'mainCurveCTL_5_ZERO', mo=1)
        pm.parentConstraint(frame_CTL_offset_grp, to_constrain, 'mainCurveCTL_1_ZERO', mo=1)


        # # jnt
        # pm.select(pouch_ctl_leg, r=True)
        # pouch_jnt = pm.joint(n='Pouch_joint')
        # pm.parentConstraint(pouch_ctl_leg, pouch_jnt, mo=False)
        # pm.scaleConstraint(pouch_ctl_leg, pouch_jnt, mo=False)

    def assign_skinClusters(self):

        skin_ng.import_from_file(filepath='D:/_Active_Projects/Destruna/masterAssets_forRef/props/lona_goggles/02_rig/03_imports/skinCluster/belt_SK/geo_skinGeo_002_skinCluster.json',
                                 geo=self.BELT_SKIN_GEO)

        skin.copy(self.BELT_SKIN_GEO, self.BELT_GEO)

    def go(self):
        print '='*80
        print 'Starting '+str(self.who)+' Rig Build.'
        mc.file(new=True, f=True)
        self.build_contents()
        print 'Finished!'

    def build_contents(self):

        build.imports(self.imports)
        build.groups(self.groups)
        self.build_frame_control()
        self.assign_skinClusters()
        self.post_edits()

        pm.delete(self.DELETE_GRP)

'''
from jb_destruna.rigs.a_lona.props import googles_propRig
reload(googles_propRig)

rig = googles_propRig.Rig()
rig.go()
'''

