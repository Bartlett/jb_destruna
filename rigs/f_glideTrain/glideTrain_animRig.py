from jb_destruna.maya.core import build
import maya.cmds as mc
import pymel.core as pm

from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import skin
from jb_destruna.maya.internal import control
from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.internal import wrap
from jb_destruna.maya.utility.misc import pn
from jb_destruna.maya.utility import attribute
import math
from jb_destruna.rigs.e_don.import_paths import don_import_paths as imp

reload(build)
reload(rig)


class Rig(rig.Rig):

    ASSET = 'glideTrain'
    VARIANT = 'casual'

    IMPORT_LIST = ['office', 'jordan', 'damien']
    IMPORT_KEY = IMPORT_LIST[2]

    # BODY
    BODY_GEO = 'body_geo'
    HAIR_GEO = 'hair_geo'

    CARRIAGE_PREFIXES = ['A', 'B', 'C', 'D', 'E']

    CARRIAGE_GUIDE_ORDER = [
        "carriage_E_end_guide",
        "carriage_E_mid_guide",
        "carriage_E_front_guide",
        "carriage_D_end_guide",
        "carriage_D_mid_guide",
        "carriage_D_front_guide",
        "carriage_C_end_guide",
        "carriage_C_mid_guide",
        "carriage_C_front_guide",
        "carriage_B_end_guide",
        "carriage_B_mid_guide",
        "carriage_B_front_guide",
        "carriage_A_end_guide",
        "carriage_A_mid_guide",
        "carriage_A_front_guide",
    ]

    CARRIAGE_CONNECTOR_GUIDE = [

        ['carriageConnector_A_front_geo', 'carriageConnector_A_back_geo'],
        ['carriageConnector_B_front_geo',  'carriageConnector_B_back_geo'],
        ['carriageConnector_C_front_geo',  'carriageConnector_C_back_geo'],
        ['carriageConnector_D_front_geo',  'carriageConnector_D_back_geo'],

    ]


    CARRIAGE_TO_JOINT = {
        'frame_A_geo': 'carriage_1_joint',
        'frame_B_geo': 'carriage_4_joint',
        'frame_C_geo': 'carriage_7_joint',
        'frame_D_geo': 'carriage_10_joint',
        'frame_E_geo': 'carriage_13_joint',
    }

    # GEO_TO_JOINT = {
    #     'carriage_A': 'carriage_1_joint',
    #     'carriage_B': 'carriage_4_joint',
    #     'carriage_C': 'carriage_7_joint',
    #     'carriage_D': 'carriage_10_joint',
    #     'carriage_E': 'carriage_13_joint',
    # }
    GEO_TO_JOINT = {
        'carriage_A': 'carriage_13_joint',
        'carriage_B': 'carriage_10_joint',
        'carriage_C': 'carriage_7_joint',
        'carriage_D': 'carriage_4_joint',
        'carriage_E': 'carriage_1_joint',
    }

    GEO = [
        'frame_E_geo',
        'carriageConnector_D_geo',
        'frame_D_geo',
        'carriageConnector_C_geo',
        'frame_C_geo',
        'carriageConnector_B_geo',
        'frame_B_geo',
        'carriageConnector_A_geo',
        'frame_A_geo',
    ]

    CONNECTOR = [
        'carriageConnector_A_geo',
        'carriageConnectorInterior_A_geo',
        'carriageConnector_B_geo',
        'carriageConnectorInterior_B_geo',
        'carriageConnector_C_geo',
        'carriageConnectorInterior_C_geo',
        'carriageConnector_D_geo',
        'carriageConnectorInterior_D_geo',
    ]

    def __init__(self):
        super(Rig, self).__init__()
        super(Rig, self).__init__()

        self.who = self.ASSET

        self.imports = [

            # # GEO
            # {'filepath': 'D:/_Active_Projects\Destruna/masterAssets_forRef/characters/don/3D/don_teen_casualClean/01_model/don_teen_casualClean_model_07_DMR_WIP.mb',
            #  'group': self.DELETE_GRP},

            # LOW GEO
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/props/vehicle_glideTrain/01_model/glideTrain_model_02_DMR_WIP.mb',
             'group': self.DELETE_GRP},

            # Guides
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/props/vehicle_glideTrain/02_rig/03_imports/guides/carriage_guides_06.mb',
             'group': self.DELETE_GRP},

            # Guides
            {'filepath': 'C:/Users/Damien/Desktop/temp/test_track.mb'},

        ]
        self.Connector_GEO_grp = 'carriage_geo_grp'
        self.driver_locator_grp = 'driver_locators_grp'
        self.base_locator_grp = 'base_locator_grp'
        self.up_locator_grp = 'up_locator_grp'
        self.carriage_joints_grp = 'carraige_joints_grp'

        self.base_IK_grp = 'base_IK_grp'
        self.up_IK_grp = 'up_IK_grp'

        # Spline rig

        self.current_path = {}

        self.ik_chain_joints = {}
        self.spline_orient_joint = []

        self.driver_locator = {}

        self.train_length = 0

        self.IK_spline_curve = 'spline_IK_curve'
        self.default_track_spline_curve = 'default_track_curve'
        self.default_track_up_curve = 'default_up_curve'

        self.control = None

        self.base = 'base'
        self.up = 'up'

        self.curves = {}

        # Up vector
        self.up_offset = 2

        self.groups = [
            {'g': self.TOP_GRP, 'p': None, 'v': True},
            {'g': self.RIG_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.GEO_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.Connector_GEO_grp, 'p': self.GEO_GRP, 'v': True},
            {'g': self.DNT_GRP, 'p': self.TOP_GRP, 'v': False},
            {'g': self.driver_locator_grp, 'p': self.DNT_GRP, 'v': False},
            {'g': self.carriage_joints_grp, 'p': self.DNT_GRP, 'v': False},
            {'g': self.base_IK_grp, 'p': self.DNT_GRP, 'v': False},
            {'g': self.up_IK_grp, 'p': self.DNT_GRP, 'v': False},
            {'g': self.base_locator_grp, 'p': self.driver_locator_grp, 'v': False},
            {'g': self.up_locator_grp, 'p': self.driver_locator_grp, 'v': False},
        ]

    def build_spline_rig(self):

        carriage_guide_positions = {}

        # Get guide positions and save to list, save second set of positions with up curve offset
        temp_base_positions = [mc.xform(i, rp=1, q=1) for i in self.CARRIAGE_GUIDE_ORDER]
        temp_up_positions = [(i[0], i[1] + self.up_offset, i[2]) for i in temp_base_positions]

        carriage_guide_positions[self.base] = temp_base_positions
        carriage_guide_positions[self.up] = temp_up_positions

        # Create control
        control_name = 'control'
        control_attr = 'PARAM'
        mc.group(em=1, w=1, n=control_name)
        attribute.add(control_name, n=control_attr, type='float', dv=0.0)
        self.control = control_name + '.' + control_attr


        # Build mathcing driven spline IK rigs for "base" and "up"
        for side in [self.base, self.up]:

            # CURVES
            # Build curves for both base and up spline rigs
            temp_curve_dict = {}

            ik_curve_name = '{}_ik_curve'.format(side)
            default_track_name = '{}_default_track_curve'.format(side)
            guides = carriage_guide_positions[side]

            ik_curve, default_track = self.build_spline_rig_curves(ik_curve_name, default_track_name, guides)

            # Save curves to master dictionary
            temp_curve_dict['ik_curve'] = ik_curve
            temp_curve_dict['default_track'] = default_track
            self.curves[side] = temp_curve_dict

            # LENGTH
            # Get train length from the arc length of generated curve
            self.train_length = mc.arclen(self.curves[side]['ik_curve'])
            mc.select(cl=1)

            # IK JOINT CHAIN
            # Build a joint chain based on the number/position of guides, append to list
            temp_joint_list = []

            for index, guide_position in enumerate(carriage_guide_positions[side]):

                joint_name = '{0}_ik_{1}_joint'.format(side, index)
                j = mc.joint(p=guide_position, n=joint_name)
                temp_joint_list.append(j)
                # mc.setAttr(guide_name.replace('guide', 'joint') + '.displayLocalAxis', 1)

            # Append joints into master dictionary of ik_chain_joints for both 'base' and 'up'
            self.ik_chain_joints[side] = temp_joint_list
            mc.select(cl=1)

            # IK CONSTRAINT
            # Attach joint chain to spline IK
            first_joint = self.ik_chain_joints[side][0]
            last_joiint = self.ik_chain_joints[side][-1]
            curve = self.curves[side]['ik_curve']
            mc.ikHandle(
                sj=self.ik_chain_joints[side][0],
                ee=self.ik_chain_joints[side][-1],
                c=self.curves[side]['ik_curve'],
                sol='ikSplineSolver',
                ccv=False,
                roc=True
            )

            # DRIVER LOCATORS
            #   Build a locator based on the number/position of guides that drives a cv on the Ik spline, append to list
            temp_loc_list = []
            for index, guide_position in enumerate(carriage_guide_positions[side]):

                drv_loc_name = '{0}_driver_{1}_locator'.format(side, index)
                ik_curve = self.curves[side]['ik_curve']
                default_track = self.curves[side]['default_track']

                locator_dict = self.build_driver_locator(
                 index,
                 drv_loc_name,
                 guide_position,
                 ik_curve,
                 default_track,
                 temp_loc_list,
                 )

                temp_loc_list.append(locator_dict)

            self.driver_locator[side] = temp_loc_list

            # ATTACH
            # Attach driver locators to default path
            self.attach_to_default_path(
                self.driver_locator[side],
                self.curves[side]['default_track'],
                self.control,
            )

        #
        for index, guide_position in enumerate(carriage_guide_positions[self.base]):
            joint_name = 'carriage_{}_joint'.format(index)
            j = mc.joint(p=guide_position, n=joint_name)
            mc.parent(j, self.carriage_joints_grp)
            mc.select(cl=1)

            mc.pointConstraint(self.ik_chain_joints[self.base][index], j)

            if index > 0:
                forward_driver = self.ik_chain_joints[self.base][index-1]
                forward_axis = [1,0,0]
            else:
                forward_driver = self.ik_chain_joints[self.base][index + 1]
                forward_axis = [-1, 0, 0]

            up_vec = self.ik_chain_joints[self.up][index]

            mc.aimConstraint(forward_driver, j, aimVector=[1,0,0], upVector=[0,1,0], worldUpType='object', worldUpObject=up_vec)
            mc.setAttr(j + '.displayLocalAxis', 1)

        self.current_path = self.curves[self.base]['default_track']

        # Build Connector
        connectors = [i for i in self.CARRIAGE_PREFIXES if i != 'E']
        for index, connector in enumerate(connectors):
            self.build_connector_rig(index, connector)

        for car in self.GEO_TO_JOINT:
            mc.parentConstraint(self.GEO_TO_JOINT[car], car, mo=1)






    def build_spline_rig_curves (self, ik_name, track_name, guides):

        ik_curve = mc.curve(p=guides, d=1, n=ik_name)

        default_track = mc.curve(p=guides, d=2, n=track_name)

        return ik_curve, default_track


    def build_driver_locator(self, index, drv_loc_name, guide_position, ik_curve, default_track, temp_loc_list):

        loc_dict = {}

        # Create driver locator
        loc = mc.spaceLocator(n=drv_loc_name)[0]
        parent = drv_loc_name.split('_driver_')[0] + '_locator_grp'
        mc.parent(loc, parent)

        # Position driver locator
        for attr, value in zip(['X', 'Y', 'Z'], guide_position):
            mc.setAttr(loc + '.translate' + attr, value)

        # Calculate parameter on path for all locators excluding the first

        if index > 0:
            locator_delta = calculate_magnitude(temp_loc_list[0]['locator'], drv_loc_name)
            normalized_delta = normalize_value_in_range(locator_delta, old_min=0, old_max=self.train_length)
        else:
            normalized_delta = 0

        # Store driver locator name and parameter values
        loc_dict['locator'] = loc
        loc_dict['parameter'] = normalized_delta

        # Connect driver locator to driven curve
        mc.connectAttr(loc + '.translate', ik_curve + '.controlPoints[{}]'.format(index), f=1)

        # Return driver locator name and parameter
        print loc_dict
        return loc_dict

    def attach_to_default_path(self, driver_locs, curve, cont):

        # Attach driver locators
        for loc in driver_locs:

            mopath_node, scale_node = self.transform_to_mopath(
               loc['locator'],
               curve,
               cont,
               loc['parameter'])

            loc['mopath'] = mopath_node
            loc['scale'] = scale_node




    def transform_to_mopath(self, transform, path, param_control, param):

        # Create Nodes
        motion_p_node = mc.pathAnimation(transform, c=path)
        plus_node = mc.createNode('plusMinusAverage')
        scale_node = mc.createNode('multiplyDivide')
        mc.delete(mc.listConnections(motion_p_node, type='animCurveTL')[0])

        mc.connectAttr(plus_node + '.output1D', scale_node + '.input1X')
        mc.connectAttr(scale_node + '.outputX', motion_p_node + '.uValue')

        mc.connectAttr(param_control, plus_node + '.input1D[0]')
        mc.setAttr(plus_node + '.input1D[1]', param)

        return motion_p_node, scale_node

    def attach_to_path(self, base_path, up_path):

        old_path = self.current_path
        new_path = base_path

        # Compare the arc length of old and new path
        parameter_scale = mc.arclen(old_path)/mc.arclen(new_path)

        print('PARAMETER SCALE', parameter_scale)

        for side, path in zip([self.base, self.up], [base_path, up_path]):
            for locator in self.driver_locator[side]:

                mc.connectAttr(path + '.local', locator['mopath'] + '.geometryPath', f=1)
                mc.setAttr(locator['scale'] + '.input2X', parameter_scale)

        attribute.add('path_master_control',
                      name='train_motion',
                      type='float',
                      defaultValue=0)

        mc.connectAttr('path_master_control.train_motion', self.control)




    def build_connector_rig(self, index, connector):

        mc.select(cl=1)

        front_joint_position = mc.xform(self.CARRIAGE_CONNECTOR_GUIDE[index][0], t=1, q=1)
        back_joint_position = mc.xform(self.CARRIAGE_CONNECTOR_GUIDE[index][1], t=1, q=1)

        front_joint = mc.joint(p=front_joint_position, n=self.CARRIAGE_CONNECTOR_GUIDE[index][0] + '.joint')
        back_joint = mc.joint(p=back_joint_position, n=self.CARRIAGE_CONNECTOR_GUIDE[index][1] + '.joint')

        print(">>>>>>", connector, front_joint, back_joint)

        skin_cluster_prefix = 'D:/_Active_Projects/Destruna/masterAssets_forRef/props/vehicle_glideTrain/02_rig/03_imports/skinClusters/connectors/'
        skin_cluster_file = 'carriageConnector_{}_geo_skinCluster_01.json'.format(connector)

        geo = 'carriageConnector_{}_geo'.format(connector)

        build.skin_ng.import_from_file(filepath=skin_cluster_prefix + skin_cluster_file,
                                       geo=geo)

        next_carrige_prefix = self.CARRIAGE_PREFIXES[index + 1]

        carriage = 'frame_{}_geo'.format(self.CARRIAGE_PREFIXES[index])
        next_carriage = 'frame_{}_geo'.format(self.CARRIAGE_PREFIXES[index+1])

        wrap.create('carriageConnectorInterior_{}_geo'.format(connector), geo)
        mc.parent(geo + 'Base', self.Connector_GEO_grp)

        mc.parentConstraint(carriage, front_joint, mo=1)
        mc.parentConstraint(next_carriage, back_joint, mo=1)

        mc.parent(front_joint, self.carriage_joints_grp)

    def post_edits(self):

        mc.parent(self.control.split('.')[0], self.RIG_GRP)

        for g in self.GEO_TO_JOINT:
            mc.parent(g, self.GEO_GRP)

        for g in self.CONNECTOR:
            mc.parent(g, self.Connector_GEO_grp)

        #mc.parent(self.GEO, self.GEO_GRP)GEO_TO_JOINT
        mc.parent('ikHandle1', 'base_ik_curve', 'base_ik_0_joint', self.base_IK_grp)
        mc.parent('ikHandle2', 'up_ik_curve', 'up_ik_0_joint', self.up_IK_grp)

        mc.parent('base_default_track_curve', 'up_default_track_curve', self.DNT_GRP)

        mc.setAttr("perspShape.farClipPlane", 100000)
        #mc.setAttr('carriage_A_front_joint.visibility', 0)
        #mc.setAttr('glideTrain.visibility', 0)
        mc.setAttr('glideTrain_guides.visibility', 0)


        # ==================================================================================================================

    def go(self):
        print('=' * 80)
        print('Starting ' + str(self.who) + ' Rig Build.')
        mc.file(new=True, f=True)
        self.build_contents()
        print('Finished!')

    def build_contents(self):

        build.imports(self.imports)
        build.groups(self.groups)
        self.build_spline_rig()
        self.post_edits()
        pm.delete(self.DELETE_GRP)




def calculate_magnitude(old_locator, new_locator):

    old_position = mc.pointPosition(old_locator)
    new_position = mc.pointPosition(new_locator)

    delta_p = [x - y for (x, y) in zip(new_position, old_position)]

    x = delta_p[0]
    y = delta_p[1]
    z = delta_p[2]

    return math.sqrt(x ** 2 + y ** 2 + z ** 2)


def normalize_value_in_range(value, old_min=0, old_max=1, a=0, b=1):

    value = float(value)
    old_min = float(old_min)
    old_max = float(old_max)
    a = float(a)
    b = float(b)

    value_scaled = (b - a) * (value - old_min)

    return (value_scaled / (old_max - old_min)) + a

"""
#####   GLIDE TRAIN    ###### 

from jb_destruna.rigs.f_glideTrain import glideTrain_animRig
reload(glideTrain_animRig)
rig = glideTrain_animRig.Rig()
rig.go()
"""


"""

    Example of attach to path

from jb_destruna.tools import path
reload(path)
#####   GLIDE TRAIN    ###### 

from jb_destruna.rigs.f_glideTrain import glideTrain_animRig
reload(glideTrain_animRig)
rig = glideTrain_animRig.Rig()
rig.go()

path.build('test_track', None, node=20)

rig.attach_to_path('base_curve', 'up_curve')

"""




