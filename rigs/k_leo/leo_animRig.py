from jb_destruna.maya.core import build
import maya.cmds as mc
import pymel.core as pm
from maya import mel
from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import control
from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.internal import wrap
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn
from jb_destruna.maya.rig_modules import fk_chain as fk
from jb_destruna.maya.internal import follicle as fol
from jb_destruna.maya.rig_modules import module_utility
from jb_destruna.maya.rig_modules import ligament_joints as lig
reload(lig)

reload(fol)

reload(fk)

reload(build)
reload(rig)



class Rig(rig.Rig):

    ASSET = 'Leo'
    VARIANT = ''

    FACE_NS = 'face'

    IMPORT_LIST = ['office', 'jordan', 'damien']
    IMPORT_KEY = IMPORT_LIST[2]

    # BODY
    BODY_GEO = 'body_geo'
    HAIR_GEO = 'hair_geo'
    HAIR_LINE_GEO = 'hairLine_geo'

    NAILS_GEO = 'nails_geo'

    # FACE
    FACE_GEO = 'face_geo'
    PUPIL_GEO_L = 'eye_l_geo'
    PUPIL_GEO_R = 'eye_r_geo'

    EYELASH_UPPER_L_GEO = 'eyelashTop_l_geo'
    EYELASH_UPPER_R_GEO = 'eyelashTop_r_geo'

    EYELASH_LOWER_L_GEO = 'eyelashLow_l_geo'
    EYELASH_LOWER_R_GEO = 'eyelashLow_r_geo'

    EYEBROW_GEO_L = 'brow_l_geo'
    EYEBROW_GEO_R = 'brow_r_geo'

    SCLERA_L = 'sclera_l_geo'
    SCLERA_R = 'sclera_r_geo'

    TOPTEETH_GEO = 'teeth_top_geo'
    BOTTOMTEETH_GEO = 'teeth_bottom_geo'
    TONGUE_GEO = 'tongue_geo'

    # CLOTHING
    CLOTHES_JACKET_GEO = 'jacket_geo'
    CLOTHES_PANTS_GEO = 'pocket_geo'
    CLOTHES_SHOES_GEO = 'shoes_geo'

    # ACCESSORY
    GLASSES_FRAME_GEO = 'frame_geo'
    GLASSES_LENSES_GEO = 'lenses_geo'

    ACCESSORY_SUSPENDERS_GEO = 'suspenders_geo'
    ACCESSORY_HAIRBAND_GEO = 'hairband_geo'
    ACCESSORY_WRISTBAND_L_GEO = 'wristband_l_geo'
    ACCESSORY_WRISTBAND_R_GEO = 'wristband_r_geo'
    ACCESSORY_RADIO_GEO = 'radio_geo'
    ACCESSORY_BUTTON_GEO = 'button_geo'
    ACCESSORY_CHAINPOINT_GEO = 'chainpoint_geo'
    ACCESSORY_CHAINS_GEO = 'chainsAll_geo'

    # UTILITY
    BODY_COLLISION_GEO = 'body_collision'

    FACE_GEO_LIST = [
        FACE_GEO,
        PUPIL_GEO_L,
        PUPIL_GEO_R,
        #SCLERA_L,
        #SCLERA_R,
        EYEBROW_GEO_L,
        EYEBROW_GEO_R,
        EYELASH_UPPER_L_GEO,
        EYELASH_UPPER_R_GEO,
        EYELASH_LOWER_L_GEO,
        EYELASH_LOWER_R_GEO,
        TOPTEETH_GEO,
        BOTTOMTEETH_GEO,
        TONGUE_GEO,
    ]

    GEO_LIST = [
        BODY_GEO,
        HAIR_GEO,
        HAIR_LINE_GEO,
        CLOTHES_JACKET_GEO,
        CLOTHES_PANTS_GEO,
        CLOTHES_SHOES_GEO,

        GLASSES_FRAME_GEO,
        GLASSES_LENSES_GEO,

        ACCESSORY_SUSPENDERS_GEO,
        ACCESSORY_HAIRBAND_GEO,
        ACCESSORY_WRISTBAND_L_GEO,
        ACCESSORY_WRISTBAND_R_GEO,
        ACCESSORY_RADIO_GEO,
        ACCESSORY_BUTTON_GEO,
        ACCESSORY_CHAINPOINT_GEO,
        ACCESSORY_CHAINS_GEO,
    ]

    GEO_LIST += FACE_GEO_LIST

    def __init__(self):
        super(Rig, self).__init__()

        self.who = self.ASSET
        self.imports = [
            # GEO
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/leo/3D/leo/01_model/leo_model_07_DFO_MASTER.mb',
             'group': self.DELETE_GRP},

            # AS RIG
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/leo/3D/leo/02_rig/imports/asRig/leo_asRig_02.mb',
             'group': self.DELETE_GRP},

            # GUIDES
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/leo/3D/leo/02_rig/imports/guides/suspenders/suspenders_guide_02.mb',
             'group': self.DELETE_GRP},

            # Suspender guides
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/leo/3D/leo/02_rig/imports/guides/leo_jacket_guides_06.mb',
                'group': self.DELETE_GRP},

            # Suspender collision geo
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/leo/3D/leo/02_rig/imports/collsions_geo/collsions_geo_03.mb',
             'group': self.DELETE_GRP},

            # Bootstrap guides
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/leo/3D/leo/02_rig/imports/guides/Bootstrap/Bootstrap_guides_01.mb',
             'group': self.DELETE_GRP},
            # Bootstrap guides
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/leo/3D/leo/02_rig/imports/guides/lig/lig_fit/lig_fit_02.mb',
             'group': self.DELETE_GRP},

            # # AS RIG
            # {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gustavo/02_rig/imports/face_Rig/gustavo_face_01_JNB_MASTER.mb',
            #  'group': self.DELETE_GRP},

            # AS FACE RIG
            # {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_mocap/02_rig/imports/AS_face_rig/don_faceRig_v002_1.mb',
            #  'namespace': self.FACE_NS},

        ]

        skin_weight_prefix = 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/leo/3D/leo/02_rig/imports/skinClusters/'

        self.skin_bind = [

            # BODY
            {'geo': self.BODY_GEO, 'filepath': skin_weight_prefix +  self.BODY_GEO + '/body_geo_skinCluster_02.json',
             'import_module':'skin_ng'},
            {'geo': self.HAIR_GEO, 'filepath': skin_weight_prefix + self.HAIR_GEO + '/hair_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.HAIR_LINE_GEO, 'filepath': skin_weight_prefix + self.HAIR_LINE_GEO + '/hairLine_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},

            {'geo': self.FACE_GEO, 'filepath': skin_weight_prefix + self.FACE_GEO + '/face_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            # {'geo': self.SCLERA_L, 'filepath': skin_weight_prefix + self.SCLERA_L + '/sclera_l_geo_skinCluster_01.json',
            #  'import_module': 'skin_ng'},
            # {'geo': self.SCLERA_R, 'filepath': skin_weight_prefix + self.SCLERA_R + '/sclera_r_geo_skinCluster_01.json',
            #  'import_module': 'skin_ng'},
            #
            {'geo': self.PUPIL_GEO_L, 'filepath': skin_weight_prefix + self.PUPIL_GEO_L + '/eye_l_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.PUPIL_GEO_R, 'filepath': skin_weight_prefix + self.PUPIL_GEO_R + '/eye_r_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},

            {'geo': self.EYEBROW_GEO_L, 'filepath': skin_weight_prefix + self.EYEBROW_GEO_L + '/brow_l_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYEBROW_GEO_R, 'filepath': skin_weight_prefix + self.EYEBROW_GEO_R + '/brow_r_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.TOPTEETH_GEO, 'filepath': skin_weight_prefix + self.TOPTEETH_GEO + '/teeth_top_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.BOTTOMTEETH_GEO, 'filepath': skin_weight_prefix + self.BOTTOMTEETH_GEO + '/teeth_bottom_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.TONGUE_GEO, 'filepath': skin_weight_prefix + self.TONGUE_GEO + '/tongue_geo_skincluster_02.json',
             'import_module': 'skin_ng'},

            {'geo': self.CLOTHES_JACKET_GEO, 'filepath': skin_weight_prefix + self.CLOTHES_JACKET_GEO + '/jacket_geo_skinCluster_07.json',
             'import_module': 'skin_ng'},
            {'geo': self.CLOTHES_PANTS_GEO, 'filepath': skin_weight_prefix + self.CLOTHES_PANTS_GEO + '/pocket_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.CLOTHES_SHOES_GEO, 'filepath': skin_weight_prefix + self.CLOTHES_SHOES_GEO + '/shoes_geo_skinCluster_03.json',
             'import_module': 'skin_ng'},

            {'geo': self.EYELASH_UPPER_L_GEO, 'filepath': skin_weight_prefix + self.EYELASH_UPPER_L_GEO + '/eyelashTop_l_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_UPPER_R_GEO, 'filepath': skin_weight_prefix + self.EYELASH_UPPER_R_GEO + '/eyelashTop_r_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},

            {'geo': self.EYELASH_LOWER_L_GEO, 'filepath': skin_weight_prefix + self.EYELASH_LOWER_L_GEO + '/eyelashLow_l_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_LOWER_R_GEO, 'filepath': skin_weight_prefix + self.EYELASH_LOWER_R_GEO + '/eyelashLow_r_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},


            {'geo': self.ACCESSORY_SUSPENDERS_GEO, 'filepath': skin_weight_prefix + self.ACCESSORY_SUSPENDERS_GEO + '/suspenders_geo_skinCluster_03.json',
             'import_module': 'skin_ng'},
            {'geo': self.GLASSES_FRAME_GEO, 'filepath': skin_weight_prefix + self.GLASSES_FRAME_GEO + '/frame_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.GLASSES_LENSES_GEO, 'filepath': skin_weight_prefix + self.GLASSES_LENSES_GEO + '/lenses_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},

        ]

        self.groups = [
            {'g': self.TOP_GRP, 'p': None, 'v': True},
            {'g': self.RIG_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.GEO_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.DNT_GRP, 'p': self.TOP_GRP, 'v': False},
        ]


        collar_chain_dict_format = lambda name, side: {
            'chain_name': name + '_' + side,
            'guides': ['Jacket_' + name + '_{}_{}_guide'.format(i, side) for i in range(2)],
            'ctl_shape': 'box',
            'ctl_col': 12,
            'ctl_scale': 2,
        }

        self.collar_chain_list = []

        for side, nodes in zip(['M', 'L', 'R'], [['A'], ['B', 'C', 'D', 'E'], ['B', 'C', 'D', 'E']]):

            for n in nodes:
                chain_name = 'collar' + n
                collar_fk_chain_dict = collar_chain_dict_format(chain_name, side)
                self.collar_chain_list.append(collar_fk_chain_dict)


        self.boot_strap_fk_dict = [
            {
                'chain_name': 'upperL',
                'guides': ['BootStrap_upper0_L_guide', 'BootStrap_upper1_L_guide'],
                'ctl_shape': 'box',
                'ctl_col': 12,
                'ctl_scale': 2,
            },
            {
                'chain_name': 'lowerL',
                'guides': ['BootStrap_lower0_L_guide', 'BootStrap_lower1_L_guide'],
                'ctl_shape': 'box',
                'ctl_col': 12,
                'ctl_scale': 2,
            },
            {
                'chain_name': 'upperR',
                'guides': ['BootStrap_upper0_R_guide', 'BootStrap_upper1_R_guide'],
                'ctl_shape': 'box',
                'ctl_col': 12,
                'ctl_scale': 2,
            },
            {
                'chain_name': 'lowerR',
                'guides': ['BootStrap_lower0_R_guide', 'BootStrap_lower1_R_guide'],
                'ctl_shape': 'box',
                'ctl_col': 12,
                'ctl_scale': 2,
            }
        ]

    def modify_as_rig(self):

        # good stuff happens here.
        super(Rig, self).modify_as_rig()

        # localized stuff happens here --------------
        pn('Master_CTL_Main').bendVis.set(0)

        # lock vis ctls
        for ctl in self.AS_CTL_LIST_NEW:
            pn(ctl).v.set(k=False, cb=False, l=True)

        # attributes on master
        pm.setAttr('Master_CTL_Main.jointVis', 0)
        attribute.add('Master_CTL_Main', n='geoVis', type='bool', dv=1, cb=True, k=False)
        attribute.add('Master_CTL_Main', n='geoLock', type='bool', dv=1, cb=True, k=False)

        pm.connectAttr('Master_CTL_Main.geoLock', 'Geometry.overrideEnabled')
        pm.setAttr('Geometry.overrideDisplayType', 2)

        pm.connectAttr('Master_CTL_Main.geoVis', 'Geometry.v')

        pm.scale('Head_CTL_FKNeck_M.cv[:]',    [4, 4, 4], r=True, os=True)
        pm.move('Head_CTL_FKNeck_M.cv[:]',     [3, 0, 0], r=True, os=True)

        for s in pm.PyNode('Spine_CTL_RootX_M').getShapes():
            pm.xform(s + '.cv[:]', s=[1.2, 1.2, 1.2], r=True)

        pm.scale('Spine_CTL_FKSpine1_M.cv[:]', [1.5, 1.5, 1.5], r=True, os=True)
        pm.scale('Spine_CTL_FKChest_M.cv[:]',  [1.5, 1.5, 1.5], r=True, os=True)


        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            pm.move('Arm_CTL_FKScapula_'+side+'.cv[:]', [-12*side_mult, 0, 0], r=True, os=True)
            pm.scale('Arm_CTL_FKScapula_' + side + '.cv[:]', [1.4, 1.4, 1.4], r=True, os=True)

            pm.scale('Arm_CTL_FKShoulder_'+side+'.cv[:]', [1, 1, 1], r=True, os=True)
            pm.scale('Arm_CTL_FKElbow_' + side + '.cv[:]', [1, 1, 1], r=True, os=True)
            pm.scale('Arm_CTL_FKWrist_' + side + '.cv[:]', [1, 1, 1], r=True, os=True)

            pm.scale('Hand_CTL_FKThumbFinger2_' + side + '.cv[:]', [2, 2, 2], r=True, os=True)

    def setup_facerig(self):

        ns = self.FACE_NS+':'

        #face_rig_geo = ns + 'face_rig_geo'


        # parent face rig.
        pm.parent(ns+'faceRig', 'Master_CTL_Main')
        pn(ns+'faceRig').inheritsTransform.set(False)

        pm.parent(ns+'Head_constrained_grp', 'Master_CTL_Main')

        # constrain controls to head.
        pm.parentConstraint('Head_Head_joint_M', ns+'Head_constrained_grp', mo=True)
        pm.parentConstraint('Head_Head_joint_M', ns+'Head_constrained_grp', mo=True)

        for geo in self.FACE_GEO_LIST:
            blendshape.apply(ns+geo, geo, dv=1)

        # hide face geo
        pm.hide(ns+'Geo')

        for side in 'LR':
            for atr in ['rx', 'ry', 'rz']:
                pm.connectAttr(pn('Head_Eye_joint_'+side).attr(atr), pn('face:Eye_driver_'+side+'_GRP').attr(atr))

        hide_list = [
            ns+'Tweaks_controls',
            ns+'Main_joints',
            ns+'Region_joints',

        ]

        for h in hide_list:
            pn(h).v.set(0)

    def build_jacket_collar(self):

        module_name = 'Jacket'

        collar_chain = fk.FkChain(module_name=module_name, chains=self.collar_chain_list, do_dnt=True)
        collar_chain.build()

        mc.parent(module_name, self.MASTER_CONTROL)

        # Post clean ups
        mc.setAttr(collar_chain.dnt_grp + '.inheritsTransform', 0)
        mc.parentConstraint('Spine_Chest_joint_M', 'Jacket_collarA_M_CTLS_constrained', mo=1)

        for jnt in mc.listRelatives('Jacket_joint_grp', c=1):
            mc.connectAttr(self.MASTER_CONTROL + '.jointVis', '{}.visibility'.format(jnt))

        for side in ['L', 'R']:

            # Rot joint
            mc.select(cl=1)
            rot_jnt = mc.joint(n="Arm_Shoulder_collar_rotation_{}".format(side))
            mc.delete(mc.parentConstraint('Arm_Scapula_joint_{}'.format(side), rot_jnt))
            mc.parent(rot_jnt, 'Spine_Chest_joint_M')

            # Static loc
            static_shoulder_loc = mc.spaceLocator(n="Arm_Scapula_static_loc_{}".format(side))
            mc.delete(mc.parentConstraint('Arm_Scapula_joint_{}'.format(side), static_shoulder_loc))
            mc.parentConstraint('Spine_Chest_joint_M', static_shoulder_loc, mo=1)
            mc.parent(static_shoulder_loc, collar_chain.dnt_grp)

            con = mc.parentConstraint('Arm_Scapula_joint_{}'.format(side), rot_jnt, mo=1)
            mc.parentConstraint(static_shoulder_loc, rot_jnt, mo=1)

            mc.setAttr(con[0] + '.interpType', 2)
            mc.setAttr('Arm_Shoulder_collar_rotation_{0}_parentConstraint1.Arm_Scapula_static_loc_{0}W1'.format(side), 3)

            mc.parentConstraint('Spine_Chest_joint_M', 'Jacket_collarB_{}_CTLS_constrained'.format(side), mo=1)
            #mc.parentConstraint('Arm_Scapula_joint_{}'.format(side), 'Jacket_collarB_{}_CTLS_constrained'.format(side), mo=1)

            for letter in ['C', 'D', 'E']:

                ctl_top_grp = 'Jacket_collar{0}_{1}_CTLS_constrained'.format(letter, side)
                mc.parentConstraint(rot_jnt, ctl_top_grp, mo=1)

    def install_suspenders_rig(self):

        module_name = 'Suspenders'

        group_data = module_utility.create_module_groups(module_name, dnt=True)

        # Configure collision geo
        mc.select(cl=1)
        mc.select(self.BODY_COLLISION_GEO)
        mel.eval("cMuscle_makeMuscle(0);")
        wrap.create(self.BODY_COLLISION_GEO, self.BODY_GEO)

        suspender_L_base_curve = 'suspender_driver_curve_L'
        suspender_L_up_curve = 'suspender_driver_curveUP_L'

        suspender_R_base_curve = 'suspender_driver_curve_R'
        suspender_R_up_curve = 'suspender_driver_curveUP_R'

        curve_set_L = [suspender_L_base_curve, suspender_L_up_curve]
        curve_set_R = [suspender_R_base_curve, suspender_R_up_curve]

        for side, curve_set in zip(['L', 'R'],[curve_set_L, curve_set_R]):
            self.build_suspender(side, curve_set, group_data)

        mc.parent(group_data['top_grp'], self.MASTER_CONTROL)
        mc.setAttr(group_data['dnt_grp'] + '.inheritsTransform', 0)

        print('>>', group_data['dnt_grp'])
        mc.parent(self.BODY_COLLISION_GEO, group_data['dnt_grp'])
        mc.setAttr(self.BODY_COLLISION_GEO + '.visibility', 0)

    def build_suspender(self, side, curve_set, group_data):

        base_curve = curve_set[0]
        up_curve = curve_set[1]

        utility_grp  = mc.group(em=1, n='Suspenders_{}_elements'.format(side), p=group_data['dnt_grp'])
        mc.setAttr(utility_grp + '.visibility', 0)

        # attach clusters to curve
        cluster_list = []
        deg = mc.getAttr(base_curve + '.degree')
        spans = mc.getAttr(base_curve + '.spans')

        for i in range(deg + spans):
            cluster_name = 'suspender_{0}_{1}_cluster'.format(i, side)
            base_cv = base_curve + '.cv[{}]'.format(i)
            up_cv = up_curve + '.cv[{}]'.format(i)
            cluster = mc.cluster([base_cv, up_cv], n=cluster_name)
            cluster_list.append(cluster[1])

        cluster_grp = mc.group(cluster_list, n='Suspenders_{}_curve_clusters'.format(side), p=group_data['dnt_grp'])
        mc.setAttr(cluster_grp + '.inheritsTransform', 0)
        mc.setAttr(cluster_grp + '.visibility', 0)

        # Parent cluster sets
        clus_set_shoulder = ('Arm_Scapula_joint_{}'.format(side), [cluster_list[i] for i in [3, 4, 5]])
        clus_set_pectorial = (None, [cluster_list[i] for i in [2]])
        clus_set_abdomen = (None, [cluster_list[i] for i in [1]])             #   << TODO: clean this trash up
        clus_set_waist =    ('Spine_Root_joint_M', [cluster_list[i] for i in [0]])

        set_list = [clus_set_shoulder, clus_set_waist]

        for clus_set in set_list:

            driver = clus_set[0]
            clusters = clus_set[1]

            for cluster in clusters:

                print(cluster)
                mc.parentConstraint(driver, cluster, mo=1)

        # attach pec cluster
        f = 291 if side == 'L' else 622
        pec_follicle = fol.onSelected(selection=self.BODY_COLLISION_GEO + 'Shape.f[{}]'.format(f))
        #pm.parentConstraint(pec_follicle, cluster_list[2], mo=1)
        pm.parent(pec_follicle, utility_grp)

        # Attach abdomen driver loc
        abdoman_driver_loc_name = 'abdoman_driver_{}_loc'.format(side)
        abdoman_driver_loc = mc.spaceLocator(n=abdoman_driver_loc_name)
        mc.delete(mc.parentConstraint(clus_set_abdomen[1][0], abdoman_driver_loc))
        mc.pointConstraint(clus_set_pectorial[1][0], clus_set_waist[1][0], abdoman_driver_loc, mo=1)
        mc.parent(abdoman_driver_loc, utility_grp)

        # Attach muscle driven loc
        abdoman_driven_loc_name = 'abdoman_driven_{}_loc'.format(side)
        abdoman_driven_loc = mc.spaceLocator(n=abdoman_driven_loc_name)
        mc.delete(mc.parentConstraint(abdoman_driver_loc, abdoman_driven_loc))
        mc.parent(abdoman_driven_loc, abdoman_driver_loc)

        # Attach lower back cluster
        mc.parentConstraint('Spine_Spine1Part2_joint_M', cluster_list[6], mo=1)

        # Attach back root cluster
        mc.parentConstraint('Spine_Root_joint_M', cluster_list[7], mo=1)

        # Attach keep out node
        mc.select(cl=1)
        mc.select(abdoman_driven_loc)
        mel.eval('cMuscle_rigKeepOutSel();')
        mc.select(cl=1)
        mc.select(abdoman_driven_loc, self.BODY_COLLISION_GEO)
        mel.eval("cMuscle_keepOutAddRemMuscle(1);")

        keep_out_node = mc.listRelatives(abdoman_driven_loc, c=1)[1]
        mc.setAttr(keep_out_node + '.inDirectionZ', 13)

        keep_out_driven  = mc.listRelatives(keep_out_node, c=1)[1]

        # Attach fitted bSpline
        base_crv_fitted = mc.fitBspline(base_curve, n=base_curve+'_fitted', ch=1, tol=0.01)[0]
        up_crv_fitted = mc.fitBspline(up_curve, n=base_curve + '_fitted', ch=1, tol=0.01)[0]
        mc.parent(base_crv_fitted, up_crv_fitted, utility_grp)

        joints = self.create_joints_on_curve(base_crv_fitted, up_crv_fitted, 25, 'suspender{}'.format(side))
        jnt_grp = mc.group(joints, n='Suspenders_{}_joints'.format(side), p=group_data['jnt_grp'])
        mc.setAttr(jnt_grp + '.inheritsTransform', 0)
        mc.setAttr(jnt_grp + '.visibility', 0)

        # create offset control

        control_trans = mc.group(n='Suspender_pec_{}_CTL'.format(side), em=1, w=1)
        pm.delete(pm.parentConstraint(pec_follicle, control_trans))

        pec_offset_control = control.create(
            control_trans,
            shape='ball',
            colorIdx=20,
            zeroGroup=True
        )
        pm.parent(pec_offset_control.getParent(), group_data['ctl_grp'])
        pm.parentConstraint(pec_follicle, pec_offset_control.getParent(), mo=1)
        pm.parentConstraint(pec_offset_control, cluster_list[2], mo=1)


        control_trans = mc.group(n='Suspender_abdoman_{}_CTL'.format(side), em=1, w=1)
        mc.delete(mc.parentConstraint( abdoman_driven_loc,control_trans))

        abdoman_offset_control = control.create(
            control_trans,
            shape='ball',
            colorIdx=20,
            zeroGroup=True
        )
        pm.parent(abdoman_offset_control.getParent(), group_data['ctl_grp'])
        pm.parentConstraint(keep_out_driven, abdoman_offset_control.getParent(), mo=1)
        pm.parentConstraint(abdoman_offset_control, cluster_list[1], mo=1)

        mc.parent(base_curve, up_curve, utility_grp)
    def create_joints_on_curve(self, base_curve, up_curve, joint_number, prefix):

        parameter_list = [i / float(joint_number - 1) for i in range(joint_number)]
        #joint_list = [mc.joint(n='pathJoint_{}'.format(i)) for i in range(joint_number)]

        joint_list = []

        for i, pr in zip(range(joint_number), parameter_list):
            mc.select(cl=1)

            j =mc.joint(n=prefix + '_pathJoint_{}'.format(i))

            attribute.add(j, n='pr', type='float', l=1, dv=pr)
            joint_list.append(j)
            mc.select(cl=1)

        self.attach_transforms_to_curve(base_curve, up_curve, joint_list, parameter_list)

        return joint_list

    def attach_transforms_to_curve(self, base_curve, up_curve, transform_list, parameter_list):

        for transform, pr in zip(transform_list, parameter_list):

            pointOnCurve_node = mc.createNode('pointOnCurveInfo', name=transform+'_pointOnCurve')
            mopath_node = mc.pathAnimation(transform, c=base_curve, f=1, wut='object', name=transform+'_moPath',
                                           fa='z', ua='y')

            mc.delete(mc.listConnections(mopath_node, type='animCurveTL'))
            mc.connectAttr(up_curve + '.local', pointOnCurve_node + '.inputCurve')

            self.connect_composition_to_node(pointOnCurve_node, mopath_node)

            # Assign parameter values
            mc.setAttr(pointOnCurve_node + '.parameter', pr)
            mc.setAttr(mopath_node + '.uValue', pr)

    def connect_decomposition_to_cv(self, driver_node, driven_node, cv):

        decomp = mc.createNode('decomposeMatrix', n=driver_node + '_decomp')
        mc.connectAttr(driver_node + '.worldMatrix[0]', decomp + '.inputMatrix')
        mc.connectAttr(decomp + '.outputTranslate', driven_node + '.controlPoints[{}]'.format(cv))

    def connect_composition_to_node(self, driver_node, driven_node):

        comp = mc.createNode('composeMatrix', n=driver_node + '_decomp')
        mc.connectAttr(driver_node + '.position', comp + '.inputTranslate')
        mc.connectAttr(comp + '.outputMatrix', driven_node + '.worldUpMatrix')

    def install_elbow_ligaments(self):

        module_name = 'Elbow_ligaments'

        lig_fit_side = lambda side: [i.format(side) for i in
                                     ['lig_pair_{}_joint_a', 'lig_pair_{}_joint_b', 'lig_pair_{}_joint_c']]
        arm_attach_side = lambda side: [i.format(side) for i in
                                     ['Arm_ShoulderPart2_joint_{}', 'Arm_Elbow_joint_{}', 'Arm_ElbowPart1_joint_{}']]
        for side, side_mult in zip(['L', 'R'],[1, -1]):

            lig_fit_jnts = lig_fit_side(side)
            arm_attach_jnts = arm_attach_side(side)

            l_rig = lig.Ligament(module_name + '_' + side, lig_fit_jnts[1], [0, 0, 3.5], build_mirror=True)
            l_rig.build_ligament()

            mc.setAttr(lig_fit_jnts[1] + '.rotateY', -17.226 * side_mult)

            mc.parentConstraint(arm_attach_jnts[0], lig_fit_jnts[0], mo=1)
            mc.orientConstraint(arm_attach_jnts[1], lig_fit_jnts[1], mo=1)

            mc.parent(lig_fit_jnts[0], 'DoNotTouch')
            mc.parent(module_name + '_' + side, self.MASTER_CONTROL)


    def install_bootstraps(self):

        module_name = 'BootStrap'

        bootstrap = fk.FkChain(module_name=module_name, chains=self.boot_strap_fk_dict, do_dnt=True)
        bootstrap.build()

        mc.parent(module_name, self.MASTER_CONTROL)

        # post edit
        for jnt in mc.listRelatives(module_name + '_joint_grp', c=1):
            mc.connectAttr(self.MASTER_CONTROL + '.jointVis', '{}.visibility'.format(jnt))

        mc.parentConstraint('KneePart2_L', 'BootStrap_CTL_upperL_0_ZERO', mo=1)
        mc.parentConstraint('KneePart2_L', 'BootStrap_CTL_lowerL_0_ZERO', mo=1)
        mc.parentConstraint('KneePart2_R', 'BootStrap_CTL_upperR_0_ZERO', mo=1)
        mc.parentConstraint('KneePart2_R', 'BootStrap_CTL_lowerR_0_ZERO', mo=1)

    def install_link_modules(self):

        link_top_grp = mc.group(em=1, p=self.MASTER_CONTROL, n='Link_modules')

        start_driver, end_driver, br_l_top_group = self.build_link_modules(
            'breast_L_start_guide',
            'breast_L_end_guide',
            'breast_L_start_UPguide',
            'breast_L_end_UPguide',
            prefix='breast_L',
            mid_joints=1,
            forward_aim=(1, 0, 0),
            up_aim=(0, 1, 0)
        )

        mc.parentConstraint('Spine_Chest_joint_M', start_driver, mo=1)
        con = mc.parentConstraint('Spine_Chest_joint_M', 'Arm_Scapula_joint_L', end_driver, mo=1)[0]
        mc.setAttr(con + '.interpType', 2)
        mc.setAttr(con + '.Spine_Chest_joint_MW0', 3)
        mc.scaleConstraint('Spine_Chest_joint_M', 'breast_L_link_joint_0')

        start_driver, end_driver, br_r_top_group = self.build_link_modules(
            'breast_R_start_guide',
            'breast_R_end_guide',
            'breast_R_start_UPguide',
            'breast_R_end_UPguide',
            prefix='breast_R',
            mid_joints=1,
            forward_aim=(1, 0, 0),
            up_aim=(0, 1, 0)
        )

        mc.parentConstraint('Spine_Chest_joint_M', start_driver, mo=1)
        con = mc.parentConstraint('Spine_Chest_joint_M', 'Arm_Scapula_joint_R', end_driver, mo=1)[0]
        mc.setAttr(con + '.interpType', 2)
        mc.setAttr(con + '.Spine_Chest_joint_MW0', 3)
        mc.scaleConstraint('Spine_Chest_joint_M', 'breast_R_link_joint_0')


        start_driver, end_driver, cl_l_top_group = self.build_link_modules(
            'clav_L_start_guide',
            'clav_L_end_guide',
            'clav_L_start_UPguide',
            'clav_L_end_UPguide',
            prefix='clav_L',
            mid_joints=1,
            forward_aim=(1, 0, 0),
            up_aim=(0, 1, 0)
        )

        mc.parentConstraint('Spine_Chest_joint_M', start_driver, mo=1)
        mc.parentConstraint('Arm_Scapula_joint_L', end_driver, mo=1)
        mc.scaleConstraint('Spine_Chest_joint_M', 'clav_L_link_joint_0')


        start_driver, end_driver, cl_r_top_group = self.build_link_modules(
            'clav_R_start_guide',
            'clav_R_end_guide',
            'clav_R_start_UPguide',
            'clav_R_end_UPguide',
            prefix='clav_R',
            mid_joints=1,
            forward_aim=(1, 0, 0),
            up_aim=(0, 1, 0)
        )

        mc.parentConstraint('Spine_Chest_joint_M', start_driver, mo=1)
        mc.parentConstraint('Arm_Scapula_joint_R', end_driver, mo=1)
        mc.scaleConstraint('Spine_Chest_joint_M', 'clav_R_link_joint_0')


        mc.parent(br_l_top_group, br_r_top_group, cl_l_top_group, cl_r_top_group, link_top_grp)

    def build_link_modules(self,
                           start_guide,
                           end_guide,
                           start_up,
                           end_up,
                           prefix='',
                           mid_joints=0,
                           forward_aim=(1,0,0),
                           up_aim=(0,1,0)):

        prefix = prefix + '_' if prefix else prefix # if prefix token given, attach underscore

        module_top_grp = prefix + 'link_grp'

        joints_grp  = prefix + 'joint_grp'
        driver_grp  = prefix +'drivers_grp'
        driven_grp  = prefix +'driven_grp'
        utility_grp = prefix +'utility_grp'

        link_module_groups = [
            {'g': module_top_grp,  'p': None,            'v': True},
            {'g': joints_grp,      'p': module_top_grp,  'v': False},
            {'g': driver_grp,      'p': module_top_grp,  'v': True},
            {'g': driven_grp,      'p': module_top_grp,  'v': True},
            {'g': utility_grp,     'p': module_top_grp,  'v': False},
        ]

        build.groups(link_module_groups)
        mc.setAttr(utility_grp + ".inheritsTransform", 0)
        mc.setAttr(joints_grp + ".inheritsTransform", 0)

        # CREATE TRANSFORMS

        start_guides = [start_guide, start_up]
        end_guides   = [end_guide, end_up]

        transforms_dict = dict()

        driver = None
        driven = None

        for i_guides, i_side in zip([start_guides, end_guides],['start', 'end']):

            temp_dict = dict()

            base_guide, up_guide = i_guides

            driver_n = prefix + i_side + '_driver'
            driver = mc.group(name=driver_n, em=1, w=1)
            mc.delete(mc.parentConstraint(base_guide, driver))
            mc.parent(driver, driver_grp)
            temp_dict['driver'] = driver

            driver_up_n = prefix + i_side + '_driver_up'
            driver_up = mc.spaceLocator(name=driver_up_n)[0]
            mc.delete(mc.parentConstraint(up_guide, driver_up))
            mc.setAttr(driver_up + '.visibility', 0)
            mc.parent(driver_up, driver_n)
            temp_dict['up'] = driver_up

            driven_n = prefix + i_side + '_driven'
            driven = mc.spaceLocator(name=driven_n)[0]
            mc.delete(mc.parentConstraint(base_guide, driven))
            mc.setAttr(driven + '.visibility', 0)
            mc.pointConstraint(driver, driven)
            mc.parent(driven, driven_grp)
            temp_dict['driven'] = driven

            transforms_dict[i_side] = temp_dict # Save created transforms to lookup dictionary

        # CREATE CURVES

        curves = []

        base_curve_n = prefix + 'curve'
        base_curve = mc.curve(n=base_curve_n, p=[[0, 0, 0], [1, 0, 0]], d=1)
        self.connect_locs_to_curve(base_curve, transforms_dict['start']['driven'], transforms_dict['end']['driven'])
        curves.append(base_curve)

        for i_side, i_opposite_side in zip(['start', 'end'], ['end', 'start']):

            up_curve_n = prefix + i_side + '_upCurve'
            up_curve = mc.curve(n=up_curve_n, p=[[0, 0, 0], [1, 0, 0]], d=1)

            curve_start = transforms_dict[i_side]['up']
            curve_end = transforms_dict[i_opposite_side]['driven']

            self.connect_locs_to_curve(up_curve, curve_start, curve_end)

            curves.append(up_curve)

        mc.parent(curves, utility_grp)


        # AIM TRANSFORMS

        flip_forward_axis = lambda f_axis :[a*-1 for a in f_axis]

        start_driver = transforms_dict['start']['driver']
        start_driven = transforms_dict['start']['driven']

        end_driver = transforms_dict['end']['driver']
        end_driven = transforms_dict['end']['driven']

        mc.aimConstraint(

            start_driver,  #                              Constrain Start to End
            end_driven,

            aimVector      =  forward_aim,
            upVector       =  up_aim,
            worldUpType    =  'object',
            worldUpObject  =  transforms_dict['end']['up'])

        mc.aimConstraint(

            end_driver,  #                                 Constrain End to Start
            start_driven,

            aimVector     =   flip_forward_axis(forward_aim),
            upVector      =   up_aim,
            worldUpType   =   'object',
            worldUpObject =  transforms_dict['start']['up'])


       # BUILD JOINTS

        if mid_joints:

            joint_parameter_list = [(i+1)/float(mid_joints + 1) for i in range(mid_joints)]
            print('PR LIST >>', joint_parameter_list)
            curve_shape = pn(base_curve).getShape()

            print curve_shape

            for i, pr in enumerate(joint_parameter_list):

                mc.select(cl=1)
                joint_n = prefix + 'link_joint_' + str(i)
                jnt = mc.joint(name=joint_n)
                print jnt
                p_on_Curve_node = mc.createNode('pointOnCurveInfo')
                mc.connectAttr(curve_shape + '.local', p_on_Curve_node + '.inputCurve')
                mc.setAttr(p_on_Curve_node + '.parameter', pr)

                mc.connectAttr(p_on_Curve_node + '.position', jnt + '.translate')

                orient_c = mc.orientConstraint(
                             transforms_dict['start']['driven'],
                             transforms_dict['end']['driven'],
                             jnt
                )[0]

                mc.setAttr(orient_c + ".{}W0".format(transforms_dict['start']['driven']), 1-pr)
                mc.setAttr(orient_c + ".{}W1".format(transforms_dict['end']['driven']),   pr)
                mc.setAttr(orient_c + '.interpType', 2)

                mc.parent(jnt, joints_grp)


        return start_driver, end_driver, module_top_grp

    def connect_locs_to_curve(self, curve, loc_a, loc_b):

        for i, loc in enumerate([loc_a, loc_b]):

            loc_shape = pn(loc).getShape()
            curve_shape = pn(curve).getShape()

            mc.connectAttr(loc_shape + '.worldPosition[0]', curve_shape + '.controlPoints[{}]'.format(i))

    def post_edits(self):
        mc.setAttr("Master_CTL_Main.geoLock", 0)
        mc.setAttr("Master_CTL_Main.jointVis", 1)

    # ==================================================================================================================
    def go(self):
        print '='*80
        print 'Starting '+str(self.who)+' Rig Build.'
        mc.file(new=True, f=True)
        self.build_contents()
        print 'Finished!'

    def build_contents(self):

        build.imports(self.imports)

        pm.rename('Group', self.RIG_GRP)
        build.groups(self.groups)
        pm.parent(self.GEO_LIST, self.GEO_GRP)
        self.modify_as_rig()

        self.install_elbow_ligaments()
        self.install_suspenders_rig()
        self.install_bootstraps()

        self.build_jacket_collar()

        # #self.setup_facerig()

        build.skin_bind(self.skin_bind)
        pm.delete(self.DELETE_GRP)

        #self.post_edits()


"""
#      Leo      # 

from jb_destruna.rigs.k_leo import leo_animRig
reload(leo_animRig)
rig = leo_animRig.Rig()
rig.go()
"""





