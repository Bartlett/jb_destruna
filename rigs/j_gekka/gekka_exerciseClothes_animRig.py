from jb_destruna.maya.core import build
import maya.cmds as mc
import pymel.core as pm

from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import skin
from jb_destruna.maya.internal import control
from jb_destruna.maya.internal import wrap
from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.internal import mesh
from jb_destruna.maya.internal import cpom
from jb_destruna.maya.internal import follicle
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn
from jb_destruna.maya.utility.misc import zero_grp
from jb_destruna.tools import path as path_tools

from jb_destruna.rigs.e_don.import_paths import don_import_paths as imp

reload(build)
reload(rig)

"""
NOTES: 
Geo has be transfered 11.479 on Z axis 
"""

class Rig(rig.Rig):

    ASSET = 'Gekka'
    VARIANT = 'Exercise Clothes'

    FACE_NS = 'face'

    IMPORT_LIST = ['office', 'jordan', 'damien']
    IMPORT_KEY = IMPORT_LIST[2]

    # BODY
    BODY_GEO = 'bodyDefault_geo'
    HAIR_GEO = 'hairPonytail_geo'

    NAILS_GEO = 'nails_geo'

    # FACE
    #FACE_GEO = 'faceSkin_geo'
    PUPIL_GEO_L = 'eye_l_geo'
    PUPIL_GEO_R = 'eye_r_geo'

    EYELASH_A_L_GEO = 'eyelash_segA_l_geo'
    EYELASH_B_L_GEO = 'eyelash_segB_l_geo'
    EYELASH_C_L_GEO = 'eyelash_segC_l_geo'
    EYELASH_D_L_GEO = 'eyelash_segD_l_geo'

    EYELASH_A_R_GEO = 'eyelash_segA_r_geo'
    EYELASH_B_R_GEO = 'eyelash_segB_r_geo'
    EYELASH_C_R_GEO = 'eyelash_segC_r_geo'
    EYELASH_D_R_GEO = 'eyelash_segD_r_geo'

    EYEBROW_GEO_L = 'eyebrowDefault_l_geo'
    EYEBROW_GEO_R = 'eyebrowDefault_r_geo'
    SCLERA_L = 'sclera_l_geo'
    SCLERA_R = 'sclera_r_geo'

    TOPTEETH_GEO = 'topTeeth_geo'
    BOTTOMTEETH_GEO = 'bottomTeeth_geo'
    TONGUE_GEO = 'tongue_geo'

    # CLOTHING
    CLOTHES_GEO = 'clothes_exercise_geo'

    # UTILITY
    LOINCLOTH_WRAP_GEO = 'loinCloth_wrap_geo'
    FACE_GEO_LIST = [
        #FACE_GEO,
        PUPIL_GEO_L,
        PUPIL_GEO_R,
        SCLERA_L,
        SCLERA_R,
        EYELASH_A_L_GEO,
        EYELASH_B_L_GEO,
        EYELASH_C_L_GEO,
        EYELASH_D_L_GEO,
        EYELASH_A_R_GEO,
        EYELASH_B_R_GEO,
        EYELASH_C_R_GEO,
        EYELASH_D_R_GEO,
        EYEBROW_GEO_L,
        EYEBROW_GEO_R,
        TOPTEETH_GEO,
        BOTTOMTEETH_GEO,
        TONGUE_GEO,
    ]

    GEO_LIST = [
        BODY_GEO,
        HAIR_GEO,
        CLOTHES_GEO,
        NAILS_GEO,
    ]

    GEO_LIST += FACE_GEO_LIST

    def __init__(self):
        super(Rig, self).__init__()

        self.who = self.ASSET
        self.imports = [
            # GEO
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gekka/3D/gekka_exerciseClothes/01_model/gekka_exerciseClothes_model_04_DMR_MASTER.mb',
             'group': self.DELETE_GRP},

            #GUIDES
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gekka/3D/gekka_exerciseClothes/02_rig/imports/link_guides/link_test_03.mb',
                'group': self.DELETE_GRP},

            # # GUIDES
            # {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gekka/3D/gekka_exerciseClothes/02_rig/imports/guides/torso/gekka_torso_gudes_01.mb',
            #  'group': self.DELETE_GRP},

            # AS RIG
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gekka/3D/gekka_exerciseClothes/02_rig/imports/asRig/gekka_exerciseClothes_asRig_03.mb',
             'group': self.DELETE_GRP},

            # AS FACE RIG
            # {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_mocap/02_rig/imports/AS_face_rig/don_faceRig_v002_1.mb',
            #  'namespace': self.FACE_NS},

        ]

        skin_weight_prefix = 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gekka/3D/gekka_exerciseClothes/02_rig/imports/skinCluster/'

        self.skin_bind = [

            # BODY
            {'geo': self.BODY_GEO, 'filepath': skin_weight_prefix +  self.BODY_GEO + '/bodyDefault_geo_skinCluster_08'
                                                                                     ''
                                                                                     '.json',
             'import_module':'skin_ng'},
            {'geo': self.HAIR_GEO, 'filepath': skin_weight_prefix + self.HAIR_GEO + '/hairPonytail_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},

            {'geo': self.SCLERA_L, 'filepath': skin_weight_prefix + self.SCLERA_L + '/sclera_l_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.SCLERA_R, 'filepath': skin_weight_prefix + self.SCLERA_R + '/sclera_r_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},

            {'geo': self.PUPIL_GEO_L, 'filepath': skin_weight_prefix + self.PUPIL_GEO_L + '/eye_l_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.PUPIL_GEO_R, 'filepath': skin_weight_prefix + self.PUPIL_GEO_R + '/eye_r_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},

            {'geo': self.EYELASH_A_L_GEO, 'filepath': skin_weight_prefix + self.EYELASH_A_L_GEO + '/eyelash_segA_l_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_B_L_GEO, 'filepath': skin_weight_prefix + self.EYELASH_B_L_GEO + '/eyelash_segB_l_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_C_L_GEO, 'filepath': skin_weight_prefix + self.EYELASH_C_L_GEO + '/eyelash_segC_l_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_D_L_GEO, 'filepath': skin_weight_prefix + self.EYELASH_D_L_GEO + '/eyelash_segD_l_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},

            {'geo': self.EYELASH_A_R_GEO, 'filepath': skin_weight_prefix + self.EYELASH_A_R_GEO + '/eyelash_segA_r_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_B_R_GEO, 'filepath': skin_weight_prefix + self.EYELASH_B_R_GEO + '/eyelash_segB_r_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_C_R_GEO, 'filepath': skin_weight_prefix + self.EYELASH_C_R_GEO + '/eyelash_segC_r_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_D_R_GEO, 'filepath': skin_weight_prefix + self.EYELASH_D_R_GEO + '/eyelash_segD_r_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},

            {'geo': self.EYEBROW_GEO_L, 'filepath': skin_weight_prefix + self.EYEBROW_GEO_L + '/eyebrowDefault_l_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYEBROW_GEO_R, 'filepath': skin_weight_prefix + self.EYEBROW_GEO_R + '/eyebrowDefault_r_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.TOPTEETH_GEO, 'filepath': skin_weight_prefix + self.TOPTEETH_GEO + '/topTeeth_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.BOTTOMTEETH_GEO, 'filepath': skin_weight_prefix + self.BOTTOMTEETH_GEO + '/bottomTeeth_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.TONGUE_GEO, 'filepath': skin_weight_prefix + self.TONGUE_GEO + '/tongue_geo_skincluster_01.json',
             'import_module': 'skin_ng'},

            {'geo': self.CLOTHES_GEO, 'filepath': skin_weight_prefix + self.CLOTHES_GEO + '/clothes_exercise_geo_skinCluster_09.json',
             'import_module': 'skin_ng'},
            {'geo': self.NAILS_GEO, 'filepath': skin_weight_prefix + self.NAILS_GEO + '/nails_geo_skinCluster_01.json',
             'import_module': 'skin_ng'},

        ]

        self.groups = [
            {'g': self.TOP_GRP, 'p': None, 'v': True},
            {'g': self.RIG_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.GEO_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.DNT_GRP, 'p': self.TOP_GRP, 'v': False},
        ]

    def modify_as_rig(self):

        # good stuff happens here.
        super(Rig, self).modify_as_rig()

        # localized stuff happens here --------------
        pn('Master_CTL_Main').bendVis.set(0)

        # lock vis ctls
        for ctl in self.AS_CTL_LIST_NEW:
            pn(ctl).v.set(k=False, cb=False, l=True)

        # attributes on master
        pm.setAttr('Master_CTL_Main.jointVis', 0)
        attribute.add('Master_CTL_Main', n='geoVis', type='bool', dv=1, cb=True, k=False)
        attribute.add('Master_CTL_Main', n='geoLock', type='bool', dv=1, cb=True, k=False)

        pm.connectAttr('Master_CTL_Main.geoLock', 'Geometry.overrideEnabled')
        pm.setAttr('Geometry.overrideDisplayType', 2)

        pm.connectAttr('Master_CTL_Main.geoVis', 'Geometry.v')

        pm.scale('Head_CTL_FKNeck_M.cv[:]',    [4, 4, 4], r=True, os=True)
        pm.move('Head_CTL_FKNeck_M.cv[:]',     [3, 0, 0], r=True, os=True)

        for s in pm.PyNode('Spine_CTL_RootX_M').getShapes():
            pm.xform(s + '.cv[:]', s=[1.2, 1.2, 1.2], r=True)

        pm.scale('Spine_CTL_FKSpine1_M.cv[:]', [1.5, 1.5, 1.5], r=True, os=True)
        pm.scale('Spine_CTL_FKChest_M.cv[:]',  [1.5, 1.5, 1.5], r=True, os=True)


        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            pm.move('Arm_CTL_FKScapula_'+side+'.cv[:]', [-8*side_mult, 0, 0], r=True, os=True)
            pm.scale('Arm_CTL_FKScapula_' + side + '.cv[:]', [1.2, 1.2, 1.2], r=True, os=True)

            pm.scale('Arm_CTL_FKShoulder_'+side+'.cv[:]', [1, 1, 1], r=True, os=True)
            pm.scale('Arm_CTL_FKElbow_' + side + '.cv[:]', [1, 1, 1], r=True, os=True)
            pm.scale('Arm_CTL_FKWrist_' + side + '.cv[:]', [1, 1, 1], r=True, os=True)

            pm.scale('Hand_CTL_FKThumbFinger2_' + side + '.cv[:]', [2, 2, 2], r=True, os=True)

    def setup_facerig(self):

        ns = self.FACE_NS+':'

        #face_rig_geo = ns + 'face_rig_geo'


        # parent face rig.
        pm.parent(ns+'faceRig', 'Master_CTL_Main')
        pn(ns+'faceRig').inheritsTransform.set(False)

        pm.parent(ns+'Head_constrained_grp', 'Master_CTL_Main')

        # constrain controls to head.
        pm.parentConstraint('Head_Head_joint_M', ns+'Head_constrained_grp', mo=True)
        pm.parentConstraint('Head_Head_joint_M', ns+'Head_constrained_grp', mo=True)

        for geo in self.FACE_GEO_LIST:
            blendshape.apply(ns+geo, geo, dv=1)

        # hide face geo
        pm.hide(ns+'Geo')

        for side in 'LR':
            for atr in ['rx', 'ry', 'rz']:
                pm.connectAttr(pn('Head_Eye_joint_'+side).attr(atr), pn('face:Eye_driver_'+side+'_GRP').attr(atr))

        hide_list = [
            ns+'Tweaks_controls',
            ns+'Main_joints',
            ns+'Region_joints',

        ]

        for h in hide_list:
            pn(h).v.set(0)

    def build_torso_controls(self):

        # breast ctl
        torso_grp = pm.group(em=True, n='Breasts', p='Master_CTL_Main')
        torso_ctl_grp = pm.group(em=True, n='Breasts_controls', p=torso_grp)
        torso_jnt_grp = pm.group(em=True, n='Breasts_joints', p=torso_grp)
        pm.connectAttr('Master_CTL_Main.jointVis', torso_jnt_grp.v)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1
            color = 20 if side == 'R' else 18

            # bootA
            breast_ctl_main = control.create(
                'Torso_CTL_breast_'+side,
                shape='ball',
                colorIdx=color,
                zeroGroup=True,
            )
            breast_ctl_main_zero = breast_ctl_main.getParent()
            breast_ctl_main_zero.t.set([8.049 * side_mult, 131.847, 13.987])
            breast_ctl_main_zero.r.set([-14.66, 24.6 * side_mult,  -3.14])
            pm.scale(breast_ctl_main.cv[:], [8, 8, 8], r=True, os=True)
            pm.parent(breast_ctl_main_zero, torso_ctl_grp)
            pm.parentConstraint('breast_{}_link_joint_0'.format(side), breast_ctl_main_zero, mo=True)
            breast_ctl_main.v.set(cb=False, k=False, l=True)

            # jnt
            pm.select(torso_jnt_grp, r=True)
            breast_jnt = pm.joint(n='Torso_breast_joint_'+side)
            #pm.delete(pm.parentConstraint(breast_ctl_main, breast_jnt, mo=False))
            #pm.delete(pm.scaleConstraint(breast_ctl_main, breast_jnt, mo=False))
            pm.scaleConstraint(breast_ctl_main, breast_jnt, mo=False)
            pm.parentConstraint(breast_ctl_main, breast_jnt, mo=False)



    def install_link_modules(self):

        link_top_grp = mc.group(em=1, p=self.MASTER_CONTROL, n='Link_modules')

        start_driver, end_driver, br_l_top_group = self.build_link_modules(
            'breast_L_start_guide',
            'breast_L_end_guide',
            'breast_L_start_UPguide',
            'breast_L_end_UPguide',
            prefix='breast_L',
            mid_joints=1,
            forward_aim=(1, 0, 0),
            up_aim=(0, 1, 0)
        )

        mc.parentConstraint('Spine_Chest_joint_M', start_driver, mo=1)
        con = mc.parentConstraint('Spine_Chest_joint_M', 'Arm_Scapula_joint_L', end_driver, mo=1)[0]
        mc.setAttr(con + '.interpType', 2)
        mc.setAttr(con + '.Spine_Chest_joint_MW0', 3)
        mc.scaleConstraint('Spine_Chest_joint_M', 'breast_L_link_joint_0')

        start_driver, end_driver, br_r_top_group = self.build_link_modules(
            'breast_R_start_guide',
            'breast_R_end_guide',
            'breast_R_start_UPguide',
            'breast_R_end_UPguide',
            prefix='breast_R',
            mid_joints=1,
            forward_aim=(1, 0, 0),
            up_aim=(0, 1, 0)
        )

        mc.parentConstraint('Spine_Chest_joint_M', start_driver, mo=1)
        con = mc.parentConstraint('Spine_Chest_joint_M', 'Arm_Scapula_joint_R', end_driver, mo=1)[0]
        mc.setAttr(con + '.interpType', 2)
        mc.setAttr(con + '.Spine_Chest_joint_MW0', 3)
        mc.scaleConstraint('Spine_Chest_joint_M', 'breast_R_link_joint_0')


        start_driver, end_driver, cl_l_top_group = self.build_link_modules(
            'clav_L_start_guide',
            'clav_L_end_guide',
            'clav_L_start_UPguide',
            'clav_L_end_UPguide',
            prefix='clav_L',
            mid_joints=1,
            forward_aim=(1, 0, 0),
            up_aim=(0, 1, 0)
        )

        mc.parentConstraint('Spine_Chest_joint_M', start_driver, mo=1)
        mc.parentConstraint('Arm_Scapula_joint_L', end_driver, mo=1)
        mc.scaleConstraint('Spine_Chest_joint_M', 'clav_L_link_joint_0')


        start_driver, end_driver, cl_r_top_group = self.build_link_modules(
            'clav_R_start_guide',
            'clav_R_end_guide',
            'clav_R_start_UPguide',
            'clav_R_end_UPguide',
            prefix='clav_R',
            mid_joints=1,
            forward_aim=(1, 0, 0),
            up_aim=(0, 1, 0)
        )

        mc.parentConstraint('Spine_Chest_joint_M', start_driver, mo=1)
        mc.parentConstraint('Arm_Scapula_joint_R', end_driver, mo=1)
        mc.scaleConstraint('Spine_Chest_joint_M', 'clav_R_link_joint_0')


        mc.parent(br_l_top_group, br_r_top_group, cl_l_top_group, cl_r_top_group, link_top_grp)

    def build_link_modules(self,
                           start_guide,
                           end_guide,
                           start_up,
                           end_up,
                           prefix='',
                           mid_joints=0,
                           forward_aim=(1,0,0),
                           up_aim=(0,1,0)):

        prefix = prefix + '_' if prefix else prefix # if prefix token given, attach underscore

        module_top_grp = prefix + 'link_grp'

        joints_grp  = prefix + 'joint_grp'
        driver_grp  = prefix +'drivers_grp'
        driven_grp  = prefix +'driven_grp'
        utility_grp = prefix +'utility_grp'

        link_module_groups = [
            {'g': module_top_grp,  'p': None,            'v': True},
            {'g': joints_grp,      'p': module_top_grp,  'v': False},
            {'g': driver_grp,      'p': module_top_grp,  'v': True},
            {'g': driven_grp,      'p': module_top_grp,  'v': True},
            {'g': utility_grp,     'p': module_top_grp,  'v': False},
        ]

        build.groups(link_module_groups)
        mc.setAttr(utility_grp + ".inheritsTransform", 0)
        mc.setAttr(joints_grp + ".inheritsTransform", 0)

        # CREATE TRANSFORMS

        start_guides = [start_guide, start_up]
        end_guides   = [end_guide, end_up]

        transforms_dict = dict()

        driver = None
        driven = None

        for i_guides, i_side in zip([start_guides, end_guides],['start', 'end']):

            temp_dict = dict()

            base_guide, up_guide = i_guides

            driver_n = prefix + i_side + '_driver'
            driver = mc.group(name=driver_n, em=1, w=1)
            mc.delete(mc.parentConstraint(base_guide, driver))
            mc.parent(driver, driver_grp)
            temp_dict['driver'] = driver

            driver_up_n = prefix + i_side + '_driver_up'
            driver_up = mc.spaceLocator(name=driver_up_n)[0]
            mc.delete(mc.parentConstraint(up_guide, driver_up))
            mc.setAttr(driver_up + '.visibility', 0)
            mc.parent(driver_up, driver_n)
            temp_dict['up'] = driver_up

            driven_n = prefix + i_side + '_driven'
            driven = mc.spaceLocator(name=driven_n)[0]
            mc.delete(mc.parentConstraint(base_guide, driven))
            mc.setAttr(driven + '.visibility', 0)
            mc.pointConstraint(driver, driven)
            mc.parent(driven, driven_grp)
            temp_dict['driven'] = driven

            transforms_dict[i_side] = temp_dict # Save created transforms to lookup dictionary

        # CREATE CURVES

        curves = []

        base_curve_n = prefix + 'curve'
        base_curve = mc.curve(n=base_curve_n, p=[[0, 0, 0], [1, 0, 0]], d=1)
        self.connect_locs_to_curve(base_curve, transforms_dict['start']['driven'], transforms_dict['end']['driven'])
        curves.append(base_curve)

        for i_side, i_opposite_side in zip(['start', 'end'], ['end', 'start']):

            up_curve_n = prefix + i_side + '_upCurve'
            up_curve = mc.curve(n=up_curve_n, p=[[0, 0, 0], [1, 0, 0]], d=1)

            curve_start = transforms_dict[i_side]['up']
            curve_end = transforms_dict[i_opposite_side]['driven']

            self.connect_locs_to_curve(up_curve, curve_start, curve_end)

            curves.append(up_curve)

        mc.parent(curves, utility_grp)


        # AIM TRANSFORMS

        flip_forward_axis = lambda f_axis :[a*-1 for a in f_axis]

        start_driver = transforms_dict['start']['driver']
        start_driven = transforms_dict['start']['driven']

        end_driver = transforms_dict['end']['driver']
        end_driven = transforms_dict['end']['driven']

        mc.aimConstraint(

            start_driver,  #                              Constrain Start to End
            end_driven,

            aimVector      =  forward_aim,
            upVector       =  up_aim,
            worldUpType    =  'object',
            worldUpObject  =  transforms_dict['end']['up'])

        mc.aimConstraint(

            end_driver,  #                                 Constrain End to Start
            start_driven,

            aimVector     =   flip_forward_axis(forward_aim),
            upVector      =   up_aim,
            worldUpType   =   'object',
            worldUpObject =  transforms_dict['start']['up'])


       # BUILD JOINTS

        if mid_joints:

            joint_parameter_list = [(i+1)/float(mid_joints + 1) for i in range(mid_joints)]
            print('PR LIST >>', joint_parameter_list)
            curve_shape = pn(base_curve).getShape()

            print curve_shape

            for i, pr in enumerate(joint_parameter_list):

                mc.select(cl=1)
                joint_n = prefix + 'link_joint_' + str(i)
                jnt = mc.joint(name=joint_n)
                print jnt
                p_on_Curve_node = mc.createNode('pointOnCurveInfo')
                mc.connectAttr(curve_shape + '.local', p_on_Curve_node + '.inputCurve')
                mc.setAttr(p_on_Curve_node + '.parameter', pr)

                mc.connectAttr(p_on_Curve_node + '.position', jnt + '.translate')

                orient_c = mc.orientConstraint(
                             transforms_dict['start']['driven'],
                             transforms_dict['end']['driven'],
                             jnt
                )[0]

                mc.setAttr(orient_c + ".{}W0".format(transforms_dict['start']['driven']), 1-pr)
                mc.setAttr(orient_c + ".{}W1".format(transforms_dict['end']['driven']),   pr)
                mc.setAttr(orient_c + '.interpType', 2)

                mc.parent(jnt, joints_grp)


        return start_driver, end_driver, module_top_grp

    def connect_locs_to_curve(self, curve, loc_a, loc_b):

        for i, loc in enumerate([loc_a, loc_b]):

            loc_shape = pn(loc).getShape()
            curve_shape = pn(curve).getShape()

            mc.connectAttr(loc_shape + '.worldPosition[0]', curve_shape + '.controlPoints[{}]'.format(i))

    def post_edits(self):
        mc.setAttr("Master_CTL_Main.geoLock", 0)
        mc.setAttr("Master_CTL_Main.jointVis", 1)

    # ==================================================================================================================
    def go(self):
        print '='*80
        print 'Starting '+str(self.who)+' Rig Build.'
        mc.file(new=True, f=True)
        self.build_contents()
        print 'Finished!'

    def build_contents(self):
        build.imports(self.imports)

        #self.import_fix()

        pm.rename('Group', self.RIG_GRP)
        build.groups(self.groups)
        pm.parent(self.GEO_LIST, self.GEO_GRP)
        self.modify_as_rig()

        self.install_link_modules()

        self.build_torso_controls()
        # self.build_cape_rig()
        # self.install_loincloth()
        # self.install_horns()
        # self.install_shoulderPlate_controls()
        # #self.setup_facerig()

        build.skin_bind(self.skin_bind)

        pm.delete(self.DELETE_GRP)

        #self.post_edits()


"""
#      Gekka      # 

from jb_destruna.rigs.j_gekka import gekka_exerciseClothes_animRig
reload(gekka_exerciseClothes_animRig)
rig = gekka_exerciseClothes_animRig.Rig()
rig.go()
"""





