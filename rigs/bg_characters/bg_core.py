from maya import cmds as mc
from jb_destruna.maya.internal import follicle
from jb_destruna.maya.internal import wrap


def grid_characters(sel):
    '''
    Displace a set of selected objects in a grid,
    useful for viewing all bg characters in a single file
    '''

    x = 1
    z = 1

    for i, geo in enumerate(sel):

        mc.move(x, 0, z, geo, r=True)
        x += 100

        if i % 10 == 0 and i > 0:
            x = 0
            z += 100


def transfer_skeleton():
    pass


def wrap_transform(sel, driver_geo, name='wrap_transform', exclusive_bind=True):

    wrap_geo = name + '_wrap_geo'

    mesh_grp = []

    # Create Wrap Geo
    for target in sel:
        # target = 'Hand_IndexFinger2_joint_L'

        p_scale = 0.5
        p = mc.polyPlane()[0]
        mesh_grp.append(str(p))
        p_shape = mc.listRelatives(p, c=1)[0]
        node = mc.listConnections(p_shape, type='polyPlane')[0]
        mc.setAttr(node + '.subdivisionsWidth', 1)
        mc.setAttr(node + '.subdivisionsHeight', 1)
        mc.setAttr(node + '.height', p_scale)
        mc.setAttr(node + '.width', p_scale)
        mc.delete(mc.parentConstraint(target, p))

    mc.polyUnite(*mesh_grp, ch=0, mergeUVSets=1, centerPivot=1, n=wrap_geo)
    mc.polyAutoProjection(wrap_geo, l=2, sc=1, o=1, p=6, lm=0, ps=0.5, ch=0)

    # Attach follicles
    position_list = [mc.xform(t, ws=1, q=1, t=1) for t in sel]
    uv_pos_list = follicle.uv_from_position(position_list, wrap_geo)

    follicles_list = list()
    for loc, uv_val in zip(sel, uv_pos_list):
        follicle_name = name + '_' + loc + '_fol'
        f = follicle.create(wrap_geo, n=follicle_name, setUV=True, uvVal=uv_val)
        follicles_list.append(f.name())

    fol_grp = mc.group(follicles_list, n=name + 'follices_grp')
    mc.setAttr(fol_grp + '.visibility', 0)

    # Create driven locator
    wrapDriven_list = list()
    for node, fol in zip(sel, follicles_list):
        node = node.replace('__positionLocator', '')
        loc_n = name + '_' + node + '_wrapDriven'
        loc = mc.spaceLocator(n=loc_n)[0]
        wrapDriven_list.append(loc)
        mc.parentConstraint(fol, loc)
        mc.setAttr(loc + ".overrideEnabled", 1)
        mc.setAttr(loc + ".overrideColor", 17)

    wrapDriven_grp = mc.group(wrapDriven_list, n=name + '_wrapDriven_grp')

    mc.group(wrapDriven_grp, fol_grp, wrap_geo, n=name + '_grp')

    # Create wrap
    wrap_name = name + '_wrap'
    wrap.create(wrap_geo, driver_geo, n=wrap_name)
    eb = 1 if exclusive_bind else 0
    mc.setAttr(wrap_name + '.exclusiveBind', eb)

"""
from jb_destruna.maya.core import build
from jb_destruna.maya.internal import blendshape

BS_NAME = 'HAND_TRANSPLANT_BS'
BODY_GEO = 'body_geo'
build.imports([{'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/bg/bg_yon/hand_transplant_set/transplant_rig/bg_Yon_transplant_rig_02.mb'}])

blendshape.apply(
    'yon_hand_L_old',
    BODY_GEO,
    n=BS_NAME,
    dv=1)
blendshape.apply(
    'yon_hand_R_old',
    BODY_GEO,
    n=BS_NAME,
    dv=1)
blendshape.import_weights_from_file(filepath='D:/_Active_Projects/Destruna/masterAssets_forRef/characters/bg/bg_yon/hand_transplant_set/attach_blendshape/bg_yon_handTransplant_attach_weights_03.json',
    geo=BODY_GEO, blendshape=BS_NAME)

mc.setAttr(BS_NAME + '.origin', 0)
"""

