from jb_destruna.maya.core import build
import maya.cmds as mc
import pymel.core as pm

from jb_destruna.maya.core import build
from jb_destruna.maya.core import rig
from jb_destruna.maya.internal import skin
from jb_destruna.maya.internal import control

from jb_destruna.maya.internal import blendshape
from jb_destruna.maya.internal import wrap
from jb_destruna.maya.internal import wrap
from jb_destruna.maya.internal import cpom
from jb_destruna.maya.internal import follicle
from jb_destruna.maya.utility import attribute
from jb_destruna.maya.utility.misc import pn
from jb_destruna.maya.utility.misc import zero_grp
from jb_destruna.tools import path as path_tools

from jb_destruna.rigs.e_don.import_paths import don_import_paths as imp

reload(build)
reload(rig)

"""
NOTES: 
Geo has be transfered 11.479 on Z axis 
"""

class Rig(rig.Rig):

    ASSET = 'Gustavo'
    VARIANT = ''

    FACE_NS = 'face'

    IMPORT_LIST = ['office', 'jordan', 'damien']
    IMPORT_KEY = IMPORT_LIST[2]


    # BODY
    BODY_GEO = 'body_geo'
    HAIR_GEO = 'hair_geo'

    # FACE
    FACE_GEO = 'head_geo'
    PUPIL_GEO_L = 'eye_l_geo'
    PUPIL_GEO_R = 'eye_r_geo'
    SCLERA_GEO_L = 'sclera_l_geo'
    SCLERA_GEO_R = 'sclera_r_geo'
    EYELASH_L_GEO = 'eyelash_L_geo'
    EYELASH_R_GEO = 'eyelash_R_geo'
    EYEBROW_GEO_L = 'eyebrow_l_geo'
    EYEBROW_GEO_R = 'eyebrow_r_geo'
    TOPTEETH_GEO = 'topTeeth_geo'
    BOTTOMTEETH_GEO = 'bottomTeeth_geo'
    TONGUE_GEO = 'tongue_geo'
    GOATIE = 'goatie_geo'

    # CLOTHING
    CLOTHES_GEO = 'clothes_geo'
    SHOULDER_PLATES = 'shoulder_plates_geo'
    CHEST_PLATE_GEO = 'chest_plate_geo'
    HORNS = 'horns_geo'
    THREADS_GEO = 'threads'
    BELT_GEO = 'belt_geo'
    FUR_GEO = 'fur_cape_geo'
    LOINCLOTH_GEO = 'loinCloth_geo'
    DRAGON_SKULL_GEO = 'dragonSkull_geo'

    # ACCESSORY
    SKULLBUCKLE = 'skullBuckle_geo'
    DOME = 'dome_geo'
    SKULL_UNDERBUCKLE = 'skullUnder_geo'
    DOME_MOUNT = 'domeMount_geo'
    SCREEN_GEO = 'screen_geo'
    SKULL_GEO = 'skull_geo'
    TEETH_GEO = 'teeth_geo'
    CARD_GEO = 'cardReader_geo'
    CLAW_JOINTS_GEO = 'clawJoints_geo'
    CLAW_GEO = 'claws_geo'

    FACE_GEO_LIST = [
        FACE_GEO,
        PUPIL_GEO_L,
        PUPIL_GEO_R,
        SCLERA_GEO_L,
        SCLERA_GEO_R,
        EYELASH_L_GEO,
        EYELASH_R_GEO,
        EYEBROW_GEO_L,
        EYEBROW_GEO_R,
        TOPTEETH_GEO,
        BOTTOMTEETH_GEO,
        TONGUE_GEO,
    ]

    GEO_LIST = [
        BODY_GEO,
        HAIR_GEO,
        GOATIE,
        BELT_GEO,
        THREADS_GEO,
        CLOTHES_GEO,
        HORNS,
        CHEST_PLATE_GEO,
        SHOULDER_PLATES,
        DRAGON_SKULL_GEO,
        FUR_GEO,
        LOINCLOTH_GEO,
        SKULLBUCKLE,
        DOME,
        SKULL_UNDERBUCKLE,
        DOME_MOUNT,
        SCREEN_GEO,
        SKULL_GEO,
        TEETH_GEO,
        CARD_GEO,
        CLAW_JOINTS_GEO,
        CLAW_GEO,
    ]
    GEO_LIST += FACE_GEO_LIST

    def __init__(self):
        super(Rig, self).__init__()

        self.who = self.ASSET
        self.imports = [
            # GEO
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gustavo/01_model/gustavo_model_11_DMR_MASTER.mb',
             'group': self.DELETE_GRP},

            #GUIDES
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gustavo/02_rig/imports/skin_geo/furSkirt_skinGeo_02.mb',
                'group': self.DELETE_GRP},

            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gustavo/02_rig/imports/guides/fur_cape_curves/fur_cape_curves_04.mb',
                'group': self.DELETE_GRP},

            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gustavo/02_rig/imports/guides/shoulder_plates/shoulderPlate_guides_02.mb',
                'group': self.DELETE_GRP},

            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gustavo/02_rig/imports/guides/horn_guides/gustavo_horn_guides_02.mb',
                'group': self.DELETE_GRP},

            # AS RIG
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gustavo/02_rig/imports/asRig/gustavo_asRig_02.mb',
             'group': self.DELETE_GRP},

            # AS FACE RIG
            {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gustavo/02_rig/imports/face_Rig/gustavo_face_01_JNB_MASTER.mb',
             'group': self.DELETE_GRP, 'namespace': self.FACE_NS},

            # AS FACE RIG
            # {'filepath': 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/don/3D/don_teen_mocap/02_rig/imports/AS_face_rig/don_faceRig_v002_1.mb',
            #  'namespace': self.FACE_NS},

        ]

        skin_weight_prefix = 'D:/_Active_Projects/Destruna/masterAssets_forRef/characters/gustavo/02_rig/imports/skinclusters/'

        self.skin_bind = [

            # BODY
            {'geo': self.BODY_GEO, 'filepath': skin_weight_prefix +  self.BODY_GEO + '/body_geo_skincluster_06.json',
             'import_module':'skin_ng'},
            {'geo': self.HAIR_GEO, 'filepath': skin_weight_prefix + 'gustavo_hair_geo/gustavo_hair_geo_skincluster_02.json',
             'import_module': 'skin_ng'},

            # FACE
            {'geo': self.FACE_GEO, 'filepath': skin_weight_prefix + 'faceSkin_geo/faceSkin_geo_skincluster_01.json',
             'import_module': 'skin_ng'},

            {'geo': self.PUPIL_GEO_L, 'filepath': skin_weight_prefix + self.PUPIL_GEO_L + '/eye_L_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.PUPIL_GEO_R, 'filepath': skin_weight_prefix + self.PUPIL_GEO_R + '/eye_R_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.SCLERA_GEO_L, 'filepath': skin_weight_prefix + self.SCLERA_GEO_L + '/sclera_l_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.SCLERA_GEO_R, 'filepath': skin_weight_prefix + self.SCLERA_GEO_R + '/sclera_r_geo_skinCluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_L_GEO, 'filepath': skin_weight_prefix + self.EYELASH_L_GEO + '/eyelash_L_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYELASH_R_GEO, 'filepath': skin_weight_prefix + self.EYELASH_R_GEO + '/eyeLash_R_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYEBROW_GEO_L, 'filepath': skin_weight_prefix + self.EYEBROW_GEO_L + '/eyebrow_L_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.EYEBROW_GEO_R, 'filepath': skin_weight_prefix + self.EYEBROW_GEO_R + '/eyebrow_R_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.TOPTEETH_GEO, 'filepath': skin_weight_prefix + 'topTeeth_geo/topTeeth_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.BOTTOMTEETH_GEO, 'filepath': skin_weight_prefix + 'bottomTeeth_geo/bottomTeeth_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.TONGUE_GEO, 'filepath': skin_weight_prefix + 'tongue_geo/tongue_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            # {'geo': self.GOATIE, 'filepath': skin_weight_prefix + 'goatie_geo/goatie_geo_skincluster_01.json',
            #  'import_module': 'skin_ng'},

            {'geo': self.DOME, 'filepath': skin_weight_prefix + self.DOME + '/dome_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.SKULL_UNDERBUCKLE, 'filepath': skin_weight_prefix + self.SKULL_UNDERBUCKLE + '/skullUnder_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.DOME_MOUNT, 'filepath': skin_weight_prefix + self.DOME_MOUNT + '/domeMount_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.SCREEN_GEO, 'filepath': skin_weight_prefix + self.SCREEN_GEO + '/screen_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.SKULL_GEO, 'filepath': skin_weight_prefix + self.SKULL_GEO + '/skull_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.TEETH_GEO, 'filepath': skin_weight_prefix + self.TEETH_GEO + '/teeth_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.CARD_GEO, 'filepath': skin_weight_prefix + self.CARD_GEO + '/cardReader_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.CLAW_JOINTS_GEO, 'filepath': skin_weight_prefix + self.CLAW_JOINTS_GEO + '/clawJoints_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.CLAW_GEO, 'filepath': skin_weight_prefix + self.CLAW_GEO + '/claws_geo_skincluster_01.json',
             'import_module': 'skin_ng'},

            {'geo': self.CLOTHES_GEO, 'filepath': skin_weight_prefix + self.CLOTHES_GEO + '/clothes_geo_skincluster_08.json',
             'import_module': 'skin_ng'},
            {'geo': self.SHOULDER_PLATES, 'filepath': skin_weight_prefix + self.SHOULDER_PLATES + '/shoulder_plates_geo_skincluster_04.json',
             'import_module': 'skin_ng'},
            {'geo': self.HORNS, 'filepath': skin_weight_prefix + self.HORNS + '/horns_geo_skinCluster_04.json',
             'import_module': 'skin_ng'},
            {'geo': self.CHEST_PLATE_GEO, 'filepath': skin_weight_prefix + self.CHEST_PLATE_GEO + '/chest_plate_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.THREADS_GEO, 'filepath': skin_weight_prefix + self.THREADS_GEO + '/threads_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.BELT_GEO, 'filepath': skin_weight_prefix + self.BELT_GEO + '/belt_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.FUR_GEO, 'filepath': skin_weight_prefix + self.FUR_GEO + '/fur_cape_geo_skincluster_03.json',
             'import_module': 'skin_ng'},
            {'geo': self.LOINCLOTH_GEO, 'filepath': skin_weight_prefix + self.LOINCLOTH_GEO + '/loinCloth_geo_skincluster_01.json',
             'import_module': 'skin_ng'},
            {'geo': self.DRAGON_SKULL_GEO, 'filepath': skin_weight_prefix + self.DRAGON_SKULL_GEO + '/dragonSkull_geo_skincluster_02.json',
             'import_module': 'skin_ng'},
            {'geo': self.SKULLBUCKLE, 'filepath': skin_weight_prefix + self.SKULLBUCKLE + '/skullBuckle_geo_skincluster_01.json',
             'import_module': 'skin_ng'},

        ]

        self.groups = [
            {'g': self.TOP_GRP, 'p': None, 'v': True},
            {'g': self.RIG_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.GEO_GRP, 'p': self.TOP_GRP, 'v': True},
            {'g': self.DNT_GRP, 'p': self.TOP_GRP, 'v': False},
        ]

    def build_horn_controls(self):

        horn_grp = mc.group(em=True, n='Horns', p=self.MASTER_CONTROL)
        horn_ctl_grp = mc.group(em=True, n='Horns_controls', p=horn_grp)
        horn_jnt_grp = mc.group(em=True, n='Horns_joints', p=horn_grp)

        horn_side = lambda side: ['Horn_{0}_0{1}_guide_loc'.format(side, n+1) for n in range(6)]
        horns_guide = [horn_side(side) for side in ['L', 'R']]
        sides = ['L', 'R']

        for i_guide_set, i_side in zip(horns_guide, sides):

            joint_list = []
            control_list = []
            con_list = []

            for i, guide in enumerate(i_guide_set):

                nn = guide.replace('_guide_loc', '_joint')
                mc.select(cl=1)
                jnt = mc.joint(n=nn)
                joint_list.append(jnt)
                con_list.append(mc.parentConstraint(guide, jnt)[0])

                if i > 0:
                    mc.parent(jnt, joint_list[i - 1])
                else:
                    mc.parent(jnt, horn_jnt_grp)

                for attr in ['X', 'Y', 'Z']:
                    mc.setAttr(jnt + '.jointOrient{}'.format(attr), 0)


                side_mult = 1 if i_side == 'L' else -1
                colour =  18 if i_side == 'L' else 13


                if i == 0:
                    shape = 'ball'
                    scale = [11, 11, 11]
                else:
                    shape = 'box_pinched_rounded'
                    size_offset = 6 - i
                    scale = [(6 + size_offset)*1.3 for n in range(3)]

                temp_transform = mc.group(em=1, w=1, n=jnt.replace('joint', 'CTL'))
                horn_control = control.create(
                    temp_transform,
                    shape=shape,
                    colorIdx=colour,
                    zeroGroup=True
                )

                pm.scale(horn_control.cv[:], scale, r=True, os=True)
                pm.rotate(horn_control.cv[:], [0, 0, -90*side_mult], r=True, os=True)

                control_list.append(horn_control)

                pm.delete(pm.parentConstraint(jnt, horn_control.getParent()))

                if i > 0:
                    pm.parent(horn_control.getParent(), control_list[i-1])
                else:
                    pm.parent(horn_control.getParent(), horn_ctl_grp)

            mc.delete(con_list)

            pm.parent(joint_list[0], 'Spine_Chest_joint_M')
            pm.parentConstraint('Spine_Chest_joint_M', control_list[0].getParent(), mo=1)

            for ctl, jnt in zip(control_list, joint_list):
                pm.parentConstraint(ctl, jnt, mo=1)
                pm.scaleConstraint(ctl, jnt)


            # horn_pos = pm.xform(horn, t=True, q=True, ws=True)
            #
            # uvVal = cpom.closest_uv_on_geo_from_position(horn_pos, self.CHEST_PLATE_GEO)
            # print("%"*20)
            # print(horn_pos, uvVal)
            # print("%" * 20)
            # follicle.create(self.CHEST_PLATE_GEO, setUV=True, uvVal=uvVal, n=joint_name.replace('_guide', '_fol'))

    def modify_as_rig(self):

        # good stuff happens here.
        super(Rig, self).modify_as_rig()

        # localized stuff happens here --------------
        pn('Master_CTL_Main').bendVis.set(0)

        # lock vis ctls
        for ctl in self.AS_CTL_LIST_NEW:
            pn(ctl).v.set(k=False, cb=False, l=True)

        # attributes on master
        pm.setAttr('Master_CTL_Main.jointVis', 0)
        attribute.add('Master_CTL_Main', n='geoVis', type='bool', dv=1, cb=True, k=False)
        attribute.add('Master_CTL_Main', n='geoLock', type='bool', dv=1, cb=True, k=False)

        pm.connectAttr('Master_CTL_Main.geoLock', 'Geometry.overrideEnabled')
        pm.setAttr('Geometry.overrideDisplayType', 2)

        pm.connectAttr('Master_CTL_Main.geoVis', 'Geometry.v')

        pm.scale('Spine_CTL_FKRoot_M.cv[:]', [1.4, 1.4, 1.4], r=True, os=True)
        pm.scale('Spine_CTL_FKSpine1_M.cv[:]', [1.4, 1.4, 1.4], r=True, os=True)
        pm.scale('Spine_CTL_FKChest_M.cv[:]', [1.4, 1.4, 1.4], r=True, os=True)

        pm.scale('Head_CTL_FKNeck_M.cv[:]', [4, 4, 4], r=True, os=True)
        pm.move('Head_CTL_FKNeck_M.cv[:]', [3, 0, 0], r=True, os=True)

        for side in 'LR':
            side_mult = -1 if side == 'R' else 1

            pm.move('Arm_CTL_FKScapula_'+side+'.cv[:]', [-8*side_mult, 0, 0], r=True, os=True)
            pm.scale('Arm_CTL_FKScapula_' + side + '.cv[:]', [3.5, 3.5, 3.5], r=True, os=True)

            pm.scale('Arm_CTL_FKShoulder_'+side+'.cv[:]', [2.5, 2.5, 2.5], r=True, os=True)
            pm.scale('Arm_CTL_FKElbow_' + side + '.cv[:]', [2, 2, 2], r=True, os=True)
            pm.scale('Arm_CTL_FKWrist_' + side + '.cv[:]', [2, 2, 2], r=True, os=True)

            pm.scale('Hand_CTL_FKThumbFinger2_' + side + '.cv[:]', [2, 2, 2], r=True, os=True)

    def skull_vis(self):

        attr_name = 'skull_vis'
        attribute.add(self.MASTER_CONTROL, n=attr_name, type='bool', dv=0, cb=True, k=False)
        mc.connectAttr(self.MASTER_CONTROL + '.' + attr_name, self.DRAGON_SKULL_GEO + '.visibility')

    def wrap_goatee(self):

        wrap.create(
            self.GOATIE,
            self.FACE_GEO
        )

    def install_shoulderPlate_controls(self):

        guides =  ['shoulder_plates_guide_L', 'shoulder_plates_guide_R']
        sides  =  ['L', 'R']

        rig_top_grp  = mc.group(name='ShoulderPlates',          em=1, w=1)
        ctl_grp      = mc.group(name='ShoulderPlates_controls', em=1, p=rig_top_grp)            #Arm_Scapula_joint_L
        jnt_grp      = mc.group(name='ShoulderPlates_joints',   em=1, p=rig_top_grp)

        for i_guide, i_side in zip(guides, sides):

            ctl_name      = 'ShoulderPlate_CTL_' + i_side
            ctl_transform = mc.group(name=ctl_name, em=1, w=1)

            ctl = control.create(
                ctl_transform,
                shape='box_pinched_rounded',
                colorIdx=2,
                zeroGroup=True,
            )

            # Position control, then constrain in place
            pm.delete(pm.parentConstraint(i_guide, ctl.getParent()))
            pm.parentConstraint('Arm_Scapula_joint_' + i_side, ctl.getParent(), mo=1)
            pm.scaleConstraint('Master_CTL_Main', ctl.getParent(), mo=1)
            pm.parent(ctl.getParent(), ctl_grp)

            jnt_name = 'ShoulderPlate_jnt_' + i_side
            jnt = mc.joint(n=jnt_name)

            # Constrain joint to control
            pm.parentConstraint(ctl, jnt)
            pm.scaleConstraint(ctl, jnt)
            pm.parent(jnt, jnt_grp)

            # Modify control shape
            side_multiplier = 1 if i_side == 'R' else -1 # to account for L/R symmetrical transformations
            pm.rotate(ctl.name() + '.cv[:]', [0, 0, -90*side_multiplier], r=True, os=True)
            pm.scale(ctl.name() + '.cv[:]', [32, 30, 30], r=True, os=True)
            pm.move(ctl.name() + '.cv[:]', [-10*side_multiplier, 0, 0], r=True, ws=True)
            pm.setAttr(ctl + '.visibility', l=1, ch=False, k=False)

        mc.parent(rig_top_grp, 'Master_CTL_Rig')

    def build_cape_rig(self):

        top_grp = mc.group(em=1, p='Master_CTL_Rig')

        curve_guide_prefix = 'fur_cape_base{0}_{1}_curve'

        # build cape master control
        cape_main_ctl_name = 'Cape_Main_CTL'
        cape_main_guide = mc.group(em=1, w=1, name=cape_main_ctl_name)

        cape_main_ctl = control.create(
            cape_main_guide,
            shape='arc_rounded',
            colorIdx=6,
            zeroGroup=True,
        )

        # Create CTL attrs
        attribute.add(cape_main_ctl.name(), n='FK_IK_SWITCH', type='enum', en=['FK', 'IK', 'BOTH'], dv=0)

        # Create Logic nodes
        fk_condition = mc.createNode('condition', n='FK_condition')
        ik_condition = mc.createNode('condition', n='IK_condition')

        mc.connectAttr(cape_main_ctl.name() + '.FK_IK_SWITCH', fk_condition + '.firstTerm')
        mc.setAttr(fk_condition + '.operation', 1)
        mc.setAttr(fk_condition + '.secondTerm', 1)
        mc.setAttr(fk_condition + '.colorIfTrueR', 1)
        mc.setAttr(fk_condition + '.colorIfFalseR', 0)

        mc.connectAttr(cape_main_ctl.name() + '.FK_IK_SWITCH', ik_condition + '.firstTerm')
        mc.setAttr(ik_condition + '.operation', 3)
        mc.setAttr(ik_condition + '.secondTerm', 1)
        mc.setAttr(ik_condition + '.colorIfTrueR', 1)
        mc.setAttr(ik_condition + '.colorIfFalseR', 0)

        condition_nodes = [fk_condition, ik_condition]

        # Position main CTL
        mc.delete(mc.parentConstraint('Spine_RootPart2_joint_M', cape_main_ctl.getParent().name()))
        pm.rotate(cape_main_ctl.cv[:], [-90, 90, 0], r=True, os=True)
        pm.scale(cape_main_ctl.cv[:], [30, 30, 30], r=True, os=True)
        pm.move(cape_main_ctl.cv[:], [0, -11, 0], r=True, os=True)
        mc.parentConstraint('Spine_RootPart2_joint_M', cape_main_ctl.getParent().name())

        pm.parent(cape_main_ctl.getParent(), top_grp)

        for side in ["L", "R", "M"]:

            rigs_per_side = 1 if side == "M"  else 2

            for i in range(rigs_per_side):

                # Get curve names
                base_curve = curve_guide_prefix.format(i, side)
                up_curve = base_curve.replace('base', 'up')
                prefix = base_curve.replace('base', '').replace('_curve', '')

                # Build Path rig
                path_joint_list, path_controls = path_tools.build(base_curve,
                                                             up_curve,
                                                             node=3,
                                                             joint_num=8,
                                                             prefix=prefix,
                                                             shape='pivot',
                                                             col=5)
                self.build_FK_Path_rig(path_controls[1], prefix, cape_main_ctl, condition_nodes)

                mc.setAttr(prefix + '_path_joints.visibility', 1)
                mc.setAttr(prefix + '_level_1_driven_grp.visibility', 0)
                mc.setAttr(prefix + '_path_master_control.visibility', 0)
                pm.parentConstraint(cape_main_ctl, prefix + '_path_master_control', mo=1)

                pm.parent(prefix + '_path_rig', top_grp)
                pm.parent(prefix + '_FkCTL_1_ZERO', top_grp)

    def build_FK_Path_rig(self, path_controls, prefix, main_ctl, condition_nodes):

        fk_condition, ik_condition = condition_nodes

        # Build FK CTLS
        ctl_list = []

        for i, path_ctl in enumerate(path_controls[1:-1]):

            path_ctl_node = pn(path_ctl)

            fk_ctl_name = path_ctl.replace('mainCurveCTL', 'FkCTL')
            guide = mc.group(n=fk_ctl_name, em=1, w=1)

            fk_ctl = control.create(
                guide,
                shape='box',
                colorIdx=17,
                zeroGroup=True,
            )

            # Scale controls shapes
            pm.scale(path_ctl_node.cv[:], [9, 9, 9], r=True, os=True)
            pm.scale(fk_ctl.cv[:], [8, 8, 8], r=True, os=True)

            # Position control
            pm.delete(pm.parentConstraint(path_ctl, fk_ctl.getParent()))

            # Attach Fk control to path control
            pm.parentConstraint(fk_ctl, path_ctl_node.getParent())

            ctl_list.append(fk_ctl)

            if i == 0:
                pm.parentConstraint(main_ctl, fk_ctl.getParent(), mo=1)
                pm.scaleConstraint(main_ctl, fk_ctl.getParent(), mo=1)

            elif i==(len(path_controls[1:-1])-1):
                pm.parent(fk_ctl.getParent(), ctl_list[i - 1])
                pm.parentConstraint(fk_ctl, pn(path_controls[-1]).getParent(), mo=1)
                pm.scale(pn(path_controls[-1]).cv[:], [9, 9, 9], r=True, os=True)
                mc.connectAttr(ik_condition + '.outColorR', path_controls[-1] + '.visibility')
            else:
                pm.parent(fk_ctl.getParent(), ctl_list[i - 1])

            mc.connectAttr(fk_condition + '.outColorR', fk_ctl + '.visibility')
            mc.connectAttr(ik_condition + '.outColorR', path_ctl + '.visibility')

        # Post edit
        pm.scale(ctl_list[0].cv[:], [1, 2, 1], r=True, os=True)


        #ctls_grp = mc.group(em=1, w=1, n=prefix + '_fkCTL_grp')

    def setup_facerig(self):

        ns = self.FACE_NS+':'

        #face_rig_geo = ns + 'face_rig_geo'


        # parent face rig.
        pm.parent(ns+'faceRig', 'Master_CTL_Main')
        pn(ns+'faceRig').inheritsTransform.set(False)

        pm.parent(ns+'Head_constrained_grp', 'Master_CTL_Main')

        # constrain controls to head.
        pm.parentConstraint('Head_Head_joint_M', ns+'Head_constrained_grp', mo=True)
        pm.parentConstraint('Head_Head_joint_M', ns+'Head_constrained_grp', mo=True)

        for geo in self.FACE_GEO_LIST:
            blendshape.apply(ns+geo, geo, dv=1)

        # hide face geo
        pm.hide(ns+'Geo')

        for side in 'LR':
            for atr in ['rx', 'ry', 'rz']:
                pm.connectAttr(pn('Head_Eye_joint_'+side).attr(atr), pn('face:Eye_driver_'+side+'_GRP').attr(atr))

        hide_list = [
            ns+'Tweaks_controls',
            ns+'Main_joints',
            ns+'Region_joints',

        ]

        for h in hide_list:
            pn(h).v.set(0)

    def post_edits(self):
        mc.setAttr("Master_CTL_Main.geoLock", 0)
        mc.setAttr("Master_CTL_Main.jointVis", 1)

    # ==================================================================================================================
    def go(self):
        print '='*80
        print 'Starting '+str(self.who)+' Rig Build.'
        mc.file(new=True, f=True)
        self.build_contents()
        print 'Finished!'

    def build_contents(self):
        build.imports(self.imports)

        #self.import_fix()

        pm.rename('Group', self.RIG_GRP)
        build.groups(self.groups)
        pm.parent(self.GEO_LIST, self.GEO_GRP)
        self.modify_as_rig()

        self.build_cape_rig()
        self.build_horn_controls()
        self.install_shoulderPlate_controls()

        self.setup_facerig()

        self.wrap_goatee()
        self.skull_vis()

        build.skin_bind(self.skin_bind)

        pm.delete(self.DELETE_GRP)

        #self.post_edits()


"""
#      GUSTAVO      # 

from jb_destruna.rigs.h_gustavo import gustavo_animRig
reload(gustavo_animRig)
rig = gustavo_animRig.Rig()
rig.go()
"""





